
/*
 * g711.h
 *
 * u-law, A-law and linear PCM conversions.
 */


int linear2alaw(int	pcm_val);
int alaw2linear(int	a_val);

int linear2ulaw( int	pcm_val);
unsigned char lin16ulaw8(short lin);
int ulaw2linear( int	u_val);
