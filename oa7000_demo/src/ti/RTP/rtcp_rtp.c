//*****************************************************************************
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************
#include <ti/drivers/net/wifi/simplelink.h>
#include <string.h>
#include "rtcp_rtp.h"
#include "video_tx_task.h"
#include <pthread.h>

#ifdef SL_PLATFORM_MULTI_THREADED

pthread_mutex_t    rtp_tx_lock;
pthread_mutex_t    rtp_rx_lock;
#else /* SL_PLATFORM_MULTI_THREADED */

#endif /* SL_PLATFORM_MULTI_THREADED */

#define RTP_HEADER_PAYLOAD  0x7F
#define RTP_HEADER_MARKER   0x80

#define NAL_MTYPE_MASK      0x1F
#define NAL_NRI_MASK        0x60
#define NAL_F_MASK          0x80

#define FU_FTYPE_MASK       0x1F
#define FU_R_MASK           0x20
#define FU_E_MASK           0x40
#define FU_S_MASK           0x80

#ifdef DEBUG_PRINT
extern int Report(const char *format, ...);
#else
#define Report(...)
#endif
#define AUDIO_RTP_SSRC              (0xF9143263)
#define AUDIO_RTP_SSRC_HTOL         (0x633214F9)




typedef struct stun_header
{
    unsigned short msg_type;
    unsigned short msg_length;
    unsigned int magic_cookie;
    unsigned char transaction_id[12];

}stun_header_s;


static uint8_t tmp_endian_buffer[2048];
unsigned int header_log_buffer[1024];
unsigned char pkt_log_buffer[512];
int header_log_ind=0;
int header_log_cnt=0;

/*!
 * Converts 'val' from host byte order to network byte order
 */
static uint16_t rtp_htons(uint16_t val);

/*!
 * Converts 'val' from host byte order to network byte order
 */
static uint32_t rtp_htonl(uint32_t val);

/*!
 * Video Profile Encoding
 */
static int32_t
rtp_encode_v_profile(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,\
                     uint8_t *const p_out_buffer, uint8_t *p_data,\
                     uint16_t const data_len);

/*!
 * Audio Profile Encoding
 */
static int32_t
rtp_encode_a_profile(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,\
                     uint8_t *const p_out_buffer, uint8_t *p_data,\
                     uint16_t const data_len);

/*!
 * H264 AVC Encoding
 */
static int32_t
rtp_encode_h264avc(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile, \
                   uint8_t *const p_out_buffer, uint8_t *p_data,                 \
                   uint16_t const data_len);

/*!
 * L16 Encoding
 */
static int32_t
rtp_encode_pcm16(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,  \
                 uint8_t *const p_out_buffer, uint8_t *p_data,                  \
                 uint16_t const data_len);


/*!
 * PCMU Encoding
 */
static int32_t
rtp_encode_pcmu(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,  \
                 uint8_t *const p_out_buffer, uint8_t *p_data,                  \
                 uint16_t const data_len);
/*
unsigned char* rtp_decode(uint8_t *p_data, uint16_t const packet_len,
           uint16_t *data_len, rtp_profile_s *p_rtp_profile);
*/
/*!
 * Big-E to Little-E - Processing is required before sending L16 audio data
 * over RTP
 */
//static int32_t
//rtp_x_endianness(uint8_t *p_data, uint16_t const data_len);

static int16_t
rtp_w_endianness(uint8_t *p_data, uint16_t const data_len);

/*!
 * Fills the NAL/FU
 */
static inline \
int32_t rtp_fill_nal_single(uint8_t const frame_type, uint8_t *const p_nal_header, \
                         uint8_t *const p_fu_header, uint32_t *packet_size,     \
                         uint8_t *const p_out_buffer, rtp_header_s *p_rtp_header);

/*!
 * Fills the RTP NAL/FU header
 */
static inline \
int32_t rtp_fill_nal_fu_2(uint8_t const frame_type, uint8_t *const p_nal_header, \
                         uint8_t *const p_fu_header, uint32_t *packet_size,     \
                         uint8_t *const p_out_buffer, rtp_header_s *p_rtp_header);

/*!
 * Fills the RTP NAL/FU header
 */
static inline \
int32_t rtp_fill_nal_fu_3(uint8_t const frame_type, uint8_t *const p_nal_header, \
                         uint8_t *const p_fu_header, uint32_t *packet_size,     \
                         uint8_t *const p_out_buffer, rtp_header_s *p_rtp_header);

int32_t rtp_fill_sps_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile);

/**/
int32_t rtp_rtcp_init()
{
    pthread_mutex_init(&rtp_tx_lock, (const pthread_mutexattr_t *)NULL);
    pthread_mutex_init(&rtp_rx_lock, (const pthread_mutexattr_t *)NULL);
    return 0;
}

/**/
int32_t rtp_deinit()
{
    pthread_mutex_destroy(&rtp_tx_lock);
    pthread_mutex_destroy(&rtp_rx_lock);
    return 0;
}

/**//*
int32_t rtp_process_rtcp_pkt(uint8_t *p_input, uint16_t const length,\
                            rtcp_rr_info_s *const p_out_data)
{
    rtcp_rr_header_s    rr_header   = {0};
    rtcp_rr_block_s     rr_block    = {0};

    pthread_mutex_lock(&rtp_tx_lock);

    memcpy(&rr_header, p_input, sizeof(rtcp_rr_header_s));

    if( (201 == rr_header.type) &&
        (0x80 == rr_header.version & 0xC0) ) 
    {
        p_out_data->sender_ssrc = rr_header.ssrc;

        if((rr_header.version & 0x1F) > 0)
        {
            memcpy(&rr_block, (p_input + sizeof(rtcp_rr_header_s)),\
                   sizeof(rtcp_rr_block_s));

            p_out_data->has_resource    = 1;
            p_out_data->source_ssrc     = rr_block.ssrc;
            p_out_data->jitter          = rr_block.jitter;
            p_out_data->lost_fraction   = rr_block.lost_fraction;
            p_out_data->last_sr         = rr_block.last_sr;
            p_out_data->delay_since_last_sr = rr_block.delay_since_last_sr;

            memcpy(&p_out_data->lost_pkts, rr_block.lost_pkts,\
                   sizeof(rr_block.lost_pkts));
        }
        else
        {
            p_out_data->has_resource    = 0;
        }
    }

    pthread_mutex_unlock(&rtp_tx_lock);
    return 0;
}
*/
/**/
int32_t
rtp_encode(uint8_t *p_data, uint16_t const data_len, uint8_t *const p_out_buffer,\
           uint16_t const out_buffer_len, rtp_profile_s *p_rtp_profile)
{
    rtp_header_s    rtp_header  = {0};
    int32_t          ret_val     = -1;

    pthread_mutex_lock(&rtp_tx_lock);

    if(out_buffer_len < (data_len + sizeof(rtp_header_s) + 2))
    {
        ret_val = RTP_LIMITED_BUFF_SIZE_ERROR;
        goto exit_rtp_encode;
    }

    rtp_header.payload_marker = 0;

    rtp_header.version          = 0x80;
    rtp_header.seq_num          = rtp_htons(p_rtp_profile->seq_num);
    rtp_header.ssrc             = rtp_htonl(p_rtp_profile->ssrc);
    rtp_header.timestamp        = rtp_htonl(p_rtp_profile->timestamp);
    rtp_header.payload_marker   |= (RTP_HEADER_PAYLOAD & p_rtp_profile->payload_type);

    switch(p_rtp_profile->type)
    {
        case VIDEO:
        {
            ret_val = rtp_encode_v_profile(&rtp_header, p_rtp_profile,\
                                           p_out_buffer, p_data, data_len);
            if (ret_val < 0)
            {
                Report("Error in 'rtp_encode_v_profile'\n\r");
                goto exit_rtp_encode;
            }
        }
        break;

        case AUDIO:
        {
            ret_val = rtp_encode_a_profile(&rtp_header, p_rtp_profile,\
                                           p_out_buffer, p_data, data_len);
            if (ret_val < 0)
            {
                Report("Error in 'rtp_encode_a_profile'\n\r");
                goto exit_rtp_encode;
            }
        }
        break;

        default:
        {
            Report("Unidentified/unsupported profile type\n\r");
            ret_val = -1;
            goto exit_rtp_encode;
        }
    }

exit_rtp_encode:
    pthread_mutex_unlock(&rtp_tx_lock);
    return ret_val;
}


int32_t rtp_decode(int8_t *p_data, uint16_t const packet_len, uint16_t *data_len, rtp_profile_s *p_rtp_profile)
{

    rtp_header_s    *p_rtp_header;


    pthread_mutex_lock(&rtp_rx_lock);



    //pull rtp header off
    p_rtp_header = (rtp_header_s *)p_data;

    // Decode RTP packet 
    /*
    typedef struct rtp_header
    #endif
    {
        uint8_t   version;
        uint8_t   payload_marker;
        uint16_t  seq_num;
        uint32_t  timestamp;
        uint32_t  ssrc;

    }rtp_header_s;*/

    p_rtp_profile->timestamp = rtp_htonl(p_rtp_header->timestamp);
    p_rtp_profile->seq_num = rtp_htons(p_rtp_header->seq_num);
    p_rtp_profile->ssrc = rtp_htonl(p_rtp_header->ssrc);
    ///////////////////////
    *data_len=packet_len - sizeof(rtp_header_s);
    

    pthread_mutex_unlock(&rtp_rx_lock);
    return 0;
}

/**/
static uint16_t rtp_htons(uint16_t val)
{
    int16_t    i   = 1;
    int8_t     *p  = (signed char *)&i;

    if(p[0] == 1) /* little endian */
    {
        p[0] = ((signed char* )&val)[1];
        p[1] = ((signed char* )&val)[0];
        return i;
    }

    /* big endian */
    return val;
}

/**/
static uint32_t rtp_htonl(uint32_t val )
{
    uint32_t        i   = 1;
    signed char   *p  = (signed char *)&i;

    if(p[0] == 1) /* little endian */
    {
        p[0] = ((signed char* )&val)[3];
        p[1] = ((signed char* )&val)[2];
        p[2] = ((signed char* )&val)[1];
        p[3] = ((signed char* )&val)[0];
        return i;
    }

    /* big endian */
    return val;
}

/**/
static int32_t
rtp_encode_v_profile(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,\
                     uint8_t *const p_out_buffer, uint8_t *p_data,\
                     uint16_t const data_len)
{
    int32_t ret_val = -1;

    switch(p_rtp_profile->payload_format)
    {
        case H264_AVC:
        {
            ret_val = rtp_encode_h264avc(p_rtp_header, p_rtp_profile,\
                                         p_out_buffer, p_data, data_len);
            if (ret_val < 0)
            {
                Report("H264-AVC Encoding Failed.!\n\r");
                goto exit_rtp_encode_v_profile;
            }
        }
        break;

        default:
        {
            Report("Unidentified/unsupported payload format\n\r");
            ret_val = -1;
            goto exit_rtp_encode_v_profile;
        }
    }

exit_rtp_encode_v_profile:
    return ret_val;
}

/**/
static int32_t
rtp_encode_a_profile(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,\
                     uint8_t *const p_out_buffer, uint8_t *p_data,\
                     uint16_t const data_len)
{
    int32_t ret_val = -1;

    
    switch(p_rtp_profile->payload_format)
    {
        case L16_PCM:
        {
            ret_val = rtp_encode_pcm16(p_rtp_header, p_rtp_profile,\
                                       p_out_buffer, p_data, data_len);
            if (ret_val < 0)
            {
                Report("L16_PCM Encoding Failed.!\n\r");

            }
        }
        break;
        
        case PCMU:
        {
            ret_val = rtp_encode_pcmu(p_rtp_header, p_rtp_profile,\
                                       p_out_buffer, p_data, data_len);
            if (ret_val < 0)
            {
                Report("PCMU Encoding Failed.!\n\r");

            }
        }
        break;

        default:
        {
            Report("Unidentified/unsupported payload format\n\r");
            ret_val = -1;

        }
    }

    if(p_rtp_profile->p_data.aProfile.first_packet==1) p_rtp_profile->p_data.aProfile.first_packet=0;
    return ret_val;
}


/*!
 */
static int32_t
rtp_encode_h264avc(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,\
                   uint8_t *const p_out_buffer, uint8_t *p_data, uint16_t data_len)//was const data_len
{
    uint32_t  packet_size = 0;
    int32_t  ret_val     = -1;
    uint8_t   nal_header  = 0;
    uint8_t   fu_header   = 0;

    
    static uint8_t   frame_type   = 0;

    /* NAL F */
    nal_header = 0;//NAL_F_MASK;

    /* FU  R */
    fu_header =0;//&= ~FU_R_MASK;

    if(p_rtp_profile->p_data.vProfile.is_first_frame_frag == 1)
    {

      if(!(p_rtp_profile->p_data.vProfile.frame_type&0x02)){//p_rtp_profile->p_data.vProfile.frame_type!=0){
          data_len-=5;
          p_data=&p_data[5];
      }
      frame_type=p_rtp_profile->p_data.vProfile.frame_type;

        if(p_rtp_profile->p_data.vProfile.is_last_frame_frag == 1)
        {
            /** End of frame and beginning.!
              * Do not fragment - Single NAL header and no FU header
              */
            p_rtp_header->payload_marker |= RTP_HEADER_MARKER;

            ret_val = rtp_fill_nal_single(frame_type,\
                                        &nal_header, &fu_header, &packet_size,\
                                        p_out_buffer, p_rtp_header);

            
            
            if(ret_val < 0) goto exit_rtp_encode_h264avc;
        }
        else
        {
            /* Not the last frame */
            p_rtp_header->payload_marker &= ~(RTP_HEADER_MARKER);

            /* NAL type FU-A (NType 0x1C) */
            nal_header |= 0x1C;

            /* Fu header S = 1 */
            fu_header |=  FU_S_MASK;

            /* Fu Header E = 0 */


            ret_val = rtp_fill_nal_fu_2(frame_type,\
                                        &nal_header, &fu_header, &packet_size,\
                                        p_out_buffer, p_rtp_header);
            if(ret_val < 0) goto exit_rtp_encode_h264avc;
        }
    }
    else
    {
        /* Not a new frame */

        /* NAL type FU-A NType = 0x1C */
        nal_header |= 0x1C;

        if(p_rtp_profile->p_data.vProfile.is_last_frame_frag == 1)
        {
            /* End of fragmented frame sequence */

            p_rtp_header->payload_marker |= RTP_HEADER_MARKER;

            /** S = 0
              * End of frame E = 1
              */

            fu_header |= FU_E_MASK;
            


            
            ret_val = rtp_fill_nal_fu_3(frame_type,\
                                        &nal_header, &fu_header, &packet_size,\
                                        p_out_buffer, p_rtp_header);


            if(ret_val < 0) goto exit_rtp_encode_h264avc;
        }
        else
        {
            /* Inside fragmented frame sequence  */

            /* Not end of frame */
            p_rtp_header->payload_marker &= ~(RTP_HEADER_MARKER);

            /** Fu Header S = 0
              * Fu Header E = 0
              */


            ret_val = rtp_fill_nal_fu_3(frame_type,\
                                        &nal_header, &fu_header, &packet_size,\
                                        p_out_buffer, p_rtp_header);
            if(ret_val < 0) goto exit_rtp_encode_h264avc;
        }
    }

    

    exit_rtp_encode_h264avc:

    memcpy(p_out_buffer+packet_size, p_data, data_len);
    packet_size += data_len;
      /*
    //Debug for prinitng out critical packet information
    for(int bb=0;bb<24;bb++){
      Report("%02x ",p_out_buffer[bb]);
    }
    Report("\r\n");
      */
	
    if(ret_val==-1){
        return -1;
    }
    else ret_val = packet_size;
    
    return ret_val;
}

/**/
static int32_t
rtp_encode_pcm16(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,\
                 uint8_t *const p_out_buffer, uint8_t *p_data, uint16_t const data_len)
{
    uint32_t  packet_size =  0;
    int32_t  ret_val     = -1;

    ret_val = rtp_w_endianness(p_data, data_len);
    if(ret_val < 0) goto exit_rtp_encode_pcm16;

    if(p_rtp_profile->p_data.aProfile.first_packet==1) p_rtp_header->payload_marker |= (RTP_HEADER_MARKER);
    else p_rtp_header->payload_marker &= (~RTP_HEADER_MARKER);

    memcpy(p_out_buffer+packet_size, p_rtp_header, sizeof(rtp_header_s));
    packet_size+=sizeof(rtp_header_s);

    memcpy(p_out_buffer+packet_size, &tmp_endian_buffer[0], data_len);
    //memcpy(p_out_buffer+packet_size, p_data, data_len);
    packet_size += data_len;

    ret_val = packet_size;

exit_rtp_encode_pcm16:
    return ret_val;
}

/**/
static int32_t
rtp_encode_pcmu(rtp_header_s *p_rtp_header, rtp_profile_s *p_rtp_profile,\
                 uint8_t *const p_out_buffer, uint8_t *p_data, uint16_t const data_len)
{
    uint32_t  packet_size =  0;
    int16_t  ret_val     = -1;
    /*
    if(audio_format==ULAW_16000_MCU){
        ret_val = rtp_w_endianness(p_data, data_len);
        if(ret_val < 0) goto exit_rtp_encode_pcmu;
    }
    */

    
    if(p_rtp_profile->p_data.aProfile.first_packet==1) p_rtp_header->payload_marker |= (RTP_HEADER_MARKER);
    else p_rtp_header->payload_marker &= (~RTP_HEADER_MARKER);

    memcpy(p_out_buffer+packet_size, p_rtp_header, sizeof(rtp_header_s));
    packet_size+=sizeof(rtp_header_s);
    /*
    if(audio_format==ULAW_16000_MCU){
        memcpy(p_out_buffer+packet_size, &tmp_endian_buffer[0], data_len);
    }
    else{*/
        memcpy(p_out_buffer+packet_size, p_data, data_len);
   // }
    packet_size += data_len;

    ret_val = packet_size;


    return ret_val;
}

/**/
//static int32_t
//rtp_x_endianness(uint8_t *p_data, uint16_t const data_len)
//{
//    uint8_t   *p_temp = NULL;
//
//    uint32_t  idx     = 0;
//    uint32_t  var_nl  = 0;
//    uint32_t  var_h   = 0;
//
//    p_temp = p_data;
//
//    memset(tmp_endian_buffer, 0, sizeof(tmp_endian_buffer));
//
//    for(idx = 0; idx < data_len; idx+=sizeof(long))
//    {
//        var_h = *(p_temp+idx) << 24     |  *(p_temp+idx+1) << 16 |\
//                *(p_temp+idx+2) << 8    |  *(p_temp+idx+3);
//
//        var_nl = rtp_htonl(var_h);
//
//        tmp_endian_buffer[idx]      = var_nl >> 24;
//        tmp_endian_buffer[idx+1]    = var_nl >> 16;
//        tmp_endian_buffer[idx+2]    = var_nl >> 8;
//        tmp_endian_buffer[idx+3]    = var_nl;
//    }
//
//    return 0;
//}

/**/
static int16_t
rtp_w_endianness(uint8_t *p_data, uint16_t const data_len)
{
    uint8_t   *p_temp = NULL;

    uint32_t  idx     = 0;
    uint32_t  var_nl  = 0;
    uint32_t  var_h   = 0;

    p_temp = p_data;

    memset(tmp_endian_buffer, 0, sizeof(tmp_endian_buffer));

    for(idx = 0; idx < data_len; idx+=sizeof(short))
    {
        var_h = *(p_temp+idx) << 8  |  *(p_temp+idx+1);

        var_nl = rtp_htons(var_h);

        tmp_endian_buffer[idx]      = var_nl >> 8;
        tmp_endian_buffer[idx+1]    = var_nl;

    }

    return 0;
}


/**/
static inline \
int32_t rtp_fill_nal_single(uint8_t const frame_type, uint8_t *p_nal_header, \
                         uint8_t *p_fu_header, uint32_t *p_packet_size,   \
                         uint8_t *const p_out_buffer, rtp_header_s *p_rtp_header)
{
    int32_t ret_val = 0;


    if(frame_type&0x02)//(0 == frame_type)
    {
        /** IDR frame
          * IDR frame, NAL type IDR slice
          * NRI 3, NType 5
          */
        *p_nal_header |=  0x65;
    }
    else if(frame_type&0x04)//(1 == frame_type)
    {
        /** I frame
          * Non-IDR, NAL type non-IDR slice\
          * NRI 2, NType 1\
          */
        *p_nal_header |= 0x41;
    }
    else if(frame_type&0x10)
    {
        Report("Unexpected image frame: %i\n\r",frame_type);
        ret_val = -1;

    }
    else if(frame_type&0x20)
    {
        Report("Unexpected no-data frame: %i\n\r",frame_type);
        ret_val = -1;

    }
    else if(frame_type&0xC0)
    {
        Report("Corrupt frame type: %i\n\r",frame_type);
        ret_val = -1;

    }
    else 
    {
        /** P frame 
          * Non-IDR, NAL type non-IDR slice
          * NRI 1, NType 1
          */
        *p_nal_header |= 0x21;
    }


    memcpy(p_out_buffer + *p_packet_size, p_rtp_header, sizeof(rtp_header_s));
    *p_packet_size += sizeof(rtp_header_s);

    memcpy(p_out_buffer + *p_packet_size, p_nal_header, sizeof(uint8_t));
    *p_packet_size += sizeof(uint8_t);


    return ret_val;
}

/**/
static inline \
int32_t rtp_fill_nal_fu_2(uint8_t const frame_type, uint8_t *p_nal_header, \
                         uint8_t *p_fu_header, uint32_t *p_packet_size,   \
                         uint8_t *const p_out_buffer, rtp_header_s *p_rtp_header)
{
    int32_t ret_val = 0;
*p_nal_header &= ~(NAL_NRI_MASK);

    if(frame_type&0x02)//(0 == frame_type)
    {
        /** IDR frame
          * IDR NRI = 3
          * NAL type IDR slice FType = 5
          */
        *p_nal_header |= 0x60;
        *p_fu_header |= 0x05;
    }
    else if(frame_type&0x04)//(1 == frame_type)
    {
        /** I frame
          * Non-IDR NRI  = 2
          * NAL type non-IDR slice Ftype = 1
          */
        *p_nal_header |= 0x40;//nal header in this case is also known as "FU indicator"
        *p_fu_header |= 0x01;
    }
    else if(frame_type&0x10)
    {
        Report("Unexpected image frame: %i\n\r",frame_type);
        ret_val = -1;

    }
    else if(frame_type&0x20)
    {
        Report("Unexpected no-data frame: %i\n\r",frame_type);
        ret_val = -1;

    }
    else if(frame_type&0xC0)
    {
        Report("Corrupt frame type: %i\n\r",frame_type);
        ret_val = -1;

    }
    else
    {
        /** P frame
          * Non-IDR NRI = 1
          * NAL type non-IDR slice FType = 1
          */

        *p_nal_header |=0x20;
        *p_fu_header |= 0x01;
    }


    memcpy(p_out_buffer + *p_packet_size, p_rtp_header, sizeof(rtp_header_s));
    *p_packet_size += sizeof(rtp_header_s);

    memcpy(p_out_buffer + *p_packet_size, p_nal_header, sizeof(uint8_t));
    *p_packet_size += sizeof(uint8_t);

    memcpy(p_out_buffer + *p_packet_size, p_fu_header, sizeof(uint8_t));
    *p_packet_size += sizeof(uint8_t);
    


    return ret_val;
}

/**/
static inline \
int32_t rtp_fill_nal_fu_3(uint8_t const frame_type, uint8_t *const p_nal_header, \
                         uint8_t *const p_fu_header, uint32_t *p_packet_size,   \
                         uint8_t *const p_out_buffer, rtp_header_s *p_rtp_header)
{
    int32_t ret_val = 0;
*p_nal_header &= ~(NAL_NRI_MASK);
    
    if(frame_type&0x02)//0 == frame_type)
    {
        /** IDR frame
          * IDR  NRI = 3
          * NAL type IDR slice FType = 5
          */
        *p_nal_header |= 0x60;
        *p_fu_header |= 0x05;
    }
    else if(frame_type&0x04)//(1 == frame_type)
    {
        /** I frame
          * Non-IDR NRI = 2
          * NAL type non-IDR slice FType = 1
          */

        *p_nal_header |= 0x40;
        *p_fu_header |= 0x01;
    }
    else if(frame_type&0x10)
    {
        Report("Unexpected image frame: %i\n\r",frame_type);
        ret_val = -1;

    }
    else if(frame_type&0x20)
    {
        Report("Unexpected no-data frame: %i\n\r",frame_type);
        ret_val = -1;

    }
    else if(frame_type&0xC0)
    {
        Report("Corrupt frame type: %i\n\r",frame_type);
        ret_val = -1;

    }
    else
    {
        /** P frame
          * Non-IDR NRI = 1
          * NAL type non-IDR slice FType = 1
          */

        *p_nal_header |=0x20;
        *p_fu_header |= 0x01;
    }
    

    memcpy(p_out_buffer + *p_packet_size, p_rtp_header, sizeof(rtp_header_s));
    *p_packet_size += sizeof(rtp_header_s);

    memcpy(p_out_buffer + *p_packet_size, p_nal_header, sizeof(uint8_t));
    *p_packet_size += sizeof(uint8_t);

    memcpy(p_out_buffer + *p_packet_size, p_fu_header, sizeof(uint8_t));
    *p_packet_size += sizeof(uint8_t);

    

    return ret_val;
}




/*
int32_t rtp_fill_sr_pkt(uint8_t *const p_out_buffer)
{
    rtcp_src_header_s   src_header  = {0};
    rtcp_sr_header_s    sr_header   = {0};
    int32_t              ret_val     = -1;
    rtcp_sr_info_s

    pthread_mutex_lock(&rtp_lock);

    // Sender Report Header 
    sr_header.version       = 0x80;
    sr_header.type          = 200;
    sr_header.length        = rtp_htons(0x6);
    sr_header.ssrc          = rtp_htonl(p_rtp_profile->ssrc);
    sr_header.timestamp_msw = rtp_htonl(0);
    sr_header.timestamp_lsw = rtp_htonl(0);
    sr_header.timestamp     = rtp_htonl(p_rtp_profile->timestamp);

    sr_header.pkt_cnt       = rtp_htonl(p_rtp_profile->pkt_count);
    sr_header.octet_cnt     = rtp_htonl(p_rtp_profile->octet_count);

    // Source Description Header 
    
    src_header.version      = 0x81;
    src_header.type         = 202;
    src_header.length       = rtp_htons(0x3);
    src_header.ssrc         = rtp_htonl(sr_info->ssrc);
    src_header.ttype        = 0x1;
    src_header.tlength      = 5;
    src_header.text         = 'C';
    src_header.text1        = 'C';
    src_header.text2        = '3';
    src_header.text3        = '2';
    src_header.text4        = 'X';
    src_header.etype        = 0x0;
*

    if(buffer_len <= (sizeof(rtcp_sr_header_s)+ sizeof(rtcp_src_header_s)))
    {
        ret_val = RTP_LIMITED_BUFF_SIZE_ERROR;
        goto exit_rtp_create_sr_pkt;
    }

    memcpy(p_out_buffer, &sr_header, sizeof(rtcp_sr_header_s));
    memcpy(&p_out_buffer[0 + sizeof(rtcp_sr_header_s)], &src_header,\
                                            sizeof(rtcp_src_header_s));

    ret_val = sizeof(rtcp_sr_header_s)+ sizeof(rtcp_src_header_s);

exit_rtp_create_sr_pkt:
    pthread_mutex_unlock(&rtp_tx_lock);
    return ret_val;
}
*/


/**/
int32_t rtcp_fill_rr_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile)
{
    //{rtcp_header_s, rtcp_rr_info_s, rtcp_rr_sr_rept_s ...}
    int32_t              ret_val     = -1;
    int32_t              count=0;
    uint8_t *buffer = p_out_buffer;

    pthread_mutex_lock(&rtp_tx_lock);

    rtcp_sr_info_s              rr_info;   
            rr_info.sender_ssrc=rtp_htonl(p_rtp_profile->ssrc);//sender_ssrc;  
            rr_info.ntp_timestamp.nsec=0x00000000;//ntp_timestamp;
            rr_info.ntp_timestamp.secs=0x00000000;//ntp_timestamp;
            rr_info.rtp_timestamp=rtp_htonl(p_rtp_profile->timestamp);//rtp_timestamp;
            rr_info.pkt_count=rtp_htonl(p_rtp_profile->pkt_cnt);//pkt_count;
            rr_info.octet_count=rtp_htonl(p_rtp_profile->octet_cnt);//octet_count;
        
    rtcp_header_s               rr_rtcp_header;  
            rr_rtcp_header.version_source_count=0x81; //version_source_count: version 2, count 1
            rr_rtcp_header.packet_type=200; //packet_type: 201 (sender report)
            rr_rtcp_header.length=0x0700; //length: 7 (num 32-bit words minus one)

    rtcp_rr_sr_rept_s              rr_rept;
            rr_rept.source_ssrc=0x00;//source_ssrc:
            rr_rept.lost_fraction=0x00;//lost_fraction:
            rr_rept.cuml_lost_pkts[0]=0x00;//cuml_lost_pkts[3]:
            rr_rept.cuml_lost_pkts[1]=0x00;
            rr_rept.cuml_lost_pkts[2]=0x00;
            rr_rept.ext_high_seq_num_rxd=0x00;//ext_high_seq_num_rxd:
            rr_rept.jitter=0x00;//jitter:
            rr_rept.last_sr=0x00;//last_sr:
            rr_rept.delay_since_last_sr=0x00;//delay_since_last_sr:


    memcpy(buffer, &rr_rtcp_header, sizeof(rtcp_header_s));
    buffer+=sizeof(rtcp_header_s); 
    count+=sizeof(rtcp_header_s);
    
    memcpy(buffer, &rr_info, sizeof(rtcp_rr_info_s));
    buffer+=sizeof(rtcp_rr_info_s); 
    count+=sizeof(rtcp_rr_info_s);
    
    memcpy(buffer, &rr_rept, sizeof(rtcp_rr_sr_rept_s));
    buffer+=sizeof(rtcp_rr_sr_rept_s); 
    count+=sizeof(rtcp_rr_sr_rept_s);
    
    ret_val = count;

    pthread_mutex_unlock(&rtp_tx_lock);
    return ret_val;
}



/**/
uint32_t lastSR = 0x00;
int32_t rtcp_fill_sr_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile)
{
    //{rtcp_header_s, rtcp_sr_info_s, rtcp_rr_sr_rept_s ...}
    int32_t              ret_val     = -1;
    int32_t              count=0;
    uint8_t *buffer = p_out_buffer;

    pthread_mutex_lock(&rtp_tx_lock);
    uint64_t time = cc_get_timestamp() * 1000;
    rtcp_sr_info_s              sr_info;   
            sr_info.sender_ssrc=rtp_htonl(p_rtp_profile->ssrc);//sender_ssrc;  
            sr_info.ntp_timestamp.nsec=time;//ntp_timestamp;
            sr_info.ntp_timestamp.secs=time >> 32;//ntp_timestamp;
            sr_info.rtp_timestamp=rtp_htonl(p_rtp_profile->timestamp);//rtp_timestamp;
            Report("          RTP TimeStamp Size of: %d\r\n", p_rtp_profile->timestamp);
            sr_info.pkt_count=rtp_htonl(p_rtp_profile->pkt_cnt);//pkt_count;
            sr_info.octet_count=rtp_htonl(p_rtp_profile->octet_cnt);//octet_count;
        
    rtcp_header_s               sr_rtcp_header;  
            sr_rtcp_header.version_source_count=0x81; //version_source_count: version 2, count 1
            sr_rtcp_header.packet_type=200; //packet_type: 201 (sender report)
            sr_rtcp_header.length=0x0700; //length: 7 (num 32-bit words minus one)

    rtcp_rr_sr_rept_s              sr_rept;
            sr_rept.source_ssrc=0x00;//source_ssrc:
            sr_rept.lost_fraction=0x00;//lost_fraction:
            sr_rept.cuml_lost_pkts[0]=0x00;//cuml_lost_pkts[3]:
            sr_rept.cuml_lost_pkts[1]=0x00;
            sr_rept.cuml_lost_pkts[2]=0x00;
            sr_rept.ext_high_seq_num_rxd=0x00;//ext_high_seq_num_rxd:
            sr_rept.jitter=0x00;//jitter:
            sr_rept.last_sr=0x00;//lastSR;//last_sr:
            sr_rept.delay_since_last_sr=0x00;//delay_since_last_sr:
//Set new last_sr
            lastSR = rtp_htonl(p_rtp_profile->timestamp);

    memcpy(buffer, &sr_rtcp_header, sizeof(rtcp_header_s));
    buffer+=sizeof(rtcp_header_s); 
    count+=sizeof(rtcp_header_s);
    
    memcpy(buffer, &sr_info, sizeof(rtcp_sr_info_s));
    buffer+=sizeof(rtcp_sr_info_s); 
    count+=sizeof(rtcp_sr_info_s);
    
    memcpy(buffer, &sr_rept, sizeof(rtcp_rr_sr_rept_s));
    buffer+=sizeof(rtcp_rr_sr_rept_s); 
    count+=sizeof(rtcp_rr_sr_rept_s);
    
    ret_val = count;

    pthread_mutex_unlock(&rtp_tx_lock);
    return ret_val;
}

/**/
int32_t rtcp_fill_sdes_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile)
{
    //{rtcp_header_s, rtcp_sdes_chunk_s, rtcp_sdes_item - text ... , rtcp_sdes_chunk_s, rtcp_sdes_item - text ...}
    int32_t              ret_val     = -1;
    int                 cname_len;
    int32_t              count=0;
    uint8_t *buffer = p_out_buffer;

    pthread_mutex_lock(&rtp_tx_lock);


    rtcp_header_s               sdes_rtcp_header;
            sdes_rtcp_header.version_source_count=0x81; //version_source_count: version 2, count 1
            sdes_rtcp_header.packet_type=202;//packet_type: 202 (source description)
            sdes_rtcp_header.length=0x0700; //length: 7 (num 32-bit words minus one)

    rtcp_sdes_chunk_s           sdes_chunk1;
            sdes_chunk1.source_ssrc=rtp_htonl(p_rtp_profile->ssrc);//sender_ssrc

    rtcp_sdes_item_s            sdes_item_cname;
            sdes_item_cname.id=0x01;//id: CNAME (1)
            sdes_item_cname.length=13;//length: 18

    char *                      sdes_CNAME = "cc3220@ti.com";
    
    rtcp_sdes_item_s            sdes_item_end;
            sdes_item_end.id=0x00;//id: END (0)
            sdes_item_end.length=0;//length: 0

    
    memcpy(buffer, &sdes_rtcp_header, sizeof(rtcp_header_s));
    buffer+=sizeof(rtcp_header_s); 
    count+=sizeof(rtcp_header_s);
    
    memcpy(buffer, &sdes_chunk1, sizeof(rtcp_sdes_chunk_s));
    buffer+=sizeof(rtcp_sdes_chunk_s); 
    count+=sizeof(rtcp_sdes_chunk_s);
    
    memcpy(buffer, &sdes_item_cname, sizeof(rtcp_sdes_item_s));
    buffer+=sizeof(rtcp_sdes_item_s); 
    count+=sizeof(rtcp_sdes_item_s);
    
    cname_len=strlen(sdes_CNAME);
    memcpy(buffer, &sdes_CNAME, cname_len);
    buffer+=cname_len; 
    count+=cname_len;
    
    memcpy(buffer, &sdes_item_end, sizeof(rtcp_sdes_item_s));
    buffer+=sizeof(rtcp_sdes_item_s); 
    count+=sizeof(rtcp_sdes_item_s);    
    
    ret_val = count;

    pthread_mutex_unlock(&rtp_tx_lock);
    return ret_val;
}

int32_t rtcp_fill_payload_spec_fb_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile)
{
    
  
    int32_t              ret_val     = -1;
    int32_t              count=0;
    uint8_t *buffer = p_out_buffer;
    
    pthread_mutex_lock(&rtp_tx_lock);
/*
      typedef struct rtcp_header

    {
        uint8_t   version_source_count;
        uint8_t   packet_type;
        uint16_t  length;

    }rtcp_header_s;
    typedef struct rtcp_feedback_info
    {
        uint32_t  sender_ssrc;  
        uint32_t  source_ssrc;

    }rtcp_feedback_info_s;
    typedef struct fci_rpsi_h264_msg
    {
        uint8_t   PB;
        uint8_t   payload_type;
        uint16_t  bit_str;
        
    }fci_rpsi_h264_msg_s;*/
    rtcp_header_s fb_rtcp_header;
    rtcp_feedback_info_s h264_fb;
    fci_rpsi_h264_msg_s h264_rpsi;
    
    fb_rtcp_header.length=rtp_htons(3);
    fb_rtcp_header.version_source_count=0x83;
    fb_rtcp_header.packet_type=206;
    h264_fb.sender_ssrc=rtp_htonl(0);//
    h264_fb.source_ssrc=rtp_htonl(p_rtp_profile->ssrc);
    h264_rpsi.PB=0;
    h264_rpsi.payload_type=96;
    h264_rpsi.bit_str=0xEF01;
    
    memcpy(buffer, &fb_rtcp_header, sizeof(rtcp_header_s));
    buffer+=sizeof(rtcp_header_s); 
    count+=sizeof(rtcp_header_s);
    
    memcpy(buffer, &h264_fb, sizeof(rtcp_feedback_info_s));
    buffer+=sizeof(rtcp_feedback_info_s); 
    count+=sizeof(rtcp_feedback_info_s);
    
    memcpy(buffer, &h264_rpsi, sizeof(fci_rpsi_h264_msg_s));
    buffer+=sizeof(fci_rpsi_h264_msg_s); 
    count+=sizeof(fci_rpsi_h264_msg_s);
    
    ret_val = count;

    pthread_mutex_unlock(&rtp_tx_lock);
    return ret_val;
}


int32_t rtp_fill_sps_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile){
  
    int32_t              count=0;
    uint8_t *buffer = p_out_buffer;
    uint8_t nal_header;
    rtp_header_s    rtp_header  = {0};

    int len;
    unsigned char bytes[8]={0x42,0x00,0x14,0xF4,0x02,0x78,0x2C,0x88};
    //720p:
    //01000010 00000000 00010100 11110100 00000010 01111000 00101100 10001000
    //1080p:
    //01000010 00000000 00010100 11110100 00000011 10111000 00010000 11100010
    
    
    //nal_unit_type of 7 or 8 indicates a 
    //sequence parameter set or a picture parameter set, respectively
    //NRI to 11 (in binary)
    //F: 0
    //Therefore: nal unit octet = 01100111 for SPS, 01101000 for PPS

    rtp_header.version          = 0x80;
    rtp_header.payload_marker   = (RTP_HEADER_PAYLOAD & p_rtp_profile->payload_type);
    rtp_header.seq_num          = rtp_htons(p_rtp_profile->seq_num);
    rtp_header.timestamp        = rtp_htonl(p_rtp_profile->timestamp);
    rtp_header.ssrc             = rtp_htonl(p_rtp_profile->ssrc);
    
    memcpy(buffer, &rtp_header, sizeof(rtp_header_s));
    buffer+=sizeof(rtp_header_s); 
    count+=sizeof(rtp_header_s);
    
    nal_header = 0x67;
    
    memcpy(buffer, &nal_header, sizeof(uint8_t));
    buffer+=sizeof(uint8_t); 
    count+=sizeof(uint8_t);
    
    len=8;
    memcpy(buffer, &bytes, len);
    count+=len;
    return count;
  
}

int32_t stun_encode_binding(uint8_t *const p_out_buffer){
    
    int len;
    /*
    typedef struct stun_header
    {
        unsigned short msg_type;
        unsigned short msg_length;
        unsigned int magic_cookie;
        unsigned char transaction_id[12];

    }stun_header_s;*/
    
    stun_header_s header=
    {
        0x0100,
        0x0000,
        0x42A41221,
        {117,226,35,244,53,162,211,182,93,4,95,186}
    };
    
    (*((unsigned int *)(&header.transaction_id)))^=rand();
    (*((unsigned int *)(&header.transaction_id)+1))^=rand();
    (*((unsigned int *)(&header.transaction_id)+2))^=rand();
    
    len=sizeof(stun_header_s);
    memcpy(p_out_buffer, &header, len);
    return len;
  
}
