//*****************************************************************************
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#ifndef __RTCP_RTP_H__
#define __RTCP_RTP_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*!
    \addtogroup RTP_IF
    @{
*/


/* Error Codes */
typedef enum rtp_status_code
{
    RTP_ERROR_UNKNOWN           = -1,
    RTP_LIMITED_BUFF_SIZE_ERROR = -2

}rtp_status_code_e;

/* Supported Profiles */
typedef enum profile_type
{
    VIDEO,
    AUDIO

}profile_type_e;

/* Supported Payload Formats */
typedef enum payload_format
{
    L16_PCM,
    PCMU,
    H264_AVC


}payload_format_e;

/* Construct to refer to a time (absolute or interval) */
typedef struct rtcp_ntp_time
{
    uint32_t  secs;   /* Range: 0 to 0xFFFFFFFF secs */
    uint32_t  nsec;   /* Range: 0 to  999999999 nsec */

}rtcp_ntp_time_s;


/* RTCP Header */
#ifdef ccs
typedef struct __attribute__ ((__packed__)) rtcp_header
#elif __IAR_SYSTEMS_ICC__
//#pragma pack(1)
typedef struct rtcp_header
#endif
{
    uint8_t   version_source_count;
    uint8_t   packet_type;
    uint16_t  length;

}rtcp_header_s;

    
/* RTCP Reciever Report Information */
typedef struct rtcp_feedback_info
{
    uint32_t  sender_ssrc;  
    uint32_t  source_ssrc;

}rtcp_feedback_info_s;


#ifdef ccs
typedef struct __attribute__ ((__packed__)) fci_rpsi_h264_msg
#elif __IAR_SYSTEMS_ICC__
//#pragma pack(1)
typedef struct fci_rpsi_h264_msg
#endif
{
    uint8_t   PB;
    uint8_t   payload_type;
    uint16_t  bit_str;
    
}fci_rpsi_h264_msg_s;

/* RTCP Sender Report Information */
typedef struct rtcp_sr_info
{
    uint32_t  sender_ssrc;  
    rtcp_ntp_time_s     ntp_timestamp;
    uint32_t              rtp_timestamp;
    uint32_t              pkt_count;
    uint32_t              octet_count;

}rtcp_sr_info_s;

/* RTCP Reciever Report Information */
typedef struct rtcp_rr_info
{
    uint32_t  sender_ssrc;  

}rtcp_rr_info_s;


/* RTCP Receiver/Sender Report Block */
#ifdef ccs
typedef struct __attribute__ ((__packed__)) rtcp_rr_sr_rept
#elif __IAR_SYSTEMS_ICC__
//#pragma pack(1)
typedef struct rtcp_rr_sr_rept
#endif
{
    uint32_t  source_ssrc;
    uint8_t   lost_fraction;
    uint8_t   cuml_lost_pkts[3];
    uint32_t  ext_high_seq_num_rxd;
    uint32_t  jitter;
    uint32_t  last_sr;
    uint32_t  delay_since_last_sr;

}rtcp_rr_sr_rept_s;

/* SDES Source Description */
#ifdef ccs
typedef struct __attribute__ ((__packed__)) rtcp_sdes_chunk
#elif __IAR_SYSTEMS_ICC__
//#pragma pack(1)
typedef struct rtcp_sdes_chunk
#endif
{
    uint32_t  source_ssrc;

}rtcp_sdes_chunk_s;

#ifdef ccs
typedef struct __attribute__ ((__packed__)) rtcp_sdes_item
#elif __IAR_SYSTEMS_ICC__
//#pragma pack(1)
typedef struct rtcp_sdes_item
#endif
{
    uint8_t  id;
    uint8_t  length;
}rtcp_sdes_item_s;


//{rtcp_header_s, rtcp_sr_info_s, rtcp_rr_sr_rept_s ...}
//{rtcp_header_s, rtcp_rr_info_s, rtcp_rr_sr_rept_s ...}
//{rtcp_header_s, rtcp_sdes_chunk_s, rtcp_sdes_item - text ... , rtcp_sdes_chunk_s, rtcp_sdes_item - text ...}


/* RTP Header */
#ifdef ccs
typedef struct __attribute__ ((__packed__)) rtp_header
#elif __IAR_SYSTEMS_ICC__
//#pragma pack(1)
typedef struct rtp_header
#endif
{
    uint8_t   version;
    uint8_t   payload_marker;
    uint16_t  seq_num;
    uint32_t  timestamp;
    uint32_t  ssrc;

}rtp_header_s;

/* Video Profile */
typedef struct rtp_v_profile_info
{
    uint8_t   is_first_frame_frag;
    uint8_t   is_last_frame_frag;
    uint8_t   frame_type;

}rtp_v_profile_info_s;

/* Audio Profile */
typedef struct rtp_a_profile
{
    uint8_t   mode;
    uint8_t   first_packet;

}rtp_a_profile_info_s;

/**/
typedef union
{
    rtp_a_profile_info_s    aProfile;
    rtp_v_profile_info_s    vProfile;

}rtp_profile_data_s;

/* RTP Stream Profile */
typedef struct rtp_profile
{
    rtp_profile_data_s  p_data;
    profile_type_e      type;
    payload_format_e    payload_format;

    uint32_t              timestamp;
    uint32_t              ssrc;
    uint16_t              seq_num;
    uint8_t               payload_type;
    uint32_t              pkt_cnt;
    uint32_t              octet_cnt;

}rtp_profile_s;

/*!
    \brief      Inintilizes the RTP library
    \param      none
    \return     0 on success, negative error-code on error
    \note
*/
int32_t rtp_rtcp_init();

/*!
    \brief      Deinintilizes the RTP library
    \param      none
    \return     0 on success, negative error-code on error
    \note
*/
int32_t rtp_deinit();


/*!
    \brief Process the RTCP packet and return the result

    \param[in]  p_input     : Pointer to buffer containing the RTCP packet
    \param[in]  length      : length of the input data
    \param[out] p_out_data  : Pointer to structure for storing the relevant RR
                              packet information

    \return     data size in output buffer, negative error-code on error

    \note
*/
int32_t rtp_process_rtcp_pkt(uint8_t *p_input, uint16_t const length,\
                            rtcp_rr_info_s *const p_out_data);

/*!
    \brief Create the RTP for video p_rtp_profile

    \param[in]  p_data          : Pointer to data to be send
    \param[in]  data_len        : Size of the data to be send
    \param[out] p_out_buffer    : Pointer to buffer containing the output RTP
                                  data
    \param[in]  out_buffer_len  : Size of the output buffer
    \param[in]  p_rtp_profile         : Pointer to structure containing the information required
                                  to create RTP packet

    \return     Size of data in output buffer, negative error-code on error

    \note
*/
int32_t
rtp_encode(uint8_t *p_data, uint16_t const data_len, uint8_t *const p_out_buffer,\
           uint16_t const out_buffer_len, rtp_profile_s *p_rtp_profile);

/*!
    \brief Create the sender report packet

    \param[out] p_input     :   Pointer to buffer containing the output SR
    \param[in]  buffer_len  :   Size of the output buffer
    \param[in]  sr_info     :   Pointer to structure containing the information required
                                to create SR packet

    \return     Size of data in output buffer, negative error-code on error

    \note
*/
int32_t 
rtp_decode(int8_t *p_data, uint16_t const packet_len, uint16_t *data_len, rtp_profile_s *p_rtp_profile);


int32_t rtp_create_sr_pkt(uint8_t *const p_input, uint16_t const buffer_len,\
                         rtcp_sr_info_s const *const sr_info);


int32_t rtcp_fill_sr_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile);

int32_t rtcp_fill_sdes_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile);

int32_t rtcp_fill_payload_spec_fb_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile);
        
int32_t rtp_fill_sps_pkt(uint8_t *const p_out_buffer, rtp_profile_s *p_rtp_profile);

int32_t stun_encode_binding(uint8_t *const p_out_buffer);
/*!
     Close the Doxygen group.
     @}
 */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __RTCP_RTP_H__ */
