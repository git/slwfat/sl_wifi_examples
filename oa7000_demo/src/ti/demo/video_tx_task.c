/*
 * Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <unistd.h>
#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/GPIO.h>
#include "ov_sif_if.h"
#include "video_tx_task.h"
#include "sip_rtsp_main_task.h"
//#include "cc_pal_app.h"
#include "app_common.h"
#include "app_config.h"
#include "rtcp_rtp.h"
#include "ti_drivers_config.h"
#include "ti/drivers/timer.h"
#include "mqueue.h"
#include <ti/net/utils/clock_sync.h>

#define VIDEO_RTP_SSRC              (0xF9143545)
#define VIDEO_RTP_SSRC_HTOL         (0x453514F9)

#define PKTS_BW_V_SR_INFO           (128)//128)
#define PKTS_BW_V_RR_INFO           (256)

#define MAX_UDP_PACKET_SIZE         (1280)
#define V_CLOCK_RATE                (90000)

#define _10_POW_9               (1000000000)
#define SYS_CLK_FREQ            (80000000)
#define TIMER_CNTS_PER_SEC      (50)//(50)

#define TIMER_LOAD_VALUE        (SYS_CLK_FREQ/TIMER_CNTS_PER_SEC)
#define TIME_PER_TICK_IN_NSECS  (1/SYS_CLK_FREQ) * (_10_POW_9)
//timestamp expected by RTP: units of 1/90000 sec
//timestamp from OV798: (framenum/framerate)*1000 = 1/1000 sec

extern mqd_t gRtspMainSMQueue;
extern mqd_t gRtspSrvQueue;
extern mqd_t gSipSrvQueue;
extern mqd_t gSipCliQueue;
extern mqd_t gATxSMQueue;
extern mqd_t gVTxSMQueue;

extern rtp_sock_info_s app_av_soc_info[2];

/* 0 - Index for Video */
rtp_sock_info_s *p_v_soc_info = &app_av_soc_info[0];

/* V-Stream Data */
v_data_s app_v_data = { 0 };

static uint8_t tx_buffer[MAX_VIDEO_BUFF_SIZE];

static rtp_profile_s video_rtp_profile = { 0 };

static v_task_msg_s msg_on_v_task_q;

extern pthread_mutex_t frame_lock;

int16_t fd = 0;
int16_t interval_count = 0;
int VideoBuffEmpty = 0;
uint64_t timestamp_counter = 0;
Timer_Handle timer0;
/**/
void timerCallback(Timer_Handle handle, int_fast16_t status)
{
    timestamp_counter++;
}
/**/
uint32_t cc_get_timestamp()
{
    return timestamp_counter;
}

/**/
void cc_set_timestamp(uint32_t val)
{
    timestamp_counter = val;
}
/**/
void cc_start_timestamp_cnt()
{
    Timer_Params params;
    /*
     * Setting up the timer in continuous callback mode that calls the callback
     * function every 1,000,000 microseconds, or 1 second.
     */
    Timer_Params_init(&params);
    params.period = 20000;
    params.periodUnits = Timer_PERIOD_US;
    params.timerMode = Timer_CONTINUOUS_CALLBACK;
    params.timerCallback = timerCallback;

    if (timer0 == NULL)
    {
        timer0 = Timer_open(CONFIG_TIMER_0, &params);

        if (timer0 == NULL)
        {
            /* Failed to initialized timer */
            while (1)
            {
            }
        }

        if (Timer_start(timer0) == Timer_STATUS_ERROR)
        {
            /* Failed to start timer */
            while (1)
            {
            }
        }
    }
    else
    {
        Report("Timer Already Opened!");
    }

    return;
}

/**/
void cc_stop_timestamp_cnt()
{
    /* Disable the GPT */
    Timer_stop(timer0);
    timestamp_counter = 0;
}

void EncodeFramePart(ov_vstream_info_s *p_sdp_out, int first_frame)
{

    static uint32_t pkt_size = 0;
    static uint16_t bytes_to_tx = 0;
//    static uint32_t timestamp_offset = 0;
    static uint32_t remaining_frame_bytes = 0;
    static uint32_t total_bytes_to_tx = 0;
    //static uint32_t          prev_frame_num      = 0xFFFFFFFF;

    int32_t ret_val = 0;
    int32_t bytes_to_send = 0;

    // interval_count++;
    if (first_frame)
    {
        interval_count = 0;
        video_rtp_profile.seq_num = 0x34;
        video_rtp_profile.pkt_cnt = 0;
        video_rtp_profile.octet_cnt = 0;
        video_rtp_profile.timestamp = p_sdp_out->v_frame_timestamp * 90; // - timestamp_offset ;
        //timestamp_offset = cc_get_timestamp() * (V_CLOCK_RATE/TIMER_CNTS_PER_SEC);
        //video_rtp_profile.timestamp = cc_get_timestamp() * (V_CLOCK_RATE/TIMER_CNTS_PER_SEC) - timestamp_offset;

        //prev_frame_num      = 0xFFFFFFFF;

        //Time stamp counts 'TIMER_CNTS_PER_SEC' times in one second. Therefore, timestamp = counter * (V_CLOCK_RATE/TIMER_CNTS_PER_SEC)
//        timestamp_offset = (cc_get_timestamp() * V_CLOCK_RATE)
//                / TIMER_CNTS_PER_SEC;
        //timestamp_offset = p_sdp_out->v_frame_timestamp;

        bytes_to_send = rtp_fill_sps_pkt(tx_buffer, &video_rtp_profile);

        ret_val = sl_SendTo(p_v_soc_info->rtp_sock_id, tx_buffer, bytes_to_send,
                            0, (const SlSockAddr_t*) &(p_v_soc_info->rtp_addr),
                            p_v_soc_info->addr_size);
        if (ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
        {
            ERR_PRINT(ret_val);
            return;
        }
    }

//    /* Send SR-Info every 'n' packets over the RTCP socket */
//    if (0 == (video_rtp_profile.pkt_cnt % PKTS_BW_V_SR_INFO))
//    {
//        //video_rtp_profile.timestamp = p_sdp_out->v_frame_timestamp; // - timestamp_offset ;
//        video_rtp_profile.timestamp = cc_get_timestamp() * (V_CLOCK_RATE/TIMER_CNTS_PER_SEC) - timestamp_offset;
//        bytes_to_send = rtcp_fill_sr_pkt(tx_buffer, &video_rtp_profile);
//        ret_val = sl_SendTo(p_v_soc_info->rtcp_sock_id, tx_buffer,
//                            bytes_to_send, 0,
//                            (const SlSockAddr_t*) &(p_v_soc_info->rtcp_addr),
//                            p_v_soc_info->addr_size);
//
////        bytes_to_send = rtcp_fill_sdes_pkt(tx_buffer,
////                                            &video_rtp_profile);
////        ret_val = sl_SendTo(p_v_soc_info->rtcp_sock_id, tx_buffer,
////                            bytes_to_send, 0,
////                            (const SlSockAddr_t*) &(p_v_soc_info->rtcp_addr),
////                            p_v_soc_info->addr_size);
////
////        bytes_to_send = rtcp_fill_payload_spec_fb_pkt(
////                tx_buffer, &video_rtp_profile);
////        ret_val = sl_SendTo(p_v_soc_info->rtcp_sock_id, tx_buffer,
////                            bytes_to_send, 0,
////                            (const SlSockAddr_t*) &(p_v_soc_info->rtcp_addr),
////                            p_v_soc_info->addr_size);
////        if (ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
////        {
////            ERR_PRINT(ret_val);
////            return;
////        }
//    }

    if (p_sdp_out->v_frame_type & 0x01) //(p_sdp_out->v_frame_number != prev_frame_num)//New frame
    {
        //Report("New Frame");
        video_rtp_profile.p_data.vProfile.is_first_frame_frag = 1;
        //video_rtp_profile.timestamp = (cc_get_timestamp() * V_CLOCK_RATE)/TIMER_CNTS_PER_SEC - timestamp_offset;
        //video_rtp_profile.timestamp = p_sdp_out->v_frame_timestamp * 90; // - timestamp_offset ;

        video_rtp_profile.p_data.vProfile.frame_type = p_sdp_out->v_frame_type;
        remaining_frame_bytes = p_sdp_out->v_stream_size;
    }
    else
    {
        video_rtp_profile.p_data.vProfile.is_first_frame_frag = 0;
    }

    video_rtp_profile.p_data.vProfile.is_last_frame_frag = 0;
    video_rtp_profile.timestamp = p_sdp_out->v_frame_timestamp * 90; // - timestamp_offset ;
   // video_rtp_profile.timestamp = cc_get_timestamp() * (V_CLOCK_RATE/TIMER_CNTS_PER_SEC) - timestamp_offset;
    //video_rtp_profile.seq_num = p_sdp_out->
    //prev_frame_num = p_sdp_out->v_frame_number;
    /*
     uint32_t i;
     uint32_t metafind;
     int8_t *meta_s = "OVMS";
     uint8_t meta_len = 4;
     metafind=0;//find the metadata

     if(app_v_data.rx_pkt_size>150)
     i=app_v_data.rx_pkt_size-150;
     else
     i=0;

     while(i<total_bytes_to_tx){
     if(app_v_data.v_data_buffer[i]==meta_s[metafind]){
     metafind++;
     if(metafind==meta_len) {//match
     app_v_data.rx_pkt_size=i-meta_len;//cut off the metadata
     break;
     }
     }
     else metafind=0;
     i++;
     }

     */

    total_bytes_to_tx = app_v_data.rx_pkt_size; // This is the size of data in the buffer pulled from the encoder
    while (total_bytes_to_tx > 0)
    {
    //    pkt_size = VIDEO_RECV_SIZE;
        /* Fragment the packet and send it over UDP */
        if (total_bytes_to_tx > MAX_UDP_PACKET_SIZE)
            pkt_size = MAX_UDP_PACKET_SIZE;
        else
            pkt_size = total_bytes_to_tx;

        if ((remaining_frame_bytes - pkt_size) <= 0)
            video_rtp_profile.p_data.vProfile.is_last_frame_frag = 1;
        //pkt_size = VIDEO_RECV_SIZE;
        /* Create RTP packet */
        memset(tx_buffer, 0x00, sizeof(tx_buffer));
        ret_val = rtp_encode(
                &app_v_data.v_data_buffer[app_v_data.rx_pkt_size
                        - total_bytes_to_tx],
                pkt_size, &tx_buffer[0],
                MAX_VIDEO_BUFF_SIZE,
                &video_rtp_profile);
        if (ret_val < 0)
        {
            ERR_PRINT(ret_val);
        }
        else
        {
            bytes_to_tx = ret_val;

            ret_val = sl_SendTo(p_v_soc_info->rtp_sock_id, tx_buffer,
                                bytes_to_tx, 0,
                                (const SlSockAddr_t*) &(p_v_soc_info->rtp_addr),
                                p_v_soc_info->addr_size);
            if (ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
            {
                ERR_PRINT(ret_val);
            }
            else
            {
              //  usleep(1000);
              //  Report("VideoTimestamp = %d || %d = CC_Gettimestamp \r\n",video_rtp_profile.timestamp,cc_get_timestamp());
                video_rtp_profile.pkt_cnt += 1;
                video_rtp_profile.seq_num += 1;
                video_rtp_profile.octet_cnt += pkt_size; //(bytes_to_tx - sizeof(rtp_header_s));
            }
        }

        video_rtp_profile.p_data.vProfile.is_first_frame_frag = 0;
        video_rtp_profile.p_data.vProfile.is_last_frame_frag = 0;

        total_bytes_to_tx -= pkt_size;
        remaining_frame_bytes -= pkt_size;
    }

#ifdef RX_RR_TX_SR
    memset(&sr_info, 0, sizeof(sr_info));
    sr_info.ssrc = VIDEO_RTP_SSRC;

    /*!
     */
    memset(tx_buffer, 0x00, sizeof(tx_buffer));
    addr_size   = sizeof(SlSockAddrIn_t);
    ret_val     = sl_RecvFrom(p_v_soc_info->rtcp_sock_id,       \
                              tx_buffer, MAX_VIDEO_BUFF_SIZE,   \
                              0, (SlSockAddr_t *)&client_addr,  \
                              (SlSocklen_t *)&addr_size);
    if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
    {
        /* Rx Error */
    }
    else
    {
        rtp_process_rtcp_pkt(tx_buffer, ret_val, &rr_info);
    }

    /* Send SR-Info every 'n' packets over the RTCP socket */

    if(0 == (pkt_cnt % PKTS_BW_V_SR_INFO))
    {
        /* Time to send sender Report */
        sr_info.pkt_count           = video_rtp_profile.pkt_cnt;
        sr_info.octet_count         = video_rtp_profile.octet_cnt;

        sr_info.rtp_timestamp       = cc_get_timestamp() * \
                                        (V_CLOCK_RATE/TIMER_CNTS_PER_SEC);

        sr_info.ntp_timestamp.secs  = (cc_get_timestamp()/TIMER_CNTS_PER_SEC) + \
                                        (CUR_UTC_TIME);

        sr_info.ntp_timestamp.nsec  = (uint32_t) \
                                      (cc_get_timestamp() % TIMER_CNTS_PER_SEC) * \
                                        (TIMER_LOAD_VALUE * TIME_PER_TICK_IN_NSECS);

        memset(tx_buffer, 0x00, sizeof(tx_buffer));
        ret_val = rtp_create_sr_pkt(tx_buffer, sizeof(tx_buffer), &sr_info);
        if(ret_val < 0)
        {
            ERR_PRINT(ret_val);
        }
        else
        {
            bytes_to_tx = ret_val;
            ret_val = sl_SendTo(p_v_soc_info->rtcp_sock_id,\
                                tx_buffer, bytes_to_tx, 0,\
                                (const SlSockAddr_t *)&(p_v_soc_info->rtcp_addr),\
                                p_v_soc_info->addr_size);
            if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
            {
                ERR_PRINT(ret_val);
            }
        }
    }
    #endif /* RX_RR_TX_SR */

}

/*!
 */
void* VTxTask(void *p_args)
{
    ov_vstream_info_s *p_next_stream_info = NULL;

    SlSockAddrIn_t client_addr = { 0 };
    static int first_frame = 0;

    uint8_t v_streaming = 0;
    int32_t status = -1;

    memset(&msg_on_v_task_q, 0, sizeof(msg_on_v_task_q));
    memset(&video_rtp_profile, 0, sizeof(video_rtp_profile));
    memset(&client_addr, 0, sizeof(client_addr));

    p_next_stream_info = &app_v_data.vstream_info_next;

    /** Dynamic profile number for video media as listed
     * in 'sdp_out_template'
     */
    video_rtp_profile.type = VIDEO;
    video_rtp_profile.payload_type = 96;
    video_rtp_profile.payload_format = H264_AVC;

    /* SSRC - A Random Number */
    //This allows the stream to be uniquely identified by other streams.
    video_rtp_profile.ssrc = VIDEO_RTP_SSRC;

    while (1)
    {
        mq_receive(gVTxSMQueue, (char*) &msg_on_v_task_q, sizeof(v_task_msg_s),
        NULL);

        switch (msg_on_v_task_q.msg_id)
        {

        case START_V_STREAMING:

            if (!v_streaming)
            {
                //Setup Wifi
                sl_WlanPolicySet(SL_WLAN_POLICY_PM , SL_WLAN_ALWAYS_ON_POLICY, NULL,0);

                GPIO_write(LED_B,1);
                v_streaming = 1;

                ov_v_resolution_e resolution;
                uint8_t enable = 1;
                Report("[MSG] V_ENABLE ...\r\n");
                memcpy((void*) &resolution, msg_on_v_task_q.p_data,
                       sizeof(ov_v_resolution_e));
                status = ov_config_v_encoder(fd, resolution, enable);
                if (status < 0)
                    ERR_PRINT(status);
                else
                {
                    Report("[MSG] V_ENABLE OK\r\n");
                }

                first_frame = 1;
                msg_on_v_task_q.task_id = VIDEO_TASK_ID;
                msg_on_v_task_q.msg_id = V_GET_INFO;
                msg_on_v_task_q.p_data = (void*) p_next_stream_info;
                mq_send(gVTxSMQueue, (char*) &msg_on_v_task_q,
                        sizeof(v_task_msg_s), 0);
            }
            else
            {
                Report("[Error] - Video Streaming Already Started\r\n");
            }
            break;

        case STOP_V_STREAMING:

            if (v_streaming)
            {
                sl_WlanPolicySet(SL_WLAN_POLICY_PM , SL_WLAN_NORMAL_POLICY, NULL,0);

                GPIO_write(LED_B,0);
                v_streaming = 0;

                ov_v_resolution_e resolution;
                uint8_t enable = 0;

                memcpy((void*) &resolution, msg_on_v_task_q.p_data,
                       sizeof(ov_v_resolution_e));
                status = ov_config_v_encoder(fd, resolution, enable);
                if (status < 0)
                    ERR_PRINT(status);
                else
                {
                    Report("V_DISABLE OK\r\n");
                    cc_stop_timestamp_cnt();
                }
            }
            else
            {
                Report("[Error] - Video Streaming Already Stopped\r\n");
            }

            break;
        case INIT_OV798:
        {
            Report("[MSG] INIT_OV798 ...\r\n");

            status = ov_init();
            if (status < 0)
                ERR_PRINT(status);
            else
            {
                Report("[MSG] INIT_OV798 OK\r\n");
            }
            fd = status;

        }
            break;

        case V_CONFIG_SENSOR:
        {
            ov_v_config_s v_config_data;

            memcpy((void*) &v_config_data, msg_on_v_task_q.p_data,
                   sizeof(ov_v_config_s));

//            status = ov_config_v_sensor(fd, v_config_data);
            status = 0;
            if (status < 0)
                ERR_PRINT(status);
            else
            {

                Report("V_CONFIG_SENSOR OK\r\n");
            }
        }
            break;

        case V_SET_FRAME_RATE:
        {
            ov_v_framerate_e frame_rate;

            memcpy((void*) &frame_rate, msg_on_v_task_q.p_data,
                   sizeof(ov_v_framerate_e));

            status = ov_set_frame_rate(fd, frame_rate);
            if (status < 0)
                ERR_PRINT(status);
            else
            {
                Report("V_SET_FRAME_RATE OK\r\n");
            }
        }
            break;

        case V_SET_BITRATE:
        {
            ov_v_bitrate_e bit_rate;

            memcpy((void*) &bit_rate, msg_on_v_task_q.p_data,
                   sizeof(ov_v_bitrate_e));

            status = ov_set_bit_rate(fd, bit_rate);
            if (status < 0)
                ERR_PRINT(status);
            else
            {
                Report("V_SET_BITRATE OK\r\n");
            }
        }
            break;

        case V_SET_IFRAME_INTERVAL:
        {
            uint8_t interval;

            memcpy((void*) &interval, msg_on_v_task_q.p_data, sizeof(uint8_t));
            Report("Setting IFrame Interval: %d\r\n", interval);
            status = ov_set_iframe_interval(fd, interval);
            if (status < 0)
                ERR_PRINT(status);
            else
            {
                Report("V_SET_IFRAME_INTERVAL OK\r\n");
            }
        }
            break;

            /*
             case V_DISABLE_OK:
             //send_v_stop_status();

             app_rtsp_task_msg_s msgData;
             msgData.task_id = VIDEO_TASK_ID;
             msgData.msg_id = RTSP_V_STOPPED;
             mq_send(gRtspMainSMQueue, (char *)&msgData, sizeof(app_rtsp_task_msg_s), 0);

             break;*/
        case V_GET_INFO:
            if (v_streaming)
            {

                ov_vstream_info_s *p_sdp_out = NULL;
                p_sdp_out = (ov_vstream_info_s*) msg_on_v_task_q.p_data;
                status = ov_get_vstream_info(fd, VIDEO_RECV_SIZE, p_sdp_out);
             //    Report("nxt_pkt_size: %d\r\n", p_next_stream_info->v_nxt_pkt_size);

                if (p_next_stream_info->v_nxt_pkt_size == 0)
                {
                    msg_on_v_task_q.task_id = VIDEO_TASK_ID;
                    msg_on_v_task_q.msg_id = V_GET_INFO;
                    msg_on_v_task_q.p_data = (void*) p_next_stream_info;
                    mq_send(gVTxSMQueue, (char*) &msg_on_v_task_q,
                            sizeof(v_task_msg_s), 0);
                   // usleep(100);
                }
                else
                {
                    /* Get the data from the OV798 */
                    memset(&app_v_data.v_data_buffer[0], 0x00,
                           sizeof(app_v_data.v_data_buffer));
                    msg_on_v_task_q.task_id = VIDEO_TASK_ID;
                    msg_on_v_task_q.msg_id = V_GET_DATA;
                    mq_send(gVTxSMQueue, (char*) &msg_on_v_task_q,
                            sizeof(v_task_msg_s), 0);
                }
            }
            break;
        case V_GET_DATA:
            ///////////////////////////////////////////

            if (app_v_data.vstream_info_next.v_nxt_pkt_size > VIDEO_RECV_SIZE)
            {
                app_v_data.rx_pkt_size = VIDEO_RECV_SIZE;
                VideoBuffEmpty = 0;
            }
            else
            {
                app_v_data.rx_pkt_size =
                        app_v_data.vstream_info_next.v_nxt_pkt_size;
                VideoBuffEmpty = 1;
            }

            status = ov_get_vstream_data(fd, app_v_data.rx_pkt_size,
                                         app_v_data.v_data_buffer);
            if (status < 0)
                ERR_PRINT(status);

            //Report("Frame Recieved Size of: %d\r\n", app_v_data.rx_pkt_size);
            //Encode new data packet
            EncodeFramePart(p_next_stream_info, first_frame);
            first_frame = 0;
            msg_on_v_task_q.task_id = VIDEO_TASK_ID;
            msg_on_v_task_q.msg_id = V_GET_INFO;
            msg_on_v_task_q.p_data = (void*) p_next_stream_info;
            mq_send(gVTxSMQueue, (char*) &msg_on_v_task_q, sizeof(v_task_msg_s),
                    0);
            break;

        default:
            if (v_streaming)
            {

                ov_vstream_info_s *p_sdp_out = NULL;
                p_sdp_out = (ov_vstream_info_s*) msg_on_v_task_q.p_data;
                status = ov_get_vstream_info(fd, VIDEO_RECV_SIZE, p_sdp_out);

                if (p_next_stream_info->v_nxt_pkt_size == 0)
                {
                    msg_on_v_task_q.task_id = VIDEO_TASK_ID;
                    msg_on_v_task_q.msg_id = V_GET_INFO;
                    msg_on_v_task_q.p_data = (void*) p_next_stream_info;
                    mq_send(gVTxSMQueue, (char*) &msg_on_v_task_q,
                            sizeof(v_task_msg_s), 0);
                    usleep(100);
                }
                else
                {
                    /* Get the data from the OV798 */
                    memset(&app_v_data.v_data_buffer[0], 0x00,
                           sizeof(app_v_data.v_data_buffer));
                    msg_on_v_task_q.task_id = VIDEO_TASK_ID;
                    msg_on_v_task_q.msg_id = V_GET_DATA;
                    mq_send(gVTxSMQueue, (char*) &msg_on_v_task_q,
                            sizeof(v_task_msg_s), 0);
                }
            }
            else
            {
                Report("[Error] - No Case for Default\r\n");
            }
            break;

        }
        /* Status message is processed below */
        //
    } //while(1)
}

