/*
 * Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <ti/drivers/net/wifi/simplelink.h>

#include "sip_rtsp_main_task.h"
#include "video_tx_task.h"
#include "audio_tx_task.h"
#include "provisioning.h"
//#include "cc_pal_app.h"
#include "app_common.h"
#include "app_config.h"
#include "rtcp_rtp.h"
#include "sip_rtsp.h"
#include <pthread.h>
#include <time.h>
   
#include <errno.h>
#include "mqueue.h"

#define APPLICATION_VERSION "1.0.0"

#define SIP_SERVER_IP        0xC0A80204//Not network order//192.168.2.4
#define MULTICAST_IP        (0xE00000FB)
#define RTSP_PORT           (554)
#define SIP_PORT            (5060)
#define RECV_BUFF_SIZE      (1500)  /* Bytes */
#define SEND_BUFF_SIZE      (1500)  /* Bytes */
#define MAX_SESS_RX_MESSAGE_SIZE (3000)
#define SESS_RECV_TIMEOUT   (10)    /* Msecs */

/*!
 */
extern void lp3p0_setup_power_policy(int power_policy);

extern mqd_t    gRtspMainSMQueue;
extern mqd_t    gRtspSrvQueue;
extern mqd_t    gSipSrvQueue;
extern mqd_t    gSipCliQueue;
extern mqd_t    gATxSMQueue;
extern mqd_t    gARxSMQueue;
extern mqd_t    gVTxSMQueue;
extern mqd_t    gProvisioningSMQueue;

extern pthread_cond_t net_conn_cond;
extern pthread_cond_t net_disconn_cond;

extern void SimpleLinkInitCallback(uint32_t status, SlDeviceInitInfo_t *DeviceInitInfo);

/*!
 * RTSP Module configuration
 */
rtsp_session_config_s rtsp_session_config =
{
    {
        {
            6970,           /* Server RTP port for media-1 */
            6971            /* Server RTCP port for media-1 */
        },
        {
            6974,           /* Server RTP port  for media-2 */
            6975            /* Server RTCP port for media-2 */
        }
    },
    1000,                   /* Session timeout */
    0x43585430,             /* Session number */
    0,                      /* Time stamp will be updated by application */
    "224.0.0.251",          /* Preferred Multicast IP */
    CUR_DATE,                /* Date */
    "",
    ""
      
};

/*!
 * RTSP Module configuration
 */
rtsp_session_config_s sip_session_config =
{
    {
        {
            9078,           /* Server RTP port for media-1 */
            9079            /* Server RTCP port for media-1 */
        },
        {
            7076,           /* Server RTP port  for media-2 */
            7077            /* Server RTCP port for media-2 */
        }
    },
    1000,                   /* Session timeout */
    0x43585430,             /* Session number */
    0,                      /* Time stamp will be updated by application */
    "224.0.0.251",          /* Preferred Multicast IP */
    CUR_DATE,                /* Date */
    "",
    ""
      
};

extern int curr_state;
int ipaq=0;

int invited=0;


rtp_sock_info_s         app_av_soc_info[NUM_OF_MEDIA_STREAMS];

static SlSockAddrIn_t   client_addr = {0};

uint8_t                   local_ip_str[17];
uint8_t                   peer_ip_str[17];

static int8_t            sip_rtsp_rx_buffer[MAX_SESS_RX_MESSAGE_SIZE+1] = {0};
static int8_t            sip_rtsp_tx_buffer[SEND_BUFF_SIZE+1] = {0};

static int16_t           rtsp_listen_sd = -1;
static int16_t           rtsp_client_sd = -1;
static int16_t           sip_listen_sd = -1;
static int16_t           sip_client_sd = -1;

static ov_v_framerate_e frame_rate;
static uint8_t iframe_interval;
static ov_v_bitrate_e bit_rate;
static ov_v_resolution_e resolution;

/*!
 * OV configuration parameters
 */
ov_v_config_s v_config =
{
 V_disable,
    128,      /* Brightness */
    0x40,      /* Contrast */
    0x40,      /* Saturation */
    0,      /* Flip */
    0,       /* Normal mode (daylight)*/
    1,          /* 2=none *///High dynamic range
    2,               /* Level value 0~4 *///Wide dynamic range
    4,               /* Level value 0~4 *///motion-compensated temporal filtering
    2               /* Level value 0~4 *///sharpness
};

/*!
 * OV Audio Configuration
 */
#ifdef PCMU_LAW
ov_a_config_s a_config =
{
    A_ULAW,
    A_8K
};
#else
ov_a_config_s a_config =
{
    A_PCM,
    A_11K
};
#endif
/*!
 * OV Audio Configuration
 */


/*!
 * Initializes the OV module
 */
static int32_t init_ov798();

static int16_t create_client(SlSockAddr_t *p_srv_addr, session_transport_e transport);
/*!
 * Accepts RTSP client connections
 */
static int16_t accept_client(uint16_t sd);

/*!
 * Listens for RTSP clients
 */
static int16_t listen_for_clients(uint16_t port, session_transport_e transport);

/*!
 * Closes the RTP sockets that were opened for the media streams
 */
static int32_t close_rtp_sockets(rtp_sock_info_s *const p_sock_info);

/*!
 * Opens RTP sockets for a media stream
 */
static int32_t
open_rtp_rtcp_sockets(rtp_sock_info_s        *const p_sock_info,
                 rtsp_session_config_s  *const p_session_config,
                 rtsp_setup_data_s      setup_data, int32_t idx);

/*!
 * Processes data from RTSP clients
 */
static int32_t handle_rtsp_client(app_rtsp_task_msg_s *p_rtsp_msg, session_transport_e transport);

/*!
 * Processes data from SIP clients
 */
static int32_t handle_sip_client(app_rtsp_task_msg_s *p_rtsp_msg, session_transport_e transport);


void * RtspServerTask(void *p_args);
void * SipServerTask(void *p_args);

/*!
 */
void * RtspMainTask(void *p_args)
{
    
    app_rtsp_srv_task_msg_s  msg_on_rtsp_srv_task_q;
    app_sip_srv_task_msg_s  msg_on_sip_srv_task_q;
    app_rtsp_task_msg_s     msg_on_rtsp_task_q;
    v_task_msg_s            msg_on_v_task_q;
    a_task_msg_s            msg_on_a_task_q;

    int32_t                  iRetVal = -1;

    uint8_t randseed;
    uint16_t randseedLen = 1;

    memset(&msg_on_rtsp_srv_task_q, 0, sizeof(msg_on_rtsp_srv_task_q));
    memset(&msg_on_sip_srv_task_q, 0, sizeof(msg_on_sip_srv_task_q));
    memset(&msg_on_rtsp_task_q, 0, sizeof(msg_on_rtsp_task_q));
    memset(&app_av_soc_info, 0, sizeof(app_av_soc_info));
    memset(&msg_on_v_task_q, 0, sizeof(msg_on_v_task_q));
    memset(&msg_on_a_task_q, 0, sizeof(msg_on_a_task_q));

    /*Set NWP state*/
    UnsetNWPActive();
    


    //initial state
    app_set_state(APP_STATE_IDLE);
    
    //Wake by default (change for lower power operation)
    
    msg_on_rtsp_task_q.task_id   = RTSP_TASK_ID;
    msg_on_rtsp_task_q.msg_id    = DB_WAKE_INIT;
    mq_send(gRtspMainSMQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_rtsp_task_msg_s), 0);


    /*!
     * Keep waiting for messages on this queue
     */
    while(mq_receive(gRtspMainSMQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_rtsp_task_msg_s), NULL) >=0 )
    {


        switch(curr_state)
        {
             case APP_STATE_IDLE:
            {
                if(DB_WAKE_INIT == msg_on_rtsp_task_q.msg_id)
                {
                    Report("DB_WAKE_INIT!\r\n");
                    //if(SL_IS_RESTART_REQUIRED){
                    //  sl_Stop(0xFF);
                    //      UnsetNWPActive();
                    //}

//                    if(CheckNWPActive()==-1){
//                        Report("Starting NWP...\r\n");
//                        iRetVal=sl_Start(NULL,NULL,(P_INIT_CALLBACK)SimpleLinkInitCallback);
//                        if (iRetVal == SL_ERROR_RESTORE_IMAGE_COMPLETE)
//                        {
//                            Report("\r\n*Factory Default Complete - RESET the Board*\r\n");
//                            while(1);
//                        }
//                    }
                    Report("Attempting Connection to saved Profile\r\n");
                    sleep(5);
                    if(WaitNetConnected(5) == -2)
                    {
                        char msg;
                        Report("Provisioning begin\r\n");

                        msg=AppEvent_PROVISIONING_STARTED;
                        mq_send(gProvisioningSMQueue, &msg, 1, 0);
                    }
                    //wait forever for connection
                    WaitNetConnected(0);
                    iRetVal = init_ov798();
                    if(iRetVal < 0) { ERR_PRINT(iRetVal); goto exit_app_main_task_entry; }
                    usleep(900000);
                    #ifdef START_AUDIO
                    iRetVal = init_aic3120();
                    if(iRetVal < 0) { ERR_PRINT(iRetVal); goto exit_app_main_task_entry; }
                    #endif

//
//                    while(WaitNWPActive(1000)!=ROLE_STA){
//                        sl_Stop(0xF0);
//                        //UnsetNWPActive();
//                        usleep(100000);
//                        iRetVal=sl_Start(NULL,NULL,NULL);
//                        if(iRetVal !=ROLE_STA) {
//                            Report("sl_Start failed, retrying...\r\n");
//                            usleep(100000);
//                        }
//                        else break;
//                    }
                    Report("NWP started\r\n");

                    sl_NetUtilGet(SL_NETUTIL_TRUE_RANDOM, 0, &randseed, &randseedLen);
                    srand(randseed);
                    app_set_state(APP_STATE_NETWORK_CONNECTING);
                    //Set timer for provisioning
                    
                }
                
            }
            break;

            case APP_STATE_NETWORK_CONNECTING:
            {
                


                //if(timer for provisioning)
                    //try provisioning
              //  }


                if(RTSP_CONNECTED_TO_NW == msg_on_rtsp_task_q.msg_id)
                {
                    

                    rtsp_init(RTSP_PORT);
                    sip_init(SIP_PORT);
                    rtp_rtcp_init();
                    
                    /*
                    pthread_attr_init(&pAttrs_run_sip_client_task);
                    priParam.sched_priority = 1;//4
                    iRetVal = pthread_attr_setschedparam(&pAttrs_run_sip_client_task, &priParam);
                    iRetVal |= pthread_attr_setstacksize(&pAttrs_run_sip_client_task, 2048);
                    iRetVal |= pthread_attr_setdetachstate(&pAttrs_run_sip_client_task,PTHREAD_CREATE_DETACHED);
                    if(iRetVal)
                      while(1);
                    iRetVal = pthread_create(&gRunSipClientTaskThread, &pAttrs_run_sip_client_task, SipClientTask, NULL);
                    if(iRetVal)
                      while(1);
                    app_set_state(APP_STATE_RUNNING_CLIENT);
                    */
                    
                    msg_on_rtsp_srv_task_q.task_id   = RTSP_TASK_ID;
                    msg_on_rtsp_srv_task_q.msg_id    = RTSP_SERVER_START;
                    mq_send(gRtspSrvQueue, (char *)&msg_on_rtsp_srv_task_q, sizeof(app_rtsp_srv_task_msg_s), 0);

                    msg_on_sip_srv_task_q.task_id   = RTSP_TASK_ID;
                    msg_on_sip_srv_task_q.msg_id    = SIP_SERVER_START;
                    mq_send(gSipSrvQueue, (char *)&msg_on_sip_srv_task_q, sizeof(app_sip_srv_task_msg_s), 0);
                    
                    app_set_state(APP_STATE_SERVING);
                }
            }
            break;


            case APP_STATE_SERVING:
            {
                

                if(RTSP_DISCONNECT_FROM_AP == msg_on_rtsp_task_q.msg_id)
                {
                    app_set_state(APP_STATE_NETWORK_CONNECTING);
                    msg_on_rtsp_srv_task_q.task_id   = RTSP_TASK_ID;
                    msg_on_rtsp_srv_task_q.msg_id    = RTSP_SERVER_STOP;
                    mq_send(gRtspSrvQueue, (char *)&msg_on_rtsp_srv_task_q, sizeof(app_rtsp_srv_task_msg_s), 0);
                    msg_on_sip_srv_task_q.task_id   = RTSP_TASK_ID;
                    msg_on_sip_srv_task_q.msg_id    = SIP_SERVER_STOP;
                    mq_send(gSipSrvQueue, (char *)&msg_on_sip_srv_task_q, sizeof(app_sip_srv_task_msg_s), 0);

                }

            }
            break;
            case APP_STATE_RUNNING_CLIENT:
            {
                

                if(RTSP_DISCONNECT_FROM_AP == msg_on_rtsp_task_q.msg_id)
                {
                    app_set_state(APP_STATE_NETWORK_CONNECTING);/*
                    msg_on_rtsp_srv_task_q.task_id   = RTSP_TASK_ID;
                    msg_on_rtsp_srv_task_q.msg_id    = RTSP_SERVER_STOP;
                    mq_send(gRtspSrvQueue, (char *)&msg_on_rtsp_srv_task_q, sizeof(app_rtsp_srv_task_msg_s), 0);
                    msg_on_sip_srv_task_q.task_id   = RTSP_TASK_ID;
                    msg_on_sip_srv_task_q.msg_id    = SIP_SERVER_STOP;
                    mq_send(gSipSrvQueue, (char *)&msg_on_sip_srv_task_q, sizeof(app_sip_srv_task_msg_s), 0);*/

                }

            }
            break;

        } /* switch */
    }/* while */

exit_app_main_task_entry:
    LOOP_FOREVER();
}

static int32_t recv_sip_message(int16_t sock, session_transport_e transport){
          
    int32_t recv_len=0;
    int32_t recv_tot=0;
    int32_t find_offset=0;
    int32_t max_pkt_size=0;
    int16_t ret_val;
    int8_t *find_ret_p;
    
    SlSockAddr_t    *dest_addr;           
    SlSockAddrIn_t destAddr;
    dest_addr = (SlSockAddr_t*)&destAddr;
    
    uint16_t dest_addr_len=sizeof(SlSockAddrIn_t);

    while(1){
        

        if(recv_tot+RECV_BUFF_SIZE<=MAX_SESS_RX_MESSAGE_SIZE)
            max_pkt_size=RECV_BUFF_SIZE;
        else
            max_pkt_size=MAX_SESS_RX_MESSAGE_SIZE-recv_tot;
        
        if(transport==TCP_TRANSPORT){
            ret_val = sl_Recv(sock, &sip_rtsp_rx_buffer[recv_tot], max_pkt_size, 0);
        }
        if(transport==UDP_TRANSPORT){
            ret_val = sl_RecvFrom(sock, &sip_rtsp_rx_buffer[recv_tot], max_pkt_size, 0, dest_addr, (SlSocklen_t*)&dest_addr_len);
        }
        if(ret_val >0){
            recv_len = ret_val;
            recv_tot+=recv_len;
                    //search for end of message "\r\n\r\n"
            if(recv_tot-recv_len>=4) 
                find_offset = recv_tot-recv_len-4;
            else find_offset=0;
            sip_rtsp_rx_buffer[find_offset+recv_len]=0;
            find_ret_p=strstr((const char *)&sip_rtsp_rx_buffer[find_offset],"\r\n\r\n");
            if (find_ret_p!=NULL) 
              break;
            if(recv_tot>=MAX_SESS_RX_MESSAGE_SIZE) 
              break;
        }
        else {
          if(ret_val != SL_ERROR_BSD_EAGAIN)
              Report("RX ret %i\r\n",ret_val);
          break;
        }

    }
    return ret_val;
  
}


/**/
static int32_t handle_rtsp_client(app_rtsp_task_msg_s *p_rtsp_msg, session_transport_e transport)
{
    rtsp_session_config_s   *p_session_config = NULL;
    rtsp_pkt_info_s         rtsp_pkt_info;

    int32_t      ret_val         = -1;
    int16_t      bytes_to_tx     = -1;

    

    int8_t *find_ret_p;

    int recv_len;
    int recv_tot;
    int max_pkt_size;
    int find_offset;
    
    a_task_msg_s msg_on_a_task_q;
    v_task_msg_s msg_on_v_task_q;
    
    SlSockAddr_t    *dest_addr;           
    SlSockAddrIn_t destAddr;
    dest_addr = (SlSockAddr_t*)&destAddr;
    
    uint16_t dest_addr_len=sizeof(SlSockAddrIn_t);
        
    while(1){
      
    memset(sip_rtsp_rx_buffer, 0, MAX_SESS_RX_MESSAGE_SIZE);
    memset(&rtsp_pkt_info, 0, sizeof(rtsp_pkt_info));
    recv_len=0;
    recv_tot=0;

        while(1){
            

            if(recv_tot+RECV_BUFF_SIZE<=MAX_SESS_RX_MESSAGE_SIZE)
                max_pkt_size=RECV_BUFF_SIZE;
            else
                max_pkt_size=MAX_SESS_RX_MESSAGE_SIZE-recv_tot;
            if(transport==TCP_TRANSPORT){
                ret_val = sl_Recv(rtsp_client_sd, &sip_rtsp_rx_buffer[recv_tot], max_pkt_size, 0);
            }
            if(transport==UDP_TRANSPORT){
                ret_val = sl_RecvFrom(rtsp_client_sd, &sip_rtsp_rx_buffer[recv_tot], max_pkt_size, 0, dest_addr, (SlSocklen_t*)&dest_addr_len);
            }
            if(ret_val >0){
                recv_len = ret_val;
                recv_tot+=recv_len;
                        //search for end of message "\r\n\r\n"
                if(recv_tot-recv_len>=4) 
                    find_offset = recv_tot-recv_len-4;
                else find_offset=0;
                sip_rtsp_rx_buffer[find_offset+recv_len]=0;

                find_ret_p=(int8_t *)strstr((const char *)&sip_rtsp_rx_buffer[find_offset],"\r\n\r\n");
                //Report("RX Len %i\r\n",recv_len);
                if (find_ret_p!=NULL) 
                  break;
                if(recv_tot>=MAX_SESS_RX_MESSAGE_SIZE) 
                  break;
            }
            else {
              if(ret_val != SL_ERROR_BSD_EAGAIN)
                  Report("RX ret %i\r\n",ret_val);
              break;
            }

        }
        
        if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
        {
            Report("RTSP client disconnected\r\n");

            close_rtp_sockets(&app_av_soc_info[0]);
            close_rtp_sockets(&app_av_soc_info[1]);
            
            sl_Close(rtsp_client_sd);
            rtsp_client_sd = -1;
            
            return 0;
        }
        else if (SL_ERROR_BSD_EAGAIN == ret_val)
        {
            /* No data - Ignore */
        }
        else
        {
            p_session_config = &rtsp_session_config;

            /* Parse data */
            p_session_config->timestamp = rand();

            memset(sip_rtsp_tx_buffer, 0, sizeof(sip_rtsp_tx_buffer));
            ret_val = rtsp_packet_parser(sip_rtsp_rx_buffer, p_session_config, SEND_BUFF_SIZE, sip_rtsp_tx_buffer, &rtsp_pkt_info);
            if(ret_val < 0)
            {
                ERR_PRINT(ret_val);
            }
            else
            {


                if(SETUP == rtsp_pkt_info.pkt_type)
                {
                    /* Setup request */
                    ret_val = setup_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &rtsp_pkt_info);
                    
                    bytes_to_tx = ret_val;
                    ret_val     = sl_Send(rtsp_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                    if(ret_val <= 0)
                    {
                        ERR_PRINT(ret_val);
                        return 0;
                    }
                    ret_val = \
                    open_rtp_rtcp_sockets(&app_av_soc_info[rtsp_pkt_info.setup_num],\
                                     p_session_config,                           \
                                     rtsp_pkt_info.pkt_response.setup_data[(rtsp_pkt_info.setup_num)],\
                                     rtsp_pkt_info.setup_num);
                    if(ret_val < 0)
                    {
                        ERR_PRINT(ret_val);
                        return 0;
                    }
                }
                else if(PLAY == rtsp_pkt_info.pkt_type)
                {
                    /* Start Streaming */
                    ret_val = play_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &rtsp_pkt_info);
                    bytes_to_tx = ret_val;
                    ret_val     = sl_Send(rtsp_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                    if(ret_val <= 0)
                    {
                        ERR_PRINT(ret_val);
                        return 0;
                    }
                    cc_start_timestamp_cnt();

                    if(!(app_av_soc_info[0].rtp_sock_id < 0))
                    {
                        msg_on_v_task_q.task_id = RTSP_TASK_ID;
                        msg_on_v_task_q.msg_id = START_V_STREAMING;
                        msg_on_v_task_q.p_data     = (void *)&resolution;
                        mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);
                    }

                    if(!(app_av_soc_info[1].rtp_sock_id < 0))
                    {
                        msg_on_a_task_q.task_id = RTSP_TASK_ID;
                        msg_on_a_task_q.msg_id = START_A_STREAMING;
                        mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                    }

                    
                }
                else if(TEARDOWN == rtsp_pkt_info.pkt_type)
                {
                    /* Client Disconnected */
                    //ret_val = teardown_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &rtsp_pkt_info);
                    //bytes_to_tx = ret_val;
                    //ret_val     = sl_Send(rtsp_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);


                    msg_on_a_task_q.task_id     = RTSP_TASK_ID;
                    msg_on_a_task_q.msg_id      = STOP_A_STREAMING;
                    mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                    mq_send(gARxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                    
                    msg_on_v_task_q.task_id     = RTSP_TASK_ID;
                    msg_on_v_task_q.msg_id      = STOP_V_STREAMING;
                    mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

                    close_rtp_sockets(&app_av_soc_info[0]);
                    close_rtp_sockets(&app_av_soc_info[1]);
                    
                    return 0;
                }
                else if(RTSP_OPTIONS ==rtsp_pkt_info.pkt_type){
                  
                    ret_val = options_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &rtsp_pkt_info);
                    bytes_to_tx = ret_val;
                    ret_val     = sl_Send(rtsp_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                    if(ret_val <= 0)
                    {
                        ERR_PRINT(ret_val);
                        return 0;
                    }
                }
                else if(DESCRIBE ==rtsp_pkt_info.pkt_type){
                  
                    ret_val = describe_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &rtsp_pkt_info);
                    bytes_to_tx = ret_val;
                    ret_val     = sl_Send(rtsp_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                    if(ret_val <= 0)
                    {
                        ERR_PRINT(ret_val);
                        return 0;
                    }
                }
                else{
                    //ret_val = unknown_tag(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, rtsp_pkt_info);
                }
                
            }

            memset(sip_rtsp_rx_buffer, 0, MAX_SESS_RX_MESSAGE_SIZE);
        }
    }
}

/**/
static int32_t handle_sip_client(app_rtsp_task_msg_s *p_sip_msg, session_transport_e transport)
{
    rtsp_session_config_s   *p_session_config = NULL;
    sip_pkt_info_s         sip_pkt_info;

    int32_t      ret_val         = -1;
    uint16_t      bytes_to_rx     = 0;
    int16_t      bytes_to_tx     = -1;

    
    a_task_msg_s msg_on_a_task_q;
    v_task_msg_s msg_on_v_task_q;
    
    SlSockAddr_t    *dest_addr;           
    SlSockAddrIn_t destAddr;
    dest_addr = (SlSockAddr_t*)&destAddr;
    
    uint16_t dest_addr_len=sizeof(SlSockAddrIn_t);
    
    while(1){
        memset(sip_rtsp_rx_buffer, 0, MAX_SESS_RX_MESSAGE_SIZE);
        memset(&sip_pkt_info, 0, sizeof(sip_pkt_info));


        again:

        if(transport==TCP_TRANSPORT){
            ret_val = sl_Recv(sip_client_sd, sip_rtsp_rx_buffer, MAX_SESS_RX_MESSAGE_SIZE, 0);
        }
        if(transport==UDP_TRANSPORT){
            ret_val = sl_RecvFrom(sip_client_sd, sip_rtsp_rx_buffer, MAX_SESS_RX_MESSAGE_SIZE, 0, dest_addr, (SlSocklen_t*)&dest_addr_len);
        }
        if(!memcmp(sip_rtsp_rx_buffer,"\r\n\r\n",4)) goto again;
        
        
        if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
        {
            Report("SIP client disconnected\r\n");


            /* If RTP/RTCP sockets are open, close them */

            close_rtp_sockets(&app_av_soc_info[0]);
            close_rtp_sockets(&app_av_soc_info[1]);
            sl_Close(sip_client_sd);
            sip_client_sd = -1;
            
            return 0;
        }
        else if (SL_ERROR_BSD_EAGAIN == ret_val)
        {
            /* No data - Ignore */
        }
        else
        {
            p_session_config = &sip_session_config;

            /* Parse data */
            bytes_to_rx                 = ret_val;
            p_session_config->timestamp = rand();
            sip_rtsp_rx_buffer[bytes_to_rx]=0;//null term the string
            
            
            ret_val = sip_req_packet_parser(sip_rtsp_rx_buffer, \
                                         p_session_config, SEND_BUFF_SIZE,\
                                         sip_rtsp_tx_buffer, &sip_pkt_info);
            if(ret_val == 0)
            {
                switch(sip_pkt_info.pkt_type){
                case SIP_INVITE:
                      
                    invite_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);

                    if(!invited){
                      
                        invited=1;
                        //SlSockAddr_t    *dest_addr;           
                        //SlSockAddrIn_t destAddr;
                        //dest_addr = (SlSockAddr_t*)&destAddr;
                        client_addr.sin_addr.s_addr = destAddr.sin_addr.s_addr;

                        
                        //memset(sip_rtsp_tx_buffer, 0, sizeof(sip_rtsp_tx_buffer));
                        ret_val = invite_trying_format(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
                        bytes_to_tx = ret_val;
                        Report("[->] %s\n\r", sip_rtsp_tx_buffer);
                        if(transport==TCP_TRANSPORT){
                            ret_val     = sl_Send(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                        }
                        if(transport==UDP_TRANSPORT){
                            ret_val     = sl_SendTo(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0, dest_addr, dest_addr_len);
                        }
                        if(ret_val <= 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }
                        
                        //memset(sip_rtsp_tx_buffer, 0, sizeof(sip_rtsp_tx_buffer));
                        ret_val = invite_ringing_format(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
                        bytes_to_tx = ret_val;
                        Report("[->] %s\n\r", sip_rtsp_tx_buffer);
                        if(transport==TCP_TRANSPORT){
                            ret_val     = sl_Send(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                        }
                        if(transport==UDP_TRANSPORT){
                            ret_val     = sl_SendTo(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0, dest_addr, dest_addr_len);
                        }
                        if(ret_val <= 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }
                        
                        
                        //Open sockets
                        ret_val = 
                        open_rtp_rtcp_sockets(&app_av_soc_info[0], p_session_config,         
                                         sip_pkt_info.pkt_response.setup_data[0],0);
                        ret_val = 
                        open_rtp_rtcp_sockets(&app_av_soc_info[1], p_session_config,
                                         sip_pkt_info.pkt_response.setup_data[1],1);
                        if(ret_val < 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }
                        
                        ret_val = invite_response_format(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
                        

                        bytes_to_tx = ret_val;

                        Report("[->] %s\n\r", sip_rtsp_tx_buffer);
                        if(transport==TCP_TRANSPORT){
                            ret_val     = sl_Send(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                        }
                        if(transport==UDP_TRANSPORT){
                            ret_val     = sl_SendTo(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0, dest_addr, dest_addr_len);
                        }
                        if(ret_val <= 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }


                        cc_start_timestamp_cnt();


                        if(!(app_av_soc_info[0].rtp_sock_id < 0))
                        {
                            msg_on_v_task_q.task_id = RTSP_TASK_ID;
                            msg_on_v_task_q.msg_id = START_V_STREAMING;
                            msg_on_v_task_q.p_data     = (void *)&resolution;
                            mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);
                        }

                        if(!(app_av_soc_info[1].rtp_sock_id < 0))
                        {
                            msg_on_a_task_q.task_id = RTSP_TASK_ID;
                            msg_on_a_task_q.msg_id = START_A_STREAMING;
                            mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                            mq_send(gARxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                        }


                        

                    }
                    else{
                      
                        //memset(sip_rtsp_tx_buffer, 0, sizeof(sip_rtsp_tx_buffer));
                        ret_val = invite_trying_format(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
                        bytes_to_tx = ret_val;
                        Report("[->] %s\n\r", sip_rtsp_tx_buffer);
                        if(transport==TCP_TRANSPORT){
                            ret_val     = sl_Send(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                        }
                        if(transport==UDP_TRANSPORT){
                            ret_val     = sl_SendTo(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0, dest_addr, dest_addr_len);
                        }
                        if(ret_val <= 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }
                      
                      
                        //memset(sip_rtsp_tx_buffer, 0, sizeof(sip_rtsp_tx_buffer));
                        ret_val = invite_response_format(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
                        bytes_to_tx = ret_val;
                        Report("[->] %s\n\r", sip_rtsp_tx_buffer);
                        if(transport==TCP_TRANSPORT){
                            ret_val     = sl_Send(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                        }
                        if(transport==UDP_TRANSPORT){
                            ret_val     = sl_SendTo(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0, dest_addr, dest_addr_len);
                        }
                        if(ret_val <= 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }
                    }
                break;
                  
                case SIP_OPTIONS:
                  
                      ret_val = sip_options_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
                      
                      bytes_to_tx = ret_val;
                      ret_val     = sl_Send(rtsp_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                      if(ret_val <= 0) {
                          ERR_PRINT(ret_val);
                          return 0;
                      }
                break;
                case SIP_ACK:

                      //ACK not very important, apparently
                break;
                case SIP_CANCEL:
                  
                    return 0;
                case SIP_BYE:
                  
                      /* Client Disconnected */
                      if(invited){

                          msg_on_a_task_q.task_id     = RTSP_TASK_ID;
                          msg_on_a_task_q.msg_id      = STOP_A_STREAMING;
                          mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                          mq_send(gARxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                          
                          msg_on_v_task_q.task_id     = RTSP_TASK_ID;
                          msg_on_v_task_q.msg_id      = STOP_V_STREAMING;
                          mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

                          close_rtp_sockets(&app_av_soc_info[0]);
                          close_rtp_sockets(&app_av_soc_info[1]);
                          cc_stop_timestamp_cnt();
                          invited=0;
                      }
                      
                      return 0;

                }
            }
            else
            {
                ERR_PRINT(ret_val);
            }


            memset(sip_rtsp_rx_buffer, 0, RECV_BUFF_SIZE);
            
        }
    }
}
/**/

/**/

/**/
void * SipClientTask(void *p_args)
{

    uint32_t  srv_ip = SIP_SERVER_IP;
    session_transport_e transport = UDP_TRANSPORT;
    
    
    rtsp_session_config_s   *p_session_config = NULL;
    sip_pkt_info_s         sip_pkt_info;

    int32_t      ret_val         = -1;
    uint16_t      bytes_to_rx     = 0;
    int16_t      bytes_to_tx     = -1;
    sip_client_state_e client_state = SIP_CLIENT_INVITING_STATE;

    sip_packet_type_e last_resp_packet=SIP_UNKNOWN;
    
    a_task_msg_s msg_on_a_task_q;
    v_task_msg_s msg_on_v_task_q;
    
    SlSockAddr_t    *srv_addr;           
    SlSockAddrIn_t srvAddr;
    srv_addr = (SlSockAddr_t*)&srvAddr;
    

      
    uint16_t dest_addr_len=sizeof(SlSockAddrIn_t);
    
    p_session_config = &sip_session_config;
    //INVITING:
    //(on msg MAKE_CALL)
        //send invite
        //state to CALLING
    
    //CALLING:
    //recv
        //on TRYING: state to CALLING
        //on RINGING: state to CALLING
        //on OK: state to STREAMING; send ACK
        //on CANCEL: State to IDLE
        //on BYE: State to IDLE
        //timeout: state to IDLE
    
    //STREAMING:
    //send video begin
    //send audio begin
    //recv audio begin
    //recv
        //on TRYING: state to STREAMING
        //on RINGING: state to STREAMING
        //on OK: state to STREAMING; send ACK
        //on CANCEL: State to IDLE
        //on BYE: State to IDLE
        //timeout: state to STREAMING
    //State to streaming
    
    srvAddr.sin_family       = SL_AF_INET;
    srvAddr.sin_port         = sl_Htons(SIP_PORT);
    srvAddr.sin_addr.s_addr  = sl_Htonl(srv_ip);
    

    sip_client_sd = create_client(srv_addr,transport);
    Report("Starting client!\r\n");
    while(1){
        memset(sip_rtsp_rx_buffer, 0, MAX_SESS_RX_MESSAGE_SIZE);
        

        
        switch(client_state){
        case SIP_CLIENT_INVITING_STATE:
            
            memset(&sip_pkt_info, 0, sizeof(sip_pkt_info));
            ret_val = invite_format(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
            

            bytes_to_tx = ret_val;

            Report("[->] %s\n\r", sip_rtsp_tx_buffer);
            if(transport==TCP_TRANSPORT){
                ret_val     = sl_Send(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
            }
            if(transport==UDP_TRANSPORT){
                ret_val     = sl_SendTo(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0, srv_addr, dest_addr_len);
            }
            if(ret_val <= 0) {
                ERR_PRINT(ret_val);
                return 0;
            }
            client_state=SIP_CLIENT_CALLING_STATE;
            //send invite
        case SIP_CLIENT_CALLING_STATE:
          
            ret_val = recv_sip_message(sip_client_sd, transport);
            //content_len = get_header_field(sip_rtsp_rx_buffer, "Content-Length", int8_t *out_buffer)

            
            if(ret_val>0)
            {
                p_session_config = &sip_session_config;

                // Parse data 
                bytes_to_rx                 = ret_val;
                p_session_config->timestamp = rand();
                sip_rtsp_rx_buffer[bytes_to_rx]=0;//null term the string
                
                
                ret_val = sip_resp_packet_parser(sip_rtsp_rx_buffer,
                                             p_session_config, SEND_BUFF_SIZE,
                                             sip_rtsp_tx_buffer, &sip_pkt_info);
                if(ret_val == 0)
                {
                    switch(sip_pkt_info.pkt_type){
                    case SIP_TRYING:
                        last_resp_packet=SIP_TRYING;
                      break;
                    case SIP_RINGING:
                        last_resp_packet=SIP_RINGING;
                      break;
                    case SIP_OK:
                        last_resp_packet=SIP_OK;
                        //sip_ok_parser(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);

                          
                        invited=1;
                        //SlSockAddr_t    *srv_addr;           
                        //SlSockAddrIn_t srvAddr;
                        //srv_addr = (SlSockAddr_t*)&srvAddr;
                        client_addr.sin_addr.s_addr = srvAddr.sin_addr.s_addr;

                        
                        
                        //Open sockets
                        ret_val = 
                        open_rtp_rtcp_sockets(&app_av_soc_info[0], p_session_config,         
                                         sip_pkt_info.pkt_response.setup_data[0],0);
                        ret_val = 
                        open_rtp_rtcp_sockets(&app_av_soc_info[1], p_session_config,
                                         sip_pkt_info.pkt_response.setup_data[1],1);
                        if(ret_val < 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }
                        
                        ret_val = invite_ack_format(sip_rtsp_rx_buffer, p_session_config, sip_rtsp_tx_buffer, SEND_BUFF_SIZE, &sip_pkt_info);
                        

                        bytes_to_tx = ret_val;

                        Report("[->] %s\n\r", sip_rtsp_tx_buffer);
                        if(transport==TCP_TRANSPORT){
                            ret_val     = sl_Send(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0);
                        }
                        if(transport==UDP_TRANSPORT){
                            ret_val     = sl_SendTo(sip_client_sd, sip_rtsp_tx_buffer, bytes_to_tx, 0, srv_addr, dest_addr_len);
                        }
                        if(ret_val <= 0) {
                            ERR_PRINT(ret_val);
                            return 0;
                        }


                        cc_start_timestamp_cnt();


                        if(!(app_av_soc_info[0].rtp_sock_id < 0))
                        {
                            msg_on_v_task_q.task_id = RTSP_TASK_ID;
                            msg_on_v_task_q.msg_id = START_V_STREAMING;
                            msg_on_v_task_q.p_data     = (void *)&resolution;
                            mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);
                        }

                        if(!(app_av_soc_info[1].rtp_sock_id < 0))
                        {
                            msg_on_a_task_q.task_id = RTSP_TASK_ID;
                            msg_on_a_task_q.msg_id = START_A_STREAMING;
                            mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                            mq_send(gARxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                        }
                        client_state=SIP_CLIENT_STREAMING_STATE;
                        
                    break;
                      


                    case SIP_CANCEL:
                        last_resp_packet=SIP_CANCEL;
                    case SIP_BYE:
                        last_resp_packet=SIP_BYE;
                        Report("SIP server sent BYE/CANCEL. kill client\r\n");
                        return 0;//End thread

                    }
                }
                else
                {
                    ERR_PRINT(ret_val);
                }


                memset(sip_rtsp_rx_buffer, 0, RECV_BUFF_SIZE);
                
            }
            else if (SL_ERROR_BSD_EAGAIN == ret_val)
            {

                if(last_resp_packet==SIP_UNKNOWN){//No response at all
                    sl_Close(sip_client_sd);
                    sip_client_sd = -1;
                    return 0;//end thread
                }
                if(last_resp_packet==SIP_TRYING){
                    

                }
                if(last_resp_packet==SIP_RINGING){
                  
                }
                  
            }
            else//(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
            {
              Report("SIP client socket error/closed: %i\r\n", ret_val);

                sl_Close(sip_client_sd);
                sip_client_sd = -1;

                return 0;//end thread
            }
          break;
        case SIP_CLIENT_STREAMING_STATE:
            ret_val=recv_sip_message(sip_client_sd, transport);
            
            if(ret_val>0)
            {
                p_session_config = &sip_session_config;

                // Parse data 
                bytes_to_rx                 = ret_val;
                p_session_config->timestamp = rand();
                sip_rtsp_rx_buffer[bytes_to_rx]=0;//null term the string
                
                
                ret_val = sip_resp_packet_parser(sip_rtsp_rx_buffer,
                                             p_session_config, SEND_BUFF_SIZE,
                                             sip_rtsp_tx_buffer, &sip_pkt_info);
                if(ret_val == 0)
                {
                    switch(sip_pkt_info.pkt_type){
                    case SIP_TRYING:                  
                        last_resp_packet=SIP_TRYING;
                      break;
                    case SIP_RINGING:
                        last_resp_packet=SIP_RINGING;
                      break;
                    case SIP_OK:     
                        last_resp_packet=SIP_OK;
                      break;
                    case SIP_CANCEL:
                        last_resp_packet=SIP_CANCEL;
                    case SIP_BYE:
                      Report("SIP server sent BYE/CANCEL. Kill client.\r\n");
                        last_resp_packet=SIP_BYE;
                      
                        msg_on_a_task_q.task_id     = RTSP_TASK_ID;
                        msg_on_a_task_q.msg_id      = STOP_A_STREAMING;
                        mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                        mq_send(gARxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                        
                        msg_on_v_task_q.task_id     = RTSP_TASK_ID;
                        msg_on_v_task_q.msg_id      = STOP_V_STREAMING;
                        mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

                        close_rtp_sockets(&app_av_soc_info[0]);
                        close_rtp_sockets(&app_av_soc_info[1]);


                        cc_stop_timestamp_cnt();
                        
                        return 0;//End thread

                    }
                }
                else
                {
                    ERR_PRINT(ret_val);
                }


                memset(sip_rtsp_rx_buffer, 0, RECV_BUFF_SIZE);
                
            }
            else if (SL_ERROR_BSD_EAGAIN == ret_val)
            {
                // No data - Ignore 
            }
            else//(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
            {
                Report("SIP client socket error/closed: %i. Kill client.\r\n",ret_val);
                msg_on_a_task_q.task_id     = RTSP_TASK_ID;
                msg_on_a_task_q.msg_id      = STOP_A_STREAMING;
                mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                mq_send(gARxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
                
                msg_on_v_task_q.task_id     = RTSP_TASK_ID;
                msg_on_v_task_q.msg_id      = STOP_V_STREAMING;
                mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);
                        
                close_rtp_sockets(&app_av_soc_info[0]);
                close_rtp_sockets(&app_av_soc_info[1]);

                sl_Close(sip_client_sd);
                sip_client_sd = -1;
                
                cc_stop_timestamp_cnt();
                return 0;
            }
          
          break;
        default:
          
          break;
          
        }//end switch (client_state)

    }
}
/**/


static int16_t listen_for_clients(uint16_t port, session_transport_e transport)
{
    SlSockAddrIn_t  local_addr  = {0};

    uint16_t          addr_size   = 0;
    int16_t          sock_id     = 0;
    int16_t          ret_val     = 0;

    local_addr.sin_family       = SL_AF_INET;
    local_addr.sin_port         = sl_Htons(port);
    local_addr.sin_addr.s_addr  = 0;
    
    if(transport == TCP_TRANSPORT)
        sock_id = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
    if(transport == UDP_TRANSPORT)
        sock_id = sl_Socket(SL_AF_INET,SL_SOCK_DGRAM, 0);
    
    if(sock_id < 0 )
    {
        ERR_PRINT(sock_id);
        return sock_id;
    }

    addr_size   = sizeof(SlSockAddrIn_t);
    ret_val     = sl_Bind(sock_id, (SlSockAddr_t *)&local_addr, addr_size);
    if(ret_val < 0)
    {
        sl_Close(sock_id);
        ERR_PRINT(ret_val);
        return ret_val;
    }

    if(transport == TCP_TRANSPORT){
        ret_val = sl_Listen(sock_id, 0);
        if(ret_val < 0)
        {
            sl_Close(sock_id);
            ERR_PRINT(ret_val);
            return ret_val;
        }
    }

    return sock_id;
}

/**/
static int16_t create_client(SlSockAddr_t *p_srv_addr, session_transport_e transport)
{
    SlSockAddrIn_t  local_addr  = {0};
    
    
    
    uint16_t          addr_size   = 0;
    int16_t          sock_id     = 0;
    int16_t          ret_val     = 0;

   
    
    local_addr.sin_family       = SL_AF_INET;
    local_addr.sin_port         = ((SlSockAddrIn_t *)p_srv_addr)->sin_port;
    local_addr.sin_addr.s_addr  = 0;
    

    
    if(transport == TCP_TRANSPORT)
        sock_id = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
    if(transport == UDP_TRANSPORT)
        sock_id = sl_Socket(SL_AF_INET,SL_SOCK_DGRAM, 0);
    
    if(sock_id < 0 )
    {
        ERR_PRINT(sock_id);
        return sock_id;
    }
    
    if(transport == UDP_TRANSPORT){
        addr_size   = sizeof(SlSockAddrIn_t);
        ret_val     = sl_Bind(sock_id, (SlSockAddr_t *)&local_addr, addr_size);
        if(ret_val < 0)
        {
            sl_Close(sock_id);
            ERR_PRINT(ret_val);
            return ret_val;
        }
    }
        if(transport == TCP_TRANSPORT){
        addr_size   = sizeof(SlSockAddrIn_t);
        ret_val = sl_Connect(sock_id, p_srv_addr, addr_size);
        if(ret_val < 0)
        {
            sl_Close(sock_id);
            ERR_PRINT(ret_val);
            return ret_val;
        }
    }
        sprintf((char *)rtsp_session_config.peer_ip_str, "%d.%d.%d.%d",
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),3),
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),2),
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),1),
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),0));
        sprintf((char *)sip_session_config.peer_ip_str, "%d.%d.%d.%d",
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),3),
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),2),
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),1),
        SL_IPV4_BYTE(sl_Htonl(((SlSockAddrIn_t *)p_srv_addr)->sin_addr.s_addr),0));

    return sock_id;
}

/**/
int16_t accept_client(uint16_t listen_sd)
{
    struct SlTimeval_t  time_val    = {0};
    uint16_t              addr_size = 0;
    int16_t              client_sock_id = 0;
    

    addr_size       = sizeof(SlSockAddrIn_t);
    client_sock_id  = sl_Accept(listen_sd,\
                                (struct SlSockAddr_t *)&client_addr,\
                                (SlSocklen_t*)&addr_size);
    if(client_sock_id < 0)
    {
        sl_Close(listen_sd);
        ERR_PRINT(client_sock_id);
    }
    else
    {
        /* Configure the Recv Time out */
        time_val.tv_sec = SESS_RECV_TIMEOUT;
        time_val.tv_usec = 0;
        sl_SetSockOpt(client_sock_id, SL_SOL_SOCKET, SL_SO_RCVTIMEO,\
                      (_u8 *)&time_val, sizeof(time_val));
        sprintf((char *)rtsp_session_config.peer_ip_str, "%d.%d.%d.%d",
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,3),
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,2),
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,1),
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,0));
        sprintf((char *)sip_session_config.peer_ip_str, "%d.%d.%d.%d",
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,3),
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,2),
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,1),
        SL_IPV4_BYTE(client_addr.sin_addr.s_addr,0));

    }

    return client_sock_id;

}

/**/
static int32_t close_rtp_sockets(rtp_sock_info_s *p_sock_info)
{
    if(!(p_sock_info->rtp_sock_id < 0))
    {
        sl_Close(p_sock_info->rtp_sock_id);
        Report("Media/RTP socket [%d] closed..!\n\r", p_sock_info->rtp_sock_id);

        p_sock_info->rtp_sock_id = -1;
    }

    if(!(p_sock_info->rtcp_sock_id < 0))
    {
        sl_Close(p_sock_info->rtcp_sock_id);
        Report("RTCP socket [%d] closed\r\n", p_sock_info->rtcp_sock_id);

        p_sock_info->rtcp_sock_id = -1;

    }

    rtsp_release_setup();
    return 0;
}

/**/
static int32_t \
open_rtp_rtcp_sockets(rtp_sock_info_s *p_sock_info,              \
                 rtsp_session_config_s *p_session_config,   \
                 rtsp_setup_data_s setup_data, int32_t idx)
{
    SlSockNonblocking_t sock_opt    = {0};
    SlSockAddrIn_t      local_addr  = {0};
    int32_t              ret_val     = 0;

    /* Create RTP socket */
    p_sock_info->rtp_addr.sin_family    = SL_AF_INET;
    p_sock_info->rtp_addr.sin_port      = sl_Htons((_u16)setup_data.rtp_port);
    if(setup_data.is_multicast == 1)
    {
        /* Multicast IP here */
        p_sock_info->rtp_addr.sin_addr.s_addr = MULTICAST_IP;
    }
    else
    {
        /* Client IP here*/
        p_sock_info->rtp_addr.sin_addr.s_addr = client_addr.sin_addr.s_addr;
    }

    p_sock_info->addr_size      = sizeof(SlSockAddrIn_t);
    p_sock_info->rtp_sock_id    = sl_Socket(SL_AF_INET, SL_SOCK_DGRAM, 0);
    if(p_sock_info->rtp_sock_id < 0 )
    {
        ERR_PRINT(p_sock_info->rtp_sock_id);
        return p_sock_info->rtp_sock_id;
    }

    Report("Media/RTP socket [%d] opened..!\n\r", p_sock_info->rtp_sock_id);

    local_addr.sin_family       = SL_AF_INET;
    local_addr.sin_port         = sl_Htons((_u16)p_session_config->stream_port[idx].rtp_port);
    local_addr.sin_addr.s_addr  = sl_Htonl(SL_INADDR_ANY);

    ret_val = sl_Bind(p_sock_info->rtp_sock_id,     \
                      (SlSockAddr_t *)&local_addr,  \
                      p_sock_info->addr_size);
    if( ret_val < 0 )
    {
        ERR_PRINT(ret_val);
        return ret_val;
    }

    sock_opt.NonBlockingEnabled = 1;

    ret_val = sl_SetSockOpt(p_sock_info->rtp_sock_id,           \
                            SL_SOL_SOCKET,SL_SO_NONBLOCKING,    \
                            (_u8 *)&sock_opt, sizeof(sock_opt));
    if(ret_val < 0) ERR_PRINT(ret_val);

    /* Create RTCP socket */
    p_sock_info->rtcp_addr.sin_family   = SL_AF_INET;
    p_sock_info->rtcp_addr.sin_port     = sl_Htons((_u16)setup_data.rtcp_port);
    if(setup_data.is_multicast == 1)
    {
       p_sock_info->rtcp_addr.sin_addr.s_addr = MULTICAST_IP;
    }
    else
    {
        p_sock_info->rtcp_addr.sin_addr.s_addr = client_addr.sin_addr.s_addr;
    }

    p_sock_info->addr_size = sizeof(SlSockAddrIn_t);

    p_sock_info->rtcp_sock_id = sl_Socket(SL_AF_INET,SL_SOCK_DGRAM, 0);
    if( p_sock_info->rtcp_sock_id < 0 )
    {
        ERR_PRINT(p_sock_info->rtcp_sock_id);
        return p_sock_info->rtcp_sock_id;
    }

    local_addr.sin_family       = SL_AF_INET;
    local_addr.sin_port         = sl_Htons((_u16)p_session_config->stream_port[idx].rtcp_port);
    local_addr.sin_addr.s_addr  = sl_Htonl(SL_INADDR_ANY);

    ret_val = sl_Bind(p_sock_info->rtcp_sock_id, (SlSockAddr_t *)&local_addr,\
                      p_sock_info->addr_size);
    if( ret_val < 0 )
    {
        ERR_PRINT(ret_val);
        return ret_val;
    }

    ret_val = sl_SetSockOpt(p_sock_info->rtcp_sock_id, SL_SOL_SOCKET,\
                            SL_SO_NONBLOCKING, (_u8 *)&sock_opt, sizeof(sock_opt));
    if(ret_val < 0) ERR_PRINT(ret_val);

    return 0;
}



/**/
static int32_t init_ov798()
{

    v_task_msg_s           msg_on_v_task_q;

    int32_t                  ret_val = 0;

    msg_on_v_task_q.task_id    = RTSP_TASK_ID;
    msg_on_v_task_q.msg_id     = INIT_OV798;
    mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

    msg_on_v_task_q.task_id    = RTSP_TASK_ID;
    msg_on_v_task_q.msg_id     = V_CONFIG_SENSOR;
    msg_on_v_task_q.p_data     = (void *)&v_config;
    mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

    msg_on_v_task_q.task_id    = RTSP_TASK_ID;
    msg_on_v_task_q.msg_id     = V_SET_FRAME_RATE;
    frame_rate                  = VIDEO_FRAME_RATE;
    msg_on_v_task_q.p_data     = (void *)&frame_rate;
    mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

    msg_on_v_task_q.task_id    = RTSP_TASK_ID;
    msg_on_v_task_q.msg_id     = V_SET_BITRATE;
    bit_rate                  = VIDEO_BITRATE;
    msg_on_v_task_q.p_data     = (void *)&bit_rate;
    mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

    msg_on_v_task_q.task_id    = RTSP_TASK_ID;
    msg_on_v_task_q.msg_id     = V_SET_IFRAME_INTERVAL;
    iframe_interval            = VIDEO_IFRAME_INTERVAL;
    msg_on_v_task_q.p_data     = (void *)&iframe_interval;
    mq_send(gVTxSMQueue, (char *)&msg_on_v_task_q, sizeof(v_task_msg_s), 0);

    resolution=VIDEO_RESOLUTION;
    return ret_val;
}



/**/
void * RtspServerTask(void *p_args)
{

    session_transport_e transport = TCP_TRANSPORT;
    
    app_rtsp_task_msg_s msg_on_rtsp_task_q;
    app_rtsp_srv_task_msg_s msg_on_rtsp_srv_task_q;

    while(1)
    {
        
        mq_receive(gRtspSrvQueue, (char *)&msg_on_rtsp_srv_task_q, sizeof(app_rtsp_srv_task_msg_s), NULL);
        
        if(RTSP_SERVER_START == msg_on_rtsp_srv_task_q.msg_id)
        {
            rtsp_listen_sd = -1;
            rtsp_client_sd = -1;

            Report("Listening for RTSP clients..\r\n");

            rtsp_listen_sd = listen_for_clients(RTSP_PORT, transport);
            if(rtsp_listen_sd < 0) ERR_PRINT(rtsp_listen_sd);

            msg_on_rtsp_task_q.task_id  = RTSP_MAIN_TASK_ID;
            msg_on_rtsp_task_q.msg_id   = RTSP_SERVER_STARTED;
            mq_send(gRtspMainSMQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_rtsp_task_msg_s), 0);    
            msg_on_rtsp_srv_task_q.task_id   = RTSP_MAIN_TASK_ID;
            msg_on_rtsp_srv_task_q.msg_id    = RTSP_SERVER_ACCEPT;
            mq_send(gRtspSrvQueue, (char *)&msg_on_rtsp_srv_task_q, sizeof(app_rtsp_srv_task_msg_s), 0);
        }
        
        if(RTSP_SERVER_ACCEPT == msg_on_rtsp_srv_task_q.msg_id)
        {
        /* Wait for client connections */
            
            
                if(transport==TCP_TRANSPORT){
                    rtsp_client_sd = accept_client(rtsp_listen_sd);
                    if(rtsp_client_sd < 0)
                    {
                        //ERR_PRINT(rtsp_client_sd);
                    }
                    else
                    {
                        Report("An RTSP client has connected!\r\n");

                    }
                }
                else{
                    rtsp_client_sd=rtsp_listen_sd;
                }
                    
                if(rtsp_client_sd >= 0){
                    handle_rtsp_client(&msg_on_rtsp_task_q, transport);
                }
                else
                {
                    msg_on_rtsp_srv_task_q.task_id   = RTSP_MAIN_TASK_ID;
                    msg_on_rtsp_srv_task_q.msg_id    = RTSP_SERVER_ACCEPT;
                    mq_send(gRtspSrvQueue, (char *)&msg_on_rtsp_srv_task_q, sizeof(app_rtsp_srv_task_msg_s), 0);
                }
        }
        /* Check for msg in queue */
        if(RTSP_SERVER_STOP == msg_on_rtsp_srv_task_q.msg_id)
        {
            /* If RTP/RTCP sockets are open, close them */
            close_rtp_sockets(&app_av_soc_info[0]);
            close_rtp_sockets(&app_av_soc_info[1]);
            Report("Closing RTSP Sockets\r\n");
            if(rtsp_client_sd > 0)
            {
                sl_Close(rtsp_client_sd);
            }
            sl_Close(rtsp_listen_sd);
            rtsp_listen_sd = -1;
            rtsp_client_sd = -1;

        }
    }
}


/**/
void * SipServerTask(void *p_args)
{

    session_transport_e transport = UDP_TRANSPORT;
    sip_listen_sd = -1;
    sip_client_sd = -1;

    app_rtsp_task_msg_s msg_on_rtsp_task_q;
    app_sip_srv_task_msg_s msg_on_sip_srv_task_q;
    
    
    while(1)
    {
        /* Check for msg in queue */
        mq_receive(gSipSrvQueue, (char *)&msg_on_sip_srv_task_q, sizeof(app_sip_srv_task_msg_s), NULL);
        /* Wait for client connections */
        if(SIP_SERVER_START == msg_on_sip_srv_task_q.msg_id)
        {
            Report("Listening for SIP clients..\r\n");

            sip_listen_sd = listen_for_clients(SIP_PORT, transport);
            if(sip_listen_sd < 0)  ERR_PRINT(sip_listen_sd); 

            msg_on_rtsp_task_q.task_id  = RTSP_MAIN_TASK_ID;
            msg_on_rtsp_task_q.msg_id   = SIP_SERVER_STARTED;
            mq_send(gRtspMainSMQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_rtsp_task_msg_s), 0);
            msg_on_sip_srv_task_q.task_id   = RTSP_MAIN_TASK_ID;
            msg_on_sip_srv_task_q.msg_id    = SIP_SERVER_ACCEPT;
            mq_send(gSipSrvQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_sip_srv_task_msg_s), 0);

        }
        if(SIP_SERVER_ACCEPT == msg_on_sip_srv_task_q.msg_id)
        {

            if(transport==TCP_TRANSPORT){
                sip_client_sd = accept_client(sip_listen_sd);
                if(sip_client_sd < 0)
                {
                    ERR_PRINT(sip_client_sd);
                }
                else
                {
                    Report("A SIP client has connected!\r\n");
                }
            }
            else if(transport==UDP_TRANSPORT){
                sip_client_sd = sip_listen_sd;
            }
            if(sip_client_sd >= 0){
                handle_sip_client(&msg_on_rtsp_task_q, transport);
            }
            else{
                //recur
                msg_on_sip_srv_task_q.task_id   = RTSP_MAIN_TASK_ID;
                msg_on_sip_srv_task_q.msg_id    = SIP_SERVER_ACCEPT;
                mq_send(gSipSrvQueue, (char *)&msg_on_sip_srv_task_q, sizeof(app_sip_srv_task_msg_s), 0);
            }
        }

        if(SIP_SERVER_STOP == msg_on_sip_srv_task_q.msg_id)
        {
            /* If RTP/RTCP sockets are open, close them */
            close_rtp_sockets(&app_av_soc_info[0]);
            close_rtp_sockets(&app_av_soc_info[1]);

            Report("Closing SIP Sockets\r\n");
            if(sip_client_sd > 0)
            {
                sl_Close(sip_client_sd);
            }
            sl_Close(sip_listen_sd);
            sip_listen_sd = -1;
            sip_client_sd = -1;

        }
    }
}


