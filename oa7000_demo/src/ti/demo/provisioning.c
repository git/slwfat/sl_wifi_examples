/*
 * Copyright (C) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*****************************************************************************

 Application Name     - Provisioning application
 Application Overview - This application demonstrates how to use the provisioning method
                        in order to establish connection to the AP.  The application
                        connects to an AP and ping's the gateway to verify the connection.
						
 Application Details  - Refer to 'Provisioning' README.html

*****************************************************************************/
//****************************************************************************
//
//! \addtogroup
//! @{
//
//****************************************************************************

/* Standard Include */
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
/* TI-DRIVERS Header files */
#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/net/wifi/source/driver.h>
#include <ti/net/slnetif.h>
#include <ti/net/slnetutils.h>
#include <ti/net/slnetsock.h>
#include <ti/net/slnet.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>

#include <ti/drivers/SPI.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/Timer.h>
#include "ti_drivers_config.h"
#include "hw_types.h"
#include "uart_if.h"
#include "pinmux.h"

#include "provisioning.h"
#include "sip_rtsp_main_task.h"
#include "video_tx_task.h"
#include "audio_tx_task.h"
#include "rtcp_rtp.h"
#include "sip_rtsp.h"

#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <mqueue.h>
#include <time.h>


/* Application Version and Naming*/
#define APPLICATION_NAME         "CC3220-OV798 Video Streaming 1.0"
#define APPLICATION_VERSION "01.00.00.00"

/* USER's defines */
#define SPAWN_TASK_PRIORITY                     (9)
#define TASK_STACK_SIZE                         (2048)
#define TIMER_TASK_STACK_SIZE                   (1024)
#define DISPLAY_TASK_STACK_SIZE                 (512)
#define SL_STOP_TIMEOUT                         (200)
#define PROVISIONING_INACTIVITY_TIMEOUT         (1200)     /* Provisioning inactivity timeout in seconds (20 min)*/
#define RECONNECTION_ESTABLISHED_TIMEOUT_SEC    (10)   /* 10 seconds */
#define CONNECTION_PHASE_TIMEOUT_SEC            (10)    /* 10 seconds */

/* Enable UART Log */
#define LOG_MESSAGE_ENABLE
#define LOG_MESSAGE Report

#define HTTPS_ENABLE        (0)
#define SC_KEY_ENABLE       (0)
#define SECURED_AP_ENABLE   (0)


#define AP_SEC_TYPE         (SL_WLAN_SEC_TYPE_WPA_WPA2)
#define AP_SEC_PASSWORD     "1234567890"
#define SC_KEY              "1234567890123456"

/* For Debug - Start Provisioning for each reset, or stay connected */
/* Used mainly in case of provisioning robustness tests             */
#define FORCE_PROVISIONING   (0)


#define NWP_OFF -1
#define ROLE_STA 0
#define ROLE_AP  2

#define SSL_SERVER_KEY          "dummy-root-ca-cert-key"
#define SSL_SERVER_CERT         "dummy-root-ca-cert"



 /* Function pointer to the event handler */
typedef int32_t (*fptr_EventHandler)(void);

/* Entry in the lookup table */
typedef struct _Provisioning_TableEntry_t_
{
    fptr_EventHandler   p_evtHndl;  /* Pointer to the event handler */
    AppState            nextState;  /* Next state of the application */
}Provisioning_TableEntry_t;

/* LED State */
typedef enum
{
    LedState_CONNECTION,
    LedState_FACTORY_DEFAULT,
    LedState_ERROR
}LedState;

/****************************************************************************
                      LOCAL FUNCTION PROTOTYPES
****************************************************************************/
int32_t ReportError(void);
int32_t HandleConnection(void);
int32_t WaitForReconnectionTimerStart(void);
int32_t DoNothing(void);
int32_t HandleProvisioningTimeOut(void);
int32_t HandleProvisioningComplete(void);
int32_t WaitForIpTimerStart(void);
int32_t ProcessRestartRequest(void);
int32_t ProvisioningStart(void);
int32_t SendPingToGW(void);
int32_t WaitForConnectionTimerStart(void);
int32_t returnToFactoryDefault(void);
int32_t ti_net_SlNet_initConfig(void);
void    StopAsyncEvtTimer(void);

/****************************************************************************
                      GLOBAL VARIABLES
****************************************************************************/
pthread_t gSpawnThread = (pthread_t)NULL;
pthread_t gProvisioningThread = (pthread_t)NULL;

pthread_t gRtspMainTaskThread = (pthread_t)NULL;
pthread_t gRtspServerTaskThread = (pthread_t)NULL;
pthread_t gSipServerTaskThread = (pthread_t)NULL;
pthread_t gATxTaskThread = (pthread_t)NULL;
pthread_t gARxTaskThread = (pthread_t)NULL;
pthread_t gVTxTaskThread = (pthread_t)NULL;


pthread_cond_t net_conn_cond;
pthread_mutex_t net_conn_lock;
pthread_mutex_t frame_lock;
pthread_cond_t nwp_active_cond;
pthread_mutex_t nwp_active_lock;

int net_conn=0;
int nwp_active=NWP_OFF;

mqd_t    gProvisioningSMQueue;

mqd_t    gRtspMainSMQueue;
mqd_t    gRtspSrvQueue;
mqd_t    gSipSrvQueue;
mqd_t    gSipCliQueue;
mqd_t    gATxSMQueue;
mqd_t    gARxSMQueue;
mqd_t    gVTxSMQueue;


uint8_t  gStopInProgress = 0;
uint32_t gPingSent = 0;
uint32_t gPingSuccess = 0;
uint8_t  gRole = ROLE_STA;


extern rtsp_session_config_s rtsp_session_config;
extern rtsp_session_config_s sip_session_config;
extern int ipaq;

uint16_t gLedCount = 0;
uint8_t  gLedState = 0;
uint32_t gErrledCount = 0;
uint8_t  gErrLedState = 0;
uint8_t  gWaitForConn = 0;
LedState gLedDisplayState = LedState_CONNECTION;

timer_t gTimer;

const char *Roles[] = {"STA","STA","AP","P2P"};
const char *WlanStatus[] = {"DISCONNECTED","SCANNING","CONNECTING","CONNECTED"};


/* Application lookup/transition table */
const Provisioning_TableEntry_t gTransitionTable[AppState_MAX][AppEvent_MAX] =
{
        /* AppState_IDLE */
    {
            /* Event: AppEvent_CONNECTED */    {DoNothing            	   , AppState_IDLE             },
            /* Event: AppEvent_IP_ACQUIRED */  {DoNothing    	           , AppState_IDLE              },
            /* Event: AppEvent_DISCONNECT */   {DoNothing           	   , AppState_IDLE     },
            /* AppEvent_PROVISIONING_STARTED */{ProvisioningStart          , AppState_PROVISIONING_IN_PROGRESS},
            /* AppEvent_PROVISIONING_SUCCESS */{DoNothing                  , AppState_IDLE              },
            /* AppEvent_PROVISIONING_STOPPED */{DoNothing		   , AppState_IDLE              },
            /*AppEvent_PROVISIONING_WAIT_CONN*/{DoNothing                  , AppState_IDLE              },
            /* Event: AppEvent_TIMEOUT */      {DoNothing                  , AppState_IDLE              },
            /* Event: AppEvent_ERROR */        {ReportError                , AppState_IDLE                   },
            /* Event: AppEvent_RESTART */      {ProcessRestartRequest      , AppState_WAIT_FOR_CONNECTION     }
    },
        /* AppState_WAIT_FOR_CONNECTION */
    {
            /* Event: AppEvent_CONNECTED */    {WaitForIpTimerStart        , AppState_WAIT_FOR_IP             },
            /* Event: AppEvent_IP_ACQUIRED */  {ReportError                , AppState_IDLE                   },
            /* Event: AppEvent_DISCONNECT */   {WaitForConnectionTimerStart, AppState_WAIT_FOR_CONNECTION     },
            /* AppEvent_PROVISIONING_STARTED */{ProvisioningStart          , AppState_PROVISIONING_IN_PROGRESS},
            /* AppEvent_PROVISIONING_SUCCESS */{DoNothing                  , AppState_WAIT_FOR_CONNECTION     },
            /* AppEvent_PROVISIONING_STOPPED */{DoNothing                  , AppState_IDLE     },
            /*AppEvent_PROVISIONING_WAIT_CONN*/{DoNothing                  , AppState_WAIT_FOR_CONNECTION     },
            /* Event: AppEvent_TIMEOUT */      {ProvisioningStart          , AppState_PROVISIONING_IN_PROGRESS},
            /* Event: AppEvent_ERROR */        {ProvisioningStart          , AppState_PROVISIONING_IN_PROGRESS},
            /* Event: AppEvent_RESTART */      {ProcessRestartRequest      , AppState_WAIT_FOR_CONNECTION     }
    },
        /* AppState_WAIT_FOR_IP */
    {
            /* Event: AppEvent_CONNECTED */    {ReportError                , AppState_IDLE                   },
            /* Event: AppEvent_IP_ACQUIRED */  {DoNothing                  , AppState_IDLE              },/////////////This is the last step in the whole process
            /* Event: AppEvent_DISCONNECT */   {WaitForConnectionTimerStart, AppState_WAIT_FOR_CONNECTION     },
            /* AppEvent_PROVISIONING_STARTED */{ProvisioningStart          , AppState_PROVISIONING_IN_PROGRESS},
            /* AppEvent_PROVISIONING_SUCCESS */{DoNothing                  , AppState_WAIT_FOR_IP             },
            /* AppEvent_PROVISIONING_STOPPED */{DoNothing , AppState_IDLE              },
            /*AppEvent_PROVISIONING_WAIT_CONN*/{DoNothing                , AppState_WAIT_FOR_IP             },
            /* Event: AppEvent_TIMEOUT */      {ReportError                , AppState_IDLE                   },
            /* Event: AppEvent_ERROR */        {ReportError                , AppState_IDLE                   },
            /* Event: AppEvent_RESTART */      {ProcessRestartRequest      , AppState_WAIT_FOR_CONNECTION     }
    },

        /* AppState_PROVISIONING_IN_PROGRESS  */
    {
            /* Event: AppEvent_CONNECTED */    {HandleConnection           , AppState_PROVISIONING_IN_PROGRESS},
            /* Event: AppEvent_IP_ACQUIRED */  {DoNothing    , AppState_PROVISIONING_IN_PROGRESS},
            /* Event: AppEvent_DISCONNECT */   {WaitForReconnectionTimerStart, AppState_PROVISIONING_IN_PROGRESS},
            /* AppEvent_PROVISIONING_STARTED */{DoNothing                  , AppState_PROVISIONING_IN_PROGRESS},
            /* AppEvent_PROVISIONING_SUCCESS */{DoNothing                  , AppState_PROVISIONING_WAIT_COMPLETE},
            /* AppEvent_PROVISIONING_STOPPED */{DoNothing                       , AppState_IDLE              },
            /*AppEvent_PROVISIONING_WAIT_CONN*/{DoNothing                  , AppState_WAIT_FOR_CONNECTION     },
            /* Event: AppEvent_TIMEOUT */      {ProcessRestartRequest      , AppState_WAIT_FOR_CONNECTION     },
            /* Event: AppEvent_ERROR */        {ReportError                , AppState_IDLE                   },
            /* Event: AppEvent_RESTART */      {ProcessRestartRequest      , AppState_WAIT_FOR_CONNECTION     }
    },
        /* AppState_PROVISIONING_WAIT_COMPLETE */
    {
            /* Event: AppEvent_CONNECTED */    {DoNothing                  , AppState_PROVISIONING_WAIT_COMPLETE},
            /* Event: AppEvent_IP_ACQUIRED */  {DoNothing                  , AppState_PROVISIONING_WAIT_COMPLETE},
            /* Event: AppEvent_DISCONNECT */   {WaitForReconnectionTimerStart, AppState_PROVISIONING_WAIT_COMPLETE},
            /* AppEvent_PROVISIONING_STARTED */{DoNothing                  , AppState_PROVISIONING_WAIT_COMPLETE},
            /* AppEvent_PROVISIONING_SUCCESS */{DoNothing                  , AppState_PROVISIONING_WAIT_COMPLETE},
            /* AppEvent_PROVISIONING_STOPPED */{HandleProvisioningComplete                  , AppState_IDLE                },
            /*AppEvent_PROVISIONING_WAIT_CONN*/{DoNothing                , AppState_WAIT_FOR_CONNECTION       },
            /* Event: AppEvent_TIMEOUT */      {HandleProvisioningTimeOut  , AppState_PROVISIONING_WAIT_COMPLETE},
            /* Event: AppEvent_ERROR */        {ReportError                , AppState_IDLE                     },
            /* Event: AppEvent_RESTART */      {ProcessRestartRequest      , AppState_PROVISIONING_WAIT_COMPLETE}
    },

};

/* Application state's context */
AppState g_CurrentState;

int32_t HandleConnection(void)
{
    return 0;
}

void SetNetConnected(){
  
    pthread_mutex_lock(&net_conn_lock);
      net_conn=1;
      pthread_cond_broadcast(&net_conn_cond);
    pthread_mutex_unlock(&net_conn_lock);
    
}

void UnsetNetConnected(){
  
    pthread_mutex_lock(&net_conn_lock);
      net_conn=0;
    pthread_mutex_unlock(&net_conn_lock);
}

int CheckNetConnected(){
  
    int ret;
    pthread_mutex_lock(&net_conn_lock);
      ret = net_conn;
      pthread_cond_broadcast(&net_conn_cond);
    pthread_mutex_unlock(&net_conn_lock);
    
    return ret;
}

int WaitNetConnected(int timeout_sec){
   int ret;
   struct timespec ts;
   pthread_mutex_lock(&net_conn_lock);
   
   if(timeout_sec>0){
      ts.tv_nsec=0;
      ts.tv_sec=timeout_sec; 
      
      if(!net_conn){
        
          ret=pthread_cond_timedwait(&net_conn_cond, &net_conn_lock, &ts);
          if(ret==ETIMEDOUT) ret = -2;
          else if(ret!=0) ret = -1;

      }
      else{
        
          ret = 0;
      }
   }
   else{
      if(!net_conn){
        
          ret=pthread_cond_wait(&net_conn_cond, &net_conn_lock);
          if(ret==ETIMEDOUT) ret = -2;
          else if(ret!=0) ret = -1;

      }
      else{
        
          ret = 0;
      }
   }
   pthread_mutex_unlock(&net_conn_lock);
   return ret;
}

void SetNWPActive(int role){
  
    pthread_mutex_lock(&nwp_active_lock);
    nwp_active=role;
    pthread_cond_broadcast(&nwp_active_cond);
    pthread_mutex_unlock(&nwp_active_lock);
    
}

void UnsetNWPActive(){
  
    pthread_mutex_lock(&nwp_active_lock);
    nwp_active=NWP_OFF;

    pthread_mutex_unlock(&nwp_active_lock);
}

int CheckNWPActive(){
  
    int ret;
    pthread_mutex_lock(&nwp_active_lock);
    ret = nwp_active;
    pthread_mutex_unlock(&nwp_active_lock);
    
    return ret;
}

int WaitNWPActive(int timeout_msec){
   int ret=NWP_OFF;
   struct timespec ts;

     
   if(timeout_msec>0){
      ts.tv_nsec=(timeout_msec%1000000)*1000000;
      ts.tv_sec=timeout_msec/1000;
      ret=CheckNWPActive();
      if(ret<0){
          ret=pthread_cond_timedwait(&nwp_active_cond, &nwp_active_lock, &ts);
          pthread_mutex_unlock(&nwp_active_lock);
          if(ret==ETIMEDOUT) ret = -2;
          else ret=CheckNWPActive();

      }
   }
   else{
      ret=CheckNWPActive();
      if(ret<0){
          ret=pthread_cond_wait(&nwp_active_cond, &nwp_active_lock);
          pthread_mutex_unlock(&nwp_active_lock);
          if(ret==ETIMEDOUT) ret = -2;
          else ret=CheckNWPActive();
      }
   }
   

   return ret;
}


/*****************************************************************************
                  SimpleLink Callback Functions
*****************************************************************************/

//*****************************************************************************
//
//! \brief The Function Handles WLAN Events
//!
//! \param[in]  pWlanEvent - Pointer to WLAN Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{
    char msg;
    app_rtsp_task_msg_s  msg_on_rtsp_task_q;
    memset(&msg_on_rtsp_task_q, 0, sizeof(msg_on_rtsp_task_q));
    
    switch(pWlanEvent->Id)
    {
    case SL_WLAN_EVENT_CONNECT:
        LOG_MESSAGE("    [Event] STA connected to AP\r\n    BSSID:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x\r\n    ",
            pWlanEvent->Data.Connect.Bssid[0],
            pWlanEvent->Data.Connect.Bssid[1],
            pWlanEvent->Data.Connect.Bssid[2],
            pWlanEvent->Data.Connect.Bssid[3],
            pWlanEvent->Data.Connect.Bssid[4],
            pWlanEvent->Data.Connect.Bssid[5]);

        /* set the string terminate */
        pWlanEvent->Data.Connect.SsidName[pWlanEvent->Data.Connect.SsidLen] = '\0';

        LOG_MESSAGE("SSID:%s\r\n", pWlanEvent->Data.Connect.SsidName);
        
        msg=AppEvent_CONNECTED;
        mq_send(gProvisioningSMQueue, &msg, 1, 0);
        break;

    case SL_WLAN_EVENT_DISCONNECT:
        LOG_MESSAGE("    [Event] STA disconnected from AP\r\n    (Reason Code = %d)\r\n", pWlanEvent->Data.Disconnect.ReasonCode);
        
        msg=AppEvent_DISCONNECT;
        mq_send(gProvisioningSMQueue, &msg, 1, 0);

        
        msg_on_rtsp_task_q.task_id   = ASYNC_EVENT_HANDLER;
        msg_on_rtsp_task_q.msg_id    = RTSP_DISCONNECT_FROM_AP;
        mq_send(gRtspMainSMQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_rtsp_task_msg_s), 0);
        GPIO_write(LED_A,0);
        UnsetNetConnected();
        ipaq=0;
        break;

    case SL_WLAN_EVENT_STA_ADDED:
        LOG_MESSAGE("    [Event] New STA Addeed\r\n    (MAC Address: %.2x:%.2x:%.2x:%.2x:%.2x)\r\n",
            pWlanEvent->Data.STAAdded.Mac[0],
            pWlanEvent->Data.STAAdded.Mac[1],
            pWlanEvent->Data.STAAdded.Mac[2],
            pWlanEvent->Data.STAAdded.Mac[3],
            pWlanEvent->Data.STAAdded.Mac[4],
            pWlanEvent->Data.STAAdded.Mac[5] );
        break;

    case SL_WLAN_EVENT_STA_REMOVED:
        LOG_MESSAGE("    [Event] STA Removed\r\n    (MAC Address: %.2x:%.2x:%.2x:%.2x:%.2x)\r\n",
            pWlanEvent->Data.STAAdded.Mac[0],
            pWlanEvent->Data.STAAdded.Mac[1],
            pWlanEvent->Data.STAAdded.Mac[2],
            pWlanEvent->Data.STAAdded.Mac[3],
            pWlanEvent->Data.STAAdded.Mac[4],
            pWlanEvent->Data.STAAdded.Mac[5]);
        break;

    case SL_WLAN_EVENT_PROVISIONING_PROFILE_ADDED:
        LOG_MESSAGE("    [Provisioning] Profile Added: SSID: %s\r\n", pWlanEvent->Data.ProvisioningProfileAdded.Ssid);
        if(pWlanEvent->Data.ProvisioningProfileAdded.ReservedLen > 0)
        {
            LOG_MESSAGE("    [Provisioning] Profile Added: PrivateToken:%s\r\n", pWlanEvent->Data.ProvisioningProfileAdded.Reserved);
        }
        break;

    case SL_WLAN_EVENT_PROVISIONING_STATUS:
        {
            switch(pWlanEvent->Data.ProvisioningStatus.ProvisioningStatus)
            {
            case SL_WLAN_PROVISIONING_GENERAL_ERROR:
            case SL_WLAN_PROVISIONING_ERROR_ABORT:
            case SL_WLAN_PROVISIONING_ERROR_ABORT_INVALID_PARAM:
            case SL_WLAN_PROVISIONING_ERROR_ABORT_HTTP_SERVER_DISABLED:
            case SL_WLAN_PROVISIONING_ERROR_ABORT_PROFILE_LIST_FULL:
            case SL_WLAN_PROVISIONING_ERROR_ABORT_PROVISIONING_ALREADY_STARTED:
                LOG_MESSAGE("    [Provisioning] Provisioning Error status=%d\r\n",pWlanEvent->Data.ProvisioningStatus.ProvisioningStatus);
                
                msg=AppEvent_ERROR;
                mq_send(gProvisioningSMQueue, &msg, 1, 0);
                break;

            case SL_WLAN_PROVISIONING_CONFIRMATION_STATUS_FAIL_NETWORK_NOT_FOUND:
                LOG_MESSAGE("    [Provisioning] Profile confirmation failed: Network not found\r\n");
                msg=AppEvent_PROVISIONING_STARTED;
                mq_send(gProvisioningSMQueue, &msg, 1, 0);

                break;

            case SL_WLAN_PROVISIONING_CONFIRMATION_STATUS_FAIL_CONNECTION_FAILED:
                LOG_MESSAGE("    [Provisioning] Profile confirmation failed: Connection failed\r\n");
                msg=AppEvent_PROVISIONING_STARTED;
                mq_send(gProvisioningSMQueue, &msg, 1, 0);

                break;

            case SL_WLAN_PROVISIONING_CONFIRMATION_STATUS_CONNECTION_SUCCESS_IP_NOT_ACQUIRED:
                LOG_MESSAGE("    [Provisioning] Profile confirmation failed: IP address not acquired\r\n");
                msg=AppEvent_PROVISIONING_STARTED;
                mq_send(gProvisioningSMQueue, &msg, 1, 0);

                break;

            case SL_WLAN_PROVISIONING_CONFIRMATION_STATUS_SUCCESS_FEEDBACK_FAILED:
                LOG_MESSAGE("    [Provisioning] Profile Confirmation failed (Connection Success, feedback to Smartphone app failed)\r\n");
                msg=AppEvent_PROVISIONING_STOPPED;
                mq_send(gProvisioningSMQueue, &msg, 1, 0);
   
                break;

            case SL_WLAN_PROVISIONING_CONFIRMATION_STATUS_SUCCESS:
                LOG_MESSAGE("    [Provisioning] Profile Confirmation Success!\r\n");
                msg=AppEvent_PROVISIONING_SUCCESS;
                mq_send(gProvisioningSMQueue, &msg, 1, 0);

                break;

            case SL_WLAN_PROVISIONING_AUTO_STARTED:
                LOG_MESSAGE("    [Provisioning] Auto-Provisioning Started\r\n");
                msg=AppEvent_RESTART;
                mq_send(gProvisioningSMQueue, &msg, 1, 0);

                break;

            case SL_WLAN_PROVISIONING_STOPPED:
                LOG_MESSAGE("    [Provisioning] Stopped:");
                LOG_MESSAGE("    Current Role: %s\r\n",Roles[pWlanEvent->Data.ProvisioningStatus.Role]);
                if(ROLE_STA == pWlanEvent->Data.ProvisioningStatus.Role)
                {
                    LOG_MESSAGE("    WLAN Status: %s\r\n",WlanStatus[pWlanEvent->Data.ProvisioningStatus.WlanStatus]);

                    if(SL_WLAN_STATUS_CONNECTED == pWlanEvent->Data.ProvisioningStatus.WlanStatus)
                    {

                        LOG_MESSAGE("    Connected to SSID: %s\r\n",pWlanEvent->Data.ProvisioningStatus.Ssid);
                        msg=AppEvent_PROVISIONING_STOPPED;
                        mq_send(gProvisioningSMQueue, &msg, 1, 0);

                    }
                    else if (SL_WLAN_STATUS_SCANING == pWlanEvent->Data.ProvisioningStatus.WlanStatus)
                    {
                        gWaitForConn = 1;
                        msg=AppEvent_PROVISIONING_WAIT_CONN;
                        mq_send(gProvisioningSMQueue, &msg, 1, 0);
                    }
                }
                else
                {
                    msg=AppEvent_RESTART;
                    mq_send(gProvisioningSMQueue, &msg, 1, 0);
                }
                break;

            case SL_WLAN_PROVISIONING_SMART_CONFIG_SYNCED:
                LOG_MESSAGE("    [Provisioning] Smart Config Synced!\r\n");
                break;

            case SL_WLAN_PROVISIONING_SMART_CONFIG_SYNC_TIMEOUT:
                LOG_MESSAGE("    [Provisioning] Smart Config Sync Timeout!\r\n");
                break;

            case SL_WLAN_PROVISIONING_CONFIRMATION_WLAN_CONNECT:
                LOG_MESSAGE("    [Provisioning] Profile confirmation: WLAN Connected!\r\n");
                break;

            case SL_WLAN_PROVISIONING_CONFIRMATION_IP_ACQUIRED:
                LOG_MESSAGE("    [Provisioning] Profile confirmation: IP Acquired!\r\n");
                msg_on_rtsp_task_q.task_id   = ASYNC_EVENT_HANDLER;
                msg_on_rtsp_task_q.msg_id    = RTSP_CONNECTED_TO_NW;
                mq_send(gRtspMainSMQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_rtsp_task_msg_s), 0);
                            
                //SetNetConnected();
                ipaq=1;
                break;

            case SL_WLAN_PROVISIONING_EXTERNAL_CONFIGURATION_READY:
                LOG_MESSAGE("    [Provisioning] External configuration is ready! \r\n");
                break;

            default:
                LOG_MESSAGE("    [Provisioning] Unknown Provisioning Status: %d\r\n",pWlanEvent->Data.ProvisioningStatus.ProvisioningStatus);
                break;
            }
        }
        break;

    default:
        LOG_MESSAGE("    [Event] - WlanEventHandler has received %d !!!!\r\n",pWlanEvent->Id);
        break;
    }
}

//*****************************************************************************
//
//! \brief The Function Handles the Fatal errors
//!
//! \param[in]  slFatalErrorEvent - Pointer to Fatal Error Event info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkFatalErrorEventHandler(SlDeviceFatal_t *slFatalErrorEvent)
{

    switch (slFatalErrorEvent->Id)
    {
    case SL_DEVICE_EVENT_FATAL_DEVICE_ABORT:
        {
            LOG_MESSAGE("    [ERROR] - FATAL ERROR: Abort NWP event detected\r\n    "
                "AbortType=%d, AbortData=0x%x\n\r",
                slFatalErrorEvent->Data.DeviceAssert.Code,
                slFatalErrorEvent->Data.DeviceAssert.Value);
        }
        break;

    case SL_DEVICE_EVENT_FATAL_DRIVER_ABORT:
        {
            LOG_MESSAGE("    [ERROR] - FATAL ERROR: Driver Abort detected. \n\r");
        }
        break;

    case SL_DEVICE_EVENT_FATAL_NO_CMD_ACK:
        {
            LOG_MESSAGE("    [ERROR] - FATAL ERROR: No Cmd Ack detected\r\n    "
                "[cmd opcode = 0x%x] \n\r",
                slFatalErrorEvent->Data.NoCmdAck.Code);
        }
        break;

    case SL_DEVICE_EVENT_FATAL_SYNC_LOSS:
        {
            LOG_MESSAGE("    [ERROR] - FATAL ERROR: Sync loss detected n\r");
        }
        break;

    case SL_DEVICE_EVENT_FATAL_CMD_TIMEOUT:
        {
            LOG_MESSAGE("    [ERROR] - FATAL ERROR: Async event timeout detected "
                "[event opcode =0x%x]  \n\r",
                slFatalErrorEvent->Data.CmdTimeout.Code);
        }
        break;

    default:
        LOG_MESSAGE("    [ERROR] - FATAL ERROR: Unspecified error detected \n\r");
        break;
    }
}

//*****************************************************************************
//
//! \brief This function handles network events such as IP acquisition, IP
//!           leased, IP released etc.
//!
//! \param[in]  pNetAppEvent - Pointer to NetApp Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
    char msg;
    app_rtsp_task_msg_s  msg_on_rtsp_task_q;
    memset(&msg_on_rtsp_task_q, 0, sizeof(msg_on_rtsp_task_q));
    
    switch(pNetAppEvent->Id)
    {
    case SL_NETAPP_EVENT_IPV4_ACQUIRED:
        LOG_MESSAGE("    [NETAPP EVENT] IP Acquired\r\n    IP=%d.%d.%d.%d , "
            "Gateway=%d.%d.%d.%d\n\r",
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,3),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,2),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,1),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,0),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,3),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,2),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,1),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,0));
        
        sprintf((char *)rtsp_session_config.local_ip_str, "%d.%d.%d.%d",
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,3),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,2),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,1),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,0));
        sprintf((char *)sip_session_config.local_ip_str, "%d.%d.%d.%d",
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,3),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,2),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,1),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,0));
        msg=AppEvent_IP_ACQUIRED;
        mq_send(gProvisioningSMQueue, &msg, 1, 0);

        msg_on_rtsp_task_q.task_id   = ASYNC_EVENT_HANDLER;
        msg_on_rtsp_task_q.msg_id    = RTSP_CONNECTED_TO_NW;
        mq_send(gRtspMainSMQueue, (char *)&msg_on_rtsp_task_q, sizeof(app_rtsp_task_msg_s), 0);
        GPIO_write(LED_A,1);
        SetNetConnected();
        ipaq=1;
        break;

    case SL_NETAPP_EVENT_DHCPV4_LEASED:

        LOG_MESSAGE("    [NETAPP Event] IP Leased\r\n    %d.%d.%d.%d\r\n",
            SL_IPV4_BYTE(pNetAppEvent->Data.IpLeased.IpAddress,3),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpLeased.IpAddress,2),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpLeased.IpAddress,1),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpLeased.IpAddress,0));
        msg=AppEvent_IP_ACQUIRED;
        mq_send(gProvisioningSMQueue, &msg, 1, 0);
        
        SetNetConnected();
        break;

    case SL_NETAPP_EVENT_DHCP_IPV4_ACQUIRE_TIMEOUT:
        LOG_MESSAGE("    [NETAPP Event] IP acquired timeout \r\n");
        break;


    default:
        LOG_MESSAGE("    [NETAPP EVENT] Unhandled event [0x%x] \n\r", pNetAppEvent->Id);
        break;
    }
}

//*****************************************************************************
//
//! \brief This function handles HTTP server events
//!
//! \param[in]  pServerEvent    - Contains the relevant event information
//! \param[in]  pServerResponse - Should be filled by the user with the
//!                               relevant response information
//!
//! \return None
//!
//****************************************************************************
void SimpleLinkHttpServerEventHandler(SlNetAppHttpServerEvent_t *pHttpEvent,
    SlNetAppHttpServerResponse_t *pHttpResponse)
{
    /* Unused in this application */
}

//*****************************************************************************
//
//! \brief This function handles General Events
//!
//! \param[in]  pDevEvent - Pointer to General Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{

    /* Most of the general errors are not FATAL are are to be handled
    appropriately by the application */
    LOG_MESSAGE("    [GENERAL EVENT] - ID=[%d] Sender=[%d]\n\n",
        pDevEvent->Data.Error.Code,
        pDevEvent->Data.Error.Source);
}

//*****************************************************************************
//
//! \brief This function handles socket events indication
//!
//! \param[in]  pSock - Pointer to Socket Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{


    switch( pSock->Event )
    {
    case SL_SOCKET_TX_FAILED_EVENT:
        switch( pSock->SocketAsyncEvent.SockTxFailData.Status)
        {
        case SL_ERROR_BSD_ECLOSE:
            LOG_MESSAGE("    [SOCK ERROR] - close socket (%d) operation\r\n    "
                "Failed to transmit all queued packets\n\r",
                pSock->SocketAsyncEvent.SockTxFailData.Sd);
            break;
        default:
            LOG_MESSAGE("    [SOCK ERROR] - TX FAILED\r\n     socket %d , "
                "reason (%d) \r\n",
                pSock->SocketAsyncEvent.SockTxFailData.Sd,
                pSock->SocketAsyncEvent.SockTxFailData.Status);
            break;
        }
        break;

    default:
        LOG_MESSAGE("    [SOCK EVENT] - Unexpected Event [%x0x]\n\n",pSock->Event);
        break;
    }
}

void SimpleLinkNetAppRequestEventHandler(SlNetAppRequest_t *pNetAppRequest, SlNetAppResponse_t *pNetAppResponse)
{
    /* Unused in this application */
}

void SimpleLinkNetAppRequestMemFreeEventHandler(uint8_t *buffer)
{
    /* Unused in this application */
}

//*****************************************************************************
//                 Local Functions
//*****************************************************************************



//*****************************************************************************
//
//! \brief The interrupt handler for the async-evt timer
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void AsyncEvtTimerHandler(sigval arg)
{
    char msg;
    msg=AppEvent_TIMEOUT;
    mq_send(gProvisioningSMQueue, &msg, 1, 0);
}

//*****************************************************************************
//
//! \brief Call Host Timer
//!
//! \param  Duration in mSec
//!
//! \return none
//!
//*****************************************************************************
void StartAsyncEvtTimer(uint32_t duration)
{
    struct itimerspec value;

    /* Start timer */
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = 0;
    value.it_value.tv_sec = duration;
    value.it_value.tv_nsec = 0;

    timer_settime(gTimer, 0, &value, NULL);
}

//*****************************************************************************
//
//! \brief Stop Timer
//!
//! \param  None
//!
//! \return none
//!
//*****************************************************************************
void StopAsyncEvtTimer(void)
{
    struct itimerspec value;

    /* Stop timer */
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = 0;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = 0;

    timer_settime(gTimer, 0, &value, NULL);
}

//*****************************************************************************
//
//! \brief Error Handler - Print and restart connection
//!
//! \param  None
//!
//! \return none
//!
//*****************************************************************************
int32_t ReportError(void)
{
    LOG_MESSAGE("    [Provisioning] Provisioning Application Error \r\n ");
    


    return 0;
}

//*****************************************************************************
//
//! \brief Restart request - disconnected and open timer to re-open provisioning for non connection
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t ProcessRestartRequest(void)
{

    StartAsyncEvtTimer(CONNECTION_PHASE_TIMEOUT_SEC);

    return 0;
}
//*****************************************************************************
//
//! \brief Get AP security parameters
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t GetSecureStatus(void)
{
    uint8_t sec_type;
    uint16_t len = 1;
    uint16_t  config_opt = SL_WLAN_AP_OPT_SECURITY_TYPE;
    sl_WlanGet(SL_WLAN_CFG_AP_ID, &config_opt, &len, (_u8* )&sec_type);
    if (sec_type == AP_SEC_TYPE)
    {
        return 1;
    }
    return 0;
}

//*****************************************************************************
//
//! \brief Set AP security parameters
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t SetSecuredAP(const uint8_t sec_en)
{
    //set secured AP parameters
     uint8_t  val = AP_SEC_TYPE;
     uint8_t  open = SL_WLAN_SEC_TYPE_OPEN ;
     uint8_t  password[65];
     uint16_t  len = strlen((char *)AP_SEC_PASSWORD);
     if (sec_en)
     {
         sl_WlanSet(SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_SECURITY_TYPE, 1, (uint8_t *)&val);
         memset(password, 0, sizeof(password));
         memcpy(password, (char *)AP_SEC_PASSWORD, len);
         sl_WlanSet(SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_PASSWORD, len, (uint8_t *)password);
         LOG_MESSAGE(" Setting AP secured parameters\n\r");
     }
     else
     {
        sl_WlanSet(SL_WLAN_CFG_AP_ID, SL_WLAN_AP_OPT_SECURITY_TYPE, 1, (uint8_t *)&open);
     }
     return 0;
}

//*****************************************************************************
//
//! \brief  Start connection - wait for connection
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t WaitForConnectionTimerStart(void)
{

    StartAsyncEvtTimer(CONNECTION_PHASE_TIMEOUT_SEC);

    return 0;
}





int32_t HandleProvisioningComplete(void)
{
    uint16_t ConfigOpt = 0;
    uint16_t pConfigLen = sizeof(SlNetCfgIpv4DhcpClient_t);
    SlNetCfgIpv4DhcpClient_t dhcpCl;
    int ret = sl_NetCfgGet(SL_NETCFG_IPV4_DHCP_CLIENT, &ConfigOpt, &pConfigLen, (_u8 *)&dhcpCl);
    if(ret < 0)
    {
        LOG_MESSAGE("Error getting IP address = %d\n", ret);
    }
    else{
        LOG_MESSAGE("IP Acquired: IP=%d.%d.%d.%d , "
            "Gateway=%d.%d.%d.%d\n\r",
            SL_IPV4_BYTE(dhcpCl.Ip,3),
            SL_IPV4_BYTE(dhcpCl.Ip,2),
            SL_IPV4_BYTE(dhcpCl.Ip,1),
            SL_IPV4_BYTE(dhcpCl.Ip,0),
            SL_IPV4_BYTE(dhcpCl.Gateway,3),
            SL_IPV4_BYTE(dhcpCl.Gateway,2),
            SL_IPV4_BYTE(dhcpCl.Gateway,1),
            SL_IPV4_BYTE(dhcpCl.Gateway,0));
    }
    SetNetConnected();
  return 0;
}
//*****************************************************************************
//
//! \brief   Wait for IP, open timer for error
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t WaitForIpTimerStart(void)
{
    StartAsyncEvtTimer(CONNECTION_PHASE_TIMEOUT_SEC * 3);
    return 0;
}

//*****************************************************************************
//
//! \brief  Disconnect received, disconnect and open timer for re-connect
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t WaitForReconnectionTimerStart(void)
{
    StartAsyncEvtTimer(RECONNECTION_ESTABLISHED_TIMEOUT_SEC);
    return 0;
}


//*****************************************************************************
//
//! \brief   Do Nothing
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t DoNothing(void)
{
    return 0;
}

//*****************************************************************************
//
//! \brief   Handle Provisioning timeout, check if connection success
//!          Continue if success, or start over if fail
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t HandleProvisioningTimeOut(void)
{
    char msg;
    /* Connection failed, restart */
    msg=AppEvent_RESTART;
    mq_send(gProvisioningSMQueue, &msg, 1, 0);
    return 0;
}



//*****************************************************************************
//
//! \brief This function configures netapp settings
//!
//! \param[in]  None
//!
//! \return None
//!
//****************************************************************************
void SetNetAppHttp(int32_t *retVal, const uint8_t Option, const uint8_t OptionLen, const uint8_t *pOptionValue)
{
    *retVal =  sl_NetAppSet(SL_NETAPP_HTTP_SERVER_ID, Option, OptionLen, pOptionValue);
    if(*retVal != 0)
    {
        LOG_MESSAGE("HTTP setting error: %d, option: %d \n\r", retVal, Option);
    }
}

//*****************************************************************************
//
//! \brief  This function configures the HTTPS server
//!
//! \param  None
//!
//! \return NetApp error codes or 0 upon success.
//!
//*****************************************************************************
int32_t ConfigureHttpsServer(void)
{
    int32_t retVal = 0;
    uint8_t httpsPort[] = {0xBB, 0x01};  /* 0x1BB = 443 */
    uint8_t secondaryPort[] = {0x50, 0x00}; /* 0x050 = 80 */
    uint8_t secondaryPortEnable[] = {0x1};
    uint8_t securityMode = 1;

    LOG_MESSAGE("ConfigureHttpsServer for secured mode...\n\r");

    /* Set the file names used for SSL key exchange */
    SetNetAppHttp(&retVal,
            SL_NETAPP_HTTP_DEVICE_CERTIFICATE_FILENAME,
            strlen((char *)SSL_SERVER_CERT),
            (const uint8_t *)SSL_SERVER_CERT);

    SetNetAppHttp(&retVal,
            SL_NETAPP_HTTP_PRIVATE_KEY_FILENAME,
            strlen((char *)SSL_SERVER_KEY),
            (const uint8_t *)SSL_SERVER_KEY);


    /* Activate SSL security on primary HTTP port and change it to
     443 (standard HTTPS port) */
    SetNetAppHttp(&retVal,
            SL_NETAPP_HTTP_PRIMARY_PORT_SECURITY_MODE,
            sizeof(securityMode),
            &securityMode);

    SetNetAppHttp(&retVal,
            SL_NETAPP_HTTP_PRIMARY_PORT_NUMBER,
            sizeof(httpsPort),
            httpsPort);

    /* Enable secondary HTTP port (can only be used for redirecting
     connections to the secure primary port) */
    SetNetAppHttp(&retVal,
            SL_NETAPP_HTTP_SECONDARY_PORT_NUMBER,
            sizeof(secondaryPort),
            secondaryPort);

    SetNetAppHttp(&retVal,
            SL_NETAPP_HTTP_SECONDARY_PORT_ENABLE,
            sizeof(secondaryPortEnable),
            secondaryPortEnable);

    if(retVal >= 0)
    {
        retVal = sl_NetAppStop(SL_NETAPP_HTTP_SERVER_ID);
        LOG_MESSAGE("[Provisioning App] HTTP Server Stopped\n\r");
        if(retVal >= 0)
        {
            retVal = sl_NetAppStart(SL_NETAPP_HTTP_SERVER_ID);
            LOG_MESSAGE("[Provisioning App] HTTP Server Re-started\n\r");
        }
    }
    return retVal;
}

//*****************************************************************************
//
//! \brief  This function handles init-complete event from SL
//!
//! \param  status - Mode the device is configured or error if initialization failed
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkInitCallback(uint32_t status, SlDeviceInitInfo_t *DeviceInitInfo)
{
    char msg;
    if ((int32_t)status == SL_ERROR_RESTORE_IMAGE_COMPLETE)
    {
        LOG_MESSAGE("\r\n******\r\nReturn to Factory Default been Completed\r\nPlease RESET the Board\r\n******\r\n");
        gLedDisplayState = LedState_FACTORY_DEFAULT;
        while(1);
    }



    
    LOG_MESSAGE("    [NWP] Device started in %s role\n\r", (0 == status) ? "Station" :\
        ((2 == status) ? "AP" : "P2P"));

    if(ROLE_STA == status)
    {
        gRole = ROLE_STA;

    }
    else if (ROLE_AP == status)
    {
        gRole = ROLE_AP;
    }
    else
    {
        msg=AppEvent_ERROR;
        mq_send(gProvisioningSMQueue, &msg, 1, 0);
        
    }
    SetNWPActive(status);
}

//*****************************************************************************
//
//! \brief  In case external confirmation (cloud) failed, need to abort
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
void _AbortProvExternalConfirmation(void)
{
    int16_t retVal;

    LOG_MESSAGE("Aborting provisioning external confirmation...\r\n");
    retVal = sl_WlanProvisioning(SL_WLAN_PROVISIONING_CMD_ABORT_EXTERNAL_CONFIRMATION, 0, 0, NULL, 0);
    LOG_MESSAGE("Abort retVal=%d\r\n", retVal);
}

//*****************************************************************************
//
//! \brief  Delete all profiles and return to factory default
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t returnToFactoryDefault(void)
{
    char msg;
    int32_t slRetVal;
    SlFsRetToFactoryCommand_t RetToFactoryCommand;
    int32_t ExtendedError;
    unsigned char event;
    Provisioning_TableEntry_t   *pEntry = NULL;

    RetToFactoryCommand.Operation = SL_FS_FACTORY_RET_TO_DEFAULT;
    slRetVal = sl_FsCtl( (SlFsCtl_e)SL_FS_CTL_RESTORE, 0, NULL , (uint8_t *)&RetToFactoryCommand , sizeof(SlFsRetToFactoryCommand_t), NULL, 0 , NULL );

    /* if command was not executed because provisioning is currently running, send
     * PRVISIONING_STOP command, and wait for the PROVISIONING_STOPPED event
     * (which confirms that the provisioning process was successfully stopped)
     * before trying again */

    if(SL_RET_CODE_PROVISIONING_IN_PROGRESS == slRetVal)
    {
        LOG_MESSAGE("    [ERROR] Provisioning is already running, stopping current session...\r\n");
        gStopInProgress = 1;
        slRetVal = sl_WlanProvisioning(SL_WLAN_PROVISIONING_CMD_STOP,0 , 0, NULL, (uint32_t)NULL);
        ASSERT_ON_ERROR(slRetVal);

        while(gStopInProgress)
        {
            mq_receive(gProvisioningSMQueue, (char *)&event, 1, NULL);

            StopAsyncEvtTimer();

            /* Find Next event entry */
            pEntry = (Provisioning_TableEntry_t *)&gTransitionTable[g_CurrentState][event];

            if (NULL != pEntry->p_evtHndl)
            {
                if (pEntry->p_evtHndl() < 0)
                {
                    LOG_MESSAGE("Event handler failed..!! \n\r");
                    while(1);
                }
            }

            /* Change state according to event */
            if (pEntry->nextState != g_CurrentState)
            {
                g_CurrentState = pEntry->nextState;
            }
        }
        msg=AppEvent_RESTART;
        mq_send(gProvisioningSMQueue, &msg, 1, 0);

        slRetVal = sl_FsCtl( (SlFsCtl_e)SL_FS_CTL_RESTORE, 0, NULL , (uint8_t *)&RetToFactoryCommand , sizeof(SlFsRetToFactoryCommand_t), NULL, 0 , NULL );
        ASSERT_ON_ERROR(slRetVal);
    }
    if ((_i32)slRetVal < 0)
    {
        /* Pay attention, for this function the slRetVal is composed from Signed RetVal & extended error */
        slRetVal = (_i16)slRetVal>> 16;
        ExtendedError = (_u16)slRetVal& 0xFFFF;
        return ExtendedError;
    }
    /* Reset */
    sl_Stop(SL_STOP_TIMEOUT);
    UnsetNWPActive();
    gRole = sl_Start(NULL, NULL, NULL);
    SetNWPActive(gRole);



    return slRetVal;
}

//*****************************************************************************
//
//! \brief  Start Provisioning example
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
int32_t ProvisioningStart(void)
{
    char msg;
    int32_t  retVal = 0;
    int32_t  status = 0;
    uint8_t  configOpt = 0;
    uint16_t configLen = 0;
    uint8_t  simpleLinkMac[SL_MAC_ADDR_LEN];
    uint16_t macAddressLen;
    uint8_t  provisioningCmd;
    SlDeviceVersion_t ver = {0};

    LOG_MESSAGE("\n\r\n\r\n\r==================================\n\r");
    LOG_MESSAGE(" Provisioning Start Ver. %s\n\r",APPLICATION_VERSION);
    LOG_MESSAGE("==================================\n\r");

    if (HTTPS_ENABLE)
    {
        ConfigureHttpsServer();
    }
    status = GetSecureStatus();
    if ( (SECURED_AP_ENABLE) || (status != SECURED_AP_ENABLE))
    /*Will enter only if previous and present Secure AP are different or Secure AP is 1*/ 
    {
        SetSecuredAP(SECURED_AP_ENABLE);
        if (ROLE_AP == gRole)
        {
            /* Reset device to start secured AP */
            sl_Stop(SL_STOP_TIMEOUT);
            UnsetNWPActive();
            gRole = sl_Start(NULL, NULL, NULL);
            SetNWPActive(gRole);
        }
    }

    /* Get device's info */
    configOpt = SL_DEVICE_GENERAL_VERSION;
    configLen = sizeof(ver);
    retVal = sl_DeviceGet(SL_DEVICE_GENERAL, &configOpt, &configLen, (uint8_t *)(&ver));

    if(SL_RET_CODE_PROVISIONING_IN_PROGRESS == retVal)
    {
        LOG_MESSAGE(" [ERROR] Provisioning is already running, stopping current session...\r\n");
        msg=AppEvent_ERROR;
        mq_send(gProvisioningSMQueue, &msg, 1, 0);

        return 0;
    }

    LOG_MESSAGE("\n\r CHIP %d\r\n MAC  31.%d.%d.%d.%d\r\n PHY  %d.%d.%d.%d\r\n NWP  %d.%d.%d.%d\r\n ROM  %d\r\n HOST %d.%d.%d.%d\r\n",
        ver.ChipId,
        ver.FwVersion[0],ver.FwVersion[1],
        ver.FwVersion[2],ver.FwVersion[3],
        ver.PhyVersion[0],ver.PhyVersion[1],
        ver.PhyVersion[2],ver.PhyVersion[3],
        ver.NwpVersion[0],ver.NwpVersion[1],ver.NwpVersion[2],ver.NwpVersion[3],
        ver.RomVersion,
        SL_MAJOR_VERSION_NUM,SL_MINOR_VERSION_NUM,SL_VERSION_NUM,SL_SUB_VERSION_NUM);

    macAddressLen = sizeof(simpleLinkMac);
    sl_NetCfgGet(SL_NETCFG_MAC_ADDRESS_GET,NULL,&macAddressLen,(unsigned char *)simpleLinkMac);
    LOG_MESSAGE(" MAC address: %x:%x:%x:%x:%x:%x\r\n\r\n",simpleLinkMac[0],simpleLinkMac[1],simpleLinkMac[2],simpleLinkMac[3],simpleLinkMac[4],simpleLinkMac[5]);

    /* start provisioning */
    provisioningCmd = SL_WLAN_PROVISIONING_CMD_START_MODE_APSC;

    if(provisioningCmd <= SL_WLAN_PROVISIONING_CMD_START_MODE_APSC_EXTERNAL_CONFIGURATION)
    {
        LOG_MESSAGE("\r\n Starting Provisioning! mode=%d (0-AP, 1-SC, 2-AP+SC, 3-AP+SC+WAC)\r\n\r\n",provisioningCmd);
    }
    else
    {
        LOG_MESSAGE("\r\n Provisioning Command = %d \r\n\r\n",provisioningCmd);
    }



    if( SC_KEY_ENABLE)
    {
        uint8_t key[SL_WLAN_SMART_CONFIG_KEY_LENGTH];

        memcpy(key, (char *)SC_KEY, SL_WLAN_SMART_CONFIG_KEY_LENGTH);
        retVal = sl_WlanProvisioning(provisioningCmd, ROLE_STA, PROVISIONING_INACTIVITY_TIMEOUT,(char *)&key, (uint32_t)NULL);
    }
    else
    {
        retVal = sl_WlanProvisioning(provisioningCmd, ROLE_STA, PROVISIONING_INACTIVITY_TIMEOUT, NULL, (uint32_t)NULL);
    }
    if(retVal < 0)
    {
        LOG_MESSAGE(" Provisioning Command Error, num:%d\r\n",retVal);
    }

    return 0;
}

//*****************************************************************************
//
//! \brief  Start the main task, initialize SimpleLink device
//!
//! \param  None
//!
//! \return None
//!
//*****************************************************************************
void * ProvisioningTask(void *arg)
{

    unsigned char      event;
    Provisioning_TableEntry_t         *pEntry = NULL;


    sigevent           sev;
    pthread_attr_t     timerThreadAttr;

    extern _SlDriverCb_t* g_pCB;
    
    
    
    pthread_attr_init(&timerThreadAttr);
    timerThreadAttr.stacksize = TIMER_TASK_STACK_SIZE;

    /* Create Timer */
    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_function = &AsyncEvtTimerHandler;
    sev.sigev_notify_attributes = &timerThreadAttr;

    timer_create(CLOCK_MONOTONIC, &sev, &gTimer);
    
    ipaq=0;
    int iRetVal = 0;
    /* Initialize Simple Link */
    if((iRetVal =
            sl_Start(NULL, NULL,NULL) < 0))
    {
        LOG_MESSAGE("sl_Start Failed\r\n");
        if (iRetVal == SL_ERROR_RESTORE_IMAGE_COMPLETE)
        {
            LOG_MESSAGE(
                "\r\n**********************************\r\n"
                "Return to Factory Default been Completed\r\n"
                "Please RESET the Board\r\n"
                "**********************************\r\n");
            gLedDisplayState = LedState_FACTORY_DEFAULT;
        }
        while(1)
        {
            ;
        }
    }
    sl_WlanPolicySet(SL_WLAN_POLICY_CONNECTION,SL_WLAN_CONNECTION_POLICY(1,1,0,0),NULL,0);
    sl_Stop(200);
    if((iRetVal =
            sl_Start(NULL, NULL, (P_INIT_CALLBACK)SimpleLinkInitCallback)) < 0)
    {
        LOG_MESSAGE("sl_Start Failed\r\n");
    }


    
//
//    while(WaitNWPActive(555)!=ROLE_STA){
//        usleep(10000);
//    }
//    usleep(100000);
    
    
   // sl_WlanProfileDel(SL_WLAN_DEL_ALL_PROFILES);
    
//    _i16 Index=4, Status;
//    signed char Name[32];
//    _i16 NameLength;
//    unsigned char MacAddr[6];
//    SlWlanSecParams_t SecParams;
//    SlWlanGetSecParamsExt_t SecExtParams;
//    _u32 Priority;
    //Manually add a profile for debugging
//    Status= sl_WlanProfileGet(Index, Name, &NameLength, MacAddr, &SecParams, &SecExtParams,&Priority);
//    if(Status==SL_ERROR_WLAN_GET_PROFILE_INVALID_INDEX){
//        SecParams.Type = SL_WLAN_SEC_TYPE_WPA_WPA2;
//        SecParams.Key = "wifikeyhere";
//        SecParams.KeyLen = strlen (SecParams.Key);
//        Index = sl_WlanProfileAdd("wifissidhere", strlen("wifissidhere"), 0, &SecParams, NULL, 7 , 0);
//        LOG_MESSAGE("Added Profile %d \n\r",Index);
//    }
    


    g_CurrentState = AppState_IDLE;
      
    while(1)
    {
        mq_receive(gProvisioningSMQueue, (char *)&event, 1, NULL);

        StopAsyncEvtTimer();

        /* Find Next event entry */
        pEntry = (Provisioning_TableEntry_t *)&gTransitionTable[g_CurrentState][event];

        if (NULL != pEntry->p_evtHndl)
        {
            if (pEntry->p_evtHndl() < 0)
            {
                LOG_MESSAGE("Event handler failed..!! \n\r");
                while(1);
            }
        }

        /* Change state acording to event */
        if (pEntry->nextState != g_CurrentState)
        {
            g_CurrentState = pEntry->nextState;
        }
    }
}

//*****************************************************************************
//
//! \brief  Application startup display on UART
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
static void DisplayBanner(char * AppName)
{
    LOG_MESSAGE("\n\n\n\r");
    LOG_MESSAGE("******************%s Application******************\n\r", AppName);
    LOG_MESSAGE("\n\n\n\r");
}




//*****************************************************************************
//
//! \brief  Main application thread
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void * mainThread( void *arg )
{
    uint32_t             iRetVal;
    
    pthread_attr_t      pAttrs_spawn; 
    pthread_attr_t      pAttrs_prov;
    pthread_attr_t      pAttrs_rtsp_main_task;
    pthread_attr_t      pAttrs_rtsp_server_task;
    pthread_attr_t      pAttrs_sip_server_task;
    pthread_attr_t      pAttrs_v_tx_task;
    
    mq_attr      mq_Attrs_prov;
    mq_attr      mq_pAttrs_rtsp_main_task;
    mq_attr      mq_pAttrs_rtsp_server_task;
    mq_attr      mq_pAttrs_sip_server_task;
    mq_attr      mq_pAttrs_a_tx_task;
    mq_attr      mq_pAttrs_a_rx_task;
    mq_attr      mq_pAttrs_v_tx_task;
    
    struct timespec     ts = {0};

    struct sched_param  priParam;
//UDMAInit();
    GPIO_init();
    SPI_init();
    Timer_init();
    //I2C_init();
    //I2S_init();
    //PWM_init();
    //UART_init();
    
    //ti_net_SlNet_initConfig();
    pinmux_config();
    //I2C_IF_Open(1);

//
    //Enable Hysterisis
     
    HWREG(0x4402E144) = HWREG(0x4402e144) | (3 << 2);

    
 //   cc_gpio_configure();


    
//    cc_gpio_config_high(GPIO_POWER_EN_2);
//    cc_gpio_config_high(GPIO_POWER_EN_1);
//    cc_gpio_config_low(GPIO_POWER_EN_2);
//    cc_gpio_config_low(GPIO_POWER_EN_1);
//    cc_gpio_config_high(GPIO_OV_RST);
    //cc_gpio_config_low(GPIO_TI_TO_OV);
    GPIO_write(CONFIG_GPIO_0,0);
GPIO_write(GPIO_TI_TO_OV,0);
GPIO_write(LED_A,0);
GPIO_write(LED_B,0);
    pthread_mutex_init(&net_conn_lock, (const pthread_mutexattr_t *)NULL);
    pthread_mutex_init(&nwp_active_lock, (const pthread_mutexattr_t *)NULL);
    pthread_mutex_init(&frame_lock, (const pthread_mutexattr_t *)NULL);
    pthread_cond_init(&net_conn_cond, NULL);
    pthread_cond_init(&nwp_active_cond, NULL);
    
    /* Initial Terminal, and print Application name */
    InitTerm();
    /* initilize the realtime clock */
    clock_settime(CLOCK_REALTIME, &ts);
    DisplayBanner(APPLICATION_NAME);
    
    UnsetNWPActive();
    UnsetNetConnected();



        /* Create message queue */
    mq_Attrs_prov.mq_curmsgs = 0;
    mq_Attrs_prov.mq_flags = 0;
    mq_Attrs_prov.mq_maxmsg = 10;
    mq_Attrs_prov.mq_msgsize = sizeof( unsigned char );
    gProvisioningSMQueue = mq_open("SMQueue", O_CREAT, 0, &mq_Attrs_prov);


    mq_pAttrs_rtsp_main_task.mq_curmsgs = 0;
    mq_pAttrs_rtsp_main_task.mq_flags = 0;
    mq_pAttrs_rtsp_main_task.mq_maxmsg = 10;
    mq_pAttrs_rtsp_main_task.mq_msgsize = sizeof(app_rtsp_task_msg_s);
    gRtspMainSMQueue = mq_open("RtspMainSMQueue", O_CREAT, 0, &mq_pAttrs_rtsp_main_task);

    mq_pAttrs_rtsp_server_task.mq_curmsgs = 0;
    mq_pAttrs_rtsp_server_task.mq_flags = 0;
    mq_pAttrs_rtsp_server_task.mq_maxmsg = 10;
    mq_pAttrs_rtsp_server_task.mq_msgsize = sizeof(app_rtsp_srv_task_msg_s);
    gRtspSrvQueue = mq_open("RtspSrvQueue", O_CREAT, 0, &mq_pAttrs_rtsp_server_task);
    
    mq_pAttrs_sip_server_task.mq_curmsgs = 0;
    mq_pAttrs_sip_server_task.mq_flags = 0;
    mq_pAttrs_sip_server_task.mq_maxmsg = 10;
    mq_pAttrs_sip_server_task.mq_msgsize = sizeof(app_sip_srv_task_msg_s);
    gSipSrvQueue = mq_open("SipSrvQueue", O_CREAT, 0, &mq_pAttrs_sip_server_task);
    
    mq_pAttrs_a_tx_task.mq_curmsgs = 0;
    mq_pAttrs_a_tx_task.mq_flags = 0;
    mq_pAttrs_a_tx_task.mq_maxmsg = 10;
    mq_pAttrs_a_tx_task.mq_msgsize = sizeof(a_task_msg_s);
    gATxSMQueue = mq_open("ATxSMQueue", O_CREAT, 0, &mq_pAttrs_a_tx_task);
    mq_pAttrs_a_rx_task.mq_curmsgs = 0;
    mq_pAttrs_a_rx_task.mq_flags = 0;
    mq_pAttrs_a_rx_task.mq_maxmsg = 10;
    mq_pAttrs_a_rx_task.mq_msgsize = sizeof(a_task_msg_s);
    gARxSMQueue = mq_open("ARxSMQueue", O_CREAT, 0, &mq_pAttrs_a_rx_task);
    
    mq_pAttrs_v_tx_task.mq_curmsgs = 0;
    mq_pAttrs_v_tx_task.mq_flags = 0;
    mq_pAttrs_v_tx_task.mq_maxmsg = 20;
    mq_pAttrs_v_tx_task.mq_msgsize = sizeof(v_task_msg_s);
    gVTxSMQueue = mq_open("VTxSMQueue", O_CREAT, 0, &mq_pAttrs_v_tx_task);
    
    
    
    /* create the sl_Task */
    pthread_attr_init(&pAttrs_spawn);
    priParam.sched_priority = 1;//SPAWN_TASK_PRIORITY;
    iRetVal = pthread_attr_setschedparam(&pAttrs_spawn, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_spawn, TASK_STACK_SIZE+1024);
    if(iRetVal)
      while(1);
    iRetVal = pthread_create(&gSpawnThread, &pAttrs_spawn, sl_Task, NULL);
    if(iRetVal)
      while(1);
    
    /* create the Provisioning Task */
    pthread_attr_init(&pAttrs_prov);
    priParam.sched_priority = 1;//1
    iRetVal = pthread_attr_setschedparam(&pAttrs_prov, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_prov, 1224);//TASK_STACK_SIZE
    if(iRetVal)while(1);
    iRetVal = pthread_create(&gProvisioningThread, &pAttrs_prov, ProvisioningTask, NULL);
    if(iRetVal)while(1);
    
    
    pthread_attr_init(&pAttrs_rtsp_main_task);
    priParam.sched_priority = 1;//4
    iRetVal = pthread_attr_setschedparam(&pAttrs_rtsp_main_task, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_rtsp_main_task, 1600);//2048
    iRetVal |= pthread_attr_setdetachstate(&pAttrs_rtsp_main_task,PTHREAD_CREATE_DETACHED);
    if(iRetVal)
      while(1);
    iRetVal = pthread_create(&gRtspMainTaskThread, &pAttrs_rtsp_main_task, RtspMainTask, NULL);
    if(iRetVal)
      while(1);

    pthread_attr_init(&pAttrs_rtsp_server_task);
    priParam.sched_priority = 1;//3
    iRetVal = pthread_attr_setschedparam(&pAttrs_rtsp_server_task, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_rtsp_server_task, 1224);//2048
    iRetVal |= pthread_attr_setdetachstate(&pAttrs_rtsp_server_task,PTHREAD_CREATE_DETACHED);
    if(iRetVal)
      while(1);
    iRetVal = pthread_create(&gRtspServerTaskThread, &pAttrs_rtsp_server_task, RtspServerTask, NULL);
    if(iRetVal)
      while(1);
    
    pthread_attr_init(&pAttrs_sip_server_task);
    priParam.sched_priority = 1;//3
    iRetVal = pthread_attr_setschedparam(&pAttrs_sip_server_task, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_sip_server_task, 1224);//2048
    iRetVal |= pthread_attr_setdetachstate(&pAttrs_sip_server_task,PTHREAD_CREATE_DETACHED);
    if(iRetVal)
      while(1);
    iRetVal = pthread_create(&gSipServerTaskThread, &pAttrs_sip_server_task, SipServerTask, NULL);
    if(iRetVal)
      while(1);
    
    
    
#ifdef START_AUDIO
    pthread_attr_init(&pAttrs_a_tx_task);
    priParam.sched_priority = 1;//2
    iRetVal = pthread_attr_setschedparam(&pAttrs_a_tx_task, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_a_tx_task, 1224);
    iRetVal |= pthread_attr_setdetachstate(&pAttrs_a_tx_task,PTHREAD_CREATE_DETACHED);
    if(iRetVal)
      while(1);
    iRetVal = pthread_create(&gATxTaskThread, &pAttrs_a_tx_task, ATxTask, NULL);
    if(iRetVal)
      while(1);

    
    pthread_attr_init(&pAttrs_a_rx_task);
    priParam.sched_priority = 1;//2
    iRetVal = pthread_attr_setschedparam(&pAttrs_a_rx_task, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_a_rx_task, 1224);
    iRetVal |= pthread_attr_setdetachstate(&pAttrs_a_rx_task,PTHREAD_CREATE_DETACHED);
    if(iRetVal)
      while(1);
    iRetVal = pthread_create(&gATxTaskThread, &pAttrs_a_rx_task, ARxTask, NULL);
    if(iRetVal)
      while(1);
#endif
    
    pthread_attr_init(&pAttrs_v_tx_task);
    priParam.sched_priority = 2;//2
    iRetVal = pthread_attr_setschedparam(&pAttrs_v_tx_task, &priParam);
    iRetVal |= pthread_attr_setstacksize(&pAttrs_v_tx_task, 1900);
    iRetVal |= pthread_attr_setdetachstate(&pAttrs_v_tx_task,PTHREAD_CREATE_DETACHED);
    if(iRetVal)while(1);
    iRetVal = pthread_create(&gVTxTaskThread, &pAttrs_v_tx_task, VTxTask, NULL);
    if(iRetVal)while(1);
    


    return(0);
}
/* This generated function must be called after the network stack(s) are
 * initialized.
 */
int32_t ti_net_SlNet_initConfig() {
    int32_t     status = 0;
    static bool slNetInitialized = false;

    if (slNetInitialized == false) {
        slNetInitialized = true;

        status = SlNetIf_init(0);

        if (status == 0) {
            status = SlNetSock_init(0);
        }

        if (status == 0) {
            SlNetUtil_init(0);
        }


        /* add CONFIG_SLNET interface */
        if (status == 0) {
            status = SlNetIf_add(SLNETIF_ID_1, "CC32xx",
                    (const SlNetIf_Config_t *)&SlNetIfConfigWifi,
                    5);
        }

    }
    return (status);
}
