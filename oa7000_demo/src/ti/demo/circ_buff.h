//*****************************************************************************
// circ_buffer.h
//
// APIs for Implementing circular buffer
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#ifndef __CIRCULAR_BUFFER_API_H__
#define __CIRCULAR_BUFFER_API_H__
//#include "cc_pal_app.h"
//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif
  
/****************************************************************************/
/*				TYPEDEFS										*/
/****************************************************************************/
/*!
 * Circular buffer element
 */
typedef struct a_circular_buffer
{
    uint8_t   *p_read;
    uint8_t   *p_write;
    uint8_t   *p_start;
    uint8_t   *p_end;
    uint32_t  buffer_size;

}circular_buffer_s;

#ifndef TRUE
#define TRUE                    1
#endif

#ifndef FALSE
#define FALSE                   0
#endif

//*****************************************************************************
//
// Define a boolean type, and values for true and false.
//
//*****************************************************************************
typedef unsigned int tboolean;

/****************************************************************************/
/*		        FUNCTION PROTOTYPES							*/
/****************************************************************************/
/*!
 * Creates circular buffer for audio
 */
circular_buffer_s *
create_circular_buffer(uint32_t const buffer_size);

circular_buffer_s *
create_circular_buffer_static(uint8_t *buffer, uint32_t buffer_size);
/*!
 * Fills the buffer w/ audio data
 */
int32_t
fill_buffer_2o4_ulaw(circular_buffer_s *p_circular_buffer, \
  uint8_t *p_buffer, uint32_t dest_pkt_size);

int32_t
fill_buffer_2o4(circular_buffer_s *p_circular_buffer, \
  uint8_t *p_buffer, uint32_t dest_pkt_size);

int32_t
fill_buffer(circular_buffer_s *p_circular_buffer,\
            uint8_t *p_buffer, uint32_t pkt_size);
/*!
 * Pulls data from audio buffer
 */
int32_t
pull_buffer_4o2_ulaw(circular_buffer_s *p_circular_buffer, \
  uint8_t *p_buffer, uint32_t src_pkt_size);

int32_t
pull_buffer_4o2(circular_buffer_s *p_circular_buffer, \
  uint8_t *p_buffer, uint32_t src_pkt_size);

int32_t
pull_buffer(circular_buffer_s *p_circular_buffer,\
            uint8_t *p_buffer, uint32_t pkt_size);
/*!
 * Checks if the buffer has space for a copy
 */
uint8_t
is_buffer_vacant(circular_buffer_s *p_circular_buffer, uint32_t size);

/*!
 * Gets the buffer size
 */
uint32_t
get_buffer_fill_level(circular_buffer_s *p_circular_buffer);

/*!
 * Updates the 'write' pointer
 */
void
update_write_ptr(circular_buffer_s *p_circular_buffer, uint32_t pkt_size);

/*!
 * Updates the 'read' pointer
 */
void
update_read_ptr(circular_buffer_s *p_circular_buffer, uint32_t size);

/*!
 * Checks if the buffer is empty
 */
uint8_t
is_buffer_empty(circular_buffer_s *p_circular_buffer);

/*!
 * Checks if the buffer is already filled
 */
uint8_t
is_buffer_filled(circular_buffer_s *p_circular_buffer, uint32_t size);

/*!
 * Gets the 'read' pointer
 */
inline uint8_t*
get_read_ptr(circular_buffer_s *p_circular_buffer);

/*!
 * Gets the 'write' pointer
 */
inline uint8_t*
get_write_ptr(circular_buffer_s *p_circular_buffer);


#ifdef __cplusplus
}
#endif

#endif // __CIRCULAR_BUFFER_API_H__

