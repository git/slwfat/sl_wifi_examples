/*
 * Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <ti/drivers/net/wifi/simplelink.h>
#include "fs_if.h"

/*!
 */
int32_t sl_OpenFile(uint8_t *fName, int32_t *fHdl)
{

    int32_t  retVal = -1;
    _u32 MasterToken = 0;
    retVal = sl_FsOpen(fName, SL_FS_READ, &MasterToken);
    *fHdl=retVal;
    
    return retVal;
}

/*!
 */
int32_t sl_GetFileSize(uint8_t *fName, uint32_t *fSize)
{
    SlFsFileInfo_t fInfo = {0};


    int32_t retVal   = -1;

    retVal = sl_FsGetInfo(fName, 0, &fInfo);
    if(retVal >= 0)
    {
        *fSize = fInfo.Len;
    }

    return retVal;
}

/*!
 */
int32_t sl_GetData(int32_t fHdl, uint8_t *pBuff,
                uint32_t length, uint32_t offset)
{
    int32_t retVal = -1;

    retVal = sl_FsRead(fHdl, offset, pBuff, length);
    return  retVal;
}

/*!
 */
int32_t sl_CloseFile(int32_t fHdl)
{
    int32_t retVal = -1;

    retVal = sl_FsClose(fHdl, 0, 0, 0);
    return retVal;
}
