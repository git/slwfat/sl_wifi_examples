//*****************************************************************************
// circ_buffer.c
//
// APIs for Implementing circular buffer
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

//*****************************************************************************
//
//! \addtogroup wifi_audio_app
//! @{
//
//*****************************************************************************

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <g711.h>

//App include
#include "circ_buff.h"
/**/
circular_buffer_s *
create_circular_buffer(uint32_t const buffer_size)
{
    circular_buffer_s   *p_temp = NULL;

    p_temp = (circular_buffer_s *)malloc(sizeof(circular_buffer_s));
    if(NULL == p_temp) return NULL;

    p_temp->p_start     = (uint8_t *)calloc(buffer_size,sizeof(char));
    p_temp->p_read      = p_temp->p_start;
    p_temp->p_write     = p_temp->p_start;
    p_temp->buffer_size = buffer_size;
    p_temp->p_end       = (p_temp->p_start + buffer_size);

    return (p_temp);
}

circular_buffer_s *
create_circular_buffer_static(uint8_t *buffer, uint32_t buffer_size)
{
    circular_buffer_s   *p_temp = NULL;

    p_temp = (circular_buffer_s *)malloc(sizeof(circular_buffer_s));
    if(NULL == p_temp) return NULL;

    p_temp->p_start     = buffer;
    p_temp->p_read      = p_temp->p_start;
    p_temp->p_write     = p_temp->p_start;
    p_temp->buffer_size = buffer_size;
    p_temp->p_end       = (p_temp->p_start + buffer_size);

    return (p_temp);
}

/**/
uint8_t*
get_read_ptr(circular_buffer_s *p_circular_buffer)
{
    return (p_circular_buffer->p_read);
}

/**/
uint8_t*
get_write_ptr(circular_buffer_s *p_circular_buffer)
{
    return (p_circular_buffer->p_write);
}


/**/
int32_t
fill_buffer(circular_buffer_s *p_circular_buffer, uint8_t *p_buffer, uint32_t dest_pkt_size)
{
    int32_t offset = -1;
    offset = ( (p_circular_buffer->p_write + dest_pkt_size) -\
                p_circular_buffer->p_end);

    if(is_buffer_vacant(p_circular_buffer, p_circular_buffer->buffer_size - dest_pkt_size))
    {
        if(offset <= 0)
        {
            memcpy(get_write_ptr(p_circular_buffer), p_buffer, dest_pkt_size);
        }
        else
        {
            memcpy(get_write_ptr(p_circular_buffer), p_buffer, (dest_pkt_size - offset));
            memcpy(p_circular_buffer->p_start, ((p_buffer + dest_pkt_size) - offset), offset);
        }

        update_write_ptr(p_circular_buffer, dest_pkt_size);
        return dest_pkt_size;
    }

    return -1;
}

int32_t
fill_buffer_2o4(circular_buffer_s *p_circular_buffer, uint8_t *p_buffer, uint32_t dest_pkt_size)
{

    int32_t dest_pkt_elem = dest_pkt_size/2;
    int32_t cbuff_elem = p_circular_buffer->buffer_size/2;

    uint16_t* cbuff_start_16 = (uint16_t *)p_circular_buffer->p_start;
    uint16_t* cbuff_end_16 = (uint16_t *)p_circular_buffer->p_end;
    uint16_t* cbuff_writep_16 = (uint16_t *)p_circular_buffer->p_write;
    uint16_t* cbuff_writeendp_16;
    
    uint16_t* src_buff_16 = (uint16_t *)p_buffer;
      
    if(is_buffer_vacant(p_circular_buffer, p_circular_buffer->buffer_size - dest_pkt_size))
    {
        cbuff_writeendp_16 = cbuff_writep_16 + dest_pkt_elem;
        
        if(cbuff_writeendp_16 <= cbuff_end_16)
        {


            while (cbuff_writep_16 < cbuff_writeendp_16){

                *cbuff_writep_16 = *src_buff_16;
                cbuff_writep_16++;
                src_buff_16 += 2;
            }
        }
        else
        {
            cbuff_writeendp_16 -= cbuff_elem;

            while (cbuff_writep_16 < cbuff_end_16){
                *cbuff_writep_16 = *src_buff_16;
                cbuff_writep_16++;
                src_buff_16 += 2;
            }
            cbuff_writep_16=cbuff_start_16;
            while (cbuff_writep_16<cbuff_writeendp_16){
                *cbuff_writep_16 = *src_buff_16;
                cbuff_writep_16++;
                src_buff_16 += 2;
            }
        }

        update_write_ptr(p_circular_buffer, dest_pkt_size);
        return dest_pkt_size;
    }

    return -1;
}

int32_t
fill_buffer_2o4_ulaw(circular_buffer_s *p_circular_buffer, uint8_t *p_buffer, uint32_t dest_pkt_size)
{

    int32_t dest_pkt_elem = dest_pkt_size;//number of elements in destination
    int32_t cbuff_elem = p_circular_buffer->buffer_size;

    
    uint8_t* cbuff_start_8 = (uint8_t *)p_circular_buffer->p_start;
    uint8_t* cbuff_end_8 = (uint8_t *)p_circular_buffer->p_end;
    uint8_t* cbuff_writep_8 = (uint8_t *)p_circular_buffer->p_write;
    uint8_t* cbuff_writeendp_8;
    
    uint16_t* src_buff_16 = (uint16_t *)p_buffer;
      
    if(is_buffer_vacant(p_circular_buffer, p_circular_buffer->buffer_size - dest_pkt_size))
    {
        cbuff_writeendp_8 = cbuff_writep_8 + dest_pkt_elem;
        
        if(cbuff_writeendp_8 <= cbuff_end_8)
        {

            while (cbuff_writep_8 < cbuff_writeendp_8){
                *cbuff_writep_8 = (unsigned char)linear2ulaw(*src_buff_16-4700);
                cbuff_writep_8++;
                src_buff_16 += 4;
            }
        }
        else
        {
            cbuff_writeendp_8 -= cbuff_elem;

            while (cbuff_writep_8 < cbuff_end_8){
                *cbuff_writep_8 = (unsigned char)linear2ulaw(*src_buff_16-4700);
                cbuff_writep_8++;
                src_buff_16 += 4;
            }

            cbuff_writep_8=cbuff_start_8;
            while (cbuff_writep_8<cbuff_writeendp_8){
                *cbuff_writep_8 = (unsigned char)linear2ulaw(*src_buff_16-4700);
                cbuff_writep_8++;
                src_buff_16 += 4;
            }
        }

        update_write_ptr(p_circular_buffer, dest_pkt_size);
        return dest_pkt_size;
    }

    return -1;
}

int32_t
pull_buffer(circular_buffer_s *p_circular_buffer, uint8_t *p_buffer, uint32_t pkt_size)
{
    int32_t offset = -1;
    offset = ( (p_circular_buffer->p_read + pkt_size) - p_circular_buffer->p_end);

    if(get_buffer_fill_level(p_circular_buffer) >= pkt_size)
    {
        if(offset <= 0)
        {
            memcpy(p_buffer, get_read_ptr(p_circular_buffer), pkt_size);
        }
        else
        {
            memcpy(p_buffer, get_read_ptr(p_circular_buffer), (pkt_size - offset));
            memcpy(((p_buffer + pkt_size) - offset), p_circular_buffer->p_start, offset);
        }

        update_read_ptr(p_circular_buffer, pkt_size);
        return pkt_size;
    }

    return -1;
}

int32_t
pull_buffer_4o2(circular_buffer_s *p_circular_buffer, uint8_t *p_buffer, uint32_t src_pkt_size)
{
    int32_t offset = -1;
    int i,j;
    int fra;
    uint8_t* cbuff_rp = get_read_ptr(p_circular_buffer);
    
    
    offset = ( (p_circular_buffer->p_read + src_pkt_size) - p_circular_buffer->p_end);

    if(get_buffer_fill_level(p_circular_buffer) >= src_pkt_size)
    {
       
        if(offset <= 0)
        {
            i=0;j=0;
            while (i<src_pkt_size){
                *((unsigned short *)(&p_buffer[j])) = *((unsigned short *)(&cbuff_rp[i]));
                i+=2; j+=4;
            }
        }
        else
        {
            i=0;j=0;
            fra=(src_pkt_size - offset);
            while (i<fra){
                *((unsigned short *)(&p_buffer[j])) = *((unsigned short *)(&cbuff_rp[i]));
                i+=2; j+=4;
            }
            i=0;
            while (i<offset){
                *((unsigned short *)(&p_buffer[j])) = *((unsigned short *)(&p_circular_buffer->p_start[i]));
                i+=2; j+=4;
            }
        }

        update_read_ptr(p_circular_buffer, src_pkt_size);
        return src_pkt_size;
    }

    return -1;
}


int32_t
pull_buffer_4o2_ulaw(circular_buffer_s *p_circular_buffer, uint8_t *p_buffer, uint32_t src_pkt_size)
{
    int32_t offset = -1;
    int i,j;
    int fra;
    uint8_t* cbuff_rp = get_read_ptr(p_circular_buffer);
    
    
    offset = ( (p_circular_buffer->p_read + src_pkt_size) - p_circular_buffer->p_end);

    if(get_buffer_fill_level(p_circular_buffer) >= src_pkt_size)
    {
       
        if(offset <= 0)
        {
            i=0;j=0;
            while (i<src_pkt_size){
                *((unsigned short *)(&p_buffer[j])) = ulaw2linear(*((unsigned char *)(&cbuff_rp[i])));
                *((unsigned short *)(&p_buffer[j+2])) = *((unsigned short *)(&p_buffer[j]));
                *((unsigned short *)(&p_buffer[j+4])) = *((unsigned short *)(&p_buffer[j]));
                *((unsigned short *)(&p_buffer[j+6])) = *((unsigned short *)(&p_buffer[j]));
                i++; j+=8;
            }
        }
        else
        {
            i=0;j=0;
            fra=(src_pkt_size - offset);
            while (i<fra){
                *((unsigned short *)(&p_buffer[j])) = ulaw2linear(*((unsigned char *)(&cbuff_rp[i])));
                *((unsigned short *)(&p_buffer[j+2])) = *((unsigned short *)(&p_buffer[j]));
                *((unsigned short *)(&p_buffer[j+4])) = *((unsigned short *)(&p_buffer[j]));
                *((unsigned short *)(&p_buffer[j+6])) = *((unsigned short *)(&p_buffer[j]));
                i++; j+=8;
            }
            i=0;
            while (i<offset){
                *((unsigned short *)(&p_buffer[j])) = ulaw2linear(*((unsigned char *)(&p_circular_buffer->p_start[i])));
                *((unsigned short *)(&p_buffer[j+2])) = *((unsigned short *)(&p_buffer[j]));
                *((unsigned short *)(&p_buffer[j+4])) = *((unsigned short *)(&p_buffer[j]));
                *((unsigned short *)(&p_buffer[j+6])) = *((unsigned short *)(&p_buffer[j]));
                i++; j+=8;
            }
        }

        update_read_ptr(p_circular_buffer, src_pkt_size);
        return src_pkt_size;
    }

    return -1;
}

/**/
uint8_t
is_buffer_vacant(circular_buffer_s *p_circular_buffer, uint32_t size)
{
    uint32_t  bytes_filled = 0;
    uint8_t   ret_val = 0;

    bytes_filled = get_buffer_fill_level(p_circular_buffer);
    ret_val = (bytes_filled <= size) ? 1 : 0;
    return ret_val;
}

/**/
inline uint32_t
get_buffer_fill_level(circular_buffer_s *p_circular_buffer)
{
    uint32_t bytes_filled = 0;
    if(p_circular_buffer->p_read <= p_circular_buffer->p_write)
    {
        bytes_filled = p_circular_buffer->p_write - p_circular_buffer->p_read;
    }
    else
    {
        bytes_filled = \
            ( (p_circular_buffer->p_write - p_circular_buffer->p_start) +\
              (p_circular_buffer->p_end - p_circular_buffer->p_read) );
    }

    return bytes_filled;
}

/**/
void
update_write_ptr(circular_buffer_s *p_circular_buffer, uint32_t pkt_size)
{
    int32_t offset = 0;

    offset = ( (p_circular_buffer->p_write + pkt_size) - \
                p_circular_buffer->p_end);
    if(offset <= 0)
    {
        p_circular_buffer->p_write = (p_circular_buffer->p_write + pkt_size);
        if(p_circular_buffer->p_write == p_circular_buffer->p_end)
        {
            p_circular_buffer->p_write = p_circular_buffer->p_start;
        }
    }
    else
    {
        p_circular_buffer->p_write = (p_circular_buffer->p_start + offset);
    }
}

/**/
void
update_read_ptr(circular_buffer_s *p_circular_buffer, uint32_t size)
{
    int32_t offset = 0;

    offset = ( (p_circular_buffer->p_read + size) - \
                p_circular_buffer->p_end);
    if(offset <= 0)
    {
        p_circular_buffer->p_read = (p_circular_buffer->p_read + size);
        if(p_circular_buffer->p_end == p_circular_buffer->p_read)
        {
            p_circular_buffer->p_read = p_circular_buffer->p_start;
        }
    }
    else
    {
        p_circular_buffer->p_read = (p_circular_buffer->p_start + offset);
    }
}

/**/
uint8_t
is_buffer_empty(circular_buffer_s *p_circular_buffer)
{
    if(p_circular_buffer->p_read == p_circular_buffer->p_write)
        return 1;

    return 0;
}

/**/
uint8_t
is_buffer_filled(circular_buffer_s *p_circular_buffer, uint32_t size)
{
    uint32_t  bytes_filled = 0;
    uint8_t   ret_val = 0;

    bytes_filled = get_buffer_fill_level(p_circular_buffer);
    ret_val = (bytes_filled >= size) ? 1 : 0;
    return ret_val;
}

