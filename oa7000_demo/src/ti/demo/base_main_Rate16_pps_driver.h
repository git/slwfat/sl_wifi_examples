//CONTROL LOCATIONS
typedef struct {
            unsigned char control_page;           //coefficient page location
            unsigned char control_base;           //coefficient base address within page
            unsigned char control_mute_flag;      //non-zero means muting required
            unsigned char control_string_index;   //string table index
} control; 




//INSTRUCTIONS & COEFFICIENTS
typedef struct {
    unsigned char reg_off;
    unsigned char reg_val;
} reg_value;


reg_value REG_Section_program[] = {
    {  0,0x00},
//			# reg[0][82] = 0
    { 82,0x00},
//			# reg[0][83] = 0
    { 83,0x00},
//			# reg[0][86] = 32
    { 86,0x20},
//			# reg[0][87] = 254
    { 87,0xFE},
//			# reg[0][88] = 0
    { 88,0x00},
//			# reg[0][89] = 104
    { 89,0x68},
//			# reg[0][90] = 168
    { 90,0xA8},
//			# reg[0][91] = 6
    { 91,0x06},
//			# reg[0][92] = 0
    { 92,0x00},
//			# reg[0][65] = 0
    { 65,0x00},
//			# reg[0][68] = 31
    { 68,0x1F},
//			# reg[0][69] = 0
    { 69,0x00},
//			# reg[0][70] = 198
    { 70,0xC6},
};

reg_value miniDSP_A_reg_values[] = {
    {  0,0x04},
    {  2,0x00},
    {  3,0x00},
    {  4,0x00},
    {  5,0x00},
    {  6,0x00},
    {  7,0x00},
    {  8,0x00},
    {  9,0x00},
    { 10,0x00},
    { 11,0x00},
    { 12,0x00},
    { 13,0x00},
    { 14,0x00},
    { 15,0xB8},
    { 16,0x7E},
    { 17,0x91},
    { 18,0x7F},
    { 19,0xFF},
    { 20,0x00},
    { 21,0x00},
    { 22,0x00},
    { 23,0x00},
    { 24,0xFF},
    { 25,0xFF},
    { 26,0x80},
    { 27,0x00},
    { 28,0x7F},
    { 29,0xFF},
    { 30,0x40},
    { 31,0x00},
    { 32,0x00},
    { 33,0x00},
    { 34,0xFF},
    { 35,0x6B},
    { 36,0xFF},
    { 37,0x03},
    { 38,0xFC},
    { 39,0xC6},
    { 40,0xF5},
    { 41,0x54},
    { 42,0xF9},
    { 43,0x64},
    { 44,0x39},
    { 45,0x80},
    { 46,0x03},
    { 47,0x1C},
    { 48,0x03},
    { 49,0x2F},
    { 50,0x00},
    { 51,0x67},
    { 52,0x0F},
    { 53,0x73},
    { 54,0x2C},
    { 55,0x2B},
    { 56,0x00},
    { 57,0x22},
    {  0,0x20},
    {  2,0x03},
    {  3,0x0F},
    {  4,0xFF},
    {  5,0x00},
    {  6,0x00},
    {  7,0x00},
    {  8,0x00},
    {  9,0x00},
    { 10,0x00},
    { 11,0x02},
    { 12,0x00},
    { 13,0x00},
    { 14,0x00},
    { 15,0x00},
    { 16,0x00},
    { 17,0x00},
    { 18,0x00},
    { 19,0x00},
    { 20,0x03},
    { 21,0x1B},
    { 22,0x0B},
    { 23,0x03},
    { 24,0x9B},
    { 25,0x0D},
    { 26,0x03},
    { 27,0x9A},
    { 28,0x0A},
    { 29,0x03},
    { 30,0x9A},
    { 31,0x0E},
    { 32,0x03},
    { 33,0x99},
    { 34,0x04},
    { 35,0x03},
    { 36,0x99},
    { 37,0x14},
    { 38,0x03},
    { 39,0x98},
    { 40,0x05},
    { 41,0x03},
    { 42,0x98},
    { 43,0x13},
    { 44,0x03},
    { 45,0x97},
    { 46,0x12},
    { 47,0x03},
    { 48,0x97},
    { 49,0x06},
    { 50,0x03},
    { 51,0x96},
    { 52,0x0C},
    { 53,0x03},
    { 54,0x95},
    { 55,0x09},
    { 56,0x03},
    { 57,0x95},
    { 58,0x0F},
    { 59,0x03},
    { 60,0x94},
    { 61,0x08},
    { 62,0x03},
    { 63,0x94},
    { 64,0x10},
    { 65,0x03},
    { 66,0x93},
    { 67,0x07},
    { 68,0x03},
    { 69,0x93},
    { 70,0x11},
    { 71,0x03},
    { 72,0x92},
    { 73,0x03},
    { 74,0x03},
    { 75,0x92},
    { 76,0x15},
    { 77,0x03},
    { 78,0x91},
    { 79,0x16},
    { 80,0x03},
    { 81,0x91},
    { 82,0x02},
    { 83,0x00},
    { 84,0x00},
    { 85,0x00},
    { 86,0x05},
    { 87,0x01},
    { 88,0x00},
    { 89,0x00},
    { 90,0x00},
    { 91,0x00},
    { 92,0x05},
    { 93,0x00},
    { 94,0x00},
    { 95,0x00},
    { 96,0x00},
    { 97,0x00},
    {  0,0x21},
    {  2,0x03},
    {  3,0x0A},
    {  4,0x17},
    {  5,0x09},
    {  6,0x89},
    {  7,0x16},
    {  8,0x00},
    {  9,0x00},
    { 10,0x00},
    { 11,0x03},
    { 12,0x8B},
    { 13,0x18},
    { 14,0x06},
    { 15,0x07},
    { 16,0x18},
    { 17,0x03},
    { 18,0x88},
    { 19,0x19},
    { 20,0x02},
    { 21,0x80},
    { 22,0x17},
    { 23,0x06},
    { 24,0x87},
    { 25,0x17},
    { 26,0x00},
    { 27,0x00},
    { 28,0x00},
    { 29,0x01},
    { 30,0x82},
    { 31,0x00},
    { 32,0x02},
    { 33,0x80},
    { 34,0x18},
    { 35,0x05},
    { 36,0x84},
    { 37,0x17},
    { 38,0x00},
    { 39,0x00},
    { 40,0x00},
    { 41,0x00},
    { 42,0x00},
    { 43,0x00},
    { 44,0x02},
    { 45,0x3F},
    { 46,0x1A},
    { 47,0x03},
    { 48,0x0F},
    { 49,0x1A},
    { 50,0x00},
    { 51,0x00},
    { 52,0x00},
    { 53,0x01},
    { 54,0x80},
    { 55,0x03},
    { 56,0x03},
    { 57,0x1C},
    { 58,0x00},
    { 59,0x00},
    { 60,0x00},
    { 61,0x00},
    { 62,0x03},
    { 63,0x8C},
    { 64,0x00},
    { 65,0x08},
    { 66,0x8B},
    { 67,0xFE},
    { 68,0x00},
    { 69,0x00},
    { 70,0x00},
    { 71,0x00},
    { 72,0x80},
    { 73,0x01},
    { 74,0x00},
    { 75,0x00},
    { 76,0x00},
    { 77,0x08},
    { 78,0x81},
    { 79,0xFF},
};
#define miniDSP_A_reg_values_COEFF_START   0
#define miniDSP_A_reg_values_COEFF_SIZE    57
#define miniDSP_A_reg_values_INST_START    57
#define miniDSP_A_reg_values_INST_SIZE     176

reg_value miniDSP_D_reg_values[] = {
    {  0,0x08},
    {  2,0x00},
    {  3,0x13},
    {  4,0x7F},
    {  5,0xDB},
    {  6,0x7F},
    {  7,0xF7},
    {  8,0x80},
    {  9,0x09},
    { 10,0x7F},
    { 11,0xEF},
    { 12,0xFF},
    { 13,0xFF},
    { 14,0x80},
    { 15,0x00},
    { 16,0x40},
    { 17,0x00},
    { 18,0x7F},
    { 19,0xFF},
    { 20,0x00},
    { 21,0x00},
    { 22,0xFF},
    { 23,0xF8},
    { 24,0xFF},
    { 25,0xD8},
    { 26,0xFF},
    { 27,0xF6},
    { 28,0xFE},
    { 29,0xBF},
    { 30,0x3A},
    { 31,0xD8},
    { 32,0x00},
    { 33,0x15},
    { 34,0x00},
    { 35,0x06},
    { 36,0x00},
    { 37,0x67},
    { 38,0x00},
    { 39,0x4F},
    { 40,0x01},
    { 41,0xD5},
    { 42,0x24},
    { 43,0x89},
    { 44,0xFC},
    { 45,0xAB},
    { 46,0xFF},
    { 47,0x19},
    { 48,0xF3},
    { 49,0xE3},
    { 50,0x06},
    { 51,0x1E},
    { 52,0x00},
    { 53,0x55},
    {  0,0x40},
    {  2,0x00},
    {  3,0x00},
    {  4,0x00},
    {  5,0x00},
    {  6,0x00},
    {  7,0x00},
    {  8,0x08},
    {  9,0x00},
    { 10,0x00},
    { 11,0x00},
    { 12,0x00},
    { 13,0x00},
    { 14,0x30},
    { 15,0x10},
    { 16,0x08},
    { 17,0x38},
    { 18,0x14},
    { 19,0x0C},
    { 20,0x38},
    { 21,0x0C},
    { 22,0x07},
    { 23,0x10},
    { 24,0x00},
    { 25,0x1A},
    { 26,0x60},
    { 27,0x04},
    { 28,0x0C},
    { 29,0x28},
    { 30,0x00},
    { 31,0x0B},
    { 32,0x38},
    { 33,0x08},
    { 34,0x0A},
    { 35,0x68},
    { 36,0x04},
    { 37,0x0B},
    { 38,0x00},
    { 39,0x00},
    { 40,0x00},
    { 41,0x18},
    { 42,0x10},
    { 43,0x02},
    { 44,0x08},
    { 45,0x10},
    { 46,0x01},
    { 47,0x30},
    { 48,0x1C},
    { 49,0x01},
    { 50,0x38},
    { 51,0x27},
    { 52,0xFC},
    { 53,0x28},
    { 54,0x00},
    { 55,0x09},
    { 56,0x00},
    { 57,0x00},
    { 58,0x00},
    { 59,0x88},
    { 60,0x14},
    { 61,0x02},
    { 62,0x90},
    { 63,0x08},
    { 64,0x00},
    { 65,0x88},
    { 66,0x00},
    { 67,0x02},
    { 68,0x30},
    { 69,0x2B},
    { 70,0xFC},
    { 71,0x90},
    { 72,0x88},
    { 73,0x00},
    { 74,0x50},
    { 75,0x08},
    { 76,0x00},
    { 77,0x38},
    { 78,0x1C},
    { 79,0x05},
    { 80,0x80},
    { 81,0x1F},
    { 82,0xFF},
    { 83,0x80},
    { 84,0x1F},
    { 85,0xFE},
    { 86,0x20},
    { 87,0x00},
    { 88,0x02},
    { 89,0x88},
    { 90,0x0C},
    { 91,0x04},
    { 92,0x88},
    { 93,0x10},
    { 94,0x07},
    { 95,0x10},
    { 96,0x14},
    { 97,0x02},
    {  0,0x41},
    {  2,0x08},
    {  3,0x14},
    {  4,0x03},
    {  5,0x88},
    {  6,0x00},
    {  7,0x07},
    {  8,0x10},
    {  9,0x17},
    { 10,0xFF},
    { 11,0x08},
    { 12,0x14},
    { 13,0x03},
    { 14,0x00},
    { 15,0x00},
    { 16,0x00},
    { 17,0x88},
    { 18,0x00},
    { 19,0x03},
    { 20,0x10},
    { 21,0x17},
    { 22,0xFE},
    { 23,0x08},
    { 24,0x14},
    { 25,0x03},
    { 26,0x00},
    { 27,0x00},
    { 28,0x00},
    { 29,0x30},
    { 30,0x20},
    { 31,0x05},
    { 32,0x38},
    { 33,0x20},
    { 34,0x03},
    { 35,0x00},
    { 36,0x00},
    { 37,0x00},
    { 38,0x18},
    { 39,0x1C},
    { 40,0x01},
    { 41,0x20},
    { 42,0x04},
    { 43,0x04},
    { 44,0x30},
    { 45,0x2C},
    { 46,0x19},
    { 47,0x38},
    { 48,0x2C},
    { 49,0x0B},
    { 50,0x38},
    { 51,0x30},
    { 52,0x15},
    { 53,0x38},
    { 54,0x30},
    { 55,0x0F},
    { 56,0x38},
    { 57,0x34},
    { 58,0x17},
    { 59,0x38},
    { 60,0x34},
    { 61,0x0D},
    { 62,0x38},
    { 63,0x38},
    { 64,0x11},
    { 65,0x38},
    { 66,0x38},
    { 67,0x13},
    { 68,0x38},
    { 69,0x3C},
    { 70,0x12},
    { 71,0x38},
    { 72,0x40},
    { 73,0x18},
    { 74,0x38},
    { 75,0x40},
    { 76,0x0C},
    { 77,0x38},
    { 78,0x44},
    { 79,0x16},
    { 80,0x38},
    { 81,0x44},
    { 82,0x0E},
    { 83,0x38},
    { 84,0x48},
    { 85,0x14},
    { 86,0x38},
    { 87,0x48},
    { 88,0x10},
    { 89,0x00},
    { 90,0x00},
    { 91,0x00},
    { 92,0x50},
    { 93,0x00},
    { 94,0x01},
    { 95,0x30},
    { 96,0x4C},
    { 97,0x18},
    {  0,0x42},
    {  2,0x38},
    {  3,0x4C},
    {  4,0x0B},
    {  5,0x20},
    {  6,0x08},
    {  7,0x1A},
    {  8,0x38},
    {  9,0x50},
    { 10,0x16},
    { 11,0x38},
    { 12,0x50},
    { 13,0x0D},
    { 14,0x38},
    { 15,0x54},
    { 16,0x12},
    { 17,0x38},
    { 18,0x54},
    { 19,0x11},
    { 20,0x38},
    { 21,0x58},
    { 22,0x0E},
    { 23,0x38},
    { 24,0x58},
    { 25,0x15},
    { 26,0x38},
    { 27,0x5C},
    { 28,0x17},
    { 29,0x38},
    { 30,0x5C},
    { 31,0x0C},
    { 32,0x38},
    { 33,0x60},
    { 34,0x13},
    { 35,0x38},
    { 36,0x60},
    { 37,0x10},
    { 38,0x38},
    { 39,0x64},
    { 40,0x14},
    { 41,0x38},
    { 42,0x64},
    { 43,0x0F},
    { 44,0x00},
    { 45,0x00},
    { 46,0x00},
    { 47,0x50},
    { 48,0x00},
    { 49,0x01},
    { 50,0x00},
    { 51,0x00},
    { 52,0x00},
    { 53,0x00},
    { 54,0x00},
    { 55,0x00},
    { 56,0x20},
    { 57,0x08},
    { 58,0x19},
    { 59,0x30},
    { 60,0x6B},
    { 61,0xFB},
    { 62,0x00},
    { 63,0x00},
    { 64,0x00},
    { 65,0x38},
    { 66,0x1B},
    { 67,0xFB},
    { 68,0x88},
    { 69,0x17},
    { 70,0xFE},
    { 71,0x00},
    { 72,0x00},
    { 73,0x00},
    { 74,0x30},
    { 75,0x24},
    { 76,0x00},
    { 77,0x00},
    { 78,0x00},
    { 79,0x00},
    { 80,0x10},
    { 81,0x00},
    { 82,0x1A},
    { 83,0x20},
    { 84,0x00},
    { 85,0x06},
    { 86,0x88},
    { 87,0x03},
    { 88,0xFF},
};
#define miniDSP_D_reg_values_COEFF_START   0
#define miniDSP_D_reg_values_COEFF_SIZE    53
#define miniDSP_D_reg_values_INST_START    53
#define miniDSP_D_reg_values_INST_SIZE     282
