//*****************************************************************************
// pcm_handler.c
//
// PCM Handler Interface
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

//*****************************************************************************
//
//! \addtogroup WifiAudioPlayer
//! @{
//
//*****************************************************************************
/* Standard includes. */
#include <stdio.h>
#include <string.h>
#include <stdint.h>


/* Hardware & DriverLib library includes. */
#include "hw_types.h"
#include "hw_mcasp.h"
#include "hw_udma.h"
#include "hw_ints.h"
#include "rom.h"
#include "rom_map.h"
#include "debug.h"
#include "interrupt.h"
#include "hw_memmap.h"
#include "i2s.h"
#include "udma.h"
#include "pin.h"
#include "circ_buff.h"
#include "pcm_handler.h"
#include "udma_if.h"
#include "i2c_if.h"
#include "uart_if.h"

#include "audio_tx_task.h"
#include "audio.h"
#include <pthread.h>
#include "mqueue.h"

extern mqd_t    gATxSMQueue;

//*****************************************************************************
//                          LOCAL DEFINES
//*****************************************************************************
#define UDMA_DSTSZ_32_SRCSZ_16          0x21000000



#define CB_TRANSFER_ELEM	        512//number of 16 bit elem from i2s
#ifdef PCMU_LAW
  #define CB_DATA_SZ              128//bytes of actual data within CB_TRANSFER_ELEMs//1 byte, but at half of the speed
#else
  #define CB_DATA_SZ              512//bytes of actual data within CB_TRANSFER_ELEMs// 2 bytes for each 16 bit channel
#endif

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
extern circular_buffer_s *p_play_buffer;
extern circular_buffer_s *p_rec_buffer;
extern unsigned int g_uiPlayWaterMark;

unsigned char gaucZeroBuffer[(CB_TRANSFER_ELEM * HALF_WORD_SIZE)];
unsigned int g_uiPlayWaterMark = 0;

extern int audio_format;
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************
unsigned char i2s_rx_mic_buff_pri[CB_TRANSFER_ELEM * HALF_WORD_SIZE];
unsigned char i2s_rx_mic_buff_alt[CB_TRANSFER_ELEM * HALF_WORD_SIZE];
unsigned char i2s_tx_spk_buff_pri[CB_TRANSFER_ELEM * HALF_WORD_SIZE];
unsigned char i2s_tx_spk_buff_alt[CB_TRANSFER_ELEM * HALF_WORD_SIZE];

void notify_mic_consumer(){
    a_task_msg_s  msg_on_a_task_q;

    msg_on_a_task_q.task_id = I2S_EVENT_HANDLER;
    msg_on_a_task_q.msg_id  = MIC_DATA_AVAIL;

    mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);
  
}


//*****************************************************************************
//
//! Callback function implementing ping pong mode DMA transfer
//!
//! \param None
//!
//! This function
//!        1. sets the primary and the secondary buffer for ping pong transfer.
//!           2. sets the control table.
//!           3. transfer the appropriate data according to the flags.
//!
//! \return None.
//
//*****************************************************************************
void DMAPingPongCompleteAppCB_opt()
{
    unsigned long ulPrimaryIndexTx = 0x4;
    unsigned long ulAltIndexTx = 0x24;
    unsigned long ulPrimaryIndexRx = 0x5;
    unsigned long ulAltIndexRx = 0x25;
    unsigned int intstatus;
    tDMAControlTable *pControlTable;
    intstatus = MAP_uDMAIntStatus();
    if(intstatus & 0x00000010)
    {
    	I2SIntClear(I2S_BASE, I2S_INT_RDMA);

        pControlTable = MAP_uDMAControlBaseGet();

        if((pControlTable[ulPrimaryIndexTx].ulControl & UDMA_CHCTL_XFERMODE_M) == 0)
        {
          //..and I quote: TransferSize parameter is the number of data items, not the number
            //of bytes. The value of this parameter should not exceed 1024.
                MAP_uDMAChannelTransferSet(UDMA_CH4_I2S_RX,
                                             UDMA_MODE_PINGPONG,
                                             (void *)I2S_RX_DMA_PORT,
                                             i2s_rx_mic_buff_pri,
                                             CB_TRANSFER_ELEM);
                 MAP_uDMAChannelEnable(UDMA_CH4_I2S_RX);
                  switch(audio_format){
                  case L16_16000:
                    fill_buffer_2o4(p_rec_buffer, i2s_rx_mic_buff_pri, 512);//CB_TRANSFER_ELEM*2 bytes per channel /2 channels (at 16000 =  all of data)
                    break;
                  case ULAW_8000_MCU:
                    fill_buffer_2o4_ulaw(p_rec_buffer, i2s_rx_mic_buff_pri, 128);//CB_TRANSFER_ELEM*2 bytes per channel /2 channels (at 8000 = half of data)
                    break;
                  case ALAW_8000_MCU:
                    //fill_buffer_1o4_alaw(p_rec_buffer, i2s_rx_mic_buff_pri, 128);
                    break;
                  case ULAW_8000_DSP:
                  case ALAW_8000_DSP:
                    //fill_buffer_1o4(p_rec_buffer, i2s_rx_mic_buff_pri, 128);
                    break;
                  }
        }
        else if((pControlTable[ulAltIndexTx].ulControl & UDMA_CHCTL_XFERMODE_M) == 0)
        {
                MAP_uDMAChannelTransferSet(UDMA_CH4_I2S_RX|UDMA_ALT_SELECT,
                                             UDMA_MODE_PINGPONG,
                                             (void *)I2S_RX_DMA_PORT,
                                             i2s_rx_mic_buff_alt,
                                             CB_TRANSFER_ELEM );
                 MAP_uDMAChannelEnable(UDMA_CH4_I2S_RX|UDMA_ALT_SELECT);
                  switch(audio_format){
                  case L16_16000:
                    fill_buffer_2o4(p_rec_buffer, i2s_rx_mic_buff_alt, 512);
                    break;
                  case ULAW_8000_MCU:
                    fill_buffer_2o4_ulaw(p_rec_buffer, i2s_rx_mic_buff_alt, 128);
                    break;
                  case ALAW_8000_MCU:
                    //fill_buffer_2o4_alaw(p_rec_buffer, i2s_rx_mic_buff_alt, 128);
                    break;
                  case ULAW_8000_DSP:
                  case ALAW_8000_DSP:
                    //fill_buffer_1o4(p_rec_buffer, i2s_rx_mic_buff_alt, 128);
                    break;
                  }
                 
         }
        
    }

    if(intstatus & 0x00000020)
    {
        //Clear the MCASP write interrupt
		I2SIntClear(I2S_BASE, I2S_INT_XDMA);
        pControlTable = MAP_uDMAControlBaseGet();
        if((pControlTable[ulPrimaryIndexRx].ulControl & UDMA_CHCTL_XFERMODE_M) == 0)
        {
/*
            if( is_buffer_empty(p_play_buffer) || !g_uiPlayWaterMark )
            {
                g_uiPlayWaterMark = is_buffer_filled(p_play_buffer,PLAY_WATERMARK);
                MAP_uDMAChannelTransferSet(UDMA_CH5_I2S_TX,
                                            UDMA_MODE_PINGPONG,
                                            (void *)&gaucZeroBuffer[0],
                                            (void *)I2S_TX_DMA_PORT,
                                            CB_TRANSFER_ELEM );
            }
            else
            {*/
                MAP_uDMAChannelTransferSet(UDMA_CH5_I2S_TX,
                                            UDMA_MODE_PINGPONG,
                                            i2s_tx_spk_buff_pri,
                                            (void *)I2S_TX_DMA_PORT,
                                            CB_TRANSFER_ELEM );
                  switch(audio_format){
                  case L16_16000:
                    pull_buffer_4o2(p_play_buffer, i2s_tx_spk_buff_pri, 512);
                    break;
                  case ULAW_8000_MCU:
                    pull_buffer_4o2_ulaw(p_play_buffer, i2s_tx_spk_buff_pri, 128);
                    break;
                  case ALAW_8000_MCU:
                    //pull_buffer_4o2_alaw(p_play_buffer, i2s_tx_spk_buff_pri, 256);
                    break;
                  case ULAW_8000_DSP:
                  case ALAW_8000_DSP:
                    //pull_buffer_4o1(p_play_buffer, i2s_tx_spk_buff_pri, 128);
                    break;
                  }
                
           // }
            MAP_uDMAChannelEnable(UDMA_CH5_I2S_TX);
        }
        else if((pControlTable[ulAltIndexRx].ulControl & UDMA_CHCTL_XFERMODE_M) == 0)
        {/*
            if( is_buffer_empty(p_play_buffer) || !g_uiPlayWaterMark )
            {
                 g_uiPlayWaterMark = is_buffer_filled(p_play_buffer,PLAY_WATERMARK);
                 MAP_uDMAChannelTransferSet(UDMA_CH5_I2S_TX|UDMA_ALT_SELECT,
                                              UDMA_MODE_PINGPONG,
                                              (void *)&gaucZeroBuffer[0],
                                              (void *)I2S_TX_DMA_PORT,
                                              CB_TRANSFER_ELEM );
            }
            else
            {*/
                MAP_uDMAChannelTransferSet(UDMA_CH5_I2S_TX|UDMA_ALT_SELECT,
                                             UDMA_MODE_PINGPONG,
                                             i2s_tx_spk_buff_alt,
                                             (void *)I2S_TX_DMA_PORT,
                                             CB_TRANSFER_ELEM );
                  switch(audio_format){
                  case L16_16000:
                    pull_buffer_4o2(p_play_buffer, i2s_tx_spk_buff_alt, 512);
                    break;
                  case ULAW_8000_MCU:
                    pull_buffer_4o2_ulaw(p_play_buffer, i2s_tx_spk_buff_alt, 128);
                    break;
                  case ALAW_8000_MCU:
                    //pull_buffer_4o2_alaw(p_play_buffer, i2s_tx_spk_buff_alt, 256);
                    break;
                  case ULAW_8000_DSP:
                  case ALAW_8000_DSP:
                    //pull_buffer_4o1(p_play_buffer, i2s_tx_spk_buff_alt, 128);
                    break;
                  }
                

            //}
            MAP_uDMAChannelEnable(UDMA_CH5_I2S_TX|UDMA_ALT_SELECT);
        }
    }

}

//*****************************************************************************
//
//! configuring the DMA transfer
//!
//! \param psAudPlayerCtrl is the control structure managing the input data.
//!
//! This function
//!        1. setting the source and the destination for the DMA transfer.
//!           2. setting the uDMA registers to control actual transfer.
//!
//! \return None.
//
//*****************************************************************************
void SetupPingPongDMATransferTx()
{


    UDMASetupTransfer(UDMA_CH4_I2S_RX,
                      UDMA_MODE_PINGPONG,
                      CB_TRANSFER_ELEM,
                      UDMA_SIZE_16,
                      UDMA_ARB_8,
                      (void *)I2S_RX_DMA_PORT,
                      UDMA_CHCTL_SRCINC_NONE,
                      i2s_rx_mic_buff_pri,
                      UDMA_CHCTL_DSTINC_16);


    UDMASetupTransfer(UDMA_CH4_I2S_RX|UDMA_ALT_SELECT,
                      UDMA_MODE_PINGPONG,
                      CB_TRANSFER_ELEM,
                      UDMA_SIZE_16,
                      UDMA_ARB_8,
                      (void *)I2S_RX_DMA_PORT,
                      UDMA_CHCTL_SRCINC_NONE,
                      i2s_rx_mic_buff_alt,
                      UDMA_CHCTL_DSTINC_16);
}

void SetupPingPongDMATransferRx()
{

    
    UDMASetupTransfer(UDMA_CH5_I2S_TX,
                      UDMA_MODE_PINGPONG,
                      CB_TRANSFER_ELEM,
                      UDMA_SIZE_16,
                      UDMA_ARB_8,
                      i2s_tx_spk_buff_pri,
                      UDMA_CHCTL_SRCINC_16,
                      (void *)I2S_TX_DMA_PORT,
                      UDMA_DST_INC_NONE);

    UDMASetupTransfer(UDMA_CH5_I2S_TX|UDMA_ALT_SELECT,
                      UDMA_MODE_PINGPONG,
                      CB_TRANSFER_ELEM,
                      UDMA_SIZE_16,
                      UDMA_ARB_8,
                      i2s_tx_spk_buff_alt,
                      UDMA_CHCTL_SRCINC_16,
                      (void *)I2S_TX_DMA_PORT,
                      UDMA_DST_INC_NONE);

}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
