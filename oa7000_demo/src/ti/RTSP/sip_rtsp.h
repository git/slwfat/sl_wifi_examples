//*****************************************************************************
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#ifndef __RTSP_H__
#define __RTSP_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*!
    \addtogroup RTSP_IF
    @{
*/

#define NUM_OF_MEDIA_STREAMS    2


/* Error Codes */
typedef enum rtsp_status_code
{
    RTSP_GENERAL_ERROR              = -1,
    RTSP_LIMITED_BUFF_SIZE_ERROR    = -2,
    RTSP_NULL_DESCRIPTION_POINTER   = -3

}rtsp_status_code_e;

/* RTSP Packet Types */
typedef enum rtsp_packet_type
{
 
    UNKNOWN,
    RTSP_OPTIONS,
    DESCRIBE,
    SETUP,
    PLAY,
    TEARDOWN,
    GET_PARAMETER,
    SET_PARAMETER,


}rtsp_packet_type_e;

/* RTSP Packet Types */
typedef enum sip_packet_type
{

    SIP_TRYING,
    SIP_RINGING,
    SIP_OK,
    SIP_OPTIONS,
    SIP_INVITE,
    SIP_REGISTER,
    SIP_CANCEL,
    SIP_ACK,
    SIP_BYE,
    SIP_UNKNOWN

}sip_packet_type_e;

/* RTP Port Pair */
typedef struct rtsp_stream_port
{
    uint16_t rtp_port;
    uint16_t rtcp_port;

}rtsp_stream_port_s;

/* RTSP Session Configuration Structure */
typedef struct rtsp_session_config
{
    rtsp_stream_port_s stream_port[NUM_OF_MEDIA_STREAMS];

    int32_t  session_timeout;
    int32_t  session_num;

    uint32_t  timestamp;

    uint8_t   multicast_ip[16];
    uint8_t   date[64];
    uint8_t   local_ip_str[17];
    uint8_t   peer_ip_str[17];

}rtsp_session_config_s;

/* RTSP Setup Data*/
typedef struct rtsp_setup_data
{
    uint16_t  rtp_port;
    uint16_t  rtcp_port;
    int8_t   is_multicast;
    int8_t   padding[3];

}rtsp_setup_data_s;
/* SIP Setup Data*/
typedef struct sip_setup_data
{
    uint16_t  rtp_port;
    uint16_t  rtcp_port;
    int8_t   is_multicast;
    int8_t   padding[3];

}sip_setup_data_s;
typedef union
{
    rtsp_setup_data_s setup_data[NUM_OF_MEDIA_STREAMS];

}rtsp_pkt_response_u;

/* RTSP Packet Info */
typedef struct rtsp_packet_info
{
    rtsp_packet_type_e      pkt_type;
    rtsp_pkt_response_u     pkt_response;
    int32_t                  seq_num;
    int32_t                  setup_num;

}rtsp_pkt_info_s;

/* SIP Packet Info */
typedef struct sip_packet_info
{
    sip_packet_type_e      pkt_type;
    rtsp_pkt_response_u     pkt_response;
    int32_t                  seq_num;
    int32_t                  setup_num;

}sip_pkt_info_s;

/*!
    \brief      Inintilize the RTSP library
    \param[out] p_param: Pointer to buffer containing the source description
                         information
    \return     0 on success, negative error-code on error

    \note
*/
int32_t  sip_rtsp_init();

/*!
    \brief      Deinintilize the RTSP library
    \param      None
    \return     0 on success, negative error-code on error
    \note
*/
int32_t rtsp_deinit();

/*!
    \brief      Resets the setup-counter that was incremented on receiving an
                SETUP packet
    \param      None
    \return     0 on success, negative error-code on error
    \note
 */
int32_t rtsp_release_setup();

/*!
    \brief      Process the RTSP packet and create the RTSP response packet

    \param[in]  p_in_buffer             :   Pointer to buffer containing RTSP packet
    \param[in]  rtp_pkt_size            :   Size of the RTSP packet
    \param[in]  p_rtsp_session_config   :   Pointer to structure containing the information
                                            required to create RTSP response packet
    \param[out] p_out_buffer            :   Pointer to buffer for RTSP response packet
    \param[in]  rtsp_resp_pkt_len       :   RTSP response packet buffer length
    \param[out] pkt_info                :   RTSP packet informmation

    \return     Size of data in output buffer, negative error-code on error

    \note
*/
int32_t  \
rtsp_packet_parser(int8_t *const p_in_buffer,           \
                   rtsp_session_config_s *p_rtsp_session_config,            \
                   uint16_t rtsp_resp_pkt_len, int8_t *const p_out_buffer,     \
                   rtsp_pkt_info_s *pkt_info);
                   
int32_t  \
sip_resp_packet_parser(int8_t *const p_in_buffer,            \
                   rtsp_session_config_s *p_rtsp_session_config,            \
                   uint16_t sip_resp_pkt_len, int8_t *const p_out_buffer,     \
                   sip_pkt_info_s *pkt_info);
int32_t  \
sip_req_packet_parser(int8_t *const p_in_buffer,            \
                   rtsp_session_config_s *p_rtsp_session_config,            \
                   uint16_t sip_resp_pkt_len, int8_t *const p_out_buffer,     \
                   sip_pkt_info_s *pkt_info);

int32_t
unknown_tag(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info);
int32_t
sip_unknown_tag(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t
options_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
               int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info);
int32_t
describe_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info);
int32_t
setup_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
             int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info);
int32_t
play_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info);
int32_t
teardown_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info);
int32_t
sip_options_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
               int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t
ack_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t
invite_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t
invite_ack_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);

int32_t
invite_response_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t
invite_trying_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t
invite_ringing_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t rtsp_init(uint16_t port);
int32_t sip_init(uint16_t port);
int32_t invite_ack_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
int32_t invite_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info);
/*!
     Close the Doxygen group.
     @}
 */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __RTSP_H__ */
