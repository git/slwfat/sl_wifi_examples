//*****************************************************************************
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include "sip_rtsp.h"
#include "audio.h"

#ifdef SL_PLATFORM_MULTI_THREADED


    pthread_mutex_t    rtsp_lock;
    pthread_mutex_t    sip_lock;
#else /* SL_PLATFORM_MULTI_THREADED */

#endif /* SL_PLATFORM_MULTI_THREADED */

#define RTSP_OPTION_REQUEST         "OPTIONS"
#define RTSP_DESCRIBE_REQUEST       "DESCRIBE"
#define RTSP_SETUP_REQUEST          "SETUP"
#define RTSP_PLAY_REQUEST           "PLAY"
#define RTSP_TEARDOWN_REQUEST       "TEARDOWN"
#define RTSP_SET_PARAMETER_REQUEST  "SET_PARAMETER"
#define RTSP_GET_PARAMETER_REQUEST  "GET_PARAMETER"

#define SIP_OPTION_REQUEST              "OPTIONS"
#define SIP_INVITE_REQUEST              "INVITE"
#define SIP_BYE_REQUEST                 "BYE"
#define SIP_REGISTER_REQUEST            "REGISTER"
#define SIP_CANCEL_REQUEST              "CANCEL"
#define SIP_ACK_REQUEST                 "ACK"
#define SIP_TRYING_RESPONSE             "100 Trying"
#define SIP_RINGING_RESPONSE            "180 Ringing"
#define SIP_OK_RESPONSE                 "SIP/2.0 200 Ok"

#define SEQUENCE_STRING             "CSeq:"
#define RESP_BUF_SIZE               1500

#define BASE_10                     10
#define BASE                        BASE_10

#define SETUP_UCAST_RESPONSE_STRING "%s\r\n"\
                                    "CSeq: %d\r\n"\
                                    "%s\r\n"\
                                    "Transport: RTP/AVP;unicast;client_port=%d-%d;server_port=%d-%d\r\n"\
                                    "Session: %d;timeout=%d\r\n\r\n"

#define SETUP_MCAST_RESPONSE_STRING "%s\r\n"\
                                    "CSeq: %d\r\n"\
                                    "Transport: RTP/AVP;multicast;destination=%d;port=%d-%d\r\n"\
                                    "Session: %d\r\n\r\n"

#define DESCRIBE_RESPONSE_STRING    "%s\r\n"\
                                    "CSeq: %d\r\n"\
                                    "%s\r\n"\
                                    "Content-Base: %s\r\n"\
                                    "Content-Type: application/sdp\r\n"\
                                    "Content-Length: %d\r\n\r\n"

#define PLAY_RESPONSE_STRING        "%s\r\n"\
                                    "CSeq: %d\r\n"\
                                    "%s\r\n"\
                                    "Range: npt=0.000-\r\n"\
                                    "Session: %d\r\n"\
                                    ";rtptime=%ld\r\n\r\n"

#define INVITE_TRYING_STRING        "%s\r\n"\
                                    "Via: %s\r\n"\
                                    "From: %s\r\n"\
                                    "To: <%s>\r\n"\
                                    "Call-ID: %s\r\n"\
                                    "CSeq: %d INVITE\r\n"\
                                    "Content-Length: 0\r\n\r\n"

#define INVITE_RINGING_STRING       "%s\r\n"\
                                    "Via: %s\r\n"\
                                    "From: %s\r\n"\
                                    "To: <%s>;tag=BBBBBBB\r\n"\
                                    "Call-ID: %s\r\n"\
                                    "CSeq: %d INVITE\r\n"\
                                    "Supported: replaces, outbound\r\n"\
                                    "Content-Length: 0\r\n\r\n"

#define INVITE_RESPONSE_STRING      "%s\r\n"\
                                    "Via: %s\r\n"\
                                    "From: %s\r\n"\
                                    "To: <%s>;tag=BBBBBBB\r\n"\
                                    "Call-ID: %s\r\n"\
                                    "Max-Forwards: 70\r\n"\
                                    "CSeq: %d INVITE\r\n"\
                                    "Supported: replaces, outbound\r\n"\
                                    "Allow: %s\r\n"\
                                    "Contact: <sip:%s:5060>\r\n"\
                                    "Content-Type: application/sdp\r\n"\
                                    "Content-Length: %d\r\n\r\n"

#define INVITE_REQUEST_STRING       "INVITE sip:%s SIP/2.0\r\n"\
                                    "Via: SIP/2.0/UDP %s:5060;rport;branch=z9hG4bKjd98q2hf98hw4gunw4g89hw4g9f4g5h6j7\r\n"\
                                    "From: <sip:%s>;tag=a1b2c3d4e5\r\n"\
                                    "To: <sip:%s>\r\n"\
                                    "Call-ID: %s\r\n"\
                                    "Max-Forwards: 70\r\n"\
                                    "CSeq: %d INVITE\r\n"\
                                    "Supported: replaces\r\n"\
                                    "Allow: %s\r\n"\
                                    "Contact: <sip:%s:5060;ob>\r\n"\
                                    "User-Agent: cc3220\r\n"\
                                    "Content-Type: application/sdp\r\n"\
                                    "Content-Length: %d\r\n\r\n"
                                      
#define INVITE_ACK_STRING           "ACK sip:%s:5060 SIP/2.0\r\n"\
                                    "Via: SIP/2.0/UDP %s:5060\r\n"\
                                    "From: <sip:cc3220@%s>;tag=CCCCCCC\r\n"\
                                    "To: <sip:%s>\r\n"\
                                    "Call-ID: %s\r\n"\
                                    "Max-Forwards: 70\r\n"\
                                    "CSeq: %d ACK\r\n\r\n"


                                      

                                   
int8_t sdp_out_template[] =
{
"v=0\r\n\
o=cc3220 8126 1 IN IP4 %s\r\n\
s=CC32XX + OV798 video streaming\r\n\
c=IN IP4 %s\r\n\
i=A/V\r\n\
t=0 0\r\n\
a=type:unicast\r\n\
a=control:*\r\n\
a=range:npt=0-\r\n\
a=x-qt-text-nam:H.264 Video\r\n\
a=x-qt-text-inf:Video.264\r\n\
m=video 0 RTP/AVP 96\r\n\
c=IN IP4 0.0.0.0\r\n\
a=rtpmap:96 H264/90000\r\n\
a=framerate:24.0\r\n\
a=fmtp:96 packetization-mode=1;profile-level-id=42001F\r\n\
a=control:track0\r\n\
a=sendonly\r\n\
m=audio %i RTP/AVP 0\r\n\
c=IN IP4 %s\r\n\
a=rtpmap:%s\r\n\
a=ptime:%i\r\n\
a=control:track1\r\n"
};
//;sprop-parameter-sets=Z00AKKlQFAeyAAAA,aO48gAAAAAAAAAAA

//m=audio %i
    //RX port for audio
//a=rtpmap:%i 
    //"0 PCMU/8000" (0 predefined)
    //"97 L16/16000"  (97 is dynamic)
//a=ptime: Length of time in milliseconds represented by the media in a packet. 
    // = AUDIO_REC_PKT_SAMP/SAMPLING_RATE * 1000 
    //L16 @ 16000 ptime = 32
    //L16 @ 16000 ptime = 32
    //PCMU @8000 ptime = 128 //(packet size bytes is AUDIO_REC_PKT_SIZE)

int8_t sdp_out_invite_response[] =
{
"v=0\r\n\
o=- 3852 1180 IN IP4 %s\r\n\
s=Talk\r\n\
c=IN IP4 %s\r\n\
t=0 0\r\n\
m=audio %i RTP/AVP 0 101\r\n\
a=sendrecv\r\n\
a=rtpmap:0 PCMU/8000\r\n\
a=rtpmap:101 telephone-event/8000\r\n\
a=rtcp-fb:* trr-int 5000\r\n\
a=rtcp-fb:* ccm tmmbr\r\n\
m=video 0 RTP/AVP 0\r\n\
a=inactive\r\n"};


int8_t sdp_out_hc[] =
{
"v=0\r\n\
o=- 3852385289 3852385289 IN IP4 %s\r\n\
s=cc3220\r\n\
c=IN IP4 %s\r\n\
t=0 0\r\n\
m=audio %i RTP/AVP 0 101\r\n\
c=IN IP4 %s\r\n\
a=rtcp:%i IN IP4 %s\r\n\
a=sendrecv\r\n\
a=rtpmap:0 PCMU/8000\r\n\
a=rtpmap:101 telephone-event/8000\r\n\
a=fmtp:101 0-16\r\n\
m=video %i RTP/AVP 96\r\n\
a=rtpmap:96 H264/90000\r\n\
a=fmtp:96 profile-level-id=42801F\r\n\
a=rtcp-fb:* trr-int 5000\r\n\
a=rtcp-fb:* ccm tmmbr\r\n\
a=rtcp-fb:96 nack pli\r\n\
a=rtcp-fb:96 nack sli\r\n\
a=rtcp-fb:96 ack rpsi\r\n\
a=rtcp-fb:96 ccm fir\r\n"
};//a=rtcp-xr:rcvr-rtt=all:10000 stat-summary=loss,dup,jitt,TTL voip-metrics\r\n\

int8_t sdp_out_hc_invite[] =
{
"v=0\r\n\
o=- 3852385299 3852385299 IN IP4 %s\r\n\
s=Talk\r\n\
c=IN IP4 %s\r\n\
t=0 0\r\n\
m=audio %i RTP/AVP 0 101\r\n\
a=sendrecv\r\n\
a=rtpmap:0 PCMU/8000\r\n\
a=rtpmap:101 telephone-event/8000\r\n\
a=fmtp:101 0-16\r\n\
m=video %i RTP/AVP 96\r\n\
a=rtpmap:96 H264/90000\r\n\
a=fmtp:96 profile-level-id=42801F\r\n"
};//42801? a=framerate:30.0\r\n\
    
//a=sendonly\r\n"
extern int audio_rate;
extern int audio_format;
int8_t sdp_out[1024];

uint16_t rtsp_port=0;
uint16_t sip_port=0;

#ifdef DEBUG_PRINT
extern int Report(const char *format, ...);
#else
#define Report(...)
#endif

static int8_t   rtsp_ok_response[]          = "RTSP/1.0 200 OK";
static int8_t   rtsp_supported_options[]    = "Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY";
static int8_t   sip_ok_response[]           = "SIP/2.0 200 Ok";
static int8_t   sip_trying_response[]       = "SIP/2.0 100 Trying";
static int8_t   sip_ringing_response[]      = "SIP/2.0 180 Ringing";
static int8_t   sip_supported_options[]     = "INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, UPDATE";

static int8_t   resp_buffer[RESP_BUF_SIZE]  = {0};

int8_t call_id_str[64];
int8_t via_str[128];
int8_t from_str[128];
int8_t to_str[64];




/* RTSP Packet Parsers */
typedef int32_t \
(*fptr_rtsp_packet_parser)\
      (int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
       int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info);



/*!
 */

static int32_t
rtsp_pre_process(int8_t *p_in_buffer);

/*!
 */
static int32_t
rtsp_post_process(uint16_t rtsp_resp_pkt_len, int8_t *p_out_buffer);

/*!
 */
static inline \
int32_t rtsp_get_seq_num(int16_t index, int32_t const length,\
                        int8_t const *const p_token);



/*!
 */
int32_t rtsp_init(uint16_t port)
{
    rtsp_port=port;
    pthread_mutex_init(&rtsp_lock, (const pthread_mutexattr_t *)NULL);
    return 0;
}

/*!
 */
int32_t rtsp_deinit()
{
    pthread_mutex_destroy(&rtsp_lock);
    return 0;
}
/*!
 */
int32_t sip_init(uint16_t port)
{
    sip_port=port;
    pthread_mutex_init(&sip_lock, (const pthread_mutexattr_t *)NULL);
    return 0;
}

/*!
 */
int32_t sip_deinit()
{
    pthread_mutex_destroy(&sip_lock);
    return 0;
}

/*!
 */
int32_t rtsp_release_setup()
{

    return 0;
}
/*!

 */
int32_t sip_req_packet_parser(int8_t *const p_in_buffer, \
                          rtsp_session_config_s *p_rtsp_session_config,\
                          uint16_t sip_resp_pkt_len, int8_t *const p_out_buffer,\
                          sip_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pthread_mutex_lock(&sip_lock);

    memset(resp_buffer, 0, sizeof(resp_buffer));

    /* Parse and process the SIP packet */
    if(!memcmp(p_in_buffer, SIP_OPTION_REQUEST, strlen(SIP_OPTION_REQUEST)))
    {
        Report("[SIP]-> OPTIONS Packet Received \n\r");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_OPTIONS;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_INVITE_REQUEST, strlen(SIP_INVITE_REQUEST)))
    {
        Report("[SIP]-> INVITE Packet Received \n\r ");
        //Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_INVITE;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_BYE_REQUEST, strlen(SIP_BYE_REQUEST)))
    {
        Report("[SIP]-> BYE Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_BYE;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_REGISTER_REQUEST, strlen(SIP_REGISTER_REQUEST)))
    {
        Report("[SIP]-> REGISTER Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_REGISTER;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_CANCEL_REQUEST, strlen(SIP_CANCEL_REQUEST)))
    {
        Report("[SIP]-> CANCEL Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_CANCEL;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_ACK_REQUEST, strlen(SIP_ACK_REQUEST)))
    {
        Report("[SIP]-> ACK Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_ACK;
        ret_val = 0;
    }

    else
    {
        Report("[<-] %s\n\r", p_in_buffer);
        pkt_info->pkt_type = SIP_UNKNOWN;
        ret_val = 0;
    }

    if(ret_val < 0)
    {
        ret_val = -1;
        goto exit_process_rtsp_packet;
    }

    Report("[->] %s\n\r", p_out_buffer);

exit_process_rtsp_packet:
    pthread_mutex_unlock(&sip_lock);
    return ret_val;
}

/*!

Returns length of packet
 */
int32_t rtsp_packet_parser(int8_t *const p_in_buffer, \
                          rtsp_session_config_s *p_rtsp_session_config,\
                          uint16_t rtsp_resp_pkt_len, int8_t *const p_out_buffer,\
                          rtsp_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pthread_mutex_lock(&rtsp_lock);

    memset(resp_buffer, 0, sizeof(resp_buffer));

    /* Parse and process the RTSP packet */
    if(!memcmp(p_in_buffer, RTSP_OPTION_REQUEST, strlen(RTSP_OPTION_REQUEST)))
    {
        Report("[RTSP]-> OPTIONS Packet Received \n\r");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = RTSP_OPTIONS;
        
    }
    else if(!memcmp(p_in_buffer, RTSP_DESCRIBE_REQUEST, strlen(RTSP_DESCRIBE_REQUEST)))
    {
        Report("[RTSP]-> DESCRIBE Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = DESCRIBE;
        
    }
    else if(!memcmp(p_in_buffer, RTSP_SETUP_REQUEST, strlen(RTSP_SETUP_REQUEST)))
    {
        Report("[RTSP]-> SETUP Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SETUP;
        
    }
    else if(!memcmp(p_in_buffer, RTSP_PLAY_REQUEST, strlen(RTSP_PLAY_REQUEST)))
    {
        Report("[RTSP]-> PLAY Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = PLAY;
        
    }
    else if(!memcmp(p_in_buffer, RTSP_TEARDOWN_REQUEST, strlen(RTSP_TEARDOWN_REQUEST)))
    {
        Report("[RTSP]-> TEARDOWN Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = TEARDOWN;
        
    }
    else
    {
        Report("[<-] %s\n\r", p_in_buffer);
        pkt_info->pkt_type = UNKNOWN;
        
    }

    if(ret_val < 0)
    {
        ret_val = -1;
        goto exit_process_rtsp_packet;
    }

    

exit_process_rtsp_packet:
    pthread_mutex_unlock(&rtsp_lock);
    return ret_val;
}

/*!
Returns length of packet
 */
int32_t sip_resp_packet_parser(int8_t *const p_in_buffer, \
                          rtsp_session_config_s *p_rtsp_session_config,\
                          uint16_t rtsp_resp_pkt_len, int8_t *const p_out_buffer,\
                          sip_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pthread_mutex_lock(&sip_lock);

    memset(resp_buffer, 0, sizeof(resp_buffer));
    if(!memcmp(p_in_buffer, SIP_BYE_REQUEST, strlen(SIP_BYE_REQUEST)))
    {
        Report("[SIP]-> BYE Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_BYE;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_CANCEL_REQUEST, strlen(SIP_CANCEL_REQUEST)))
    {
        Report("[SIP]-> CANCEL Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_CANCEL;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_TRYING_RESPONSE, strlen(SIP_TRYING_RESPONSE)))
    {
        Report("[SIP]-> TRYING Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_TRYING;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_RINGING_RESPONSE, strlen(SIP_RINGING_RESPONSE)))
    {
        Report("[SIP]-> RINGING Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_RINGING;
        ret_val = 0;
    }
    else if(!memcmp(p_in_buffer, SIP_OK_RESPONSE, strlen(SIP_OK_RESPONSE)))
    {
        Report("[SIP]-> OK Packet Received \n\r ");
        Report("[<-] %s\n\r", p_in_buffer);

        pkt_info->pkt_type = SIP_OK;
        ret_val = 0;
    }
    

    else
    {
        Report("[<-] %s\n\r", p_in_buffer);
        pkt_info->pkt_type = SIP_UNKNOWN;
        ret_val = 0;
    }

    if(ret_val < 0)
    {
        ret_val = -1;

    }

    Report("[->] %s\n\r", p_out_buffer);


    pthread_mutex_unlock(&sip_lock);
    return ret_val;
}

/*!
 */
int32_t
unknown_tag(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info)
{
    Report("UNRECOGNISED PACKET RECEIVED\r\n");
    return -1;
}

/*!
 */
int32_t
sip_unknown_tag(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info)
{
    Report("UNRECOGNISED PACKET RECEIVED\r\n");
    return -1;
}
/*!
 */
int32_t
options_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
               int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[RTSP]-> Sequence number not found\n\r ");
        ret_val = -1;
        goto exit_options_parser;
    }

    sprintf((char *) resp_buffer , "%s\r\nCSeq: %d\r\n%s\r\n%s\r\n\r\n", rtsp_ok_response,
    pkt_info->seq_num, p_rtsp_session_config->date, rtsp_supported_options);

    ret_val = rtsp_post_process(rtsp_resp_pkt_len, p_out_buffer);
    if(ret_val < 0)
    {
        goto exit_options_parser;
    }

exit_options_parser:
    return ret_val;
}

/*!
 */
int32_t
describe_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info)
{
    int8_t   rtsp_url[64] = {0};
    int8_t   *p_token = NULL;

    int32_t  content_length = 0;
    int32_t  ret_val = 0;
    int32_t  length = 0;
    int16_t  index = 0;


    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[RTSP]-> Sequence number not found\n\r ");
        ret_val = -1;
        goto exit_describe_parser;
    }

    /* Get URL for content */
    p_token = (int8_t *)strstr((const char *)p_in_buffer, "rtsp");
    if (NULL == p_token)
    {
        Report("[RTSP]-> ERROR DESCRIBE Can't get Content-Base: \n\r ");
        ret_val = -1;
        goto exit_describe_parser;
    }

    /* store the URL */
    index = 0;
    length = strlen((char *)p_token);
    while ( (index < length) && ( p_token[index] != ' ') )
        index++;

    if (index <= length)
    {
        strncpy((char *)rtsp_url , (char *)&p_token[0], index);
        rtsp_url[index] = '\0';
    }

    //m=audio %i
        //RX port for audio
    //a=rtpmap:%i 
        //"0 PCMU/8000" (0 predefined)
        //"97 L16/16000"  (97 is dynamic)
    //a=ptime: Length of time in milliseconds represented by the media in a packet. 
        // = AUDIO_REC_PKT_SAMP/SAMPLING_RATE * 1000 
        //L16 @ 16000 ptime = 32
        //L16 @ 16000 ptime = 32
        //PCMU @8000 ptime = 128
    

    sprintf((char *)sdp_out, (char *)sdp_out_template, 
            p_rtsp_session_config->local_ip_str, 
            p_rtsp_session_config->local_ip_str, 
            0,
            p_rtsp_session_config->local_ip_str, 
            "0 PCMU/8000",//0 PCMU/8000
            AUDIO_REC_PKT_SIZE*1000/audio_rate);
      
    if(sdp_out != NULL)
    {
        content_length = strlen((const char *)sdp_out);
    }
    else
    {
        ret_val = RTSP_NULL_DESCRIPTION_POINTER;
        goto exit_describe_parser;
    }
    
    

    sprintf((char *)resp_buffer, DESCRIBE_RESPONSE_STRING,\
            rtsp_ok_response, pkt_info->seq_num, p_rtsp_session_config->date,\
            rtsp_url, content_length);

    strcat((char *)resp_buffer, (const char *)sdp_out);

    ret_val = rtsp_post_process(rtsp_resp_pkt_len, p_out_buffer);
    if(ret_val < 0)
    {
        goto exit_describe_parser;
    }

exit_describe_parser:
    return ret_val;
}

/*!
 */
int32_t
setup_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
             int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info)
{
    int8_t   *p_token = NULL;
    int8_t   setup_control_str[40];
    int32_t  length = 0;
    int32_t  ret_val = 0;
    int16_t  index = 0;
    



    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[RTSP]-> Sequence number not found\n\r ");
        ret_val = -1;
        goto exit_setup_parser;
    }

    //Get the track number
    ret_val=sprintf((char *)setup_control_str,"SETUP rtsp://%s:%i/track", (char *)p_rtsp_session_config->local_ip_str, rtsp_port);
    setup_control_str[ret_val]=0;
    
    p_token = (int8_t *)strstr((const char *)&p_in_buffer[0] , (char *)setup_control_str);
    if(NULL != p_token)
    {
        //Advance to end of str
        index = strlen((char *)setup_control_str);
        length = strlen((char *)p_token);

        pkt_info->setup_num= strtol((const char *)&p_token[index], NULL , BASE);

    }
   
    //Extract the client port RTP number for this track
    p_token = (int8_t *)strstr((const char *)&p_in_buffer[0] , "client_port=");
    if(NULL != p_token)
    {
        /* skip "port=" */
        index = strlen("client_port=");
        length = strlen((char *)p_token);

        /* Get first port number */
        pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port = \
                            strtol((const char *)&p_token[index], NULL , BASE);

        /* Advance to second port number ("-")*/
        while( (index < length) && ( p_token[index] != '-') ) index++;


        if (index <= length)
        {
            /* Get second UDP port number */
            pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtcp_port = \
                            strtol((const char *)&p_token[index+1], NULL, BASE);
        }
    }
    else
    {
        pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port = 0;
        pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtcp_port = 0;
    }

    if(strstr((const char *)&p_in_buffer[0], "unicast"))
    {
        pkt_info->pkt_response.setup_data[pkt_info->setup_num].is_multicast = 0;

        if(pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port != 0)
        {
            sprintf((char *)resp_buffer, SETUP_UCAST_RESPONSE_STRING,
                    rtsp_ok_response, pkt_info->seq_num, p_rtsp_session_config->date,
                    pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port,
                    pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtcp_port,
                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtp_port,
                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtcp_port,
                    p_rtsp_session_config->session_num, p_rtsp_session_config->session_timeout);
        }
        else
        {
            pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port = \
                                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtp_port;
            pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtcp_port = \
                                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtcp_port;

            sprintf((char *)resp_buffer, SETUP_UCAST_RESPONSE_STRING,
                    rtsp_ok_response, pkt_info->seq_num, p_rtsp_session_config->date,
                    pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port,
                    pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtcp_port,
                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtp_port,
                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtcp_port,
                    p_rtsp_session_config->session_num, p_rtsp_session_config->session_timeout);
        }
    }
    else
    {
        pkt_info->pkt_response.setup_data[pkt_info->setup_num].is_multicast = 1;

        if(pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port != 0)
        {
            /* if port is defined by client used it otherwise use the set ports */
            sprintf((char *)resp_buffer, SETUP_MCAST_RESPONSE_STRING,
                    rtsp_ok_response, pkt_info->seq_num, (char *)p_rtsp_session_config->multicast_ip,
                    pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port,
                    pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtcp_port,
                    p_rtsp_session_config->session_num);
        }
        else
        {
            pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtp_port = \
                                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtp_port;
            pkt_info->pkt_response.setup_data[pkt_info->setup_num].rtcp_port = \
                                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtcp_port;

            /* if port is defined by client used it otherwise use the set ports */
            sprintf((char *)resp_buffer, SETUP_MCAST_RESPONSE_STRING,
                    rtsp_ok_response, pkt_info->seq_num, (char *)p_rtsp_session_config->multicast_ip,
                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtp_port,
                    p_rtsp_session_config->stream_port[pkt_info->setup_num].rtcp_port,
                    p_rtsp_session_config->session_num);
        }
    }
    
    ret_val = rtsp_post_process(rtsp_resp_pkt_len, p_out_buffer);
    if(ret_val < 0)
    {
        goto exit_setup_parser;
    }

exit_setup_parser:
    return ret_val;
}

/*!
 */
int32_t
play_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[RTSP]-> Sequence number not found\n\r ");
        ret_val = -1;
        goto exit_play_parser;
    }

    sprintf((char *) &resp_buffer[0], PLAY_RESPONSE_STRING,
            rtsp_ok_response, pkt_info->seq_num, p_rtsp_session_config->date,
            p_rtsp_session_config->session_num, p_rtsp_session_config->timestamp);

    ret_val = rtsp_post_process(rtsp_resp_pkt_len, p_out_buffer);
    if(ret_val < 0)
    {
        goto exit_play_parser;
    }

exit_play_parser:
    return ret_val;
}

/*!
 */
int32_t
teardown_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t rtsp_resp_pkt_len, rtsp_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[RTSP]-> Sequence number not found\n\r ");
        ret_val = -1;
        goto exit_teardown_parser;
    }

    sprintf((char *) resp_buffer, "%s\r\nCSeq: %d\r\n\r\n", rtsp_ok_response, pkt_info->seq_num);

    ret_val = rtsp_post_process(rtsp_resp_pkt_len, p_out_buffer);
    if(ret_val < 0)
    {
        goto exit_teardown_parser;
    }

exit_teardown_parser:
    return ret_val;
}

/*!
 */
static int32_t
rtsp_pre_process(int8_t *p_in_buffer)
{
    int8_t   *p_token = NULL;

    int32_t  ret_val = 0;
    int32_t  length = 0;
    int16_t  index = 0;

    p_token = (int8_t *) strstr((const char *)p_in_buffer, SEQUENCE_STRING);
    if(NULL == p_token)
    {
        Report("[RTSP]-> ERROR Sequence NO not found \n\r ");
        ret_val = -1;
        goto exit_rtsp_pre_process;
    }

    /* Skip SEQUENCE_STRING */
    index = strlen(SEQUENCE_STRING);
    length = strlen((char *)p_token);


    ret_val = rtsp_get_seq_num(index, length, p_token);
    if(ret_val < 0)
    {
        ret_val = -1;
        goto exit_rtsp_pre_process;
    }

exit_rtsp_pre_process:
    return ret_val;

}
/*!
 */
static int32_t
get_header_field(int8_t *p_in_buffer, int8_t *field_name, int8_t *out_buffer)
{
    int8_t   *p_token = NULL;

    int32_t  ret_val = 0;
    int16_t  index = 0;
    char c;
    int i=0;
    int j=0;
    char *substr;
    
    p_token = (int8_t *) strstr((const char *)p_in_buffer, (char *)field_name);
    if(NULL == p_token)
    {
        Report("[SIP]-> field %s not found \n\r ",field_name);
        return -1;
    }
    
    index = strlen((char *)field_name);

    substr=(char *)&p_token[index];
    c=substr[0];
    if(c!=':') return -1;
    i++;
    c=substr[i];
    while(c==' '){
       i++;
       c=substr[i];
    }
    while(c!='\r' && c!='\n'){
       out_buffer[j]=c;
       i++;j++;
       c=substr[i];
    }
    out_buffer[j]=0;


    return ret_val;

}

/*!
 */
static uint16_t
sdp_get_audio_port(int8_t *p_in_buffer)
{
    int8_t   *p_token = NULL;

    uint16_t  port = 0;
    int32_t  ret_val = 0;
    //int32_t  length = 0;
    int16_t  index = 0;

    p_token = (int8_t *) strstr((const char *)p_in_buffer, "m=audio ");
    if(NULL == p_token)
    {
        Report("[SDP]-> ERROR media audio not found \n\r ");
        return 0;
    }

    /* Skip SEQUENCE_STRING */
    index = strlen("m=audio ");
    //length = strlen((char *)p_token);

    ret_val = strtol((const char *)&p_token[index], NULL , BASE);

    if(ret_val < 0)
    {
        Report("[SDP]-> ERROR audio port parse fail\n\r ");
        return 0;
    }
    port= ret_val;
      
    

    return port;

}
/*!
 */
static uint16_t
sdp_get_video_port(int8_t *p_in_buffer)
{
    int8_t   *p_token = NULL;

    int16_t  ret_val = 0;
    //int32_t  length = 0;
    int16_t  index = 0;

    p_token = (int8_t *) strstr((const char *)p_in_buffer, "m=video ");
    if(NULL == p_token)
    {
        Report("[SDP]-> ERROR media video not found \n\r ");
        return 0;
    }

    /* Skip SEQUENCE_STRING */
    index = strlen("m=video ");
    //length = strlen((char *)p_token);

    ret_val = strtol((const char *)&p_token[index], NULL , BASE);

    if(ret_val < 0)
    {
        Report("[SDP]-> ERROR video port parse fail\n\r ");
        return 0;
    }

    return ret_val;

}
/*!
 */
int32_t
sip_options_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
               int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[SIP]-> Sequence number not found\n\r ");
        ret_val = -1;
        goto exit_sip_options_parser;
    }

    sprintf((char *) resp_buffer , "%s\r\nCSeq: %d\r\n%s\r\n\r\n", sip_ok_response,
    pkt_info->seq_num, sip_supported_options);

    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);
    if(ret_val < 0)
    {
        goto exit_sip_options_parser;
    }

exit_sip_options_parser:
    return ret_val;
}

/*!
 */
int32_t
invite_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info)
{
    int8_t   sip_url[64] = {0};
    int8_t   *p_token = NULL;
    uint16_t port;

    int32_t  length = 0;
    int16_t  index = 0;

      
    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[SIP]-> Sequence number not found\n\r ");
        return -1;
    }

    /* Get URL for content */
    p_token = (int8_t *)strstr((const char *)p_in_buffer, "sip");
    if (NULL == p_token)
    {
        Report("[SIP]-> ERROR INVITE Can't get Content-Base: \n\r ");
        return -1;
    }

    /* store the URL */
    index = 0;
    length = strlen((char *)p_token);
    while ( (index < length) && ( p_token[index] != ' ') )
        index++;

    if (index <= length)
    {
        strncpy((char *)sip_url , (char *)&p_token[0], index);
        sip_url[index] = '\0';
    }

    
    /*
    pkt_info->pkt_response.setup_data[0].rtp_port = 9078;
    pkt_info->pkt_response.setup_data[0].rtcp_port = 9079;
    pkt_info->pkt_response.setup_data[1].rtp_port = 7076;
    pkt_info->pkt_response.setup_data[1].rtcp_port = 7077;
    */

    port=sdp_get_video_port(p_in_buffer);
    if(port>0){
        pkt_info->pkt_response.setup_data[0].rtp_port = port;
        pkt_info->pkt_response.setup_data[0].rtcp_port = port+1;
        p_rtsp_session_config->stream_port[0].rtp_port = port;
        p_rtsp_session_config->stream_port[0].rtcp_port = port+1;
        Report("[SDP]-> Audio RTP/RTCP ports %i/%i\n\r ",port,port+1);
    }
    else{
        Report("[SDP]-> Audio RTP/RTCP ports not found");
    }
    port=sdp_get_audio_port(p_in_buffer);
    if(port>0){
        pkt_info->pkt_response.setup_data[1].rtp_port = port;
        pkt_info->pkt_response.setup_data[1].rtcp_port = port+1;
        p_rtsp_session_config->stream_port[1].rtp_port = port;
        p_rtsp_session_config->stream_port[1].rtcp_port = port+1;
        Report("[SDP]-> Video RTP/RTCP ports %i/%i\n\r ",port,port+1);
    }
    else{
        Report("[SDP]-> Video RTP/RTCP ports not found");
    }
    get_header_field(p_in_buffer,"Call-ID", call_id_str);
    get_header_field(p_in_buffer,"From", from_str);
    get_header_field(p_in_buffer,"To", to_str);
    get_header_field(p_in_buffer,"Via", via_str);
    
    return 0;
}


int32_t invite_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info){
    


    int32_t  content_length = 0;
    int32_t  ret_val = 0;
    uint16_t port;
    uint32_t i=0;
    uint32_t rnum;
                  //////////////////////////
    
    //m=audio %i
        //RX port for audio
    //a=rtpmap:%i 
        //"0 PCMU/8000" (0 predefined)
        //"97 L16/16000"  (97 is dynamic)
    //a=ptime: Length of time in milliseconds represented by the media in a packet. 
        // = AUDIO_REC_PKT_SAMP/SAMPLING_RATE * 1000 
        //L16 @ 16000 ptime = 32
        //L16 @ 16000 ptime = 32
        //PCMU @8000 ptime = 128
    
    
    /*
    int8_t sdp_out_hc_invite[] =
    {
    "v=0\r\n\
    o=CC32XX 3852385289 3852385299 IN IP4 %s\r\n\
    s=cc3220\r\n\
    c=IN IP4 %s\r\n\
    t=0 0\r\n\
    m=audio %i RTP/AVP 0 101\r\n\
    c=IN IP4 %s\r\n\
    a=rtcp:%i IN IP4 %s\r\n\
    a=sendrecv\r\n\
    a=rtpmap:0 PCMU/8000\r\n\
    a=rtpmap:101 telephone-event/8000\r\n\
    a=fmtp:101 0-16\r\n\
    m=video %i RTP/AVP 96\r\n\
    a=rtpmap:96 H264/90000\r\n\
    a=framerate:30.0\r\n\
    a=fmtp:96 packetization-mode=1;profile-level-id=42001F\r\n\
    a=control:track0\r\n"
    };
    */
    /*
    sprintf(sdp_out, sdp_out_hc, 
            p_rtsp_session_config->local_ip_str, 
            p_rtsp_session_config->local_ip_str, 
            p_rtsp_session_config->local_ip_str,
            pkt_info->pkt_response.setup_data[1].rtcp_port,
            p_rtsp_session_config->local_ip_str);
    */
    port=7078;
    pkt_info->pkt_response.setup_data[1].rtp_port = port;
    pkt_info->pkt_response.setup_data[1].rtcp_port = port+1;
    p_rtsp_session_config->stream_port[1].rtp_port = port;
    p_rtsp_session_config->stream_port[1].rtcp_port = port+1;
    Report("[SDP]-> Audio RTP/RTCP ports %i/%i\n\r ",port,port+1);
    port=9078;
    pkt_info->pkt_response.setup_data[0].rtp_port = port;
    pkt_info->pkt_response.setup_data[0].rtcp_port = port+1;
    p_rtsp_session_config->stream_port[0].rtp_port = port;
    p_rtsp_session_config->stream_port[0].rtcp_port = port+1;
    Report("[SDP]-> Video RTP/RTCP ports %i/%i\n\r ",port,port+1);

    sprintf((char *)sdp_out, (char *)sdp_out_hc_invite, 
        p_rtsp_session_config->local_ip_str, 
        p_rtsp_session_config->local_ip_str, 
        pkt_info->pkt_response.setup_data[1].rtp_port,
        pkt_info->pkt_response.setup_data[0].rtp_port);

    //////////////////////////////////////
    if(sdp_out != NULL)
    {
        content_length = strlen((const char *)sdp_out);
    }
    else
    {
        return RTSP_NULL_DESCRIPTION_POINTER;
    }
        
    
    i=0;//Generate a random alphanumeric call ID
    while(i<10){
        rnum=(uint8_t)(rand()%62);
        if(rnum<=9) call_id_str[i]=rnum+48;
        else if(rnum<=35) call_id_str[i]=rnum+55;//65-10;
        else call_id_str[i]=rnum+61;//97-36;
        i++;
    }
    
    pkt_info->seq_num = 20;//Some initial seq num (doesnt matter)
    /*
     "INVITE sip:%s;transport=udp\r\n"\
      "Via: SIP/2.0/UDP %s:5060\r\n"\
      "From: <sip:cc3220@%s>;tag=CCCCCCC\r\n"\
      "To: <sip:%s>;tag=DDDDDDD\r\n"\
      "Call-ID: %s\r\n"\
      "CSeq: %d INVITE\r\n"\
      "Allow: %s\r\n"\
      "Contact: <sip:%s:5060>\r\n"\
      "Content-Type: application/sdp\r\n"\
      "Content-Length: %d\r\n\r\n"

    */
    
    sprintf((char *)resp_buffer, INVITE_REQUEST_STRING,
            p_rtsp_session_config->peer_ip_str,
            p_rtsp_session_config->local_ip_str, 
            p_rtsp_session_config->local_ip_str,
            p_rtsp_session_config->peer_ip_str, 
            call_id_str, pkt_info->seq_num, 
            sip_supported_options, 
            p_rtsp_session_config->local_ip_str, 
            content_length);

    strcat((char *)resp_buffer, (const char *)sdp_out);

    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);

    return ret_val;
}


int32_t invite_ack_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info){
    


    int32_t  ret_val = 0;

        
    /*
      "ACK sip:%s:5060 SIP/2.0\r\n"\
      "Via: SIP/2.0/UDP %s:5060\r\n"\
      "From: <sip:cc3220@%s>;tag=CCCCCCC\r\n"\
      "To: <sip:%s>;tag=DDDDDDD\r\n"\
      "Call-ID: %s\r\n"\
      "CSeq: %d ACK\r\n"\
    */
    
    sprintf((char *)resp_buffer, INVITE_ACK_STRING,
            p_rtsp_session_config->peer_ip_str,
            p_rtsp_session_config->local_ip_str, 
            p_rtsp_session_config->local_ip_str,
            p_rtsp_session_config->peer_ip_str, 
            call_id_str, 
            pkt_info->seq_num);

    strcat((char *)resp_buffer, (const char *)sdp_out);

    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);

    return ret_val;
}

int32_t invite_response_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info){
    


    int32_t  content_length = 0;
    int32_t  ret_val = 0;

                  //////////////////////////
    
    //m=audio %i
        //RX port for audio
    //a=rtpmap:%i 
        //"0 PCMU/8000" (0 predefined)
        //"97 L16/16000"  (97 is dynamic)
    //a=ptime: Length of time in milliseconds represented by the media in a packet. 
        // = AUDIO_REC_PKT_SAMP/SAMPLING_RATE * 1000 
        //L16 @ 16000 ptime = 32
        //L16 @ 16000 ptime = 32
        //PCMU @8000 ptime = 128
    
    
    /*
    uint8_t sdp_out_hc[] =
    {
    "v=0\r\n\
    o=- 3852385289 3852385299 IN IP4 %s\r\n\
    s=cc3220\r\n\
    c=IN IP4 %s\r\n\
    t=0 0\r\n\
    m=audio 7078 RTP/AVP 0 101\r\n\
    c=IN IP4 %s\r\n\
    a=rtcp:%i IN IP4 %s\r\n\
    a=sendrecv\r\n\
    a=rtpmap:0 PCMU/8000\r\n\
    a=rtpmap:101 telephone-event/8000\r\n\
    a=fmtp 101 0-16\r\n"};
    */
    
    sprintf((char *)sdp_out, (char *)sdp_out_invite_response,
            p_rtsp_session_config->local_ip_str, 
            p_rtsp_session_config->local_ip_str,
            pkt_info->pkt_response.setup_data[1].rtp_port);
    /*
    sprintf((char *)sdp_out, (char *)sdp_out_hc, 
        p_rtsp_session_config->local_ip_str, 
        p_rtsp_session_config->local_ip_str, 
        pkt_info->pkt_response.setup_data[1].rtp_port,
        p_rtsp_session_config->local_ip_str,
        pkt_info->pkt_response.setup_data[1].rtcp_port,
        p_rtsp_session_config->local_ip_str,
        pkt_info->pkt_response.setup_data[0].rtp_port,
        p_rtsp_session_config->local_ip_str);
*/
    //////////////////////////////////////
    if(sdp_out != NULL)
    {
        content_length = strlen((const char *)sdp_out);
    }
    else
    {
        return RTSP_NULL_DESCRIPTION_POINTER;
    }
    

    
    sprintf((char *)resp_buffer, INVITE_RESPONSE_STRING,\
            sip_ok_response, via_str, from_str, to_str, call_id_str, pkt_info->seq_num, sip_supported_options, p_rtsp_session_config->local_ip_str, content_length);

    strcat((char *)resp_buffer, (const char *)sdp_out);

    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);

    return ret_val;
}


int32_t invite_request_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info){
    


    int32_t  content_length = 0;
    int32_t  ret_val = 0;

    


    /*
    uint8_t sdp_out_hc[] =
    {
    "v=0\r\n\
    o=- 3852385289 3852385299 IN IP4 %s\r\n\
    s=cc3220\r\n\
    c=IN IP4 %s\r\n\
    t=0 0\r\n\
    m=audio 7078 RTP/AVP 0 101\r\n\
    c=IN IP4 %s\r\n\
    a=rtcp:%i IN IP4 %s\r\n\
    a=sendrecv\r\n\
    a=rtpmap:0 PCMU/8000\r\n\
    a=rtpmap:101 telephone-event/8000\r\n\
    a=fmtp 101 0-16\r\n"};
    */
    sprintf((char *)sdp_out, (char *)sdp_out_hc_invite, 
        p_rtsp_session_config->local_ip_str, 
        p_rtsp_session_config->local_ip_str, 
        pkt_info->pkt_response.setup_data[1].rtp_port,
        p_rtsp_session_config->local_ip_str,
        pkt_info->pkt_response.setup_data[1].rtcp_port,
        p_rtsp_session_config->local_ip_str,
        pkt_info->pkt_response.setup_data[0].rtp_port,
        p_rtsp_session_config->local_ip_str);

    //////////////////////////////////////
    if(sdp_out != NULL)
    {
        content_length = strlen((const char *)sdp_out);
    }
    else
    {
        return RTSP_NULL_DESCRIPTION_POINTER;
    }
    
    /*
    "INVITE sip:%s;transport=udp\r\n"\
    "Via: SIP/2.0/UDP %s:5060\r\n"\
    "From: <sip:cc3220@%s>;tag=CCCCCCC\r\n"\
    "To: <sip:%s>;tag=BBBBBBB\r\n"\
    "Call-ID: %s\r\n"\
    "CSeq: %d INVITE\r\n"\
    "Allow: %s\r\n"\
    "Contact: <sip:%s:5060>\r\n"\
    "Content-Type: application/sdp\r\n"\
    "Content-Length: %d\r\n\r\n"
    */
    
    sprintf((char *)resp_buffer, INVITE_REQUEST_STRING,p_rtsp_session_config->local_ip_str,p_rtsp_session_config->local_ip_str,p_rtsp_session_config->local_ip_str,
            p_rtsp_session_config->peer_ip_str, call_id_str, pkt_info->seq_num, sip_supported_options, p_rtsp_session_config->local_ip_str, content_length);

    strcat((char *)resp_buffer, (const char *)sdp_out);

    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);

    return ret_val;
}

int32_t invite_trying_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info){
    
                  


    int32_t  ret_val = 0;
    /*
    "%s\r\n"\
    "From: \"CC3220\"\r\n"\
    "Call-ID: %s\r\n"\
    "CSeq: %d\r\n"\
    */
    sprintf((char *)resp_buffer, INVITE_TRYING_STRING,\
            sip_trying_response, via_str, from_str, to_str, call_id_str, pkt_info->seq_num);


    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);

    return ret_val;
}

int32_t invite_ringing_format(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
                int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info){
    

    int32_t  ret_val = 0;

    /*
    "%s\r\n"\
    "From: \"CC3220\"\r\n"\
    "Call-ID: %s\r\n"\
    "CSeq: %d\r\n"\

    */
    sprintf((char *)resp_buffer, INVITE_RINGING_STRING,\
            sip_ringing_response, via_str, from_str, to_str, call_id_str, pkt_info->seq_num);


    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);

    return ret_val;
}

/*!
 */
int32_t
ack_parser(int8_t *p_in_buffer, rtsp_session_config_s *p_rtsp_session_config,\
            int8_t *p_out_buffer, uint16_t sip_resp_pkt_len, sip_pkt_info_s *pkt_info)
{
    int32_t  ret_val = 0;

    pkt_info->seq_num = rtsp_pre_process(p_in_buffer);
    if(pkt_info->seq_num < 0)
    {
        Report("[SIP]-> Sequence number not found\n\r ");
        ret_val = -1;
    }
/*
    sprintf((char *) &resp_buffer[0], PLAY_RESPONSE_STRING,
            rtsp_ok_response, pkt_info->seq_num, p_rtsp_session_config->date,
            p_rtsp_session_config->session_num, p_rtsp_session_config->timestamp);

    ret_val = rtsp_post_process(sip_resp_pkt_len, p_out_buffer);
    if(ret_val < 0)
    {
        goto exit_ack_parser;
    }

exit_ack_parser:*/
    return ret_val;
}

/*!
 */
static int32_t
rtsp_post_process(uint16_t rtsp_resp_pkt_len, int8_t *p_out_buffer)
{
    int32_t  ret_val = -1;
    uint16_t  resp_len = 0;

    resp_len = strlen((char *)resp_buffer);
    if(resp_len > rtsp_resp_pkt_len)
    {
        /* limited buffer size */
        ret_val = RTSP_LIMITED_BUFF_SIZE_ERROR;
        goto exit_rtsp_post_process;
    }

    memcpy(p_out_buffer, resp_buffer, resp_len);
    ret_val = resp_len;

exit_rtsp_post_process:
    return ret_val;

}

/*!
 */
static inline \
int32_t rtsp_get_seq_num(int16_t index, int32_t const length,\
                        int8_t const *const p_token)
{
    int32_t  ret_val = -1;

    /* skip non number characters ' ' */
    while( (index < length) &&
           ((p_token[index] < '0') || (p_token[index] > '9')) )
    {
        index++;
    }

    if (index <= length) ret_val = strtol((const char *)&p_token[index], NULL, BASE);
    else ret_val = -1;

    return ret_val;
}
