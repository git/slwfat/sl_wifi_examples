//*****************************************************************************
// ti3120.h
//
// Macro for TI3120 registers
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#ifndef __TI3120_H__
#define __TI3120_H__

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

#define CODEC_I2C_SLAVE_ADDR      ((48 >> 1))
#define CRYSTAL_FREQ               40000000
#define SYS_CLK                    CRYSTAL_FREQ

#define TI3120_PAGE_0				0
#define TI3120_PAGE_1				1
#define TI3120_PAGE_2				2
#define TI3120_PAGE_3				3
#define TI3120_PAGE_4				4
#define TI3120_PAGE_8				8
#define TI3120_PAGE_43				43
#define TI3120_PAGE_44				44
#define TI3120_PAGE_95				95
  
#define PAGE_CTRL_REG				0

// Page 0
#define TI3120_PAGE_SEL_REG			0//
#define TI3120_SW_RESET_REG			1//
#define TI3120_SPK_OVETEMP_FLAG			3//
#define TI3120_CLK_MUX_REG			4//
#define TI3120_CLK_PLL_P_R_REG			5//
#define TI3120_CLK_PLL_J_REG			6//
#define TI3120_CLK_PLL_D_MSB_REG		7//
#define TI3120_CLK_PLL_D_LSB_REG		8//
#define TI3120_CLK_NDAC_REG			11//
#define TI3120_CLK_MDAC_REG			12//
#define TI3120_DAC_OSR_MSB_REG			13//
#define TI3120_DAC_OSR_LSB_REG			14//
#define TI3120_DAC_IDAC_REG			15//+
#define TI3120_DSP_D_INTERPOL_REG		16//+
#define TI3120_CLK_NADC_REG			18//
#define TI3120_CLK_MADC_REG			19//
#define TI3120_ADC_AOSR_REG			20//
#define TI3120_ADC_IADC_REG			21//+
#define TI3120_ADC_DSP_DEC_REG			22//+
#define TI3120_CLKOUTMUX			25//+
#define TI3120_CLKOUTMVAL			26//+
#define TI3120_AUDIO_IF_1_REG			27//
#define TI3120_DATA_OFFSET			28//+
#define TI3120_AUDIO_IF_2_REG			29//+
#define TI3120_BCLKN				30//+
#define TI3120_AUDIO_SECIF_1_REG		31//+
#define TI3120_AUDIO_SECIF_2_REG		32//+
#define TI3120_AUDIO_SECIF_3_REG		33//+
#define TI3120_I2C_COND				34//+
#define TI3120_ADC_FLAG				36//+
#define TI3120_DAC_FLAG1			37//+
#define TI3120_DAC_FLAG2			38//+
#define TI3120_OFLOW_FLAG			39//+
#define TI3120_DAC_INT_FLAG1			44//+
#define TI3120_ADC_INT_FLAG1			45//+
#define TI3120_DAC_INT_FLAG2			46//+
#define TI3120_ADC_INT_FLAG2			47//+
#define TI3120_INT1_CTL_REG			48//+
#define TI3120_INT2_CTL_REG			49//+
#define TI3120_GPIO1_CTL			51//+
#define TI3120_DOUT_CTL				53//+
#define TI3120_DIN_CTL				54//+


#define TI3120_DAC_SIG_P_BLK_CTRL_REG	        60
#define TI3120_ADC_SIG_P_BLK_CTRL_REG	        61
#define TI3120_DSP_INS_MODE_CTL			62
#define TI3120_DAC_CHANNEL_SETUP_1_REG	        63
#define TI3120_DAC_MUTE				64
#define TI3120_DAC_VOL_CTRL_REG			65
#define TI3120_HEADSET_DET			67
#define TI3120_DRC_CTL1				68
#define TI3120_DRC_CTL2				69
#define TI3120_DRC_CTL3				70
#define TI3120_BEEPGEN				71
#define TI3120_BEEPLEN_MSB			73
#define TI3120_BEEPLEN_MB			74
#define TI3120_BEEPLEN_LSB			75
#define TI3120_BEEPSIN_MSB			76
#define TI3120_BEEPSIN_LSB			77
#define TI3120_BEEPCOS_MSB			78
#define TI3120_BEEPCOS_LSB			79

#define TI3120_ADC_CHANNEL_SETUP_REG	        81
#define TI3120_ADC_FINE_GAIN_ADJ_REG	        82
#define TI3120_ADC_VOL_CTRL_REG			83

#define TI3120_AGC_CTRL1			86
#define TI3120_AGC_CTRL2			87
#define TI3120_AGC_MAXGAIN			88
#define TI3120_AGC_ATTACK			89
#define TI3120_AGC_DECAY			90
#define TI3120_AGC_DEBOUNCE1			91
#define TI3120_AGC_DEBOUNCE2			92
#define TI3120_AGC_GA_READING			93

#define TI3120_AGC_ADC_DC_MEAS1			102
#define TI3120_AGC_ADC_DC_MEAS2			103
#define TI3120_AGC_ADC_DC_MEAS_OP1		104
#define TI3120_AGC_ADC_DC_MEAS_OP2		105
#define TI3120_AGC_ADC_DC_MEAS_OP3		106
#define TI3120_VOL_CTRL_ADC			116
#define TI3120_VOL_MICDET_GAIN			117

//Page 1

#define TI3120_SPK_AMP_ERR_CTRL			30
#define TI3120_HP_DRV_REG			31
#define TI3120_CLASSD_AMP			32

#define TI3120_POP_REM				33
#define TI3120_OP_PGA_RAMPDOWN			34
#define TI3120_DAC_OUTPUT_ROUTING		35
#define TI3120_ANA_VOL_HPOUT			36
#define TI3120_ANA_VOL_CLASSD			38
#define TI3120_HPOUT_DRV			40
#define TI3120_CLASSD_DRV			42
#define TI3120_HPOUT_CTRL			44
#define TI3120_MICBIAS				46
#define TI3120_PGA_GAIN				47
#define TI3120_MICPGA_P_CTRL_REG		48
#define TI3120_MICPGA_M_CTRL_REG		49
#define TI3120_MICPGA_CM_CTRL_REG		50



#ifdef __cplusplus
}
#endif


#endif /* __TI3120_H__ */
