
#ifndef __AUDIO_H__
#define __AUDIO_H__


#define AUDIO_REC_PKT_SIZE             160//(1024)
#define AUDIO_PLAY_PKT_SIZE             (1024)



#ifdef PCMU_LAW

  #define AUDIO_REC_PKT_SAMP               AUDIO_REC_PKT_SIZE
  #define AUDIO_PLAY_PKT_SAMP               AUDIO_PLAY_PKT_SIZE
#else //L16

  #define AUDIO_REC_PKT_SAMP               (AUDIO_REC_PKT_SIZE/2)
  #define AUDIO_PLAY_PKT_SAMP               (AUDIO_PLAY_PKT_SIZE/2)
#endif



#define REC_BUFFER_SIZE            (8 * AUDIO_REC_PKT_SIZE)//4
#define REC_WATERMARK              (2 * AUDIO_REC_PKT_SIZE)//2

#define PLAY_BUFFER_SIZE            (4 * AUDIO_PLAY_PKT_SIZE)//4
#define PLAY_WATERMARK              (2 * AUDIO_PLAY_PKT_SIZE)//2

/* RTSP Packet Types */
typedef enum audio_fmt
{
    L16_8000,
    ULAW_8000_MCU,
    ULAW_8000_DSP,
    ALAW_8000_MCU,
    ALAW_8000_DSP,
    L16_16000,
    ULAW_16000_MCU,
    ULAW_16000_DSP,
    ALAW_16000_MCU,
    ALAW_16000_DSP
  //..and so forth
}audio_fmt_e;




typedef struct astream_getinfo
{
    unsigned int  a_total_size;
    unsigned int  a_nxt_pkt_size;

}aic_astream_info_s;

int init_aic3120();

#endif // __AUDIO_H__
