//*****************************************************************************
// audiocodec.c
//
// Interface for Audio codec
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include "hw_types.h"
#include "i2c.h"
#include "hw_memmap.h"
#include "hw_ints.h"
#include "hw_i2c.h"
#include "rom.h"
#include "rom_map.h"
#include "stdlib.h"
#include "prcm.h"
#include "string.h"
#include "hw_common_reg.h"
#include "utils.h"
#include "pin.h"
#include "i2c_if.h"
#include "uart_if.h"
#include "audiocodecAIC3120.h"
#include "ti3120.h"
#include "base_main_Rate16_pps_driver.h"
#include "audio.h"
// User define


static unsigned char val[2],I,J=0;

static unsigned long AudioCodecPageSelect(unsigned char ulPageAddress);
static unsigned long AudioCodecRegWrite(unsigned char ulRegAddr,unsigned char ucRegValue);

extern int audio_format;

//*****************************************************************************
//
//! Reset or initialize code
//!
//! \param[in] codecId 	- Device id that need to reset/init
//! \param[in] arg 		- Pointer to user define structure/data type else NULL
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecReset(unsigned char codecId, void *arg)
{
    if(codecId == AUDIO_CODEC_TI_3120)
    {

        //
        // Select page 0
        //
        AudioCodecPageSelect(TI3120_PAGE_0);

        //
        // Soft RESET
        //
        AudioCodecRegWrite(TI3120_SW_RESET_REG, 0x01);
    }

    return 0;
}

//*****************************************************************************
//
//! Configure audio codec for smaple rate, bits and number of channels
//!
//! \param[in] codecId 			- Device id
//! \param[in] bitsPerSample 	- Bits per sample (8, 16, 24 etc..)
//!									Please ref Bits per sample Macro section
//! \param[in] bitRate 			- Sampling rate in Hz. (8000, 16000, 44100 etc..)
//! \param[in] noOfChannels 	- Number of channels. (Mono, stereo etc..)
//!									Please refer Number of Channels Macro section
//! \param[in] speaker 			- Audio out that need to configure. (headphone, line out, all etc..)
//!									Please refer Audio Out Macro section
//! \param[in] mic 				- Audio in that need to configure. (line in, mono mic, all etc..)
//!									Please refer Audio In Macro section
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecConfig(unsigned char codecId, unsigned char bitsPerSample, unsigned short bitRate,
                      unsigned char noOfChannels, unsigned char speaker,  unsigned char mic)
{
    unsigned int	bclk = 0;

    if(codecId == AUDIO_CODEC_TI_3120)
    {
      
        bclk = 512000;
        if(bclk == 512000)
        {
          
            /*    L16_16000,
            ULAW_8000_MCU,
            ULAW_8000_DSP,
            ALAW_8000_MCU,
            ALAW_8000_DSP*/
          
            /*
            reg[  0][  5]0x05 = 0x91	TI3120_CLK_PLL_P_R_REG; P=1, R=1, J=24
            reg[  0][  6]0x06 = 0x18	TI3120_CLK_PLL_J_REG; P=1, R=1, J=24
            reg[  0][  7]0x07 = 0x00	TI3120_CLK_PLL_D_MSB_REG; D=0000 (MSB)
            reg[  0][  8]0x08 = 0x00	TI3120_CLK_PLL_D_LSB_REG; D=0000 (LSB)
            reg[  0][  4]0x04 = 0x03	TI3120_CLK_MUX_REG; PLL_clkin = MCLK, codec_clkin = PLL_CLK, PLL on
            reg[  0][ 12]0x0c = 0x88	TI3120_CLK_MDAC_REG; MDAC = 8, divider powered on
            reg[  0][ 13]0x0d = 0x01	TI3120_DAC_OSR_MSB_REG; DOSR = 384 (MSB)
            reg[  0][ 14]0x0e = 0x80	TI3120_DAC_OSR_LSB_REG; DOSR = 384 (LSB)
            reg[  0][ 18]0x12 = 0x02	TI3120_CLK_NADC_REG; NADC = 2, divider powered off
            reg[  0][ 19]0x13 = 0x98	TI3120_CLK_MADC_REG; MADC = 24, divider powered on
            reg[  0][ 20]0x14 = 0x80	TI3120_ADC_AOSR_REG; AOSR = 128
            reg[  0][ 11]0x0b = 0x82	TI3120_CLK_NDAC_REG; NDAC = 2, divider powered on
            */
            AudioCodecPageSelect(TI3120_PAGE_0);
            AudioCodecRegWrite(TI3120_CLK_MUX_REG, 0x03);		// PLL Clock is CODEC_CLKIN
            switch(audio_format){
            case L16_16000:
                AudioCodecRegWrite(TI3120_CLK_PLL_J_REG, 0x18);		// J=24
                AudioCodecRegWrite(TI3120_CLK_PLL_D_MSB_REG, 0x00);	// D = 0
                AudioCodecRegWrite(TI3120_CLK_PLL_D_LSB_REG, 0x00);	// D = 0
                AudioCodecRegWrite(TI3120_CLK_PLL_P_R_REG, 0x18);	// PLL is powered down, P=1, R=8
                AudioCodecRegWrite(TI3120_CLK_PLL_P_R_REG, 0x98);	// PLL is powered up, P=1, R=8

                AudioCodecRegWrite(TI3120_CLK_NDAC_REG, 0x82);	// NDAC divider powered up, NDAC = 2
                AudioCodecRegWrite(TI3120_CLK_NADC_REG, 0x82);    	// NADC divider powered up, NADC = 2
                
                AudioCodecRegWrite(TI3120_CLK_MDAC_REG, 0x98);	// MDAC divider powered up, MDAC = 8
                AudioCodecRegWrite(TI3120_CLK_MADC_REG, 0x98);      // MADC divider powered up, MADC = 24
                
                AudioCodecRegWrite(TI3120_DAC_OSR_MSB_REG, 0x01);	//
                AudioCodecRegWrite(TI3120_DAC_OSR_LSB_REG, 0x80);	// DOSR = 0x0180 = 384
                AudioCodecRegWrite(TI3120_ADC_AOSR_REG, 0x80);    	// AOSR = 128
                break;
            case ULAW_8000_MCU:
            case ULAW_8000_DSP:
            case ALAW_8000_MCU:
            case ALAW_8000_DSP:
                AudioCodecRegWrite(TI3120_CLK_PLL_J_REG, 0x18);		// J=24
                AudioCodecRegWrite(TI3120_CLK_PLL_D_MSB_REG, 0x00);	// D = 0
                AudioCodecRegWrite(TI3120_CLK_PLL_D_LSB_REG, 0x00);	// D = 0
                AudioCodecRegWrite(TI3120_CLK_PLL_P_R_REG, 0x18);	// PLL is powered down, P=1, R=8
                AudioCodecRegWrite(TI3120_CLK_PLL_P_R_REG, 0x98);	// PLL is powered up, P=1, R=8

                AudioCodecRegWrite(TI3120_CLK_NDAC_REG, 0x82);	// NDAC divider powered up, NDAC = 2
                AudioCodecRegWrite(TI3120_CLK_NADC_REG, 0x82);    	// NADC divider powered up, NADC = 2
                
                AudioCodecRegWrite(TI3120_CLK_MDAC_REG, 0x88);	// MDAC divider powered up, MDAC = 8
                AudioCodecRegWrite(TI3120_CLK_MADC_REG, 0x98);      // MADC divider powered up, MADC = 24
                
                AudioCodecRegWrite(TI3120_DAC_OSR_MSB_REG, 0x03);	// 
                AudioCodecRegWrite(TI3120_DAC_OSR_LSB_REG, 0x00);	// DOSR = 0x0300 = 768// DOSR = 0x0180 = 384// DOSR = 0x00C0 = 192
                AudioCodecRegWrite(TI3120_ADC_AOSR_REG, 0x00);    	// AOSR = 256
              break;
                
            }
            
            
            // Set I2S Mode and Word Length
            switch(bitsPerSample){
                case AUDIO_CODEC_16_BIT:
                    AudioCodecRegWrite(TI3120_AUDIO_IF_1_REG, 0x00); 	// 0x00 	16bit, I2S, BCLK is input to the device     
                break;
                case AUDIO_CODEC_20_BIT:
                    AudioCodecRegWrite(TI3120_AUDIO_IF_1_REG, 0x10); 	// 0x00 	20bit, I2S, BCLK is input to the device
                break;
                case AUDIO_CODEC_24_BIT:
                    AudioCodecRegWrite(TI3120_AUDIO_IF_1_REG, 0x20); 	// 0x00 	24bit, I2S, BCLK is input to the device
                break;
                case AUDIO_CODEC_32_BIT:
                    AudioCodecRegWrite(TI3120_AUDIO_IF_1_REG, 0x30); 	// 0x00 	32bit, I2S, BCLK is input to the device
                break;
                default: 
                    return -1;
            }
            /*
            AudioCodecPageSelect(TI3120_PAGE_43);
            AudioCodecRegWrite(95, 0x00); //(Bit 23-16) ------------ MSB ADC INST No. 383
            AudioCodecRegWrite(96, 0x00); //(Bit 15-8)
            AudioCodecRegWrite(97, 0x00); //(Bit 7-0)
            AudioCodecPageSelect(TI3120_PAGE_95);
            AudioCodecRegWrite(95, 0x00); //(Bit 23-16) ------------ MSB DAC INST No. 1023
            AudioCodecRegWrite(96, 0x00); //(Bit 15-8)
            AudioCodecRegWrite(97, 0x00); //(Bit 7-0)	
            
            AudioCodecPageSelect(TI3120_PAGE_0);
            AudioCodecRegWrite(TI3120_DAC_IDAC_REG, 0x20); //256 : 0x40 reg val (actual val/4)
            AudioCodecRegWrite(TI3120_ADC_IADC_REG, 0x20); //128 : 0x40 reg val (actual val/2)
            AudioCodecProgDSPA();
            AudioCodecProgDSPD();
            AudioCodecProgReg();
            */
            
        }
        else
        {
            return -1;
        }
        usleep(2000);
        if(speaker)
        {
            AudioCodecPageSelect(TI3120_PAGE_0);	//Select Page 0
            AudioCodecRegWrite(TI3120_DAC_SIG_P_BLK_CTRL_REG, 0x04);  // DAC Signal Processing miniDSP: 0x00 PRB 4: 0x04

            AudioCodecRegWrite(TI3120_DAC_VOL_CTRL_REG, 0x00);
            
            AudioCodecRegWrite(TI3120_DAC_CHANNEL_SETUP_1_REG, 0x92);	//DAC Channel Powered Up//For left. 0xB0 for right and left
            //AudioCodecRegWrite(TI3120_DAC_MUTE, 0x04);	//DAC Channel unmute
            
            AudioCodecPageSelect(TI3120_PAGE_1);	// Select Page 1
            
            AudioCodecRegWrite(TI3120_DAC_OUTPUT_ROUTING,0x40);//Only DAC output to mixer amp
            AudioCodecRegWrite(TI3120_ANA_VOL_CLASSD,0x00);
            AudioCodecRegWrite(TI3120_CLASSD_AMP, 0x80);	
            AudioCodecRegWrite(TI3120_CLASSD_DRV, 0x14);
        }

        if(mic)
        {

            AudioCodecPageSelect(TI3120_PAGE_0);		//Select Page 0
            AudioCodecRegWrite(TI3120_ADC_SIG_P_BLK_CTRL_REG, 0x04);	// ADC Signal Processing miniDSP: 0x00 PRB 4: 0x04

            
            AudioCodecPageSelect(TI3120_PAGE_1);     //Select Page 1
            AudioCodecRegWrite(TI3120_MICBIAS, 0x0A);// MICBIAS powered up 
            if(mic & AUDIO_CODEC_MIC_1_SINGLE)
            {
                AudioCodecRegWrite(TI3120_MICPGA_P_CTRL_REG, 0x10);	// P ctrl MIC1 RP selected  0x20 for 20k  //0x10 for 10k
                AudioCodecRegWrite(TI3120_MICPGA_M_CTRL_REG, 0x40);	// CM with 10k resistance
                AudioCodecRegWrite(TI3120_MICPGA_CM_CTRL_REG, 0x40);	// CM settings
                AudioCodecRegWrite(TI3120_PGA_GAIN, 0x42); //PGA gain 6 dB
            }
            if(mic & AUDIO_CODEC_MIC_2_SINGLE)
            {
                AudioCodecRegWrite(TI3120_MICPGA_P_CTRL_REG, 0x40);	// P ctrl MIC2 LP selected  0x80 for 20k//0x40 for 10k
                AudioCodecRegWrite(TI3120_MICPGA_M_CTRL_REG, 0x10);	// 0x40: CM with 10k resistance; 0x10: LM with 10k resistance
                AudioCodecRegWrite(TI3120_MICPGA_CM_CTRL_REG, 0x20);	// CM settings //0x40
                AudioCodecRegWrite(TI3120_PGA_GAIN, 0xBF); //PGA gain 6 dB
            }

             
            AudioCodecPageSelect(TI3120_PAGE_0);	// Select Page 0     
            AudioCodecRegWrite(TI3120_ADC_CHANNEL_SETUP_REG, 0x82); //datashett specs dig mic, but purepath specs ADC powerup

            
        
	}
        /*
        AudioCodecPageSelect(TI3120_PAGE_0);
        AudioCodecRegWrite(TI3120_DSP_INS_MODE_CTL, 0x11);
        */
        //Adaptive mode enable for DAC
        //AudioCodecPageSelect(TI3120_PAGE_8);
        //AudioCodecRegWrite(1,0x04); 
        

    }

    return 0;
}

int AudioCodecProgDSPA(){
  
    int i;
    
    i = miniDSP_A_reg_values_COEFF_START;
    while(i < miniDSP_A_reg_values_COEFF_START + miniDSP_A_reg_values_COEFF_SIZE){
        AudioCodecRegWrite(miniDSP_A_reg_values[i].reg_off, miniDSP_A_reg_values[i].reg_val);
        i++;
    }
    i = miniDSP_A_reg_values_INST_START;
    while(i < miniDSP_A_reg_values_INST_START + miniDSP_A_reg_values_INST_SIZE){
        AudioCodecRegWrite(miniDSP_A_reg_values[i].reg_off, miniDSP_A_reg_values[i].reg_val);
        i++;
    }
  
    return 0;
}

int AudioCodecProgDSPD(){
  
    int i;
    
    i = miniDSP_D_reg_values_COEFF_START;
    while(i < miniDSP_D_reg_values_COEFF_START + miniDSP_D_reg_values_COEFF_SIZE){
        AudioCodecRegWrite(miniDSP_D_reg_values[i].reg_off, miniDSP_D_reg_values[i].reg_val);
        i++;
    }
    i = miniDSP_D_reg_values_INST_START;
    while(i < miniDSP_D_reg_values_INST_START + miniDSP_D_reg_values_INST_SIZE){
        AudioCodecRegWrite(miniDSP_D_reg_values[i].reg_off, miniDSP_D_reg_values[i].reg_val);
        i++;
    }
    
    return 0;
}

int AudioCodecProgReg(){
  
    int i;
    
    i = 0;
    while(i < 14){
        AudioCodecRegWrite(REG_Section_program[i].reg_off, REG_Section_program[i].reg_val);
        i++;
    }

  
    return 0;
}
//*****************************************************************************
//
//! Configure volume level for specific audio out on a codec device
//!
//! \param[in] codecId 		- Device id
//! \param[in] speaker	 	- Audio out id. (headphone, line out, all etc..)
//!								Please refer Audio out Macro section
//! \param[in] volumeLevel 	-  Volume level. 127 to 48
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecSpeakerVolCtrl(unsigned char codecId, unsigned char speaker, signed char volumeLevel)
{
    short  vol = 0;
//register accepts vals from -127 to 48 (gain in half steps: from -63.5 to 24 dB)
    if(volumeLevel < -127)
    {
        volumeLevel = -127;
    }
    else if (volumeLevel > 48)
    {
        volumeLevel = 48;
    }

    vol= volumeLevel;
    
    Report("Spk volume: %d reg: 0x%2x\n\r", volumeLevel, vol);

    AudioCodecPageSelect(TI3120_PAGE_0);
    AudioCodecRegWrite(TI3120_DAC_VOL_CTRL_REG, (unsigned char )(vol&0x00FF));

    return 0;
}

//*****************************************************************************
//
//! Mute Audio line out
//!
//! \param[in] codecId 		- Device id
//! \param[in] speaker	 	- Audio out id. (headphone, line out, all etc..)
//!								Please refer Audio out Macro section
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecSpeakerMute(unsigned char codecId, unsigned char speaker)
{
    Report("Speaker Mute\n\r");
    AudioCodecPageSelect(TI3120_PAGE_0);


    AudioCodecRegWrite(TI3120_DAC_MUTE, 0x08);
    return 0;
}

//*****************************************************************************
//
//! Unmute audio line out
//!
//! \param[in] codecId 		- Device id
//! \param[in] speaker	 	- Audio out id. (headphone, line out, all etc..)
//!								Please refer Audio out Macro section
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecSpeakerUnmute(unsigned char codecId, unsigned char speaker)
{
    Report("Speaker Unmute\n\r");

    AudioCodecPageSelect(TI3120_PAGE_0);

    AudioCodecRegWrite(TI3120_DAC_MUTE, 0x00);
    return 0;
}

//*****************************************************************************
//
//! Configure volume level for specific audio in on a codec device
//!
//! \param[in] codecId 		- Device id
//! \param[in] mic		 	- Audio in id. (line in, mono mic, all etc..)
//!								Please refer Audio In Macro section
//! \param[in] volumeLevel 	- Volume level (0 - 100)
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecMicVolCtrl(unsigned char codecId, unsigned char mic, signed char volumeLevel)
{
    static signed char vol = 0x00;
	//Input range from -24 to 40
	if(volumeLevel < -24)
    {
        volumeLevel = -24;
    }
    else if(volumeLevel > 40)
    {
        volumeLevel = 40;
    }
    
    vol = ((volumeLevel << 1)/2)&0x7F;
    //(-40 to 24) + 80 = 40 to 104
    //Register Range from 40 to 104 (corresponds to 20 to -12 dB)
    
    Report("Mic volume: %d reg: 0x%2x\n\r", volumeLevel, vol);

    AudioCodecPageSelect(TI3120_PAGE_0);	// Select Page 0


    //Unmute LADC/RADC
    AudioCodecRegWrite(TI3120_ADC_VOL_CTRL_REG, vol);
    AudioCodecRegWrite(TI3120_ADC_FINE_GAIN_ADJ_REG, 0x00);	// ADC Channel Fine Gain = 0dB, unmute
    return 0;
}

//*****************************************************************************
//
//! Mute Audio line in
//!
//! \param[in] codecId 		- Device id
//! \param[in] mic		 	- Audio in id. (line in, mono mic, all etc..)
//!								Please refer Audio In Macro section
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecMicMute(unsigned char codecId, unsigned char mic)
{
    Report("Mic Mute\n\r");

    AudioCodecPageSelect(TI3120_PAGE_0);	// Select Page 0

    //Unmute LADC/RADC
    AudioCodecRegWrite(TI3120_ADC_FINE_GAIN_ADJ_REG, 0x80);	// ADC Channel Un-muted. ADC Channel Fine Gain = 0dB,
    return 0;
}

//*****************************************************************************
//
//! Unmute audio line
//!
//! \param[in] codecId	- Device id
//! \param[in] mic	 	- Audio in id. (line in, mono mic, all etc..)
//!							Please refer Audio In Macro section
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
int AudioCodecMicUnmute(unsigned char codecId, unsigned char mic)
{
    Report("Mic unmute\n\r");
    AudioCodecPageSelect(TI3120_PAGE_0);	// Select Page 0

    //Unmute LADC/RADC
    AudioCodecRegWrite(TI3120_ADC_FINE_GAIN_ADJ_REG, 0x00);	// ADC Channel Un-muted. ADC Channel Fine Gain = 0dB,
    return 0;
}

//*****************************************************************************
//
//! Select Codec page that need to configure
//!
//! \param[in] ulPageAddress	- page id
//!
//! \return 0 on success else -ve.
//
//*****************************************************************************
static unsigned long AudioCodecPageSelect(unsigned char ulPageAddress)
{
    return AudioCodecRegWrite(TI3120_PAGE_SEL_REG,ulPageAddress);
}


//******************************************************************************
//
// Writes to specfied register
// ulRegAddr - Register Address
// ucRegValue - 8 bit Register Value
//
//******************************************************************************
static unsigned long AudioCodecRegWrite(unsigned char ulRegAddr,unsigned char ucRegValue)
{
    unsigned char ucData[2];
    ucData[0] = (unsigned char)ulRegAddr;
    ucData[1] = ucRegValue;
    val[1]=ucData[1];
#if 0
    if(I2C_IF_ReadFrom(0x44,&ucRegAddr, 1, &ucRegData[0], 2) != 0)
        return -1;
#endif
    J=I2C_IF_Write(CODEC_I2C_SLAVE_ADDR, ucData, 2, 1);
    if(J !=0)
        return (1U<<31);
    MAP_UtilsDelay(27000);

    J=I2C_IF_ReadFrom(CODEC_I2C_SLAVE_ADDR, &ucData[0], 1,&val[0],1);
    if(J !=0)
        return (1U<<31);

    if(val[0] != val[1])
        ++I;

    return 0;
}


