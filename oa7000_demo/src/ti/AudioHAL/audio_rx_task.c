/*
 * Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdlib.h>
#include <unistd.h>
//#include "cc_pal_app.h"

#include <ti/drivers/net/wifi/simplelink.h>
#include "ov_sif_if.h"
#include "rtcp_rtp.h"
#include "sip_rtsp.h"
#include "audio_tx_task.h"
#include "app_config.h"
#include <pthread.h>
#include "mqueue.h"
#include "audio.h"
#include "i2s_if.h"
#include "circ_buff.h"

#define AUDIO_RTP_SSRC              (0xF9143263)

#define PKTS_BW_A_SR_INFO           (128)
#define PKTS_BW_A_RR_INFO           (256)
   



extern mqd_t    gRtspMainSMQueue;
extern mqd_t    gRtspSrvQueue;
extern mqd_t    gSipSrvQueue;
extern mqd_t    gSipCliQueue;
extern mqd_t    gARxSMQueue;
extern mqd_t    gATxSMQueue;
extern mqd_t    gVTxSMQueue;

extern rtp_sock_info_s  app_av_soc_info[NUM_OF_MEDIA_STREAMS];

extern pthread_mutex_t frame_lock;

extern int audio_format;
extern int audio_rate;

unsigned char audio_rx_temp_buffer[AUDIO_PLAY_PKT_SIZE];


static int32_t process_spk_data();


/* Play buffer */
extern circular_buffer_s       *p_play_buffer;

/* A-Stream Data */
a_data_s                app_a_data          = {0};

static int8_t            rx_buffer[MAX_AUDIO_BUFF_SIZE]  = {0};

static rtp_profile_s    audio_rx_rtp_profile   = {0};

//static uint32_t           pkt_cnt         = 0;
//static uint32_t           octet_count     = 0;


/**/
void * ARxTask(void *p_args)
{

    
    uint8_t           a_streaming = 0;
    
    

    a_task_msg_s  msg_on_a_rx_task_q;
    
    memset(&msg_on_a_rx_task_q, 0, sizeof(msg_on_a_rx_task_q));
    memset(&audio_rx_rtp_profile, 0, sizeof(audio_rx_rtp_profile));

    /** Dynamic profile number for audio meadia as listed
      * in 'sdp_out_template'
      */
    audio_rx_rtp_profile.type              = AUDIO;

    switch(audio_format){
    case L16_16000:
      audio_rx_rtp_profile.payload_type      = 97;
      audio_rx_rtp_profile.payload_format    = L16_PCM;
      break;
    case ULAW_8000_MCU:
      audio_rx_rtp_profile.payload_type      = 0;
      audio_rx_rtp_profile.payload_format    = PCMU;
      break;
    case ALAW_8000_MCU:

      break;
    case ULAW_8000_DSP:
      audio_rx_rtp_profile.payload_type      = 0;
      audio_rx_rtp_profile.payload_format    = PCMU;
      break;
    case ALAW_8000_DSP:

      break;
    }

    /* SSRC - A Random Number */
    audio_rx_rtp_profile.ssrc  = AUDIO_RTP_SSRC;

    while(mq_receive(gARxSMQueue, (char *)&msg_on_a_rx_task_q, sizeof(a_task_msg_s), NULL) >= 0)
    {
        switch(msg_on_a_rx_task_q.msg_id)
        {
            case START_A_STREAMING:
            {

                Report("Audio RX start\r\n");
                a_streaming = 1;
                audio_rx_rtp_profile.p_data.aProfile.first_packet=1;
                msg_on_a_rx_task_q.task_id = AUDIO_RX_TASK_ID;
                msg_on_a_rx_task_q.msg_id  = SPK_DATA_RECV;

                mq_send(gARxSMQueue, (char *)&msg_on_a_rx_task_q, sizeof(a_task_msg_s), 0);    
            }
            break;

            case STOP_A_STREAMING:
            {
                Report("Audio streaming stop\r\n");
                a_streaming = 0;
                //audio_i2s_stop();
            }
            break;

            {
            case SPK_DATA_RECV:
                  //If mic data available send out
                if(a_streaming){
                    
                    if(is_buffer_vacant(p_play_buffer, AUDIO_PLAY_PKT_SIZE)){
                        process_spk_data();

                    }
                    else{
                        usleep(25000);
                    }
                    msg_on_a_rx_task_q.task_id = AUDIO_RX_TASK_ID;
                    msg_on_a_rx_task_q.msg_id  = SPK_DATA_RECV;

                    mq_send(gARxSMQueue, (char *)&msg_on_a_rx_task_q, sizeof(a_task_msg_s), 0);       
                }
            }
            break;
            
            default:
            {
                Report("Unidentified message on A-Task-Q: Msg-ID [%ld]", msg_on_a_rx_task_q.msg);
            }
            break;
        }
    }

    /**/
return(0);
}



static int32_t process_spk_data(){
  
    SlSockAddrIn_t      client_addr         = {0};
    unsigned char*      audio_data;
    uint32_t              rx_pkt_size;
    int32_t              addr_size   = 0;
    int32_t              ret_val;
    uint16_t              rx_data_size;

    

    /* 1 - Index for Audio */
    rtp_sock_info_s         *p_a_soc_info = &app_av_soc_info[1];


    //Blocking recv on rtp socket

    //If space in circ buffer: parse rtp packet, fill buffer

      

    addr_size   = sizeof(SlSockAddrIn_t);
    ret_val     = sl_RecvFrom(p_a_soc_info->rtp_sock_id,       \
                              rx_buffer, sizeof(rx_buffer), 0,  \
                              (SlSockAddr_t *)&client_addr,     \
                              (SlSocklen_t*)&addr_size);
    if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
    {
        ERR_PRINT(ret_val);
        return -1;
    }
    else if(ret_val > 0){
        rx_pkt_size = ret_val;

        rtp_decode(rx_buffer, rx_pkt_size, &rx_data_size, &audio_rx_rtp_profile);
        
        audio_data=(unsigned char *)rx_buffer+sizeof(rtp_header_s);
        //pkt_cnt += 1;
        //octet_count += rx_data_size;
        //rtp_decode(rx_buffer, rx_pkt_size, &rx_data_size, &audio_rx_rtp_profile);
        fill_buffer(p_play_buffer, audio_data, rx_data_size);
    }
       

    return 0;
  
}

