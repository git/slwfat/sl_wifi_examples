//*****************************************************************************
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************


//*****************************************************************************
//
// Application Name     -   Wifi Audio Application
// Application Overview -   This is a sample application demonstrating 
//                          Bi-directional audio transfer.Audio is streamed
//                          from one LP and rendered on another LP over wifi. 
//
// Application Details  -
// http://processors.wiki.ti.com/index.php/CC32xx_Wifi_Audio_Application
// or
// doc\examples\CC32xx Wifi Audio Application.pdf
//
//*****************************************************************************

//****************************************************************************
//
//! \addtogroup wifi_audio_app
//! @{
//
//****************************************************************************
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

//SimpleLink includes
#include <ti/drivers/net/wifi/simplelink.h>



// Hardware & DriverLib library includes.
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "interrupt.h"
#include "i2s.h"
#include "udma.h"
#include "gpio.h"

#include "prcm.h"
#include "rom.h"
#include "rom_map.h"
#include "pin.h"
#include "utils.h"

//Common interface includes

#include "udma_if.h"
#include "uart_if.h"
#include "i2c_if.h"



//App include
#include "pinmux.h"
#include "circ_buff.h"
#include "audiocodecAIC3120.h"
#include "i2s_if.h"
#include "pcm_handler.h"
#include "audio.h"
#include "app_common.h"

#define APPLICATION_VERSION     "1.1.1"
#define OSI_STACK_SIZE          1024

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************

circular_buffer_s *p_rec_buffer = NULL;
circular_buffer_s *p_play_buffer = NULL;

uint8_t rec_buffer[REC_BUFFER_SIZE];
uint8_t play_buffer[PLAY_BUFFER_SIZE];

int audio_format=ULAW_8000_MCU;
int audio_rate=8000;

int init_aic3120()
{   
    long lRetVal = -1;

    Report("AIC3120 init\r\n");
    //
    // Initialising the I2C Interface
    //    
    
    lRetVal = I2C_IF_Open(1);
    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }
//
    // Initialize the DMA Module
    //    
    UDMAInit();

    p_rec_buffer = create_circular_buffer_static(rec_buffer,REC_BUFFER_SIZE);
    p_play_buffer = create_circular_buffer_static(play_buffer,PLAY_BUFFER_SIZE);



    UDMAChannelSelect(UDMA_CH5_I2S_TX, NULL);
    SetupPingPongDMATransferRx(p_play_buffer);

    UDMAChannelSelect(UDMA_CH4_I2S_RX, NULL);
    SetupPingPongDMATransferTx(p_rec_buffer);

    //
    // Setup the Audio In/Out
    //     
    lRetVal = AudioSetupDMAMode(DMAPingPongCompleteAppCB_opt, CB_EVENT_CONFIG_SZ, I2S_MODE_RX_TX);
    if(lRetVal < 0)
    {
        ERR_PRINT(lRetVal);
        LOOP_FOREVER();
    }    
    AudioCaptureRendererConfigure(AUDIO_CODEC_16_BIT, audio_rate, AUDIO_CODEC_MONO, I2S_MODE_RX_TX, 1);

    audio_i2s_start(I2S_MODE_RX_TX);

    //
    // Configure Audio Codec
    //     
    usleep(1000);
    AudioCodecReset(AUDIO_CODEC_TI_3120, NULL);
    usleep(1000);
    AudioCodecConfig(AUDIO_CODEC_TI_3120, AUDIO_CODEC_16_BIT, audio_rate,
                      AUDIO_CODEC_MONO, AUDIO_CODEC_SPEAKER_ALL,
                      AUDIO_CODEC_MIC_2_SINGLE);
    AudioCodecSpeakerMute(AUDIO_CODEC_TI_3120, AUDIO_CODEC_SPEAKER_ALL);
    AudioCodecSpeakerVolCtrl(AUDIO_CODEC_TI_3120, AUDIO_CODEC_SPEAKER_ALL, -40);//48 max -127 min
    AudioCodecMicVolCtrl(AUDIO_CODEC_TI_3120, AUDIO_CODEC_SPEAKER_ALL, 40);//40 max
    AudioCodecSpeakerUnmute(AUDIO_CODEC_TI_3120, AUDIO_CODEC_SPEAKER_ALL);
    /*
    AudioCodecProgDSPA();
    AudioCodecProgDSPD();
    
    AudioCodecProgReg();
    */

    
    return 0;
	//threads
}
