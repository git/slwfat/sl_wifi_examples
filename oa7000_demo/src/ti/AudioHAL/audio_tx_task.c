/*
 * Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdlib.h>
#include <unistd.h>
//#include "cc_pal_app.h"

#include <ti/drivers/net/wifi/simplelink.h>
#include "ov_sif_if.h"
#include "rtcp_rtp.h"
#include "sip_rtsp.h"
#include "audio_tx_task.h"
#include "app_config.h"
#include <pthread.h>
#include "mqueue.h"
#include "audio.h"
#include "i2s_if.h"
#include "circ_buff.h"

#define AUDIO_RTP_SSRC              (0xF9143263)

#define PKTS_BW_A_SR_INFO           (128)
#define PKTS_BW_A_RR_INFO           (256)
   



extern mqd_t    gRtspMainSMQueue;
extern mqd_t    gRtspSrvQueue;
extern mqd_t    gSipSrvQueue;
extern mqd_t    gSipCliQueue;
extern mqd_t    gATxSMQueue;
extern mqd_t    gVTxSMQueue;

extern rtp_sock_info_s  app_av_soc_info[NUM_OF_MEDIA_STREAMS];

extern pthread_mutex_t frame_lock;

extern int audio_format;
extern int audio_rate;

unsigned char audio_temp_buffer[AUDIO_REC_PKT_SIZE];



static int32_t process_mic_data();


/* Play buffer */
extern circular_buffer_s       *p_rec_buffer;



static uint8_t            tx_buffer[MAX_AUDIO_BUFF_SIZE]  = {0};

static rtp_profile_s    audio_rtp_profile   = {0};


/**/
void * ATxTask(void *p_args)
{

    
    uint8_t           a_streaming = 0;
    
    

    a_task_msg_s  msg_on_a_task_q;
    
    memset(&msg_on_a_task_q, 0, sizeof(msg_on_a_task_q));
    memset(&audio_rtp_profile, 0, sizeof(audio_rtp_profile));

    /** Dynamic profile number for audio media as listed
      * in 'sdp_out_template'
      */
    audio_rtp_profile.type              = AUDIO;

    switch(audio_format){
    case L16_16000:
      audio_rtp_profile.payload_type      = 97;
      audio_rtp_profile.payload_format    = L16_PCM;
      break;
    case ULAW_8000_MCU:
      audio_rtp_profile.payload_type      = 0;
      audio_rtp_profile.payload_format    = PCMU;
      break;
    case ALAW_8000_MCU:

      break;
    case ULAW_8000_DSP:
      audio_rtp_profile.payload_type      = 0;
      audio_rtp_profile.payload_format    = PCMU;
      break;
    case ALAW_8000_DSP:

      break;
    }

    /* SSRC - A Random Number */
    audio_rtp_profile.ssrc  = AUDIO_RTP_SSRC;



    while(mq_receive(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), NULL) >= 0)
    {
        switch(msg_on_a_task_q.msg_id)
        {
            case START_A_STREAMING:
            {
                audio_rtp_profile.octet_cnt = 0;
                audio_rtp_profile.pkt_cnt = 0;
                audio_rtp_profile.seq_num = 0;
                Report("Audio streaming start\r\n");
                //audio_i2s_start(I2S_MODE_RX_TX);
                                
                a_streaming = 1;
                audio_rtp_profile.p_data.aProfile.first_packet=1;
                msg_on_a_task_q.task_id = AUDIO_TX_TASK_ID;
                msg_on_a_task_q.msg_id  = MIC_DATA_AVAIL;

                mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);    
            }
            break;

            case STOP_A_STREAMING:
            {
                Report("Audio streaming stop\r\n");
                a_streaming = 0;
                //audio_i2s_stop();
            }
            break;

            case MIC_DATA_AVAIL:
            {
                  //If mic data available send out
                if(a_streaming){
                    if(is_buffer_filled(p_rec_buffer, AUDIO_REC_PKT_SIZE)){
                          process_mic_data();
                          //Report("a");
                    }
                    else{
                          usleep(25000);//((AUDIO_REC_PKT_SIZE)/2)/audio_rate//  (samp*(bytes/samp))/(bytes/samp)/(samp/sec)
                    }
                    
                    msg_on_a_task_q.task_id = I2S_EVENT_HANDLER;
                    msg_on_a_task_q.msg_id  = MIC_DATA_AVAIL;

                    mq_send(gATxSMQueue, (char *)&msg_on_a_task_q, sizeof(a_task_msg_s), 0);       
                }
            }
            break;
            

            
            default:
            {
                Report("Unidentified message on A-Task-Q: Msg-ID [%ld]", msg_on_a_task_q.msg);
            }
            break;
        }
    }

    /**/
return(0);
}



/**/
static int32_t process_mic_data()
{
    SlSockAddrIn_t  client_addr         = {0};

    //uint32_t          total_bytes_to_tx   = 0;
   // uint32_t          pkt_size            = 0;

    //int32_t          bytes_to_send       = 0;
    int32_t          ret_val             = 0;


    //int32_t          addr_size   = 0;

    /* 1 - Index for Audio */
    rtp_sock_info_s         *p_a_soc_info = &app_av_soc_info[1];
    
//#ifdef RX_RR_TX_SR

    memset(&client_addr, 0, sizeof(client_addr));

    //sr_info.sender_ssrc = audio_rtp_profile.ssrc;
/*
    memset(tx_buffer, 0x00, sizeof(tx_buffer));
    addr_size   = sizeof(SlSockAddrIn_t);
    ret_val     = sl_RecvFrom(p_a_soc_info->rtcp_sock_id,       \
                              tx_buffer, sizeof(tx_buffer), 0,  \
                              (SlSockAddr_t *)&client_addr,     \
                              (SlSocklen_t*)&addr_size);
    if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
    {
        ERR_PRINT(ret_val);
        goto exit_process_a_data;
    }

    rtp_process_rtcp_pkt(tx_buffer, ret_val, &rr_info);
*/
    
    /* Send SR-Info every 'n' packets over the RTCP socket */
    /*
    if(0 == (pkt_cnt % PKTS_BW_A_SR_INFO))
    {



        bytes_to_send=rtcp_fill_sr_pkt(tx_buffer);
        bytes_to_send+=rtcp_fill_sdes_pkt(&tx_buffer[bytes_to_send]);
        
        ret_val = sl_SendTo(p_a_soc_info->rtcp_sock_id,     \
                            tx_buffer, bytes_to_send, 0,    \
                            (const SlSockAddr_t *)&(p_a_soc_info->rtcp_addr),\
                            p_a_soc_info->addr_size);
        if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
        {
            ERR_PRINT(ret_val);
            goto exit_process_a_data;
        }
    }
    */
    /* Send RR-Info every 'n' packets over the RTCP socket */
    /*
    if(0 == (audio_rtp_profile.pkt_cnt % PKTS_BW_A_RR_INFO))
    {


        //memset(tx_buffer, 0x00, sizeof(tx_buffer));
        bytes_to_send=rtcp_fill_rr_pkt(tx_buffer, &audio_rtp_profile);
        bytes_to_send+=rtcp_fill_sdes_pkt(&tx_buffer[bytes_to_send], &audio_rtp_profile);

        ret_val = sl_SendTo(p_a_soc_info->rtcp_sock_id,     \
                            tx_buffer, bytes_to_send, 0,    \
                            (const SlSockAddr_t *)&(p_a_soc_info->rtcp_addr),\
                            p_a_soc_info->addr_size);
        if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
        {
            ERR_PRINT(ret_val);
            goto exit_process_a_data;
        }
    }*/
//#endif /* RX_RR_TX_SR */


    //Pull AUDIO_TX_PACKET_SIZE packets from the circular buffer until < AUDIO_TX_PACKET_SIZE available
    //If not enough data available for whole RTP packet, then pass through, but there should be if we get here
    while(pull_buffer(p_rec_buffer, audio_temp_buffer, AUDIO_REC_PKT_SIZE) == AUDIO_REC_PKT_SIZE)
    {


        /*!
         * Time stamp counts 'TIMER_CNTS_PER_SEC' times in one second
         * Therefore, timestamp = counter * (audio_rate/TIMER_CNTS_PER_SEC)
         */
        
        //audio_rtp_profile.timestamp = cc_get_timestamp() * (audio_rate/TIMER_CNTS_PER_SEC);
        audio_rtp_profile.timestamp +=AUDIO_REC_PKT_SIZE;///audio_rate;8000/
        // Fragment the packet and send it over UDP 
      
        // Sequence 


        audio_rtp_profile.p_data.aProfile.mode = 2; /* NA */
        
        // Create RTP packet 
        memset(tx_buffer, 0x00, sizeof(tx_buffer));
        ret_val = rtp_encode(audio_temp_buffer, AUDIO_REC_PKT_SIZE, &tx_buffer[0],\
                             sizeof(tx_buffer), &audio_rtp_profile);
        if(ret_val < 0)
        {
            ERR_PRINT(ret_val);
            goto exit_process_a_data;
        }
        
        //AA
        //Report("Audio out: %i\r\n", AUDIO_REC_PKT_SIZE);
        
        //bytes_to_send = ret_val;
        ret_val = sl_SendTo(p_a_soc_info->rtp_sock_id, tx_buffer, ret_val,\
                            0, (const SlSockAddr_t *)&(p_a_soc_info->rtp_addr),\
                            p_a_soc_info->addr_size);
        if(ret_val <= 0 && ret_val != SL_ERROR_BSD_EAGAIN)
        {
            ERR_PRINT(ret_val);
            goto exit_process_a_data;
        }

        //Report("[Audio Task] - Data Sent - %d bytes\r\n", bytes_to_send);


        audio_rtp_profile.pkt_cnt += 1;
        audio_rtp_profile.seq_num += 1;
        audio_rtp_profile.octet_cnt += AUDIO_REC_PKT_SIZE;

    }



exit_process_a_data:
    return ret_val;
}

