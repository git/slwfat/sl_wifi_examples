//*****************************************************************************
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <mqueue.h>
//#include "cc_pal_app.h"
#include "ti_drivers_config.h"
#include "fs_if.h"
#include "ti/drivers/spi.h"
#include "ti/drivers/gpio.h"
#include "ov_sif_if.h"


#define OSD_COMMAND_CLASS_ID                0x03
#define SYSTEM_COMMAND_CLASS_ID             0x06

#define SENSOR_CNTRL_COMMAND_CLASS_ID       0x10
#define VIDEO_CNTRL_COMMAND_CLASS_ID        0x11
#define AUDIO_CNTRL_COMMAND_CLASS_ID        0x12
#define GET_VIDEO_COMMAND_CLASS_ID          0xA0
#define GET_AUDIO_COMMAND_CLASS_ID          0xA1
#define GET_MOTION_IMAGE_COMMAND_CLASS_ID   0xA3

#define OVT_FW_FILE                         "/user/ovt_firmware.bin"
#define OVT_SINGLE_FILE                     "/user/ovt_single.bin"
#define OV_CMD_ACK                          0x02

#define OV_SLAVE_BOOT_ADDR (0x10000000)
#define OV_SLAVE_SINGLE_ADDR (0x10100000)

#define SWAP32(v) (v)//((v&0x000000ff)<<24 | (v&0x0000ff00)<<8 | (v&0x00ff0000)>>8 |  (v&0xff000000)>>24)
#define SWAP16(v) (v)//((v&0x00ff)<<8 | (v&0xff00)>>8)
                    
#define DELAY_US_1 900000
#define DELAY_US_2 900000

#define SPI_IF_BIT_RATE         10000000    /* 10MHz */
#define SPI_IF_BIT_RATE_SLOW         2000000    /* 2MHz */

#define MAX_Q_ELEMENTS                      20
#define COMMAND_SIZE                        32
#define FW_BUFF_SIZE                        1024
#define NUM_OF_TRIALS                       20

#ifdef SL_PLATFORM_MULTI_THREADED


    pthread_mutex_t    ov_if_lock;
#else /* SL_PLATFORM_MULTI_THREADED */

#endif /* SL_PLATFORM_MULTI_THREADED */

#ifdef DEBUG_PRINT
extern int Report(const char *format, ...);
#else
#define Report(...)
#endif

SPI_Handle      masterSpi;
/* OV Response Packets */
typedef enum ov_response
{
    OV_RESP_NO_PAYLOAD,
    OV_RESP_PAYLOAD,
    OV_RESP_CMD_FLAG_ERROR,
    OV_RESP_CLASS_ID_NOT_SUPPORTED,
    OV_RESP_CMD_ID_NOT_SUPPORTED,
    OV_RESP_INCORRECT_DATA_PACKET_SIZE,
    OV_RESP_INVALID_PARAM,
    OV_RESP_PARAM_OUT_OF_RANGE,
    OV_RESP_FWID_OUT_OF_RANGE

}ov_response_e;


typedef enum {
    OV_BOOT_MAGIC_NUMBER = 0x4F565449,
    OV_BOOT_MAGIC_ACK1 = 0x48454144,
    OV_BOOT_MAGIC_ACK3 = 0x46574F4B
} e_dsif_boot_magic;

typedef struct s_header{
    unsigned int magic;
    unsigned int reserved1;
    unsigned int quickset_len;
    unsigned int reserved2;
    unsigned int sccbtb1_addr;
    unsigned int sccbtb1_len;
    unsigned int fw_addr;
    unsigned int fw_len;
    unsigned int sif_cfg0;
    unsigned int sif_cfg1;
    unsigned int sif_cfg2;
    unsigned int sif_cfg3;
    unsigned int reserved3;
    unsigned int reserved4;
    unsigned int sys_config0;
    unsigned short fw_crc;
    unsigned short hdr_crc;
} __attribute__((packed)) ov_boot_header_t;

unsigned int boot_798_slave();
/*!
 * Downloads the OV FW stored on NVM and programs the content onto the OV
 */
//static int32_t ov_download_fw(uint16_t fd);

/*!
 * Powers off the OV798 system
 */
static void ov_poweroff();

/*!
 * Converts the 'val' from host byte order to network byte order.
 */
static uint32_t ov_htonl(uint32_t val);

/*!
 * Verifies the ACK from OV
 */
static int32_t
ov_verify_ack(uint8_t *const p_command, uint8_t const *const p_ackbuffer);

/*!
 * Sends the command to OV
 */
static int32_t ov_send_command(uint8_t const cmd_class, uint8_t const cmd_id,    \
                              uint8_t const param_len, void *p_param,         \
                              uint8_t *const p_ack_payload,                   \
                              uint16_t const payload_buf_len);

/*!
 * Implements the OV's CMD_ACK sequence for sending the command
 * Refer 'SIF Protocol' document for details on the sequence
 */
static int32_t ov_execute_cmd_ack_seq(uint8_t *const p_cmdBuffer,  \
                                     uint8_t *const p_ackBuffer,  \
                                     uint8_t *const p_payload,    \
                                     uint32_t const read_size);

mqd_t       OvQueue;

static uint8_t    cmd_buffer[COMMAND_SIZE]    = {0};
static uint8_t    ack_buffer[COMMAND_SIZE]    = {0};
//static uint8_t    fw_buffer[FW_BUFF_SIZE + 2] = {0};
//
//static uint8_t    ovt_boot_loader_init[]      = {
//                                                0x00,0x01,0x30,0x00,
//                                                0x00,0x80,0x00,0x00,
//                                                0x00,0x00,0x00,0x00,
//                                                0x00,0x00,0x00,0x03,
//                                              };

static const    int8_t cmd_header[]          = {
                                                0xFF,0xFF,0xFF,0x00
                                              };
static const unsigned short crc16tab[256]= {
0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};

unsigned short crc16_ccitt(unsigned char *data, unsigned int len)
{
    unsigned int counter;
    unsigned short crc=0;
    //unsigned char *buf=data;
    for( counter=0; counter < len; counter++){
      crc = (crc<<8)^crc16tab[((crc>>8)^data[counter])&0x00FF];

    }
    return crc;
}


/**/
void ov_irq_handler(void *p_value)
{
    ov_q_msgs_e var     = NO_MESSAGE;
    uint8_t       status  = *((uint8_t *)p_value);

    var = (1 == status) ? OV_TI_GPIO_HIGH : OV_TI_GPIO_LOW;
    mq_send(OvQueue, (const char*)&var, sizeof(ov_q_msgs_e), 0);
}

/**/
int32_t ov_init()
{
    int32_t fd = -1;
    mq_attr            attr;
    SPI_Params      spiParams;
    pthread_mutex_init(&ov_if_lock, (const pthread_mutexattr_t *)NULL);
    
    
    attr.mq_curmsgs = 0;
    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_Q_ELEMENTS;
    attr.mq_msgsize = sizeof( unsigned char );
    OvQueue = mq_open("ov_queue", O_CREAT, 0, &attr);
      
    
    
    //fd = cc_configure_spi(SPI_IF_BIT_RATE_SLOW);
  //  cc_config_ov_irq(ov_irq_handler);
    GPIO_write(CONFIG_GPIO_0, 1);
    pthread_mutex_lock(&ov_if_lock);

    //ov_download_fw(fd);
    boot_798_slave();
      
    //fd = cc_configure_spi(SPI_IF_BIT_RATE);
    /* Open SPI as master (default) */
        SPI_Params_init(&spiParams);
        spiParams.frameFormat = SPI_POL0_PHA0;
       // spiParams.mode = SPI_SUB_MODE_0;
        spiParams.bitRate = 20000000;
        masterSpi = SPI_open(CONFIG_SPI_MASTER, &spiParams);
        if (masterSpi == NULL) {
            Report("Error initializing master SPI\n");
        }
        else {
            Report("Master SPI initialized\n");
            fd = 1;
        }

    pthread_mutex_unlock(&ov_if_lock);
    return fd;
}

/**/
int32_t ov_deinit()
{
    ov_poweroff();
    pthread_mutex_destroy(&ov_if_lock);
    return 0;
}

/**/
int32_t ov_get_version(uint16_t const fd, uint8_t const out_buffer_size,\
                      uint8_t *p_outbuffer, uint8_t *p_length)
{
    uint8_t const payload_size = 4;
    int32_t ret_val = 0;

    pthread_mutex_lock(&ov_if_lock);

    if( (out_buffer_size < payload_size) ||
        (NULL == p_outbuffer) )
    {
        *p_length = 0;
        ret_val = OV_SMALL_BUFFER_SIZE;
        pthread_mutex_unlock(&ov_if_lock); 
        return ret_val;
    }

    ret_val = ov_send_command(SYSTEM_COMMAND_CLASS_ID, 0x00, 0, NULL, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}

    memcpy((void *)p_outbuffer, &ack_buffer[10], payload_size);
    *p_length = payload_size;

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_get_status(uint16_t const fd, uint8_t const out_buffer_size,\
                     uint8_t *p_outbuffer, uint8_t *p_length)
{
    uint8_t const payload_size = 1;
    int32_t ret_val = 0;

    pthread_mutex_lock(&ov_if_lock);

    if( (out_buffer_size < payload_size) ||
        (NULL == p_outbuffer) )
    {
        *p_length = 0;
        ret_val = OV_SMALL_BUFFER_SIZE;
        pthread_mutex_unlock(&ov_if_lock); 
        return ret_val;
    }

    ret_val = ov_send_command(SYSTEM_COMMAND_CLASS_ID, 0x01, 0, NULL, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}

    *p_outbuffer = ack_buffer[10];
    *p_length = payload_size;


    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}
void ov_v_force_I_frame(void)
{
    uint8_t const param_length = 0;

    int32_t ret_val = 0;
    uint8_t  param = 0;

    pthread_mutex_lock(&ov_if_lock);

    /* Configure mode night or day mode */
    param = 0;
    ret_val = ov_send_command(VIDEO_CNTRL_COMMAND_CLASS_ID, 0x09,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return;}
}

/**/
int32_t ov_config_v_sensor(uint16_t const fd, ov_v_config_s const config)
{
    uint8_t const param_length = 1;

    int32_t ret_val = 0;
    uint8_t  param = 0;

    pthread_mutex_lock(&ov_if_lock);

    /* Configure mode night or day mode */
    param = config.mode;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x08,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}

    /* Configure flip */
    param = config.flip;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x06,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}

    /* Configure frequency */
    param = config.frequency;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x05,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}

//    /* Configure saturation */
    param = config.saturation;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x04,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}

    /* Configure contrast */
    param = config.contrast;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x03,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}

    /* Configure brightness */
    param = config.brightness;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x02,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}
////////////////////////////////
    /* Configure HDR */
    param = config.hdr;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x0a,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}
    
    /* Configure WDR */
    param = config.wdr;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x10,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}
    
    /* Configure MCTF */
    param = config.mctf;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x11,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}
    
    /* Configure sharpness */
    param = config.sharpness;
    ret_val = ov_send_command(SENSOR_CNTRL_COMMAND_CLASS_ID, 0x12,\
                              param_length, &param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}
    
    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_set_frame_rate(uint16_t const fd, ov_v_framerate_e const frame_rate)
{
    uint8_t const param_length = 1;

    int32_t ret_val = 0;
    uint8_t  param = 0;

    pthread_mutex_lock(&ov_if_lock);

    param = frame_rate;
    ret_val = ov_send_command(VIDEO_CNTRL_COMMAND_CLASS_ID, 0x01,\
                              param_length, &param, NULL, 0);

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_set_bit_rate(uint16_t const fd, ov_v_bitrate_e const bit_rate)
{
    uint8_t const param_length = 1;

    int32_t ret_val = 0;
    uint8_t  param = 0;

    pthread_mutex_lock(&ov_if_lock);

    param = bit_rate;
    ret_val = ov_send_command(VIDEO_CNTRL_COMMAND_CLASS_ID, 0x02,\
                              param_length, &param, NULL, 0);

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/

int32_t ov_set_iframe_interval(uint16_t const fd, uint8_t const interval)
{
    uint8_t const param_length = 1;

    int32_t ret_val = 0;
    uint8_t  param = 0;

    pthread_mutex_lock(&ov_if_lock);

    param = interval;
    ret_val = ov_send_command(VIDEO_CNTRL_COMMAND_CLASS_ID, 0x08,\
                              param_length, &param, NULL, 0);

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_set_motion_detect(uint16_t const fd, uint8_t const enable)
{
    uint8_t const param_length = 1;

    int32_t ret_val = 0;
    uint8_t  param = 0;

    pthread_mutex_lock(&ov_if_lock);

    param = enable;
    ret_val = ov_send_command(VIDEO_CNTRL_COMMAND_CLASS_ID, 0x07,\
                              param_length, &param, NULL, 0);

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_set_sampling_rate(uint16_t const fd, \
                            ov_a_samplerate_e const sampling_rate)
{
    uint8_t const param_length = 1;

    int32_t ret_val = 0;
    uint8_t  param = 0;

    pthread_mutex_lock(&ov_if_lock);

    /* Configure format */
    param = sampling_rate;
    ret_val = ov_send_command(AUDIO_CNTRL_COMMAND_CLASS_ID, 0x02,\
                              param_length, &param, NULL, 0);

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_config_v_encoder(uint16_t const fd,                     \
                           ov_v_resolution_e const resolution,  \
                           uint8_t const enable)
{
    uint8_t const param_length = 2;

    int32_t ret_val = 0;
    uint8_t  param[2] = {0};

    pthread_mutex_lock(&ov_if_lock);

    param[0] = resolution;
    param[1] = enable;
    ret_val = ov_send_command(VIDEO_CNTRL_COMMAND_CLASS_ID, 0x00,\
                              param_length, &param[0], NULL, 0);

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_get_vstream_info(uint16_t const fd, uint32_t const pkt_size,\
                           ov_vstream_info_s *const p_vstream_info)
{
    uint32_t param = 0;
    uint32_t info = 0;
    int32_t ret_val = 0;

    pthread_mutex_lock(&ov_if_lock);

    param = ov_htonl(pkt_size);
    ret_val = ov_send_command(GET_VIDEO_COMMAND_CLASS_ID, 0x00,\
                              sizeof(param), (void *)&param, NULL, 0);
    if(ret_val < 0) {pthread_mutex_unlock(&ov_if_lock); return ret_val;}
//ack payload starts at index 10
    memcpy((void *)&(info), &ack_buffer[10], sizeof(info));
    p_vstream_info->v_stream_size = ov_htonl(info);

    memcpy((void *)&(info), &ack_buffer[14], sizeof(info));
    p_vstream_info->v_nxt_pkt_size = ov_htonl(info);

    memcpy((void *)&(info), &ack_buffer[18], sizeof(info));
    p_vstream_info->v_frame_timestamp = ov_htonl(info);

    memcpy((void *)&(info), &ack_buffer[22], sizeof(info));
    p_vstream_info->v_frame_number = ov_htonl(info);

    memcpy((void *)&(info), &ack_buffer[26], sizeof(info));
    p_vstream_info->v_frame_type = ov_htonl(info);


    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}

/**/
int32_t ov_get_vstream_data(uint16_t const fd, uint32_t const pkt_size,\
                           uint8_t *const p_outbuffer)
{
    uint32_t param = 0;
    uint32_t read_size = 0;
    int32_t ret_val = 0;

    uint8_t  idx = 0;

    pthread_mutex_lock(&ov_if_lock);

    if(0 == pkt_size)
    {
         ret_val = INVALID_INPUT;
         pthread_mutex_unlock(&ov_if_lock); 
         return ret_val;
    }

    memset(cmd_buffer, 0, sizeof(cmd_buffer));
    memcpy(cmd_buffer, cmd_header, sizeof(cmd_header));
    idx += sizeof(cmd_header);

    cmd_buffer[idx++] = GET_VIDEO_COMMAND_CLASS_ID;
    cmd_buffer[idx++] = 0x01;
    cmd_buffer[idx++] = 0x00;
    cmd_buffer[idx++] = 8;

    idx += 4;                   /* Next 4 bytes are 0 */

    param = ov_htonl(pkt_size);
    memcpy(&cmd_buffer[idx], &param, sizeof(param));
    idx += sizeof(param);

    /* Make it 32 bytes aligned */
    read_size = ((32 -(pkt_size + 18) % 32) + (pkt_size + 18));

    ret_val = ov_execute_cmd_ack_seq(&cmd_buffer[0], &ack_buffer[0],\
                                     p_outbuffer, read_size);

    pthread_mutex_unlock(&ov_if_lock);
    return ret_val;
}


/**/
static uint32_t ov_htonl(uint32_t val)
{
    unsigned long i = 1;
    signed char *p = (signed char *)&i;

    if (p[0] == 1) /* little endian */
    {
        p[0] = ((signed char* )&val)[3];
        p[1] = ((signed char* )&val)[2];
        p[2] = ((signed char* )&val)[1];
        p[3] = ((signed char* )&val)[0];
        return i;
    }

    /* big endian */
    return val;
}

/**/
static int32_t ov_verify_ack(uint8_t *const p_command, \
                            uint8_t const *const p_ackbuffer)
{
    int32_t ret_val = 0;

    ret_val = p_ackbuffer[9];
    switch (ret_val)
    {
        case OV_RESP_NO_PAYLOAD:
            /* Do nothing */
        break;

        case OV_RESP_PAYLOAD:
            /* Do nothing */
        break;

        case OV_RESP_CMD_FLAG_ERROR:
            ret_val = OV_RESPONSE_FLAG_ERROR;
        break;

        case OV_RESP_CLASS_ID_NOT_SUPPORTED:
            ret_val = OV_RESPONSE_CLASS_ID_ERROR;
        break;

        case OV_RESP_CMD_ID_NOT_SUPPORTED:
            ret_val = OV_RESPONSE_CMD_ID_ERROR;
        break;

        case OV_RESP_INCORRECT_DATA_PACKET_SIZE:
            ret_val = OV_RESPONSE_DATA_PACKET_SIZE_ERROR;
        break;

        case OV_RESP_INVALID_PARAM:
            ret_val = OV_RESPONSE_INVALID_PARAM;
        break;

        case OV_RESP_PARAM_OUT_OF_RANGE:
            ret_val = OV_RESPONSE_OUT_OF_RANGE_PARAM;
        break;

        case OV_RESP_FWID_OUT_OF_RANGE:
            ret_val = OV_RESPONSE_OUT_OF_RANGE_FWID;
        break;

        default:
            ret_val = OV_RESPONSE_UNKNOWN_ERROR;
        break;
    }

    return ret_val;
}

/**/
static int32_t ov_send_command(uint8_t const cmd_class, uint8_t const cmd_id,    \
                              uint8_t const param_len, void *p_param,         \
                              uint8_t *const p_ack_payload,                   \
                              uint16_t const payload_buf_len)
{
    uint8_t  idx = 0;
    int32_t ret_val = 0;

    if(param_len != 0 && p_param == NULL)
    {
        return INVALID_INPUT;
    }

    memset(cmd_buffer, 0, sizeof(cmd_buffer));
    memcpy(cmd_buffer, cmd_header, sizeof(cmd_header));

    idx += sizeof(cmd_header);

    cmd_buffer[idx++] = cmd_class;
    cmd_buffer[idx++] = cmd_id;
    cmd_buffer[idx++] = 0x00;
    cmd_buffer[idx++] = param_len;

    if(0 != param_len)
    {
        memcpy(&cmd_buffer[idx], p_param, param_len);
        idx += param_len;
    }

    ret_val = ov_execute_cmd_ack_seq(&cmd_buffer[0], &ack_buffer[0], p_ack_payload, payload_buf_len);
    if(ret_val < 0) return ret_val;

    ret_val = ov_verify_ack(cmd_buffer, ack_buffer);
    if(ret_val < 0) return ret_val;

    return ret_val;
}

/**/
static int32_t ov_execute_cmd_ack_seq(uint8_t *const p_cmdBuffer,  \
                                     uint8_t *const p_ackBuffer,  \
                                     uint8_t *const p_payload,    \
                                     uint32_t const read_size)
{
    uint8_t  trial = 0;
    int32_t ret_val = 0;
    int transferOK = 0;
    SPI_Transaction transaction;
    unsigned char masterRxBuffer[100];
    unsigned char masterTxBuffer[100];

    /* Initialize master SPI transaction structure */
    //masterTxBuffer[sizeof(MASTER_MSG) - 1] = (i % 10) + '0';


    /* CMD Stage - Start */
    GPIO_write(GPIO_TI_TO_OV, 1);
    do{
        usleep(1000);
    }while((GPIO_read(GPIO_OV_TO_TI) == 0) && (trial++ < NUM_OF_TRIALS));

    if(trial >= NUM_OF_TRIALS) return INVALID_GPIO_RESPONSE;
    trial = 0;
    /* Send command */
    //cc_spi_write(0, p_cmdBuffer, COMMAND_SIZE);
    /* Perform SPI transfer */
    //memset((void *) masterRxBuffer, 0, 100);
    transaction.count = COMMAND_SIZE;
    transaction.txBuf = (void *) p_cmdBuffer;
    transaction.rxBuf = masterRxBuffer;
    transferOK = SPI_transfer(masterSpi, &transaction);
    if (transferOK) {
        //Report("Master received: %s", masterRxBuffer);
    }
    else {
        Report("Unsuccessful master SPI transfer");
    }
//usleep(500000);
    GPIO_write(GPIO_TI_TO_OV, 0);
    do{
        usleep(5000);
    }while((GPIO_read(GPIO_OV_TO_TI) == 1) && (trial++ < NUM_OF_TRIALS));

    if(trial >= NUM_OF_TRIALS) return INVALID_GPIO_RESPONSE;
    trial = 0;

    /* ACK Stage - Start - A high on OV_TO_TI line indicates that an ACK is
     * already received
     *
     * CMD Stage - End
     */
    GPIO_write(GPIO_TI_TO_OV, 1);
    do{
        usleep(5000);
    }while((GPIO_read(GPIO_OV_TO_TI) == 0) && (trial++ < NUM_OF_TRIALS));

    if(trial >= NUM_OF_TRIALS) return INVALID_GPIO_RESPONSE;
    trial = 0;
    
    if( (NULL != p_payload) &&
        (0 != read_size) )
    {
        memset((void *) masterTxBuffer, 0, 100);
            transaction.count = 18;
            transaction.txBuf = masterTxBuffer;
            transaction.rxBuf = (void *) p_ackBuffer;
            transferOK = SPI_transfer(masterSpi, &transaction);
            if (transferOK) {
               // Report("Master received: %s", masterRxBuffer);
            }
            else {
                Report("Unsuccessful master SPI transfer");
            }
        memset((void *) masterTxBuffer, 0, 100);
            transaction.count = read_size - 18;
            transaction.txBuf = masterTxBuffer;
            transaction.rxBuf = (void *) p_payload;
            transferOK = SPI_transfer(masterSpi, &transaction);
            if (transferOK) {
              //  Report("Master received: %s", masterRxBuffer);
            }
            else {
                Report("Unsuccessful master SPI transfer");
            }

   //     cc_spi_read(0, p_ackBuffer, 18);
   //     cc_spi_read(0, p_payload, (read_size - 18));
    }
    else
    {
        transaction.count = COMMAND_SIZE;
                  transaction.txBuf = masterTxBuffer;
                  transaction.rxBuf = (void *) p_ackBuffer;
                  transferOK = SPI_transfer(masterSpi, &transaction);
                  if (transferOK) {
                //      Report("Master received: %s", masterRxBuffer);
                  }
                  else {
                      Report("Unsuccessful master SPI transfer");
                  }
     //   cc_spi_read(0, p_ackBuffer, COMMAND_SIZE);
    }

    GPIO_write(GPIO_TI_TO_OV, 0);
    do{
        usleep(5000);
    }while((GPIO_read(GPIO_OV_TO_TI) == 1) && (trial++ < NUM_OF_TRIALS));

    if(trial >= NUM_OF_TRIALS) return INVALID_GPIO_RESPONSE;
    trial = 0;
    /* ACK Stage - End */

    if ((ack_buffer[0] != 0xFF) || (ack_buffer[1] != 0xFF) ||
        (ack_buffer[2] != 0xFF) || (ack_buffer[3] != 0x01))
    {
        /* Error in response packet format */
        return OV_RESPONSE_MISMATCH_ERROR;
    }

    return ret_val;
}

//static void ov_rst(){
//
//
//    //cc_gpio_config_low(GPIO_OV_RST);
//    GPIO_write(CONFIG_GPIO_0, 0);
//    usleep(500000);
//
//   // cc_gpio_config_high(GPIO_OV_RST);
//    GPIO_write(CONFIG_GPIO_0, 1);
//    usleep(900000);
//}
/**/
static void ov_poweroff()
{
    /* Power off the OV798 system */
//    cc_gpio_config_low(GPIO_POWER_EN_2);
//    cc_gpio_config_low(GPIO_POWER_EN_1);
    //cc_gpio_config_low(GPIO_OV_RST);
    GPIO_write(CONFIG_GPIO_0, 0);

}

unsigned int boot_798_slave()
{
  /*
    ov_q_msgs_e ov_msg = NO_MESSAGE;
    uint32_t fw_data_sent = 0;
    uint32_t f_len_1k_mod = 0;
    uint32_t f_len_be = 0;
    uint32_t fw_len = 0;
    int32_t ret_val = 0;
    int32_t f_handle = 0;
    
    int i=0;
    unsigned int counter;
    unsigned short crc=0;
    unsigned char *buf;


    
    unsigned short fw_crc16=0;
    unsigned short hdr_crc16=0;
    */
    /* Power on the OV798 system */
    usleep(3000);
   // cc_gpio_config_high(GPIO_POWER_EN_1);
    usleep(3000);
    //cc_gpio_config_high(GPIO_POWER_EN_2);
   // cc_gpio_config_high(GPIO_OV_RST);
    usleep(90000);


    /*
    //Open fw file
    ret_val = ov_GetFwSize(OVT_FW_FILE, &fw_len);
    if(ret_val < 0)
    {

        Report("GetFileSize -> Error %d", ret_val);
        return OV_FIRMWARE_FILE_OPRATION_ERROR;
    }
    f_len_1k_mod = (1024 - (fw_len % 1024)) + fw_len;
    
    ret_val = ov_OpenFwFile(OVT_FW_FILE, &f_handle);
    if(ret_val < 0)
    {
        Report("OpenFile -> Error %d", ret_val);
        return OV_FIRMWARE_FILE_OPRATION_ERROR;
    }
    else Report("Firmware file %d bytes\r\n",fw_len);
    
    //precalc the crc
    fw_crc16=0;
    while(fw_data_sent < fw_len)
    {
        ret_val = ov_GetData(f_handle, fw_buffer, FW_BUFF_SIZE, fw_data_sent);
        if(ret_val < 0)
        {
            Report("GetData -> Error %d", ret_val);
            ov_CloseFwFile(f_handle);
            return OV_FIRMWARE_FILE_OPRATION_ERROR;
        }
        else
        {
            buf=fw_buffer;
            i=0;
            while(i<ret_val){
                fw_crc16 = (fw_crc16<<8)^crc16tab[((fw_crc16>>8)^*(char*)buf++)&0x00FF];  
                i++;
            }
            fw_data_sent += ret_val;
        }
    }
    Report("Firmware CRC16 %d\r\n", fw_crc16);
    //fw_crc16 = crc16_ccitt(fw_buf, fw_len);
    
  
    ov_boot_header_t header_req, header_rsp;
    memset(&header_req,0,sizeof(ov_boot_header_t));
    header_req.magic=SWAP32(OV_BOOT_MAGIC_NUMBER);
    header_req.fw_addr=SWAP32(OV_SLAVE_BOOT_ADDR);
    header_req.fw_len=SWAP32(fw_len);
    header_req.fw_crc= fw_crc16;
    header_req.fw_crc=SWAP16(header_req.fw_crc);
    
    hdr_crc16=crc16_ccitt((unsigned char *)(&header_req), sizeof(ov_boot_header_t)-sizeof(unsigned short));
    Report("Header CRC16 %d\r\n", hdr_crc16);
    header_req.hdr_crc= hdr_crc16;
    header_req.hdr_crc =SWAP16(header_req.hdr_crc);
    redo:
    ov_rst();
    Report("Reset OV798, writing header.\r\n");
    cc_spi_write(0, (unsigned char*)&header_req, sizeof(ov_boot_header_t));
    usleep(DELAY_US_1);
    cc_spi_read(0, (unsigned char*)&header_rsp,sizeof(ov_boot_header_t));
    if(header_rsp.magic!= SWAP32(OV_BOOT_MAGIC_ACK1)){
      Report("Header ack incorrect %d\r\n",header_rsp.magic);
      goto redo;
    }else{
      Report("Header ack correct %d\r\n",header_rsp.magic);
    }
    
    //write the firmware
    Report("Writing firmware...\r\n");
    fw_data_sent = 0;
    while(fw_data_sent < fw_len)
    {
        ret_val = ov_GetData(f_handle, fw_buffer, FW_BUFF_SIZE, fw_data_sent);
        if(ret_val < 0)
        {

            Report("GetData -> Error %d", ret_val);
            ov_CloseFwFile(f_handle);
            return OV_FIRMWARE_FILE_OPRATION_ERROR;
        }
        else
        {
            cc_spi_write(0, (unsigned char *)fw_buffer, ret_val);
            fw_data_sent += ret_val;
        }
    }

    
    usleep(DELAY_US_2);
    cc_spi_read(0, (unsigned char*)&header_rsp, sizeof(ov_boot_header_t));
    if(header_rsp.magic!= SWAP32(OV_BOOT_MAGIC_ACK3)){
      Report("Firmware ack incorrect %d\r\n",header_rsp.magic);
    goto redo;
    }
    else Report("Firmware ack correct %d\r\n",header_rsp.magic);
    
    ret_val = ov_CloseFwFile(f_handle);
    
    */
    ///////////////////////////single.bin////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
    Report("wait 5 sec");
    sleep(5);
    
    //Open single file
    ret_val = ov_GetFwSize(OVT_SINGLE_FILE, &fw_len);
    if(ret_val < 0)
    {

        Report("GetFileSize -> Error %d", ret_val);
        return OV_FIRMWARE_FILE_OPRATION_ERROR;
    }
    f_len_1k_mod = (1024 - (fw_len % 1024)) + fw_len;
    
    ret_val = ov_OpenFwFile(OVT_SINGLE_FILE, &f_handle);
    if(ret_val < 0)
    {

        Report("OpenFile -> Error %d", ret_val);
        return OV_FIRMWARE_FILE_OPRATION_ERROR;
    }
    else Report("Single file %d bytes\r\n",fw_len);
    */
    /*
    //precalc the crc
    fw_crc16=0;
    while(fw_data_sent < fw_len)
    {
        ret_val = ov_GetData(f_handle, fw_buffer, FW_BUFF_SIZE, fw_data_sent);
        if(ret_val < 0)
        {
            //Error 
            Report("GetData -> Error %d", ret_val);
            ov_CloseFwFile(f_handle);
            return OV_FIRMWARE_FILE_OPRATION_ERROR;
        }
        else
        {
            buf=fw_buffer;
            i=0;
            while(i<ret_val){
                fw_crc16 = (fw_crc16<<8)^crc16tab[((fw_crc16>>8)^*(char*)buf++)&0x00FF];  
                i++;
            }
            fw_data_sent += ret_val;
        }
    }
    Report("Single CRC16 %d\r\n", fw_crc16);
    */
    
    /*
    //write the single
    Report("Writing Single...\r\n");
    fw_data_sent = 0;
    while(fw_data_sent < fw_len)
    {
        ret_val = ov_GetData(f_handle, fw_buffer, FW_BUFF_SIZE, fw_data_sent);
        if(ret_val < 0)
        {

            Report("GetData -> Error %d", ret_val);
            ov_CloseFwFile(f_handle);
            return OV_FIRMWARE_FILE_OPRATION_ERROR;
        }
        else
        {
            cc_spi_write(0, (unsigned char *)fw_buffer, ret_val);
            fw_data_sent += ret_val;
        }
    }

    */
    usleep(DELAY_US_2);
    /*
    cc_spi_read(0, (unsigned char*)&header_rsp, sizeof(ov_boot_header_t));
    if(header_rsp.magic!= SWAP32(OV_BOOT_MAGIC_ACK3)){
      Report("Single ack incorrect %d\r\n",header_rsp.magic);
    goto redo;
    }
    else Report("Single ack correct %d\r\n",header_rsp.magic);
    */
    //ret_val = ov_CloseFwFile(f_handle);
    
    //sleep(1);
    
    return 0;
}




