//*****************************************************************************
//
// Copyright (C) 2016-2021, Texas Instruments Incorporated - http://www.ti.com/ 
// 
// 
//  Redistribution and use in source and binary forms, with or without 
//  modification, are permitted provided that the following conditions 
//  are met:
//
//    Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the 
//    documentation and/or other materials provided with the   
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

#ifndef __OV798_IF_H__
#define __OV798_IF_H__

#ifdef __cplusplus
extern "C" {
#endif

/*!
    \addtogroup OV_SIF_IF
    @{
*/


/* Error Codes */
typedef enum
{
    OV_SUCCESS                          =   0,
    OV_ERROR_UNKNOWN                    =  -1,
    OV_RESPONSE_MISMATCH_ERROR          =  -2,
    OV_RESPONSE_FLAG_ERROR              =  -3,
    OV_RESPONSE_CLASS_ID_ERROR          =  -4,
    OV_RESPONSE_CMD_ID_ERROR            =  -5,
    OV_RESPONSE_DATA_PACKET_SIZE_ERROR  =  -6,
    OV_RESPONSE_INVALID_PARAM           =  -7,
    OV_RESPONSE_OUT_OF_RANGE_PARAM      =  -8,
    OV_RESPONSE_OUT_OF_RANGE_FWID       =  -9,
    OV_RESPONSE_UNKNOWN_ERROR           = -10,
    INVALID_INPUT                       = -11,
    INVALID_GPIO_RESPONSE               = -12,
    OV_FIRMWARE_DOWNLOAD_ERROR          = -13,
    OV_FIRMWARE_FILE_OPRATION_ERROR     = -14,
    OV_SMALL_BUFFER_SIZE                = -15,

}ov_status_code_e;

/** Response for 'Get_VSTREAM_Getinfo'
  * Refer 'SIF Protocol' document
  */
typedef struct vstream_getinfo
{
    uint32_t  v_stream_size;
    uint32_t  v_nxt_pkt_size;
    uint32_t  v_frame_timestamp;
    uint32_t  v_frame_number;
    uint32_t  v_frame_type;

}ov_vstream_info_s;



/** Represents 'Param0' of 'VCC_Set_Resolution'
  * Refer 'SIF Protocol' document
  */
typedef enum video_resolution
{
    V_1280_720=4,
    V_640_360,
    V_1920_1080

}ov_v_resolution_e;

/** Represents 'Param0' of 'VCC_Set_Fame_Rate'
  * Refer 'SIF Protocol' document
  */
typedef enum video_framerate
{
    V_30FPS,
    V_20FPS,
    V_15FPS,
    V_5FPS,
    V_25FPS,
    V_1FPS,
    V_24FPS,
    V_10FPS

}ov_v_framerate_e;

/** Represents 'Param0' of 'VCC_Set_Bit_Rate'
  * Refer 'SIF Protocol' document
  */
typedef enum video_bitrate
{
    V_2Mbps,
    V_1Mbps,
    V_512kbps,
    V_384kbps,
    V_256kbps

}ov_v_bitrate_e;

/** Represents 'Param0' of 'SCC_Frequency_Set'
  * Refer 'SIF Protocol' document
  */
typedef enum sensor_frequency
{
    V_disable,
    V_50HZ,
    V_60HZ

}ov_v_scc_freq_e;

/* V Config */
typedef struct ov_v_config
{
    ov_v_scc_freq_e frequency;
    uint8_t           brightness;         /* Level value 1~255 */
    uint8_t           contrast;           /* Level value 0~4 */
    uint8_t           saturation;         /* Level value 0~7 */
    uint8_t           flip;               /* Value 0: Normal, 1:flip */
    uint8_t           mode;               /* Value 0: Normal, 1:night mode */
    uint8_t           hdr;               /* Level value 0=auto,1=HDR,2=none *///High dynamic range
    uint8_t           wdr;               /* Level value 0~4 *///Wide dynamic range
    uint8_t           mctf;               /* Level value 0~4 *///motion-compensated temporal filtering
    uint8_t           sharpness;               /* Level value 0~4 */

} ov_v_config_s;

/** Represents 'Param0' of 'AENC_setformat'
  * Refer 'SIF Protocol' document
  */
typedef enum audio_format
{
    A_PCM,
    A_ADPCM,
    A_ULAW = 0x05

}ov_a_format_e;

/** Represents 'Param0' of 'AENC_setsamplerate'
  * Refer 'SIF Protocol' document
  */
typedef enum audio_samplerate
{
    /* Response Packet format is correct */
    A_44K,
    A_32K,
    A_24K,
    A_16K,
    A_12K,
    A_11K,
    A_8K

}ov_a_samplerate_e;

/* A Config */
typedef struct ov_a_config
{
    ov_a_format_e       format;
    ov_a_samplerate_e   sampleRate;

}ov_a_config_s;

/**/
typedef enum ov_q_msgs
{
    NO_MESSAGE,
    OV_TI_GPIO_HIGH,
    OV_TI_GPIO_LOW,
    TIMEROUT_EVENT

}ov_q_msgs_e;

void ov_v_force_I_frame(void);

/*!
    \brief      Init the interface and configure the OV798 system
    \param      None
    \return     0 on success, negative error-code on error
    \note
*/
int32_t ov_init();

/*!
    \brief      Deinit the interface and OV
    \param      None
    \return     0 on success, negative error-code on error
    \note
*/
int32_t ov_deinit();


/*!
    \brief Initnialize the interface and config the OV798 system

    \param[in]  fd              :   Interface Descriptor
    \param[in]  out_buffer_size :   Buffer length
    \param[out] p_outbuffer     :   pointer to buffer containing the response
    \param[out] p_length        :   Size of the data in buffer

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_get_version(uint16_t const fd, uint8_t const out_buffer_size,\
                      uint8_t *p_outbuffer, uint8_t *p_length);

/*!
    \brief Initnialize the interface and config the OV798 system

    \param[in]  fd              :   Interface Descriptor
    \param[in]  out_buffer_size :   Buffer length
    \param[out] p_outbuffer     :   pointer to buffer containing the response
    \param[out] p_length        :   Size of the data in buffer

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_get_status(uint16_t const fd, uint8_t const out_buffer_size,\
                     uint8_t *p_outbuffer, uint8_t *p_length);


/*!
    \brief Configures the bringhtness, contrast, saturation, mode and other
           parameters for the video sensor

    \param[in]  fd      :   Interface Descriptor
    \param[in]  config  :   Structure containing the different configuration
                            parameter value

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_config_v_sensor(uint16_t const fd, ov_v_config_s const config);

/*!
    \brief Configure the parameters for audio codec

    \param[in]  fd      :   Interface Descriptor
    \param[in]  config  :   Structure containing the different configuration
                            parameter value

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_config_a_codec(uint16_t const fd, ov_a_config_s const config);

/*!
    \brief      Configure the sampling rate for audio

    \param[in]  fd              :   Interface Descriptor
    \param[in]  sampling_rate   :   Sampling rate value

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_set_sampling_rate(uint16_t const fd,\
                            ov_a_samplerate_e const sampling_rate);

/*!
    \brief Configure the Frame rate for video

    \param[in]  fd          :   Interface Descriptor
    \param[in]  frame_rate  :   Frame rate Value (Enum)

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_set_frame_rate(uint16_t const fd, ov_v_framerate_e const frame_rate);

/*!
    \brief Configure the Bit rate for video

    \param[in]  fd          :   Interface Descriptor
    \param[in]  bit_rate    :   Bit rate rate Value (Enum)

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_set_bit_rate(uint16_t const fd, ov_v_bitrate_e const bit_rate);

/*!
    \brief Set the iframe interval

    \param[in]  fd      : Interface Descriptor
    \param[in]  interval    : frame rate

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_set_iframe_interval(uint16_t const fd, uint8_t const interval);

/*!
    \brief Enable/ Disable the motion detect feature

    \param[in]  fd      :   Interface Descriptor
    \param[in]  enable  :   1 Enable, 0 Disable

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_set_motion_detect(uint16_t const fd, uint8_t const enable);

/*!
    \brief Set resolution and enable/disable the video sampling

    \param[in]  fd          :   Interface Descriptor
    \param[in]  resolution  :   Video resolution value (Enum)
    \param[in]  enable      :   Enable(1)/Disable(0) video sampleling

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_config_v_encoder(uint16_t const fd, \
                           ov_v_resolution_e const resolution, \
                           uint8_t const enable);

/*!
    \brief Get the stream info for the next available video stream packet

    \param[in]  fd              : Interface Descriptor
    \param[in]  pkt_size        : Maximum packet size that can be received by
                                  application. This value depend upon the app
                                  buffer for getting the video data
    \param[out] p_vstream_info  : Pointer to structure for the next video packet info

    \return     0 on success, negative error-code on error

    \note   This function should be called before calling the 'ov_get_vstream_data'
*/
int32_t ov_get_vstream_info(uint16_t const fd, uint32_t const pkt_size,\
                           ov_vstream_info_s *const p_vstream_info);

/*!
    \brief Get the Video data packet

    \param[in]  fd          : Interface Descriptor
    \param[in]  pkt_size    : Packet size to be recevied. This value should be
                              same as 'v_nxt_pkt_size' in previous
                              'ov_get_vstream_info()' function call
    \param[out] p_outbuffer : Pointer to data buffer to recevie the video data

    \return     0 on success, negative error-code on error

    \note
*/
int32_t ov_get_vstream_data(uint16_t const fd, uint32_t const pkt_size, \
                           uint8_t *const p_outbuffer);



/*!
    \brief Get the audio data packet

    \param[in]  fd          : Interface Descriptor
    \param[in]  pkt_size    : Packet size to be recevied.
    \param[out] p_outbuffer : Pointer to data buffer to recevie the audio data

    \return     0 on success, negative error-code on error

    \note
*/


#define ov_OpenFwFile       sl_OpenFile

/*!
    \brief      Get the size of the file

    \param[in]  fName   : Pointer to file name to be opened
    \param[out] fHdl    : Size of the file

    \return     0 on success, negative error-code on error

    \note
*/
#define ov_GetFwSize        sl_GetFileSize

/*!
    \brief Get the data from the file

    \param[in]  fHdl    : File handler
    \param[out] fHdl    : Pointer to the buffer for reading data
    \param[in]  length  : Size of the data to be read
    \param[in]  offset  : Offset location for the data to be read

    \return     0 on success, negative error-code on error

    \note
*/
#define ov_GetData          sl_GetData

/*!
    \brief Close the opened file

    \param[in]  fHdl   : File handler for the opened file

    \return     0 on success, negative error-code on error

    \note
*/
#define ov_CloseFwFile      sl_CloseFile

/*!
     Close the Doxygen group.
     @}
 */

#ifdef  __cplusplus
}
#endif  /*  __cplusplus */
#endif  /* __OV798_IF_H__ */
