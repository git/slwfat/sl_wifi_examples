COPYRIGHT (C) 2020 TESSOLVE SEMICONDUCTOR PVT. LTD., ALL RIGHTS RESERVED
THE INFORMATION CONTAINED HEREIN IS THE EXCLUSIVE PROPERTY OF TESSOLVE SEMICONDUCTOR PVT. LTD.
AND SHALL NOT BE DISTRIBUTED AND SHALL NOT REPRODUCED OR DISCLOSED IN WHOLE OR IN PART WITHOUT 
PRIOR WRITTEN PERMISSION OF TESSOLVE SEMICONDUCTOR PVT. LTD.
----------------------------------------------------------------------------------
CUSTOMER		-	TI     	
CARD NAME		-	Encoder_Main_Board       	
PART NUMBER		-	TS23255AC0	    	
DATE                   		-	30-07-2020
DESIGNED AT     		-	Tessolve Semiconductor Pvt. Ltd.
CONTACT E-Mail ID		-	engineering_cbe@tessolve.com
GERBER FORMAT 		-	RS274X , ENGLISH  2,5 LEADING
DRILL FORMAT 		-	EXCELLON ENGLISH  2,5 TRAILING
PACKAGE NAME   		-	TS23255AC0_Encoder_Main_Board _FAB_20200730.zip

----------------------------------------------------------------------------------

LIST OF FILES INSIDE PACKAGE:

ELECTRICAL LAYERS :

Encoder_Main_Board_PCB_2805.GTL		- Layer 1 (Top / Primary Side)
Encoder_Main_Board_PCB_2805.G1		- Layer 2 
Encoder_Main_Board_PCB_2805.G2		- Layer 3 
Encoder_Main_Board_PCB_2805.G3		- Layer 4 
Encoder_Main_Board_PCB_2805.G4		- Layer 5
Encoder_Main_Board_PCB_2805.G5		- Layer 6
Encoder_Main_Board_PCB_2805.G6		- Layer 7
Encoder_Main_Board_PCB_2805.G7		- Layer 8
Encoder_Main_Board_PCB_2805.G8		- Layer 9
Encoder_Main_Board_PCB_2805.GBL		- Layer 10 (Bottom / Secondary Side)

NON ELECTRICAL LAYERS :

Encoder_Main_Board_PCB_2805.GTS		- Solder Mask Top Side 
Encoder_Main_Board_PCB_2805.GBS		- Solder Mask Bottom Side 
Encoder_Main_Board_PCB_2805.GT0		- Silkscreen Top Side
Encoder_Main_Board_PCB_2805.GBO		- Silkscreen Bottom Side
Encoder_Main_Board_PCB_2805.GTP		- Solderpaste Top Side               
Encoder_Main_Board_PCB_2805.GBP		- Solderpaste Bottom Side
Encoder_Main_Board_PCB_2805.GM1		- Board Outline
Encoder_Main_Board_PCB_2805.GD1		- Fabrication Drawing

DRILL LAYERS :

Encoder_Main_Board_PCB_2805-Plated.TXT		- Plated through holes drill file

OTHER FILES :

Fabrication Testpoint Report for Encoder_Main_Board_PCB_2805.ipc 	- IPC-D-356 Netlist 
Layers.pdf	- PDF Plots of all layers
Fab.pdf		- PDF of Fabrication Drawing
Readme_Fab.txt	- This file
