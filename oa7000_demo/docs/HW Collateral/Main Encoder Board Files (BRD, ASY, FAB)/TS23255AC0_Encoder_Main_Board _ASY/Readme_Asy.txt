COPYRIGHT (C) 2020 TESSOLVE SEMICONDUCTOR PVT. LTD., ALL RIGHTS RESERVED
THE INFORMATION CONTAINED HEREIN IS THE EXCLUSIVE PROPERTY OF TESSOLVE SEMICONDUCTOR PVT. LTD.
AND SHALL NOT BE DISTRIBUTED AND SHALL NOT REPRODUCED OR DISCLOSED IN WHOLE OR IN PART WITHOUT 
PRIOR WRITTEN PERMISSION OF TESSOLVE SEMICONDUCTOR PVT. LTD.
----------------------------------------------------------------------------------
CUSTOMER		-	TI         	
CARD NAME		-	Encoder_Main_Board       	
PART NUMBER		-	TS23255AC0	    	
DATE                   		-	30-07-2020
DESIGNED AT     		-	Tessolve Semiconductor Pvt. Ltd.
CONTACT E-Mail ID		-	engineering_cbe@tessolve.com
GERBER FORMAT 		-	RS274X , ENGLISH  2,5 LEADING
PACKAGE NAME   		-	TS23255AC0_Encoder_Main_Board _ASY_20200730.zip

----------------------------------------------------------------------------------

LIST OF FILES INSIDE PACKAGE:

Encoder_Main_Board_PCB_2805.GTP			- Solder Paste Top Side
Encoder_Main_Board_PCB_2805.GBP			- Solder Paste Bottom Side
Encoder_Main_Board_PCB_2805.GM5			- Assembly Drawing Top Side
Encoder_Main_Board_PCB_2805.GM6			- Assembly Drawing Bottom Side
Encoder_Main_Board_PCB_2805.GM1			- Board Outline

OTHER FILES:

Pick Place for Encoder_Main_Board_PCB_2805.txt 	- Pick and Place data (Placement file)
Asy.pdf		       	- PDF of Assembly Drawings (Top and Bottom side)
Readme_Asy.txt	       	- This file
