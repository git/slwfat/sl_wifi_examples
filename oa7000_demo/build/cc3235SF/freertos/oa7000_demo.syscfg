/**
 * These arguments were used when this file was generated. They will be automatically applied on subsequent loads
 * via the GUI or CLI. Run CLI with '--help' for additional information on how to override these arguments.
 * @cliArgs --device "CC3235SF" --package "Default" --part "Default" --product "simplelink_cc32xx_sdk@4.30.00.06"
 * @versions {"data":"2020052512","timestamp":"2020052512","tool":"1.5.0+1397","templates":"2020052512"}
 */

/**
 * Import the modules used in this configuration.
 */
const Display     = scripting.addModule("/ti/display/Display");
const Display1    = Display.addInstance();
const GPIO        = scripting.addModule("/ti/drivers/GPIO", {}, false);
const GPIO1       = GPIO.addInstance();
const GPIO2       = GPIO.addInstance();
const GPIO3       = GPIO.addInstance();
const GPIO4       = GPIO.addInstance();
const GPIO5       = GPIO.addInstance();
const RTOS        = scripting.addModule("/ti/drivers/RTOS");
const SPI         = scripting.addModule("/ti/drivers/SPI");
const SPI1        = SPI.addInstance();
const Timer       = scripting.addModule("/ti/drivers/Timer", {}, false);
const Timer1      = Timer.addInstance();
const SimpleLinkWifi = scripting.addModule("/ti/drivers/net/wifi/SimpleLinkWifi");
const tiUtilsRtos = scripting.addModule("/ti/utils/RTOS");

/**
 * Write custom configuration values to the imported modules.
 */
Display1.$name                   = "CONFIG_Display_0";
Display1.uart.$name              = "CONFIG_UART_0";
Display1.uart.uart.$assign       = "UART1";
Display1.uart.uart.txPin.$assign = "GP01";
Display1.uart.uart.rxPin.$assign = "GP02";

GPIO1.$name               = "GPIO_TI_TO_OV";
GPIO1.mode                = "Output";
GPIO1.enableStaticParking = true;
GPIO1.gpioPin.$assign     = "GP10";

GPIO2.$name           = "GPIO_OV_TO_TI";
GPIO2.pull            = "Pull Down";
GPIO2.gpioPin.$assign = "GP11";

GPIO3.$name           = "CONFIG_GPIO_0";
GPIO3.mode            = "Output";
GPIO3.gpioPin.$assign = "GP06";

GPIO4.mode            = "Output";
GPIO4.$name           = "LED_B";
GPIO4.gpioPin.$assign = "GP07";

GPIO5.mode            = "Output";
GPIO5.$name           = "LED_A";
GPIO5.gpioPin.$assign = "GP08";

const Power          = scripting.addModule("/ti/drivers/Power", {}, false);
Power.parkPins.$name = "ti_drivers_power_PowerCC32XXPins0";

RTOS.name = "FreeRTOS";

SPI1.$name                    = "CONFIG_SPI_MASTER";
SPI1.mode                     = "Four Pin SS Active Low";
SPI1.ssControl                = "SW";
SPI1.dmaInterruptPriority     = "1";
SPI1.turboMode                = true;
SPI1.spi.$assign              = "SPI0";
SPI1.spi.sclkPin.$assign      = "GP14";
SPI1.spi.misoPin.$assign      = "GP15";
SPI1.spi.mosiPin.$assign      = "GP16";
SPI1.spi.ssPin.$assign        = "GP17";
SPI1.spi.dmaRxChannel.$assign = "UDMA_CH30";
SPI1.spi.dmaTxChannel.$assign = "UDMA_CH31";

Timer1.$name = "CONFIG_TIMER_0";

tiUtilsRtos.name = "FreeRTOS";

/**
 * Pinmux solution for unlocked pins/peripherals. This ensures that minor changes to the automatic solver in a future
 * version of the tool will not impact the pinmux you originally saw.  These lines can be completely deleted in order to
 * re-solve from scratch.
 */
Timer1.timer.$suggestSolution = "Timer0";
