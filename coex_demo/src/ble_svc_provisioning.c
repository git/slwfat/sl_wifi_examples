/* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* Standard Includes */
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <mqueue.h>
#include <assert.h>

#include <ble_if.h>
#include <ble_svc_provisioning.h>
#include <BLE/inc/gap.h>
#include <BLE/inc/gap_advertiser.h>
#include <BLE/inc/gap_initiator.h>
#include <BLE/inc/gap_scanner.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/hci_tl.h>
//#include "math.h"

/* TI-Driver Includes */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Local Includes */
#include "ti_drivers_config.h"


/* ble5stack Includes */
#include "uart_term.h"

#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/net/wifi/slwificonn.h>


enum
{
    GATT_PROVISIONING_STATUS_ID,
#if 0
    GATT_PROVISIONING_NOTIFY_ID,
#endif
    GATT_PROVISIONING_SSID_ID,
    GATT_PROVISIONING_KEY_ID,
    GATT_PROVISIONING_DEV_NAME_ID,
    GATT_PROVISIONING_START_ID,
};

/* WIFI Provisioning Service parameters */
bleProvState_e g_state = BLE_PROV_OFF;
static char *g_stateStr[] =
{
 "OFF", "ENABLED", "STARTED", "COMPLETED", "FAILURE"
};
static char g_provSSID[33]    = {0};
static char g_provKey[33]     = {0};
static char g_provDevName[33] = {0};

/*******************************************************************************
 *                         LOCAL FUNCTIONS
 ******************************************************************************/

static HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen);
static HCI_StatusCodes_t hndlr_AttributeWriteReq(uint16_t connHandle, uint8_t userId, uint8_t *pValue, uint16_t valueLen);


/*******************************************************************************
 *                        PUBLIC FUNCTIONS
 ******************************************************************************/


static charParams_t g_provChars[] =
{
 /* GATT_PROVISIONING_STATUS_ID */
 {
   0, 0,
   0xFFF2,
   (GATT_PERMIT_READ),
   (GATT_PROP_READ|GATT_PROP_NOTIFY),
   "STATUS"
 },
#if 0
 /* GATT_PROVISIONING_NOTIFY_ID */
 {
   0, 0,
   0xFFF3,
   (GATT_PERMIT_WRITE|GATT_PERMIT_READ),
   (GATT_PROP_READ|GATT_PROP_WRITE),
   "NOTIFY"
 },
#endif
  /* GATT_PROVISIONING_SSID_ID */
 {
   0, 0,
   0xFFF4,
   (GATT_PERMIT_WRITE|GATT_PERMIT_READ),
   (GATT_PROP_READ|GATT_PROP_WRITE),
   "SSID"
 },

 /* GATT_PROVISIONING_KEY_ID */
 {
   0, 0,
   0xFFF5,
   GATT_PERMIT_WRITE,
   (GATT_PROP_WRITE),
   "KEY"
 },

 /* GATT_PROVISIONING_DEV_NAME_ID */
 {
   0, 0,
   0xFFF6,
   (GATT_PERMIT_WRITE|GATT_PERMIT_READ),
   (GATT_PROP_READ|GATT_PROP_WRITE),
   "DEV_NAME"
 },

 /* GATT_PROVISIONING_START_ID */
 {
   0, 0,
   0xFFF7,
   GATT_PERMIT_WRITE,
   (GATT_PROP_WRITE),
   "START"
 },
};

serviceParams_t g_provService =
{
 0xFE5D,
 sizeof(g_provChars)/sizeof(charParams_t),
 g_provChars,
 NULL,
 hndlr_AttributeWriteReq,
 hndlr_AttributeReadReq,
};

void BLEPROV_setState(bleProvState_e state)
{
    if((uint8_t)state < BLE_PROV_STATES_MAX)
    {
        g_state = state;

        if(g_currConnHandle != 0xffff)
        {
            BLE_IF_sendNotification(g_provChars[GATT_PROVISIONING_STATUS_ID].handle,
                                    (uint8_t*)g_stateStr[g_state],
                                    strlen(g_stateStr[g_state]));
        }
    }
}


HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen)
{
    HCI_StatusCodes_t status = FAILURE;

    Report("PROV SERVICE EVENT (ATT READ REQ) = userId=%d\n\r", userId);
    switch (userId)
    {
        case GATT_PROVISIONING_SSID_ID:
            *ppValue = (uint8_t*)g_provSSID;
            *pValueLen = strlen(g_provSSID);
            break;
        case GATT_PROVISIONING_KEY_ID:
            *ppValue = (uint8_t*)g_provKey;
            *pValueLen = strlen(g_provKey);
            break;
        case GATT_PROVISIONING_DEV_NAME_ID:
            *ppValue = (uint8_t*)g_provDevName;
            *pValueLen = strlen(g_provDevName);
            break;
        case GATT_PROVISIONING_STATUS_ID:
            *ppValue = (uint8_t*)g_stateStr[g_state];
            *pValueLen = strlen(g_stateStr[g_state]);
            break;
    }
    return status;
}

HCI_StatusCodes_t hndlr_AttributeWriteReq(uint16_t connHandle, uint8_t userId, uint8_t *pValue, uint16_t valueLen)
{
    HCI_StatusCodes_t status = FAILURE;

    switch (userId)
    {
        case GATT_PROVISIONING_SSID_ID:
            if(valueLen < sizeof(g_provSSID) - 1)
            {
                memcpy(g_provSSID, pValue, valueLen);
                g_provSSID[valueLen] = 0;
                Report("PROV WRITE:: SSID => %s\n\r", g_provSSID);
                status = SUCCESS;
            }
            break;
        case GATT_PROVISIONING_KEY_ID:
            if(valueLen < sizeof(g_provKey) - 1)
            {
                memcpy(g_provKey, pValue, valueLen);
                g_provKey[valueLen] = 0;
                Report("PROV WRITE:: KEY => %s\n\r", g_provKey);
                status = SUCCESS;
             }
            break;
        case GATT_PROVISIONING_DEV_NAME_ID:
            if(valueLen < sizeof(g_provDevName) - 1)
            {
                memcpy(g_provDevName, pValue, valueLen);
                g_provDevName[valueLen] = 0;
                if(g_provDevName[0])
                {
                    /* set new device name */
                    int16_t rc;

                    rc = sl_NetAppSet (SL_NETAPP_DEVICE_ID,SL_NETAPP_DEVICE_URN, strlen(g_provDevName), (_u8 *)g_provDevName);
                    assert( rc == 0 );
                }
                Report("PROV WRITE:: KEY => %s\n\r", g_provDevName);
                status = SUCCESS;
             }
            break;
        case GATT_PROVISIONING_START_ID:
        {
            if(g_provSSID[0])
            {
                SlWlanSecParams_t secParam;
                uint16_t ssidLen = strlen(g_provSSID);

                 secParam.Type = SL_WLAN_SEC_TYPE_WPA_WPA2;
                secParam.Key = (_i8*)g_provKey;
                secParam.KeyLen = strlen(g_provKey);
                Report("PROV WRITE:: START %s (%d), %s (%d)\n\r", g_provSSID, ssidLen, g_provKey, secParam.KeyLen);

                SlWifiConn_addProfile(g_provSSID, strlen(g_provSSID), NULL, &secParam, NULL, 1, 0);
                BLEPROV_setState(BLE_PROV_STARTED);
                status = SUCCESS;
            }
        }
        break;
    }


     return status;
}


