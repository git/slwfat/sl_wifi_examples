/* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* DriverLib Includes */
//#include <ti/devices/cc32xx/driverlib/driverlib.h>

/* Standard Includes */
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <mqueue.h>
#include <assert.h>

#include <ble_if.h>
#include <BLE/inc/gap.h>
#include <BLE/inc/gap_advertiser.h>
#include <BLE/inc/gap_initiator.h>
#include <BLE/inc/gap_scanner.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/hci_tl.h>
//#include "math.h"

/* TI-Driver Includes */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Local Includes */
#include "ti_drivers_config.h"


#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/net/wifi/slwificonn.h>

#include "debug_if.h"

#undef DEBUG_IF_NAME
#undef DEBUG_IF_SEVERITY
#define DEBUG_IF_NAME       "BLE_IF"
#define DEBUG_IF_SEVERITY   E_INFO


/* Enable Transport Layer trace messages */
extern bool bTLDump;



/* Maximum allowed number of BLE connections for the application */
#define MAX_NUM_BLE_CONNS 4

/* Advertising report fields to keep in the list
 * Interested in only peer address type and peer address
 */
#define SC_ADV_RPT_FIELDS   (SCAN_ADVRPT_FLD_ADDRTYPE | SCAN_ADVRPT_FLD_ADDRESS)

/* Minimum connection interval (units of 1.25ms) if automatic parameter update
 * request is enabled
 */
#define DEFAULT_UPDATE_MIN_CONN_INTERVAL      400

/* Maximum connection interval (units of 1.25ms) if automatic parameter update
 * request is enabled
 */
#define DEFAULT_UPDATE_MAX_CONN_INTERVAL      800

/* Slave latency to use if automatic parameter update request is enabled */
#define DEFAULT_UPDATE_SLAVE_LATENCY          0

/* Supervision timeout value (units of 10ms) if automatic parameter update
 * request is enabled
 */
#define DEFAULT_UPDATE_CONN_TIMEOUT           600


static pthread_mutex_t g_ble_mutex;
static sem_t g_ble_sem;
static enum
{
    BLE_STATE_OFF,
    BLE_STATE_INITIALIZED,
    BLE_STATE_CONNECTABLE,
    BLE_STATE_CONNECTED,
} g_ble_state = BLE_STATE_OFF;

#define SET_STATE(STATE) { LOG_DEBUG("BLE STATE = %s", #STATE); g_ble_state = STATE; }

typedef struct
{
    const char *pDeviceName;
    uint8_t     role;
} bleParams_t;


/* Holds device information of the connected devices */
GapDeviceInformation_t GapDeviceInformation[MAX_BLE_DEVICES];

/* Task configuration */
static pthread_t BleTask;

uint8_t  g_advHandle = 0;

static uint8_t  g_txPower = 0;
static uint16_t g_minConnIntreval = 80, g_maxConnIntreval = 800;

static uint8_t  g_devAddr[6];
static uint8_t  g_peerAddr[6];
uint16_t g_currConnHandle = 0xffff;

static char *g_pDeviceName;

extern pthread_t TaskCreate(int prio, size_t stacksize, void* (*fTask)(void*), void *arg);

/*******************************************************************************
 *                         LOCAL FUNCTIONS
 ******************************************************************************/

static void *BleTaskFxn(void *arg0);
static void InitHW(void);
static HCI_StatusCodes_t InitGapRole(uint8_t Role);

/*******************************************************************************
 * @fn      initHW
 *
 * @brief   Called during initialization and contains application
 *          specific initialization (ie. hardware initialization/setup,
 *          table initialization, power up notification, etc), and
 *          profile initialization/setup.
 *
 * @param   None.
 *
 * @return  None.
 *******************************************(**********************************/
static void InitHW(void)
{
    int i = 0;

    /* Turn off BLE LED */
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_OFF);

    /* Toggle BPRST HW Reset */

    GPIO_write(CONFIG_GPIO_BLERESET, CONFIG_GPIO_LED_OFF);
    for (i = 0; i < 500000; i++) {};
    GPIO_write(CONFIG_GPIO_BLERESET, CONFIG_GPIO_LED_ON);
    for (i = 0; i < 500000; i++) {};
}

/*******************************************************************************
 * @fn      initGapRole
 *
 * @brief   Initialize the GAP Central role
 *
 * @param   None.
 *
 * @return  HCI_StatusCodes_t
 ******************************************************************************/
static HCI_StatusCodes_t InitGapRole(uint8_t Role)
{
    HCI_StatusCodes_t status;

    /* HCI Reset BLE device */
    HCI_CALL(HCI_ResetCmd, DEFAULT_HCI_RESET_TYPE);

    /* Wait for reset to complete */
    usleep(200000);

    /* Initialize the Device as a GAP Central Role */
    HCI_CALL(GAP_deviceInit, Role, ADDRMODE_PUBLIC);

    return status;
}


/*******************************************************************************
 * @fn      HcileHndlr_GenReport
 *
 * @brief   Generic Report Handler:
 *              Log the event
 *
 * @param   pEvent - Received message
 *
 * @return  None
 *
 ******************************************************************************/
static void HcileHndlr_GenReport(hciRxMsg_t *pEvent)
{
#if (DEBUG_IF_SEVERITY == E_TRACE)
   int i;
    Report("HCILE EVENT (GEN REPORT):: ");
    for (i=0; i<pEvent->length; i++)
    {
        Report("%02x ", pEvent->pPayload[i]);
    }
    Report("\n\r");
#endif
}


/*******************************************************************************
 * @fn      hndlr_DeviceInitDone
 *
 * @brief   Handler for Device Init Done:
 *              Log local device info
 *
 * @param   pEvent - Received message
 *
 * @return  None
 *
 ******************************************************************************/
static HCI_StatusCodes_t hndlr_DeviceInitDone(hciRxMsg_t *pEvent)
{
    HCI_StatusCodes_t status = SUCCESS;
    int i;
    /* Post that the event has been received */
    for (i=0; i<6; i++)
    {
        g_devAddr[i] = pEvent->pPayload[3+i];
    }
    LOG_INFO("***** INIT DONE *****\n\r\tDEV ADDR = %02x:%02x:%02x:%02x:%02x:%02x",
           g_devAddr[5],g_devAddr[4],g_devAddr[3],g_devAddr[2],g_devAddr[1],g_devAddr[0] );

    sem_post(&g_ble_sem);
    return status;
}

/*******************************************************************************
 * @fn      hndlr_LinkEstablished
 *
 * @brief   Handler for Link Established Event:
 *              Log connection info
 *
 * @param   pEvent - Received message
 *
 * @return  None
 *
 ******************************************************************************/
static HCI_StatusCodes_t hndlr_LinkEstablished(hciRxMsg_t *pEvent)
{
    HCI_StatusCodes_t status = SUCCESS;
    int i;
    /* Post that the event has been received */
    for (i=0; i<6; i++)
    {
        g_peerAddr[i] = pEvent->pPayload[4+i];
    }
    g_currConnHandle = BUILD_UINT16(&pEvent->pPayload[10]);
    LOG_INFO("***** CONNECTED *****\n\r\tCONN HANDLE= %d, PEER ADDR = %02x:%02x:%02x:%02x:%02x:%02x",
           g_currConnHandle, g_peerAddr[5],g_peerAddr[4],g_peerAddr[3],g_peerAddr[2],g_peerAddr[1],g_peerAddr[0] );
    SET_STATE(BLE_STATE_CONNECTED);

    return status;
}

/*******************************************************************************
 * @fn      hndlr_LinkEstablished
 *
 * @brief   Handler for Link Terminated Event:
 *              Set device as connectable
 *
 * @param   pEvent - Received message
 *
 * @return  None
 *
 ******************************************************************************/
static HCI_StatusCodes_t hndlr_LinkTerminated(hciRxMsg_t *pEvent)
{
    HCI_StatusCodes_t status = SUCCESS;
    LOG_INFO("***** DISCONNECTED *****");
    g_currConnHandle = 0xffff;
    GAP_EnableConnection(g_advHandle);
    SET_STATE(BLE_STATE_CONNECTABLE);
    return status;
}

/*******************************************************************************
 * @fn      hndlr_LinkParamUpdate
 *
 * @brief   Handler for Link Parms Update Event:
 *              (currently) do nothing
 *
 * @param   pEvent - Received message
 *
 * @return  None
 *
 ******************************************************************************/
static HCI_StatusCodes_t hndlr_LinkParamUpdate(hciRxMsg_t *pEvent)
{
    HCI_StatusCodes_t status = SUCCESS;
    LOG_DEBUG("GAP Eevent (Link Param Update)");
    return status;
}



/*******************************************************************************
 * @fn      hndlr_Notification
 *
 * @brief   Handler for APP Notification Event (event triggered by the Service APP):
 *              Send Notification message
 *
 * @param   pEvent - Received message
 *
 * @return  None
 *
 ******************************************************************************/
static HCI_StatusCodes_t hndlr_Notification(hciRxMsg_t *pEvent)
{
    HCI_StatusCodes_t status = SUCCESS;
    attHandleValueNoti_t notif;
    notif.handle = pEvent->eventOpCode;
    notif.pValue = pEvent->pPayload;
    notif.len = pEvent->length;
    HCI_CALL(GATT_Notification, g_currConnHandle, &notif, 0);
    return status;
}

/*******************************************************************************
 * @fn      BleTaskFxn
 *
 * @brief  Task to handle receiving HCI Event Packets
 *
 * @param   arg0 not used.
 *
 * @return  None.
 ******************************************************************************/
static void *BleTaskFxn(void *arg0)
{
    HCI_Params hciParams;
    bleParams_t *pParams = (bleParams_t*)arg0;
    bool bExit = false;

    /* Initialize the HW to enable device acting as a central role */
    InitHW();

    /* Setup HCI module */
    hciParams.portType = HCI_PORT_REMOTE_UART;
    hciParams.remote.boardID = CONFIG_UART_1;
    HCI_init(&hciParams);

    HCI_RegisterLeReportEvent(HcileHndlr_GenReport);

    /* Initialize GAP Peripheral Role */
    InitGapRole(pParams->role);

    g_pDeviceName = (char*)pParams->pDeviceName;

    while(!bExit)
    {
        hciRxMsg_t hciEvent;

        HCI_waitEvent(&hciEvent);
        if(hciEvent.pktType == HCI_APP_EVENT_PACKET)
        {
            switch(hciEvent.eventId)
            {
            case HCI_APP_NOTIFICATION:
                hndlr_Notification(&hciEvent);
            }
            continue;
        }
        switch (hciEvent.eventOpCode)
        {
        case GAP_DEVICEINITDONE:
            hndlr_DeviceInitDone(&hciEvent);
            break;

        case GAP_LINKESTABLISHED:
            hndlr_LinkEstablished(&hciEvent);
            break;

        case GAP_LINKTERMINATED:
            hndlr_LinkTerminated(&hciEvent);
            break;

        case GAP_LINKPARAMUPDATE:
            hndlr_LinkParamUpdate(&hciEvent);
            break;

        case 0:
            bExit = true;
            break;

        default:
            break;
        }
        if(hciEvent.pPayload)
            free(hciEvent.pPayload);
    }

    /* Turn on BLE LED */
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_ON);

    free(pParams);
    return NULL;
}

/*******************************************************************************
 *                        PUBLIC FUNCTIONS
 ******************************************************************************/
/*******************************************************************************
 * @fn      BLE_IF_init
 *
 * @brief   Allocate resources and initiate the BLE in a specific role and security.
 *
 *
 * @param   Role - one of:
 *                  GAP_PROFILE_PERIPHERAL,
 *                  GAP_PROFILE_CENTRAL,
 *                  GAP_PROFILE_PERIPHERAL|GAP_PROFILE_CENTRAL
 *
 * @return  0 upon success, or negative value upon error.
 *
 * @note    This should be called before any other BLE_IF API
 ******************************************************************************/
int BLE_IF_init(uint8_t role, const char *pDeviceName)
{
    int status = SL_ERROR_BSD_ENOMEM;
    bleParams_t *pParams = (bleParams_t *)malloc(sizeof(bleParams_t));

    if(pParams)
    {
        pParams->role = role;
        pParams->pDeviceName = pDeviceName;

        status = pthread_mutex_init(&g_ble_mutex, (pthread_mutexattr_t*)NULL);

        if(status == 0)
        {
            status = pthread_mutex_lock(&g_ble_mutex);
            if(status == 0 && g_ble_state == BLE_STATE_OFF)
            {
                sem_init(&g_ble_sem, 1, 0);

                BleTask = TaskCreate(BLE_THREAD_PRIORITY, BLE_STACK_SIZE, BleTaskFxn, pParams);
                assert(BleTask);
                sem_wait(&g_ble_sem);
                SET_STATE(BLE_STATE_INITIALIZED);
            }
            else
            {
                status = SL_ERROR_STATUS_ERROR;
            }
            pthread_mutex_unlock(&g_ble_mutex);
        }
        if(status != 0)
        {
            free(pParams);
        }
    }
    return status;
}

/*******************************************************************************
 * @fn      BLE_IF_deinit
 *
 * @brief   Terminate BLE connection and free its resources
 *
 * @param   None
 *
 * @return  0 upon success, or negative value upon error.
 *
 * @note    NOT SUPPORTED
 ******************************************************************************/
int BLE_IF_deinit()
{
    int status = pthread_mutex_lock(&g_ble_mutex);
    if(status == 0)
    {
        if(g_ble_state > BLE_STATE_OFF)
        {
            hciRxMsg_t msg = {0};

            msg.eventId = HCI_LE_EXTEVENT;
            msg.eventOpCode = 0;
            HCI_notifyEvent(&msg);
            sem_wait(&g_ble_sem);
            SET_STATE(BLE_STATE_OFF);
        }
        else
        {
            status = SL_ERROR_STATUS_ERROR;
        }
        pthread_mutex_unlock(&g_ble_mutex);
    }
    return status;
}

/*******************************************************************************
 * @fn      BLE_IF_addService
 *
 * @brief   [peripheral API] Register a service
 *
 * @param   pService - a pointer to service parameters structure.
 *
 * @return  0 upon success, or negative value upon error.
 *
 ******************************************************************************/
int BLE_IF_addService(serviceParams_t *pService)
{
    int status = pthread_mutex_lock(&g_ble_mutex);
    if(status == 0)
    {
        if(g_ble_state == BLE_STATE_INITIALIZED)
        {
            status = GATT_AddPrimaryService(pService);
        }
        else
        {
            status = SL_ERROR_STATUS_ERROR;
        }
        pthread_mutex_unlock(&g_ble_mutex);
    }
    return status;
}

/*******************************************************************************
 * @fn      BLE_IF_enableConnection
 *
 *          pPasscode - pointer to pairing key (32b integer),
 *                      or 0 for non-secure connection
 *
 * @return  0 upon success, or negative value upon error.
 *
 ******************************************************************************/
int BLE_IF_enableConnection(const uint32_t *pPasskey)
{
    int status = pthread_mutex_lock(&g_ble_mutex);
    if(status == 0)
    {
        if(g_ble_state == BLE_STATE_INITIALIZED)
        {
            ATT_FindInfoReq(0xFFFE, 0x001, 0xffff);

            /* Prepare Advertisement/Scan-Response content  */
            GAP_SetAdvertisement(&g_advHandle, g_pDeviceName, g_txPower, g_minConnIntreval, g_maxConnIntreval);

            if(pPasskey)
            {
                GAP_SetBondParams(GAPBOND_PAIRING_MODE_WAIT_FOR_REQ, true, GAPBOND_IO_CAP_DISPLAY_ONLY, true,
                                  GAPBOND_KEYDIST_MENCKEY | GAPBOND_KEYDIST_MIDKEY | GAPBOND_KEYDIST_MSIGN | GAPBOND_KEYDIST_SENCKEY | GAPBOND_KEYDIST_SIDKEY | GAPBOND_KEYDIST_SSIGN,
                                  *pPasskey);
            }

            GAP_EnableConnection(g_advHandle);

            SET_STATE(BLE_STATE_CONNECTABLE);
            LOG_INFO("***** CONNECTABLE *****");
        }
        else
        {
            status = SL_ERROR_STATUS_ERROR;
        }
        pthread_mutex_unlock(&g_ble_mutex);
    }
    return status;
}

/*******************************************************************************
 * @fn      BLE_IF_sendNotification
 *
 * @brief   [Peripheral API] Send Notification to a connected central device
 *
 * @param   None
 *
 * @return  0 upon success, or negative value upon error.
 *
 ******************************************************************************/
int BLE_IF_sendNotification(uint16_t handle, uint8_t *pValue, uint16_t valueLen)
{
    hciRxMsg_t hciEvent;
    hciEvent.pktType = HCI_APP_EVENT_PACKET;
    hciEvent.eventId = HCI_APP_NOTIFICATION;
    hciEvent.length = valueLen;
    hciEvent.pPayload = pValue;
    hciEvent.eventOpCode = handle;
    HCI_notifyEvent(&hciEvent);
    return 0;
}





