/* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* DriverLib Includes */
//#include <ti/devices/cc32xx/driverlib/driverlib.h>

/* Standard Includes */
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <mqueue.h>
#include <assert.h>

#include <ble_if.h>
#include <ble_svc_provisioning.h>
#include <BLE/inc/gap.h>
#include <BLE/inc/gap_advertiser.h>
#include <BLE/inc/gap_initiator.h>
#include <BLE/inc/gap_scanner.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/hci_tl.h>
//#include "math.h"

/* TI-Driver Includes */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Local Includes */
#include "ti_drivers_config.h"


/* ble5stack Includes */
#include "uart_term.h"

#define DEV_INFO_MANUFACTURER                  "TXN"
#define DEV_INFO_MODEL_NUM                     "COEX-1.0.0.0"
#define DEV_INFO_SYS                           "SIMPLELINK"
enum
{
    GATT_DEVINFO_MANUFACTURER_ID,
    GATT_DEVINFO_MODEL_NUM_ID,
    GATT_DEVINFO_SYSTEM_ID,
};

/*******************************************************************************
 *                         LOCAL FUNCTIONS
 ******************************************************************************/
static HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen);


/*******************************************************************************
 *                        PUBLIC FUNCTIONS
 ******************************************************************************/




static charParams_t g_devInfoChars[] =
{
 { 0, 0, 0x2a29, GATT_PERMIT_READ, GATT_PROP_READ, "MANUFACTURER" },
 { 0, 0, 0x2a24, GATT_PERMIT_READ, GATT_PROP_READ, "MODEL NUM" },
 { 0, 0, 0x2a23, GATT_PERMIT_READ, GATT_PROP_READ, "SYSTEM" },
};

serviceParams_t g_devInfoService =
{
 0x180a,
 sizeof(g_devInfoChars)/sizeof(charParams_t), g_devInfoChars,
 NULL,
 NULL,
 hndlr_AttributeReadReq,
 0, 0, 0
};

HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen)
{
    HCI_StatusCodes_t status = FAILURE;

    Report("GEN ACCESS SERVICE EVENT (ATT READ REQ) = userId=%d\n\r", userId);
    switch (userId)
    {
        case GATT_DEVINFO_MANUFACTURER_ID:
            *ppValue = (uint8_t*)DEV_INFO_MANUFACTURER;
            *pValueLen = strlen(DEV_INFO_MANUFACTURER);
            break;
        case GATT_DEVINFO_MODEL_NUM_ID:
            *ppValue = (uint8_t*)DEV_INFO_MODEL_NUM;
            *pValueLen = sizeof(DEV_INFO_MODEL_NUM);
            break;
        case GATT_DEVINFO_SYSTEM_ID:
            *ppValue = (uint8_t*)DEV_INFO_SYS;
            *pValueLen = sizeof(DEV_INFO_SYS);
            break;
    }
    return status;
}

