/* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/


/* Standard Includes */
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <mqueue.h>
#include <assert.h>

#include <ble_if.h>
#include <ble_svc_led.h>
#include <BLE/inc/gap.h>
#include <BLE/inc/gap_advertiser.h>
#include <BLE/inc/gap_initiator.h>
#include <BLE/inc/gap_scanner.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/hci_tl.h>
//#include "math.h"

/* TI-Driver Includes */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Local Includes */
#include "ti_drivers_config.h"


/* ble5stack Includes */
#include "uart_term.h"

#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/net/wifi/slwificonn.h>


enum
{
    GATT_LED_STATE_ID,
};


/* WIFI Provisioning Service parameters */
static LedState_t g_state = CONFIG_GPIO_LED_OFF;

/*******************************************************************************
 *                         LOCAL FUNCTIONS
 ******************************************************************************/
HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen);
static HCI_StatusCodes_t hndlr_AttributeWriteReq(uint16_t connHandle, uint8_t userId, uint8_t *pValue, uint16_t valLen);

/*******************************************************************************
 *                        PUBLIC FUNCTIONS
 ******************************************************************************/
static charParams_t g_ledChars[] =
{
 /* GATT_LED_STATUS_ID */
 {
   0, 0,
   0xFFE0,
   (GATT_PERMIT_READ|GATT_PERMIT_WRITE),
   (GATT_PROP_READ|GATT_PROP_WRITE|GATT_PROP_NOTIFY),
   "LED STATE",
 },
};

serviceParams_t g_ledService =
{
 0xFE5E,
 sizeof(g_ledChars)/sizeof(charParams_t),
 g_ledChars,
 NULL,
 hndlr_AttributeWriteReq,
 hndlr_AttributeReadReq,
 0, 0, 0,
};

HCI_StatusCodes_t hndlr_AttributeWriteReq(uint16_t connHandle, uint8_t userId, uint8_t *pValue, uint16_t valLen)
{
    HCI_StatusCodes_t status = FAILURE;

    switch (userId)
     {
         case GATT_LED_STATE_ID:
         {
             Report("LED WRITE:: STATE => %d\n\r", pValue[0]);
             if(pValue[0] < 2)
             {
                 g_state = pValue[0];
                 BLELED_setState(g_state);
             }
         }
         break;
     }


     return status;
}

HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen)
{
    HCI_StatusCodes_t status = FAILURE;

    switch (userId)
    {
        case GATT_LED_STATE_ID:
            *ppValue = &g_state;
            *pValueLen = 1;
            Report("LED READ:: STATE => %d\n\r", **ppValue);
            break;
    }
    return status;
}


void BLELED_setState(LedState_t state)
{
    if(state <= CONFIG_GPIO_LED_ON)
    {
        GPIO_write(CONFIG_GPIO_LED_0, state);
    }
    else
    {
        /* Invalid Status */
        return;
    }

    if(state != g_state)
    {
        g_state = state;
        if(g_currConnHandle != 0xffff)
        {
            BLE_IF_sendNotification(g_ledChars[GATT_LED_STATE_ID].handle, &g_state, 1);
        }
    }
}

void BLELED_toggle()
{
    if(g_state == CONFIG_GPIO_LED_OFF)
        BLELED_setState(CONFIG_GPIO_LED_ON);
    else
        BLELED_setState(CONFIG_GPIO_LED_OFF);
}


