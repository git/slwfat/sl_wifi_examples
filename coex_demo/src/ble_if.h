/* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

#ifndef BLE_IF_H
#define BLE_IF_H

/* Standard Includes */
#include <BLE/inc/att.h>
#include <BLE/inc/gap.h>
#include <BLE/inc/gatt.h>
#include <semaphore.h>
#include <mqueue.h>

/* ble5stack Includes */

/* Task configuration */
#define BLE_THREAD_PRIORITY              3
#define BLE_STACK_SIZE                   2048

/* UUIDs and handles for the optical sensor for the Sensors BoosterPack example */
#define OPTICSERVICE_DATA_UUID          0xF000AA7104514000
#define OPTICSERVICE_CONFIG_UUID        0xF000AA7204514000
#define OPTICSERVICE_PERIOD_UUID        0xF000AA7304514000
#define OPTICSERVICE_DATA_HANDLE        0x0042

/* UUIDs and handles for the humidity sensor for the Sensors BoosterPack example */
#define HUMIDITYSERVICE_DATA_UUID       0xF000AA2104514000
#define HUMIDITYSERVICE_CONFIG_UUID     0XF000AA2204514000
#define HUMIDITYSERVICE_PERIOD_UUID     0xF000AA2304514000
#define HUMIDITYSERVICE_DATA_HANDLE     0x002A

/* UUIDs and handles for the temperature sensor for the Sensors BoosterPack example */
#define TEMPSERVICE_DATA_UUID           0xF000AA0104514000
#define TEMPSERVICE_CONFIG_UUID         0XF000AA0204514000
#define TEMPSERVICE_PERIOD_UUID         0xF000AA0304514000
#define TEMPSERVICE_DATA_HANDLE          0x001E

/* Low bytes for the sensors UUIDs */
#define SERVICE_LOW                     0xB000000000000000

/* Default handle for the Project Zero LED characteristic */
#define PROJECT_ZERO_LED_HANDLE         0x001E

#define THREE_SEC_PERIOD              0x0BB8 // in ms

/* Default values used */
#define DEFAULT_ADDRESS_MODE            ADDRMODE_RP_WITH_PUBLIC_ID
#define DEFAULT_PEER_ADDRESS_TYPE       PEER_ADDRTYPE_PUBLIC_OR_PUBLIC_ID
#define DEFAULT_PHYS                    0x01   // INIT_PHY_1M
#define DEFAULT_TIMEOUT                 0x0000 //  in ms
#define DEFAULT_SCAN_DURATION           0x0050 // in 625-us unit
#define DEFAULT_MIN_CONNECT_INTERVAL    0x0050 // in 1.25-ms unit
#define DEFAULT_MAX_CONNECT_INTERVAL    0x0050 // in 1.25-ms unit
#define DEFAULT_CONNECT_LATENCY         0x0000 // in number of connection events.
#define DEFAULT_START_HANDLE            0x0001 // First requested handle number
#define DEFAULT_END_HANDLE              0xFFFF // Last requested handle number
#define DEFAULT_TERMINATE_LINK_REASON   0x13   // Terminate Link Reason - User Terminated Connection */
#define DEFAULT_HCI_RESET_TYPE          0x00   // HCI Reset Type - Chip Reset

#define Legacy_SCAN_RSP_to_ADV_IND_or_Data_Complete 0x001B // event type for advertiser scanner event

extern uint16_t g_currConnHandle;

#define BLE_FLAG_SECURE                 (1<<0)


/* Prototypes for APIs to perform BLE functions */

/*******************************************************************************
 * @fn      BLE_IF_init
 *
 * @brief   Allocate resources and initiate the BLE in a specific role and security.
*
 *
 * @param   Role - one of:
 *                  GAP_PROFILE_PERIPHERAL,
 *                  GAP_PROFILE_CENTRAL,
 *                  GAP_PROFILE_PERIPHERAL|GAP_PROFILE_CENTRAL
 *          pDeviceName - pointer to device name (null-terminated string)
 *
 * @return  0 upon success, or negative value upon error.
 *
 * @note    This should be called before any other BLE_IF API
 ******************************************************************************/
int              BLE_IF_init(uint8_t role, const char *pDeviceName);


/*******************************************************************************
 * @fn      BLE_IF_deinit
 *
 * @brief   Terminate BLE connection and free its resources
 *
 * @param   None
 *
 * @return  0 upon success, or negative value upon error.
 *
 * @note    NOT SUPPORTED
 ******************************************************************************/
int              BLE_IF_deinit();

/*******************************************************************************
 * @fn      BLE_IF_addService
 *
 * @brief   [Peripheral API] Register a service
 *
 * @param   pService - a pointer to service parameters structure.
 *
 * @return  0 upon success, or negative value upon error.
 *
 ******************************************************************************/
int              BLE_IF_addService(serviceParams_t *pService);

/*******************************************************************************
 * @fn      BLE_IF_enableConnection
 *
 * @brief   [Peripheral API] Enable a central device to connect
 *
 *          pPasscode - pointer to pairing key (32b integer),
 *                      or 0 for non-secure connection
 * @return  0 upon success, or negative value upon error.
 *
 ******************************************************************************/
int              BLE_IF_enableConnection(const uint32_t *pPasskey);


/*******************************************************************************
 * @fn      BLE_IF_sendNotification
 *
 * @brief   [Peripheral API] Send Notification to a connected central device
 *
 * @param   None
 *
 * @return  0 upon success, or negative value upon error.
 *
 ******************************************************************************/
int              BLE_IF_sendNotification(uint16_t handle, uint8_t *pValue, uint16_t valueLen);

#ifdef BLE_IF_CENTRAL
HCI_StatusCodes_t BLE_IF_discoverDevices(void);
HCI_StatusCodes_t BLE_IF_connect(uint8_t* pPeerAddress);
HCI_StatusCodes_t BLE_IF_cisconnect(uint16_t connHandle);
HCI_StatusCodes_t BLE_IF_setScanPhy(void);
HCI_StatusCodes_t BLE_IF_stopDiscovering(void);
HCI_StatusCodes_t BLE_IF_cancelConnecting(void);
HCI_StatusCodes_t BLE_IF_gattRead(int16_t connHandle, attReadReq_t req);
HCI_StatusCodes_t BLE_IF_gattWrite(GattWriteCharValue_t *para);
HCI_StatusCodes_t BLE_IF_updateConnectionParam(int16_t connHandle, int32_t value);
HCI_StatusCodes_t BLE_IF_processStackMsg(void);
#endif


#endif /* BLE_IF_H */
