/* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/


/* Standard Includes */
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <mqueue.h>
#include <assert.h>

#include <ble_if.h>
#include <ble_svc_provisioning.h>
#include <BLE/inc/gap.h>
#include <BLE/inc/gap_advertiser.h>
#include <BLE/inc/gap_initiator.h>
#include <BLE/inc/gap_scanner.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/hci_tl.h>
//#include "math.h"

/* TI-Driver Includes */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Local Includes */
#include "ti_drivers_config.h"


/* ble5stack Includes */
#include "uart_term.h"

enum
{
    GATT_GENACCESS_NAME_ID,
    GATT_GENACCESS_APPEARANCE_ID,
    GATT_GENACCESS_PREFCONNPARAMS_ID,
};


#define GEN_ACCESS_NAME                     "TI-COEX-DEV (CC3235)"
static  uint8_t g_appearance[] = {0, 0};
static  uint8_t g_preferredConnParams[] = {0x50, 0x00, 0xa0, 0x00, 0x00, 0x00, 0xe8, 0x03};

/*******************************************************************************
 *                         LOCAL FUNCTIONS
 ******************************************************************************/
static HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen);


/*******************************************************************************
 *                        PUBLIC FUNCTIONS
 ******************************************************************************/

static charParams_t g_genAccessChars[] =
{
 { 0, 0, DEVICE_NAME_UUID,      GATT_PERMIT_READ, GATT_PROP_READ,  "NAME" },
 { 0, 0, APPEARANCE_UUID,       GATT_PERMIT_READ, GATT_PROP_READ,  "APPEARANCE" },
 { 0, 0, PERI_CONN_PARAM_UUID,  GATT_PERMIT_READ, GATT_PROP_READ,  "CONN-PARAMS" },
};

serviceParams_t g_genAccessService =
{
 GAP_SERVICE_UUID,
 sizeof(g_genAccessChars)/sizeof(charParams_t), g_genAccessChars,
 NULL,
 NULL,
 hndlr_AttributeReadReq,
 0, 0, 0
};

HCI_StatusCodes_t hndlr_AttributeReadReq(uint16_t connHandle, uint8_t userId, uint8_t **ppValue, uint16_t *pValueLen)
{
    HCI_StatusCodes_t status = FAILURE;

    Report("GEN ACCESS SERVICE EVENT (ATT READ REQ) = userId=%d\n\r", userId);
    switch (userId)
    {
        case GATT_GENACCESS_NAME_ID:
            *ppValue = (uint8_t*)GEN_ACCESS_NAME;
            *pValueLen = strlen(GEN_ACCESS_NAME);
            break;
        case GATT_GENACCESS_APPEARANCE_ID:
            *ppValue = (uint8_t*)g_appearance;
            *pValueLen = sizeof(g_appearance);
            break;
        case GATT_GENACCESS_PREFCONNPARAMS_ID:
            *ppValue = (uint8_t*)g_preferredConnParams;
            *pValueLen = sizeof(g_preferredConnParams);
            break;
    }
    return status;
}
