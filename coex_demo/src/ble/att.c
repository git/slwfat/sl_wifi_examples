 /* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* Standard Includes */
#include <BLE/inc/att.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/hal_defs.h>
#include <BLE/inc/hci.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "debug_if.h"

#undef DEBUG_IF_NAME
#undef DEBUG_IF_SEVERITY
#define DEBUG_IF_NAME       "ATT"
#define DEBUG_IF_SEVERITY   E_INFO


extern sem_t hciEvtSem;

#define EVENT_TABLE_MIN   ATT_EVENT_READBYGRPTYPEREQ
#define EVENT_TABLE_MAX   ATT_EVENT_READBYGRPTYPERSP
#define EVENT_TABLE_SIZE  (EVENT_TABLE_MAX - EVENT_TABLE_MIN + 1)

#define ATT_CFG_SERVER_MTU  23


typedef struct
{
    uint16_t        uuid;
    uint8_t         userId;
    uint8_t         perm;
    union
    {
        void            *pContext;
        charParams_t    *pChar;
        serviceParams_t *pService;
    };
} attr_t;


#define MAX_ATTRIBUTES      64
attr_t  g_attributes[MAX_ATTRIBUTES];

int g_gattHandleNum = 1;

uint16_t ATT_SetAttribute(uint16_t userId, uint16_t uuid, uint8_t perm, void *pContext)
{
    uint16_t handle = INVALID_HANDLE;
    if(g_gattHandleNum-1 < MAX_ATTRIBUTES)
    {
        g_attributes[g_gattHandleNum - 1].uuid = uuid;
        g_attributes[g_gattHandleNum - 1].userId = userId;
        g_attributes[g_gattHandleNum - 1].perm = perm;
        g_attributes[g_gattHandleNum - 1].pContext = pContext;
        handle = g_gattHandleNum++;
    }
    return handle;
}


/*******************************************************************************
 *******************************************************************************
 *** ATT Events Response (constructors)
 *******************************************************************************
 *******************************************************************************
 */

/*******************************************************************************
 * @fn      ATT_FindInfoReq
 *
 * @brief   construct and send the Find Info Request message
 *
 * @param   connHandle - connection handle
 *          startHandle - base handle for this query
 *          endHandle - end handle for this query
 *
 * @return  status of message sent
 ******************************************************************************/
HCI_StatusCodes_t ATT_FindInfoReq( uint16_t connHandle, uint16_t startHandle, uint16_t endHandle)
{
     HCI_StatusCodes_t status;
     uint8_t pData[6];

     pData[0] = LO_UINT16(connHandle);
     pData[1] = HI_UINT16(connHandle);
     pData[2] = LO_UINT16(startHandle);
     pData[3] = HI_UINT16(startHandle);
     pData[4] = LO_UINT16(endHandle);
     pData[5] = HI_UINT16(endHandle);

     status = HCI_sendHCICommand(ATT_CMD_FINDINFOREQ, pData, 6, NULL, NULL);
     return status;

}


/*******************************************************************************
 * @fn      ATT_ExchangeMTURsp
 *
 * @brief   construct and send the MTU EXchange Response message
 *
 * @param   connHandle - connection handle
 *          serverRxMTU - supported MTU of the receive path
 *
 * @return  status of message sent
 ******************************************************************************/
static HCI_StatusCodes_t ATT_ExchangeMTURsp( uint16_t connHandle, uint16_t serverRxMTU )
{
     HCI_StatusCodes_t status;
     uint8_t pData[4];

     pData[0] = LO_UINT16(connHandle);
     pData[1] = HI_UINT16(connHandle);
     pData[2] = LO_UINT16(serverRxMTU);
     pData[3] = HI_UINT16(serverRxMTU);

     status = HCI_sendHCICommand(ATT_CMD_EXCHANGEMTURSP, pData, 4, NULL, NULL);
     return status;

}

/*******************************************************************************
 * @fn      ATT_ReadByGroupTypeRsp
 *
 * @brief   construct and send the Read-By-Group-Type Response message
 *
 * @param   connHandle - connection handle
 *          pRsp - response payload
 *
 * @return  status of message sent
 ******************************************************************************/
static HCI_StatusCodes_t ATT_ReadByGroupTypeRsp( uint16_t connHandle, attReadByGrpTypeRsp_t *pRsp )
{
    int i;
    HCI_StatusCodes_t status = bleMemAllocError;
    uint8_t *pData = malloc(pRsp->len + 3);

    if(pData)
    {
        pData[0] = (connHandle >> 0) & 0xff;
        pData[1] = (connHandle >> 8) & 0xff;
        pData[2] = pRsp->len;
        for(i=0; i<pRsp->len; i++)
        {
            pData[3+i] = pRsp->pDataList[i];
        }

        status = HCI_sendHCICommand(ATT_CMD_READBYGRPTYPERSP, pData, 3+i, NULL, NULL);
        free (pData);
    }
    return status;
}

/*******************************************************************************
 * @fn      ATT_ReadByTypeRsp
 *
 * @brief   construct and send the Read-By-Type Response message
 *
 * @param   connHandle - connection handle
 *          pRsp - response payload
 *
 * @return  status of message sent
 ******************************************************************************/
static HCI_StatusCodes_t ATT_ReadByTypeRsp( uint16_t connHandle, attReadByTypeRsp_t *pRsp )
{
    int i;
    HCI_StatusCodes_t status = bleMemAllocError;
    uint8_t *pData = malloc(pRsp->dataLen + 3);

    if(pData)
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);
        pData[2] = pRsp->len;
        for(i=0; i<pRsp->dataLen; i++)
        {
            pData[3+i] = pRsp->pDataList[i];
        }

        status = HCI_sendHCICommand(ATT_CMD_READBYTYPEVALUERSP, pData, 3+i, NULL, NULL);
        free (pData);
    }
    return status;
}

/*******************************************************************************
 * @fn      ATT_FindByTypeValueRsp
 *
 * @brief   construct and send the Find-By-Type-Value Response message
 *
 * @param   connHandle - connection handle
 *          pRsp - response payload
 *
 * @return  status of message sent
 ******************************************************************************/
static HCI_StatusCodes_t ATT_FindByTypeValueRsp( uint16_t connHandle, attFindByTypeValueRsp_t *pRsp )
{
    int i;
    HCI_StatusCodes_t status = bleMemAllocError;
    uint8_t *pData = malloc(pRsp->numInfo*4 + 2);

    if(pData)
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);
        for(i=0; i<pRsp->numInfo*4; i++)
        {
            pData[2+i] = pRsp->pHandlesInfo[i];
        }

        status = HCI_sendHCICommand(ATT_CMD_FINDBYTYPEVALUERSP, pData, 2+i, NULL, NULL);
        free (pData);
    }
    return status;
}




/*******************************************************************************
 * @fn      ATT_ReadRsp
 *
 * @brief   construct and send the Read Response (Data) message
 *
 * @param   connHandle - connection handle
 *          len - length of read payload
 *          pValue - read payload
 *
 * @return  status of message sent
 ******************************************************************************/
static HCI_StatusCodes_t ATT_ReadRsp( uint16_t connHandle, uint16_t len, uint8_t *pValue )
{
    int i;
    HCI_StatusCodes_t status = bleMemAllocError;
    uint8_t *pData = (uint8_t *)malloc(2+len);
    if(pData)
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        for(i=0; i<len; i++)
        {
            pData[2+i] = pValue[i];
        }

        status = HCI_sendHCICommand(ATT_CMD_READRSP, pData, 2+len, NULL, NULL);
        free(pData);
    }
    return status;
}

/*******************************************************************************
 * @fn      ATT_WriteRsp
 *
 * @brief   construct and send the Write Response (ACK) message
 *
 * @param   connHandle - connection handle
 *
 * @return  status of message sent
 ******************************************************************************/
static HCI_StatusCodes_t ATT_WriteRsp( uint16_t connHandle )
{
    HCI_StatusCodes_t status;
    uint8_t pData[2];
    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);
    status = HCI_sendHCICommand(ATT_CMD_WRITERSP, pData, 2, NULL, NULL);
    return status;

}

/*******************************************************************************
 *******************************************************************************
 *** ATT Events Handling
 *******************************************************************************
 *******************************************************************************
 */

/*******************************************************************************
 * @fn      ATT_hndlr_ReadByGroupTypeReq
 *
 * @brief   handler for ATT Read-By-Group-Type Request
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
static HCI_StatusCodes_t ATT_hndlr_ReadByGroupTypeReq(hciRxMsg_t *pMsg)
{
    int i;
    attReadByGrpTypeRsp_t response;
    uint8_t payload[6];
    HCI_StatusCodes_t status = FAILURE;
    uint16_t connHandle = BUILD_UINT16(&pMsg->pPayload[3]);
    uint16_t startHandle = BUILD_UINT16(&pMsg->pPayload[6]);
    uint16_t endHandle = BUILD_UINT16(&pMsg->pPayload[8]);
    uint16_t uuid = BUILD_UINT16(&pMsg->pPayload[10]);

    response.len = 6;
    response.numGrps = 1;
    response.pDataList = payload;
    for(i = startHandle; i < endHandle; i++)
    {
        if(g_attributes[i-1].uuid == uuid)
        {
            serviceParams_t *pService = g_attributes[i-1].pService;
            uint16_t lastHandle = i + pService->attrNum;
            uint16_t serviceUuid = pService->uuid;
            response.pDataList[0] = LO_UINT16(i);
            response.pDataList[1] = HI_UINT16(i);
            response.pDataList[2] = LO_UINT16(lastHandle);
            response.pDataList[3] = HI_UINT16(lastHandle);
            response.pDataList[4] = LO_UINT16(serviceUuid);
            response.pDataList[5] = HI_UINT16(serviceUuid);
            break;
        }
    }

    HCI_CALL(ATT_ReadByGroupTypeRsp, connHandle, &response);
//    HCI_CALL(GATT_DiscAllPrimaryServices, g_connHandle);
    return status;
}


/*******************************************************************************
 * @fn      ATT_hndlr_ReadByTypeReq
 *
 * @brief   handler for ATT Read-By-Type Request
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
static HCI_StatusCodes_t ATT_hndlr_ReadByTypeReq(hciRxMsg_t *pMsg)
{
    int i;
    attReadByTypeRsp_t response = { 0 };
    HCI_StatusCodes_t status = FAILURE;
    uint16_t connHandle = BUILD_UINT16(&pMsg->pPayload[3]);
    uint16_t minHandle = BUILD_UINT16(&pMsg->pPayload[6]);
    uint16_t maxHandle = BUILD_UINT16(&pMsg->pPayload[8]);
    uint16_t uuid = BUILD_UINT16(&pMsg->pPayload[10]);

    for(i = minHandle; i <= maxHandle && i < g_gattHandleNum; i++)
    {
        if(g_attributes[i-1].uuid == uuid)
            break;
    }
    if(i < maxHandle && i < g_gattHandleNum)
    {
       if(uuid == GATT_CHARACTER_UUID)
       {
           uint8_t payload[7];
           response.len = 7;
           response.dataLen = response.len;
           response.pDataList = payload;
           response.numPairs = 1;

           response.pDataList[0] = LO_UINT16(i);
           response.pDataList[1] = HI_UINT16(i);
           response.pDataList[2] = g_attributes[i-1].pChar->prop;
           response.pDataList[3] = LO_UINT16(i+1);
           response.pDataList[4] = HI_UINT16(i+1);
           response.pDataList[5] = LO_UINT16(g_attributes[i].uuid);
           response.pDataList[6] = HI_UINT16(g_attributes[i].uuid);
       }
       else
       {
           charParams_t *pChar = g_attributes[i-1].pChar;
           serviceParams_t *pService = g_attributes[pChar->Servicehandle-1].pService;
           if(pService->ReadReq_f)
                pService->ReadReq_f(connHandle, g_attributes[i-1].userId, &response.pDataList, &response.len);

           response.dataLen = response.len;
           response.numPairs = 1;
       }
    }

    HCI_CALL(ATT_ReadByTypeRsp, connHandle, &response);
    return status;
}

#define MAX_HANDLES_INFO 4
/*******************************************************************************
 * @fn      ATT_hndlr_ReadByTypeValueReq
 *
 * @brief   handler for ATT Read-By-Type-Value Request
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
static HCI_StatusCodes_t ATT_hndlr_FindByTypeValueReq(hciRxMsg_t *pMsg)
{
    int i;
    attFindByTypeValueRsp_t response = { 0 };
    HCI_StatusCodes_t status = FAILURE;
    uint16_t connHandle = BUILD_UINT16(&pMsg->pPayload[3]);
    uint16_t minHandle = BUILD_UINT16(&pMsg->pPayload[6]);
    uint16_t maxHandle = BUILD_UINT16(&pMsg->pPayload[8]);
    uint16_t uuid = BUILD_UINT16(&pMsg->pPayload[10]);
    uint16_t value = BUILD_UINT16(&pMsg->pPayload[12]);
    uint8_t handlesInfo[MAX_HANDLES_INFO * 4];

    assert(uuid == GATT_PRIMARY_SERVICE_UUID);
    for(i = minHandle; i <= maxHandle && i < g_gattHandleNum; i++)
    {
        if(g_attributes[i-1].uuid == uuid &&
           g_attributes[i-1].pService->uuid == value)
        {
            handlesInfo[response.numInfo*4 + 0] = LO_UINT16(i);
            handlesInfo[response.numInfo*4 + 1] = HI_UINT16(i);
            handlesInfo[response.numInfo*4 + 2] = 0xff;
            handlesInfo[response.numInfo*4 + 3] = 0xff;
            response.numInfo++;
            if(response.numInfo == MAX_HANDLES_INFO)
                break;
        }
    }
    response.pHandlesInfo = handlesInfo;
    HCI_CALL(ATT_FindByTypeValueRsp, connHandle, &response);
    return status;
}

/*******************************************************************************
 * @fn      ATT_hndlr_ReadReq
 *
 * @brief   handler for ATT Read Request
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
static HCI_StatusCodes_t ATT_hndlr_ReadReq(hciRxMsg_t *pMsg)
{
    HCI_StatusCodes_t status = FAILURE;
    uint16_t connHandle = BUILD_UINT16(&pMsg->pPayload[3]);
    uint16_t handle = BUILD_UINT16(&pMsg->pPayload[6]);
    charParams_t *pChar = g_attributes[handle-1].pChar;
    serviceParams_t *pService = g_attributes[pChar->Servicehandle-1].pService;
    uint8_t *pValue = NULL;
    uint16_t valueLen = 0;

    if(g_attributes[handle-1].uuid == GATT_CHAR_USER_DESC_UUID)
    {
        HCI_CALL(ATT_ReadRsp, connHandle, strlen(pChar->pDesc), (uint8_t*)pChar->pDesc);
    }
    else
    {
        if(pService->ReadReq_f)
            pService->ReadReq_f(connHandle, g_attributes[handle-1].userId, &pValue, &valueLen);
        HCI_CALL(ATT_ReadRsp, connHandle, valueLen, pValue);

    }
    return status;
}

/*******************************************************************************
 * @fn      ATT_hndlr_WriteReq
 *
 * @brief   handler for ATT Write Request
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
static HCI_StatusCodes_t ATT_hndlr_WriteReq(hciRxMsg_t *pMsg)
{
    HCI_StatusCodes_t status = FAILURE;
     uint16_t connHandle = BUILD_UINT16(&pMsg->pPayload[3]);
     uint16_t handle = BUILD_UINT16(&pMsg->pPayload[8]);
     uint8_t len = pMsg->length - 10;
     charParams_t *pChar = g_attributes[handle-1].pChar;
     serviceParams_t *pService = g_attributes[pChar->Servicehandle-1].pService;

     HCI_CALL(ATT_WriteRsp, connHandle);

     /* look for the current service and invoke the write callback */
     if(pService->WriteReq_f)
         pService->WriteReq_f(connHandle, g_attributes[handle-1].userId, &pMsg->pPayload[10], len);

     return status;
}

/*******************************************************************************
 * @fn      ATT_hndlr_ExchangeMtuReq
 *
 * @brief   handler for ATT Exchange MTU Request
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
static HCI_StatusCodes_t ATT_hndlr_ExchangeMtuReq(hciRxMsg_t *pEvent)
{
    HCI_StatusCodes_t status = SUCCESS;
    uint16_t conn = BUILD_UINT16(&pEvent->pPayload[3]);

    LOG_DEBUG("ATT EVENT (EXCHANGE MTU REQ)");
    HCI_CALL(ATT_ExchangeMTURsp, conn, ATT_CFG_SERVER_MTU);
    return status;
}


/*******************************************************************************
 * @fn      ATT_handleEvent
 *
 * @brief   ATT Event Dispatcher
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
void ATT_handleEvent(hciRxMsg_t *pHciEvent)
{
    switch(pHciEvent->eventOpCode)
    {
        case ATT_EVENT_READBYGRPTYPEREQ:
        {
            LOG_DEBUG("handleEvent: ATT_EVENT_READBYGRPTYPEREQ");
            ATT_hndlr_ReadByGroupTypeReq(pHciEvent);
        }
        break;
        case ATT_EVENT_READBYTYPEREQ:
        {
            LOG_DEBUG("handleEvent: ATT_EVENT_READBYTYPEREQ");
            ATT_hndlr_ReadByTypeReq(pHciEvent);
        }
        break;
        case ATT_EVENT_FINDBYTYPEVALUEREQ:
        {
            LOG_DEBUG("handleEvent: ATT_EVENT_FINDBYTYPEVALUEREQ");
            ATT_hndlr_FindByTypeValueReq(pHciEvent);
        }
        break;
        case ATT_EVENT_READREQ:
        {
            LOG_DEBUG("handleEvent: ATT_EVENT_READREQ");
            ATT_hndlr_ReadReq(pHciEvent);
        }
        break;
        case ATT_EVENT_WRITEREQ:
        {
            LOG_DEBUG("handleEvent: ATT_EVENT_WRITEREQ");
            ATT_hndlr_WriteReq(pHciEvent);
        }
        break;
        case ATT_EVENT_EXCHANGEMTUREQ:
        {
            LOG_DEBUG("handleEvent: ATT_EVENT_EXCHANGEMTUREQ");
            ATT_hndlr_ExchangeMtuReq(pHciEvent);
        }
        break;
        case ATT_EVENT_FLOWCTRLVIOLATED:
        {
            LOG_DEBUG("handleEvent: ATT_EVENT_FLOWCTRLVIOLATED");
        }
        break;
        case ATT_EVENT_FINDINFORSP:
        {
            /* Event is not handled by this layer */
        }
        break;
        default:
            LOG_WARNING("handleEvent: %d (%x) - no handler", pHciEvent->eventOpCode, pHciEvent->eventOpCode);

    }
}
