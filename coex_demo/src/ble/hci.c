 /* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* Standard Includes */
#include <BLE/inc/att.h>
#include <BLE/inc/gap.h>
#include <BLE/inc/gap_advertiser.h>
#include <BLE/inc/hci.h>
#include <BLE/inc/hci_tl.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <mqueue.h>
#include <unistd.h>

/* TI-Driver Includes */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>
#include "debug_if.h"

#undef DEBUG_IF_NAME
#undef DEBUG_IF_SEVERITY
#define DEBUG_IF_NAME       "HCI"
#define DEBUG_IF_SEVERITY   E_INFO


/* ble5stack Includes */

static sem_t hciCmdRspSem;
static pthread_t hciEvtTask;
uint16_t g_hciLastCommand = 0;

static uint8_t g_hciRespStatus;
static uint8_t g_hciRespLen, *g_hciRespPayload;

static mqd_t g_eventMsgQ;
static hciEventHandler_t HCILE_GenReportEventHandler = NULL;

static void *hciEvtTaskFxn(void *arg0);
extern void ATT_handleEvent(hciRxMsg_t *pMsg);
extern void GAP_handleEvent(hciRxMsg_t *pMsg);


pthread_t TaskCreate(int prio, size_t stacksize, void* (*fTask)(void*), void *arg);

/*******************************************************************************
 *                         LOCAL FUNCTIONS
 ******************************************************************************/
#if (DEBUG_IF_SEVERITY == E_TRACE)
/*******************************************************************************
 * @fn      PRINT_RX_DATA
 *
 * @brief   print received HCI data (bytes over UART)
 *
 * @param[in]  pMsg  - received packet.
 *
 * @return : the decoded event header
 ******************************************************************************/
static void PRINT_RX_DATA(hciRxMsg_t *pMsg)
{
    uint8_t *p;
    int i;
    Report("     [RX]");
    p = &pMsg->pktType;
    for(i=0; i<3; i++)
    {
        Report(" %02x", p[i]);
    }
    for(i=0; i<pMsg->length; i++)
    {
        Report(" %02x", pMsg->pPayload[i]);
    }
    Report("\n\r");
}

/*******************************************************************************
 * @fn      HCI_notifyEvent
 *
 * @brief  Print transmitted hci data (bytes over UART)
 *
 * @param[in]  pHciEvent  - transmitted packet.
 *
 * @return : the decoded event header
 ******************************************************************************/
static void PRINT_TX_DATA(uint8_t *pBuff, uint32_t len)
{
    int i;
    Report("     [TX]");
    for(i=0; i<len; i++)
    {
        Report(" %02x", pBuff[i]);
    }
    Report("\n\r");
}
#else
#define PRINT_RX_DATA(...)
#define PRINT_TX_DATA(...)
#endif

/*******************************************************************************
 * @fn      HCI_notifyEvent
 *
 * @brief   Notify (blocking) upper layer thread on a reception of a-sync event
 *
 * @param[in]  pHciEvent  - received packet.
 *
 * @return : the decoded event header
 ******************************************************************************/
void HCI_notifyEvent(hciRxMsg_t *pHciEvent)
{
    mq_send(g_eventMsgQ, (char*)pHciEvent, sizeof(hciRxMsg_t), 1);
}

/*******************************************************************************
 * @fn      HCI_notifyCmdComplete
 *
 * @brief   Notify (blocking) upper layer on a reception of command complete
 *
 * @param[in]  pCmdCmplt  - received packet.
 *
 * @return : the decoded event header
 ******************************************************************************/
static void HCI_notifyCmdComplete(hciRxMsg_t *pCmdCmplt)
{
    g_hciRespStatus = pCmdCmplt->status;
    g_hciRespLen = pCmdCmplt->length;
    g_hciRespPayload = pCmdCmplt->pPayload;
    PRINT_RX_DATA(pCmdCmplt);
    sem_post(&hciCmdRspSem);
}

/*******************************************************************************
 * @fn      HCI_decodeEventHeader
 *
 * @brief   Decode the header of a received HCI Event Packet.
 *
 * @param[in]  packet  - received packet.
 *
 * @return : the decoded event header
 ******************************************************************************/
uint8_t HCI_decodeEventHeader(hciRxMsg_t *pMsg)
{

  switch(pMsg->eventId)
  {
  case HCI_COMMANDCOMPLETEEVENT:   /* Received HCI command complete event */
      pMsg->eventOpCode = pMsg->pPayload[2];
      pMsg->eventOpCode = pMsg->pPayload[1] + (pMsg->eventOpCode << 8);
      pMsg->status = pMsg->pPayload[3];
      break;
  case HCI_LE_EXTEVENT:   /* Received HCI Vendor Specific Event */
      pMsg->eventOpCode = pMsg->pPayload[1];
      pMsg->eventOpCode = pMsg->pPayload[0] + (pMsg->eventOpCode << 8);
      pMsg->status = pMsg->pPayload[2];
       break;
  case HCI_LE_GENREPORTEVENT:   /* Received HCI Vendor Specific Event */
      pMsg->eventOpCode = 0xffff;
      pMsg->status = pMsg->pPayload[1];
       break;
  default: /* Received other event */
      pMsg->eventOpCode = 0;
      pMsg->status = FAILURE;
      break;
  }
  return (pMsg->status);
}



/*******************************************************************************
 * @fn      HCI_RegisterLeReportEvent
 *
 * @brief   used by the user to register to the LE Report events
 *
 * @param   handler - user callback to be invoked when LE Report event is received
 *
 * @return  None.
 ******************************************************************************/
int HCI_RegisterLeReportEvent(hciEventHandler_t handler)
{
    HCILE_GenReportEventHandler = handler;
    return 0;
}


/*******************************************************************************
 *                        PUBLIC FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 * @fn      HCI_sendHCICommand
 *
 * @brief   Send a HCI Command Packet over the transport layer
 *
 * @param[in]   opcode - The HCI Command opcode of the packet to transport
 * @param[in]   pData - The payload data of the HCI Command packet
 * @param[in]   dataLength - The length of the payload data
 *
 * @return  HCI_StatusCodes_t status code
 ******************************************************************************/
HCI_StatusCodes_t HCI_sendHCICommand(uint16_t opcode, uint8_t *pData, uint8_t dataLength, uint8_t *pResponse, uint8_t *pRespLen)
{
    hciPacket_t *pPkt;
    HCI_StatusCodes_t status;

    pPkt = (hciPacket_t *) malloc(sizeof(hciPacket_t) + dataLength);
    if (pPkt == NULL)
    {
        status =  FAILURE;
    }
    else
    {
        pPkt->packetType = HCI_CMD_PACKET;
        pPkt->opcodeLO = (opcode >> 0)  & 0xff;
        pPkt->opcodeHI = (opcode >> 8)  & 0xff;;
        /* Set size */
        pPkt->dataLength = dataLength;
        g_hciLastCommand = opcode;


        /* Copy pointer data */
        memcpy(&pPkt->pData, pData, dataLength);

        PRINT_TX_DATA((uint8_t*)pPkt, HCI_FRAME_SIZE + dataLength);
        /* Send HCI packet */
        HCITL_transmit((uint8_t*)pPkt, HCI_FRAME_SIZE + dataLength);
        /* Free allocated memory */
        free(pPkt);

        /* Wait to receive event */
        sem_wait(&hciCmdRspSem);
        if(pResponse)
        {
            int len = (g_hciRespLen < *pRespLen)?g_hciRespLen:*pRespLen;
            memcpy(pResponse, g_hciRespPayload, len);
            *pRespLen = len;
        }
        if(g_hciRespPayload)
            free(g_hciRespPayload);
        status = (HCI_StatusCodes_t)g_hciRespStatus;
        g_hciLastCommand = 0;
    }

    return status;
}

/*******************************************************************************
 * @fn      HCI_sendExtCommand
 *
 * @brief   Send a HCI Command Packet over the transport layer
 *
 * @param[in]   opcode - The HCI Command opcode of the packet to transport
 * @param[in]   pData - The payload data of the HCI Command packet
 * @param[in]   dataLength - The length of the payload data
 *
 * @return  HCI_StatusCodes_t status code
 ******************************************************************************/
HCI_StatusCodes_t HCI_sendExtCommand(uint16_t opcode, uint8_t *pData, uint16_t dataLength, uint8_t *pResponse, uint8_t *pRespLen)
{
    extHciPacket_t *pPkt;
    HCI_StatusCodes_t status;

    pPkt = (extHciPacket_t *) malloc(sizeof(extHciPacket_t) + dataLength);
    if (pPkt == NULL)
    {
        status =  FAILURE;
    }
    else
    {
        pPkt->packetType = 9;
        pPkt->opcodeLO = (opcode >> 0)  & 0xff;
        pPkt->opcodeHI = (opcode >> 8)  & 0xff;;
        /* Set size */
        pPkt->dataLengthLO = (dataLength >> 0)  & 0xff;;
        pPkt->dataLengthHI = (dataLength >> 8)  & 0xff;;
        g_hciLastCommand = opcode;


        /* Copy pointer data */
        memcpy(&pPkt->pData, pData, dataLength);

        /* Send HCI packet */
        PRINT_TX_DATA((uint8_t*)pPkt, HCI_FRAME_SIZE + dataLength);
        HCITL_transmit((uint8_t*)pPkt, EXT_HCI_FRAME_SIZE + dataLength);
        /* Free allocated memory */
        free(pPkt);

        /* Wait to receive event */
        sem_wait(&hciCmdRspSem);
        if(pResponse)
        {
            int len = (g_hciRespLen < *pRespLen)?g_hciRespLen:*pRespLen;
            memcpy(pResponse, g_hciRespPayload, len);
            *pRespLen = len;
        }
        if(g_hciRespPayload)
            free(g_hciRespPayload);
        status = (HCI_StatusCodes_t)g_hciRespStatus;
        g_hciLastCommand = 0;
    }

    return status;
}



/*******************************************************************************
 * @fn      HCI_init
 *
 * @brief   Initialize the HCI transport layer
 *
 * @param[in]  hciParams - Struct containing parameters to initialize the
 *              HCI transport layer
 *
 * @return : None
 ******************************************************************************/
void HCI_init(HCI_Params *hciParams)
{
    mq_attr attr;

    HCITL_init(hciParams);
    /* Initialize the semaphore to handle timing of HCI Packets and Events */
    sem_init(&hciCmdRspSem, 1, 0);

    hciEvtTask = TaskCreate(BLE_EVT_THREAD_PRIORITY, BLE_EVT_STACK_SIZE, hciEvtTaskFxn, 0);
    while(hciEvtTask == NULL)
        ;
    // initializing the message queue for the MQTT module
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(hciRxMsg_t);
    g_eventMsgQ = mq_open("hciEventQ", O_CREAT, 0, &attr);
}

/*******************************************************************************
 * @fn      HCI_ResetCmd
 *
 * @brief   Issue a hard or soft system reset. A hard reset is caused by
 *          setting the SYSRESET bit in the System Controller Reset Control
 *          register. The soft reset is currently not supported on the CC264x.
 *
 * @param[in] type - Reset type.
 *
 * @return HCI_StatusCodes_t
 ******************************************************************************/
HCI_StatusCodes_t HCI_ResetCmd(uint8_t type)
{
    HCI_StatusCodes_t status;
    uint8_t pData[1];

    pData[0] = type;

    status = HCI_sendHCICommand(HCI_EXT_RESETSYSTEMCMD, pData, 1, NULL, NULL);
    return status;
}

/*******************************************************************************
 * @fn      HCI_ResetCmd
 *
 * @brief   Issue a hard or soft system reset. A hard reset is caused by
 *          setting the SYSRESET bit in the System Controller Reset Control
 *          register. The soft reset is currently not supported on the CC264x.
 *
 * @param[in] type - Reset type.
 *
 * @return HCI_StatusCodes_t
 ******************************************************************************/
HCI_StatusCodes_t HCI_LL_SetDataLength(uint16_t handle, uint16_t txOctet, uint16_t TxTime)
{
    HCI_StatusCodes_t status;
    uint8_t pData[6];

    pData[0] = (handle >> 0) & 0xff;
    pData[1] = (handle >> 8) & 0xff;
    pData[2] = (txOctet >> 0) & 0xff;
    pData[3] = (txOctet >> 8) & 0xff;
    pData[4] = (TxTime >> 0) & 0xff;
    pData[5] = (TxTime >> 8) & 0xff;

    status = HCI_sendHCICommand(HCI_LE_SET_DATA_LENGTH, pData, 6, NULL, NULL);
    return status;
}

/*******************************************************************************
 * @fn      HCI_waitEvent
 *
 * @brief   This function must be called by a user thread to wait for incoming events.
 *          Certain events get handled internally (in this context),
 *          but all a-sync events will be transfered to the user (the blocking
 *          function will return upon reception of events)
 *
 * @param[in]   pHciEvent - structure for the incoming message (allocated by the
 *          user.
 *
 * @return  HCI_StatusCodes_t status code
 ******************************************************************************/
void HCI_waitEvent(hciRxMsg_t *pHciEvent)
{
    unsigned int prio;

    mq_receive(g_eventMsgQ, (char*)pHciEvent, sizeof(hciRxMsg_t), &prio);
    PRINT_RX_DATA(pHciEvent);
    if(pHciEvent->pktType != HCI_APP_EVENT_PACKET)
    {
        if(pHciEvent->eventId == HCI_LE_GENREPORTEVENT)
        {
            if(HCILE_GenReportEventHandler)
            {
                HCILE_GenReportEventHandler(pHciEvent);
            }
        }
        else
        {
            uint16_t  eventOpCode =  pHciEvent->eventOpCode;

            switch(eventOpCode & 0xff00)
            {
            case  0x0500:
                ATT_handleEvent(pHciEvent);
                break;
            case  0x0600:
                GAP_handleEvent(pHciEvent);
                break;
            default:
                LOG_DEBUG("Received Event: %d (%x)", pHciEvent->eventOpCode, pHciEvent->eventOpCode);

            }

        }
    }
}


/*******************************************************************************
 * @fn      BleEvtTaskFxn
 *
 * @brief   Internal thread to wait for RX from the transport layer
 *
 * @param   arg0 not used.
 *
 * @return  None.
 ******************************************************************************/
static void *hciEvtTaskFxn(void *arg0)
{
    while (1)
    {
        hciRxMsg_t msg;
        /* Wait to receive an HCI event from the transport layer */
        if (HCITL_receive(&msg) == 0)
        {
            uint16_t commandOpcode;
            /* Decode the HCI Event packet header */
            uint8_t status = HCI_decodeEventHeader(&msg);
            if(status == FAILURE)
            {
                LOG_ERROR("HCI Event Error: %d", msg.eventId);
            }
            else
            {
                uint16_t  eventOpCode =  msg.eventOpCode;

                switch (eventOpCode)
                {
                case HCI_LE_SET_DATA_LENGTH:
                case HCI_EXT_RESETSYSTEMDONE:
                    HCI_notifyCmdComplete(&msg);
                    break;
                case HCI_COMMANDSTATUS:
                    /* Get the command opcode */
                    commandOpcode = BUILD_UINT16(&msg.pPayload[3]);

                    if (commandOpcode == g_hciLastCommand)
                    {
                        /* Post that the event has been received */
                        HCI_notifyCmdComplete(&msg);
                    }
                    break;
                default:
                    HCI_notifyEvent(&msg);
                    continue;
                }
            }
        }
    }
}
