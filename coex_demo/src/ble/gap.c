 /* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* Standard Includes */
#include <BLE/inc/gap.h>
#include <BLE/inc/gap_advertiser.h>
#include <BLE/inc/gap_initiator.h>
#include <BLE/inc/gap_scanner.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/hci.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "debug_if.h"

#undef DEBUG_IF_NAME
#undef DEBUG_IF_SEVERITY
#define DEBUG_IF_NAME       "GAP"
#define DEBUG_IF_SEVERITY   E_INFO

#define ADV_TYPE_FLAGS          0x01
#define ADV_TYPE_DEVICE_NAME    0x09
#define ADV_TYPE_TX_POWER       0x0A
#define ADV_TYPE_CONN_INTERVAL  0x12

static GapAdv_params_t gapAdv =
{
 GAP_ADV_PROP_CONNECTABLE | GAP_ADV_PROP_SCANNABLE | GAP_ADV_PROP_LEGACY,
 160, 160,
 GAP_ADV_CHAN_ALL,
 PEER_ADDRTYPE_PUBLIC_OR_PUBLIC_ID,
 {0,0,0,0,0,0},
 GAP_ADV_WL_POLICY_ANY_REQ,
 0x7F,
 GAP_ADV_PRIM_PHY_1_MBPS,
 GAP_ADV_SEC_PHY_1_MBPS,
 0
};

#define SEND_SLAVE_SECURITY_REQUEST

#ifdef SEND_SLAVE_SECURITY_REQUEST
static bool  g_bSecureConn = false;
#endif

/******************* GAP API *******************/
/*******************************************************************************
 * @fn      GAP_deviceInit
 *
 * @brief   construct and send the GAP_deviceInit command
 *
 * @param   profileRole - one of:
 *                  GAP_PROFILE_PERIPHERAL,
 *                  GAP_PROFILE_CENTRAL,
 *                  GAP_PROFILE_PERIPHERAL|GAP_PROFILE_CENTRAL
 *          addrMode - see GAP_Addr_Modes_t
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAP_deviceInit(uint8_t profileRole, GAP_Addr_Modes_t addrMode)
{
    HCI_StatusCodes_t status;
    uint8_t index;
    uint8_t pData[8];

    pData[0] = profileRole;
    pData[1] = addrMode;

    for (index = 0; index < DEFAULT_ADDRESS_SIZE; index++)
    {
        pData[2 + index] = 0;
    }

    status = HCI_sendHCICommand(GAP_DEVICEINIT, pData, 8, NULL, NULL);

    return status;
}

/*******************************************************************************
 * @fn      GAP_TerminateLinkReq
 *
 * @brief   construct and send the GAP_TerminateLink request
 *
 * @param   connHandle - connection handle
 *          reason - trmination reason
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAP_TerminateLinkReq(uint16_t connHandle, uint8_t reason)
{
    HCI_StatusCodes_t status;
    uint8_t pData[3];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = reason;

    status = HCI_sendHCICommand(GAP_TERMINATELINKREQUEST, pData, 3, NULL, NULL);

    return status;
}


/*******************************************************************************
 * @fn      GAP_SetBondParam
 *
 * @brief   construct and send the GAP_SetBondParam request
 *
 * @param   connHandle - connection handle
 *          reason - trmination reason
 *
 * @return  None.
 ******************************************************************************/

HCI_StatusCodes_t GAP_SetBondParam(uint16_t paramId, uint8_t len, uint8_t *pValue)
{
    HCI_StatusCodes_t status = FAILURE;
    uint8_t *pData = (uint8_t *)malloc(3+len);

    if(pData)
    {
        pData[0] = LO_UINT16(paramId);
        pData[1] = HI_UINT16(paramId);

        pData[2] = len;

        memcpy(&pData[3], pValue, len);

        status = HCI_sendHCICommand(GAP_BONDSETPARAMS, pData, 3+len, NULL, NULL);
        free (pData);
    }

    return status;
}







/******************* GAPADV ADV API *******************/


/*******************************************************************************
 * @fn      GAPADV_loadData
 *
 * @brief   Construct and send GAPADV_create command
 *
 * @param   advParam - advertisement parameters
 *          pHandle[out] - advertisement handle
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAPADV_create(GapAdv_params_t *advParam, uint8_t *pHandle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[21];
    uint8_t resp[7], respLen = 7;


    pData[0] = (advParam->eventProps >> 0) & 0xff;
    pData[1] = (advParam->eventProps >> 8) & 0xff;

    pData[2] = (advParam->primIntMin >> 0) & 0xff;
    pData[3] = (advParam->primIntMin >> 8) & 0xff;
    pData[4] = (advParam->primIntMin >> 16) & 0xff;

    pData[5] = (advParam->primIntMax >> 0) & 0xff;
    pData[6] = (advParam->primIntMax >> 8) & 0xff;
    pData[7] = (advParam->primIntMax >> 16) & 0xff;

    pData[8] = advParam->primChanMap;

    pData[9] = advParam->peerAddrType;
    pData[10] = (advParam->peerAddr[0]);
    pData[11] = (advParam->peerAddr[1]);
    pData[12] = (advParam->peerAddr[2]);
    pData[13] = (advParam->peerAddr[3]);
    pData[14] = (advParam->peerAddr[4]);
    pData[15] = (advParam->peerAddr[5]);

    pData[16] = advParam->filterPolicy;

    pData[17] = advParam->txPower;
    pData[18] = advParam->primPhy;
    pData[19] = advParam->secPhy;
    pData[20] = advParam->sid;

    status = HCI_sendHCICommand(GAPADV_CREATE, pData, 21, resp, &respLen);

    if(status == 0 && respLen)
    {
        *pHandle = resp[6];
    }

    return status;
}

/*******************************************************************************
 * @fn      GAPADV_loadData
 *
 * @brief   Construct and send GAPADV_loadData command
 *
 * @param   handle - advertisement handle (see GAPADV_create)
 *          type -
 *          pPayload - advertisement payload
 *          len - advertisement length
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAPADV_loadData(uint8_t handle, uint8_t type, uint8_t *pPayload, uint16_t len)
{
    HCI_StatusCodes_t status;
    uint8_t *pData = (uint8_t *)malloc(4+len);

    if(pData)
    {
        pData[0] = handle;
        pData[1] = type;

        pData[2] = (len >> 0) & 0xff;
        pData[3] = (len >> 8) & 0xff;

        memcpy(pData+4, pPayload, len);
        status = HCI_sendExtCommand(GAPADV_LOAD_DATA, pData, 4+len, NULL, NULL);
        free (pData);
    }
    return status;
}


/*******************************************************************************
 * @fn      GAPADV_enable
 *
 * @brief   Construct and send GAPADV_enable command
 *
 * @param   handle - advertisement handle (see GAPADV_create)
 *          enableOptions -
 *          durationOrMaxEvents
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAPADV_enable(uint8_t handle,
                               GapAdv_enableOptions_t enableOptions,
                               uint16_t durationOrMaxEvents)
{
    HCI_StatusCodes_t status;
    uint8_t pData[4];

    pData[0] = handle;
    pData[1] = enableOptions;

    pData[2] = (durationOrMaxEvents >> 0) & 0xff;
    pData[3] = (durationOrMaxEvents >> 8) & 0xff;

    status = HCI_sendHCICommand(GAPADV_ENABLE, pData, 4, NULL, NULL);
    return status;

}


/*******************************************************************************
 * @fn      GAP_SendSlaveSecurityRequest
 *
 * @brief   Construct and send GAP_SendSlaveSecurityRequest command
 *
 * @param   connHandle - connection handle
 *          authReq - Authentication Request
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAP_SendSlaveSecurityRequest(uint16_t connHandle,
                                              uint8_t authReq)
{
    HCI_StatusCodes_t status;
    uint8_t pData[3];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = authReq;

    status = HCI_sendHCICommand(GAP_SENDSLAVESECURITYREQUEST, pData, 3, NULL, NULL);

    return status;
}


/*******************************************************************************
 * @fn      GAP_SetAdvertisement
 *
 * @brief   Construct and send GAP_SetAdvertisement command
 *
 * @param   pHandle[output] - pointer to avertisement handle (allocated by caller)
 *          pDeviceName - Device Name to advertise
 *          txPower - TX Power
 *          minConnInterval - minimal connection interval
 *          maxConnInterval - maximal connection interval
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAP_SetAdvertisement(uint8_t *pHandle, const char *pDeviceName, uint8_t txPower, uint16_t minConnInterval, uint16_t maxConnInterval)
{
    HCI_StatusCodes_t status = bleMemAllocError;
    uint8_t deviceNameLen = strlen(pDeviceName);
    uint8_t pAdvData[] = { 2, ADV_TYPE_FLAGS, 6 };
    uint8_t scanRespLen = 3 /* TxPower */ + 6 /* ConnInterval */ + 2+deviceNameLen;
    uint8_t *pScanResp = (uint8_t*)malloc(scanRespLen);
    if(pScanResp)
    {
        int i = 0;
        pScanResp[i++] = deviceNameLen+1;
        pScanResp[i++] = ADV_TYPE_DEVICE_NAME;
        for(; i<2+deviceNameLen; i++)
        {
            pScanResp[i] = pDeviceName[i-2];
        }
        pScanResp[i++] = 5;
        pScanResp[i++] = ADV_TYPE_CONN_INTERVAL;
        pScanResp[i++] = (minConnInterval >> 0) & 0xff;
        pScanResp[i++] = (minConnInterval >> 8) & 0xff;
        pScanResp[i++] = (maxConnInterval >> 0) & 0xff;
        pScanResp[i++] = (maxConnInterval >> 8) & 0xff;

        pScanResp[i++] = 2;
        pScanResp[i++] = ADV_TYPE_TX_POWER;
        pScanResp[i++] = txPower;

        /* Send GAP scan enable device discovery request */
        HCI_CALL(GAPADV_create, &gapAdv, pHandle);

        /* Set Adevertise Data */
        HCI_CALL(GAPADV_loadData, *pHandle, 0, pAdvData, sizeof(pAdvData));

        /* Set Scan REsponse Data */
        HCI_CALL(GAPADV_loadData, *pHandle, 1, pScanResp, scanRespLen);

    }
    return status;
}

/*******************************************************************************
 * @fn      GAP_EnableConnection
 *
 * @brief   GAP Link established handling
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAP_EnableConnection(uint8_t handle)
{
    HCI_StatusCodes_t status = SUCCESS;
    HCI_CALL(GAPADV_enable, handle, GAP_ADV_ENABLE_OPTIONS_USE_MAX, 0);
    return status;
}

/*******************************************************************************
 * @fn      GAP_SetBondParams
 *
 * @brief   GAP Bond Manager settings
 *
 * @param   bPairingMode -
 *          bMITMProtect -
 *          ioCapabilitys -
 *          bondingEnabled -
 *          keyDist -
 *          passkey -
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAP_SetBondParams(uint8_t bPairingMode, uint8_t bMITMProtect, uint8_t ioCapabilitys,
                                    uint8_t bondingEnabled, uint8_t keyDist, uint32_t passkey)
{
    HCI_StatusCodes_t status = SUCCESS;
    //uint8_t param = 0;

    //HCI_CALL(GAP_SetBondParam, GAPBOND_ERASE_ALLBONDS, 1, &param);
    HCI_CALL(GAP_SetBondParam, GAPBOND_PAIRING_MODE, 1, &bPairingMode);
    HCI_CALL(GAP_SetBondParam, GAPBOND_MITM_PROTECTION, 1, &bMITMProtect);
    HCI_CALL(GAP_SetBondParam, GAPBOND_IO_CAPABILITIES, 1, &ioCapabilitys);
    HCI_CALL(GAP_SetBondParam, GAPBOND_BONDING_ENABLED, 1, &bondingEnabled);
    HCI_CALL(GAP_SetBondParam, GAPBOND_KEY_DIST_LIST, 1, &keyDist);
    HCI_CALL(GAP_SetBondParam, GAPBOND_DEFAULT_PASSCODE, sizeof(passkey), (uint8_t*)&passkey);

#ifdef SEND_SLAVE_SECURITY_REQUEST
    g_bSecureConn = true;
#endif

    return status;
}

/*******************************************************************************
 * @fn      GAP_hndlr_LinkEstablished
 *
 * @brief   GAP Link established handling
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
HCI_StatusCodes_t GAP_hndlr_LinkEstablished(hciRxMsg_t *pEvent)
{
    HCI_StatusCodes_t status = SUCCESS;
    uint16_t connHandle = BUILD_UINT16(&pEvent->pPayload[10]);

    HCI_CALL(HCI_LL_SetDataLength, connHandle, 251, 2120);
#ifdef SEND_SLAVE_SECURITY_REQUEST

    if(g_bSecureConn)
    {
        HCI_CALL(GAP_SendSlaveSecurityRequest, connHandle, GAPBOND_AUTH_REQ_BONDING | GAPBOND_AUTH_REQ_MITM);
    }
#endif
    return status;
}

/*******************************************************************************
 * @fn      GAP_handleEvent
 *
 * @brief   GAP Event Dispatcher
 *
 * @param   pHciEvent - pointer to event message
 *
 * @return  None.
 ******************************************************************************/
void GAP_handleEvent(hciRxMsg_t *pHciEvent)
{
    switch(pHciEvent->eventOpCode)
    {
        case GAP_LINKESTABLISHED:
        {
            LOG_DEBUG("handleEvent: GAP_LINKESTABLISHED");
            GAP_hndlr_LinkEstablished(pHciEvent);
        }
        break;
        case GAP_DEVICEINITDONE:
        {
            /* Event is not handled by this layer */
        }
        break;
        default:
            LOG_WARNING("handleEvent: %d (%x) - no handler", pHciEvent->eventOpCode, pHciEvent->eventOpCode);
    }
}

#if 0 // TBD -  CENTRAL ROLE IS NOT SUPPORTED CURRENTLY
HCI_StatusCodes_t GAP_SetParamValue(uint8_t paramID, uint16_t paramValue)
{
    HCI_StatusCodes_t status;
    uint8_t pData[3];

    pData[0] = paramID;

    pData[1] = LO_UINT16(paramValue);
    pData[2] = HI_UINT16(paramValue);

    status = HCI_sendHCICommand(GAP_SETPARAMVALUE, pData, 3, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_GetParamValue(uint8_t paramID)
{
    HCI_StatusCodes_t status;
    uint8_t pData[1];

    pData[0] = paramID;

    status = HCI_sendHCICommand(GAP_GETPARAMVALUE, pData, 1, NULL, NULL);

    return status;
}

extern HCI_StatusCodes_t GAP_UpdateLinkParamReq(gapUpdateLinkParamReq_t *pParams)
{
    HCI_StatusCodes_t status;
    uint8_t pData[10];

    pData[0] = LO_UINT16(pParams->connHandle);
    pData[1] = HI_UINT16(pParams->connHandle);

    pData[2] = LO_UINT16(pParams->intervalMin);
    pData[3] = HI_UINT16(pParams->intervalMin);

    pData[4] = LO_UINT16(pParams->intervalMax);
    pData[5] = HI_UINT16(pParams->intervalMax);

    pData[6] = LO_UINT16(pParams->connLatency);
    pData[7] = HI_UINT16(pParams->connLatency);

    pData[8] = LO_UINT16(pParams->connTimeout);
    pData[9] = HI_UINT16(pParams->connTimeout);

    status = HCI_sendHCICommand(GAP_UPDATELINKPARAMREQ, pData, 10, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_UpdateLinkParamReqReply(gapUpdateLinkParamReqReply_t *pParams)
{
    HCI_StatusCodes_t status;
    uint8_t pData[12];

    pData[0] = LO_UINT16(pParams->connHandle);
    pData[1] = HI_UINT16(pParams->connHandle);

    pData[2] = LO_UINT16(pParams->intervalMin);
    pData[3] = HI_UINT16(pParams->intervalMin);

    pData[4] = LO_UINT16(pParams->intervalMax);
    pData[5] = HI_UINT16(pParams->intervalMax);

    pData[6] = LO_UINT16(pParams->connLatency);
    pData[7] = HI_UINT16(pParams->connLatency);

    pData[8] = LO_UINT16(pParams->connTimeout);
    pData[9] = HI_UINT16(pParams->connTimeout);

    pData[10] = pParams->signalIdentifier;

    pData[11] = pParams->accepted;

    status = HCI_sendHCICommand(GAP_UPDATELINKPARAMREQREPLY, pData, 12, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_Authenticate(gapAuthParams_t *pParams,
                                  gapPairingReq_t *pPairReq)
{
    HCI_StatusCodes_t status;
    uint16_t opcode = GAP_AUTHENTICATE;
    uint8_t pData[160];
    uint8_t idx = 0;

    pData[idx++] = LO_UINT16(pParams->connHandle);
    pData[idx++] = HI_UINT16(pParams->connHandle);

    pData[idx++] = pParams->secReqs.ioCaps;

    pData[idx++] = pParams->secReqs.oobAvailable;

    memcpy(&pData[idx], &pParams->secReqs.oob, KEYLEN);
    idx+=KEYLEN;

    memcpy(&pData[idx], &pParams->secReqs.oobConfirm, KEYLEN);
    idx+=KEYLEN;

    pData[idx++] = pParams->secReqs.localOobAvailable;

    memcpy(&pData[idx], &pParams->secReqs.localOob, KEYLEN);
    idx+=KEYLEN;

    pData[idx++] = pParams->secReqs.isSCOnlyMode;

    pData[idx++] = pParams->secReqs.eccKeys.isUsed;

    memcpy(&pData[idx], &pParams->secReqs.eccKeys.sK, SM_ECC_KEY_LEN);
    idx+=SM_ECC_KEY_LEN;

    memcpy(&pData[idx], &pParams->secReqs.eccKeys.pK_x, SM_ECC_KEY_LEN);
    idx+=SM_ECC_KEY_LEN;

    memcpy(&pData[idx], &pParams->secReqs.eccKeys.pK_y, SM_ECC_KEY_LEN);
    idx+=SM_ECC_KEY_LEN;

    pData[idx++] = pParams->secReqs.authReq;

    pData[idx++] = pParams->secReqs.maxEncKeySize;

    memcpy(&pData[idx], &pParams->secReqs.keyDist, sizeof(keyDist_t));
    idx+=sizeof(keyDist_t);

    pData[idx++] = pPairReq->enable;

    pData[idx++] = pPairReq->ioCap;

    pData[idx++] = pPairReq->oobDataFlag;

    pData[idx++] = pPairReq->authReq;

    pData[idx++] = pPairReq->maxEncKeySize;

    memcpy(&pData[idx], &pPairReq->keyDist, sizeof(keyDist_t));
    idx+=sizeof(keyDist_t);

    status = HCI_sendHCICommand(opcode, pData, idx, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_TerminateAuth(uint16_t connHandle, uint8_t reason)
{
    HCI_StatusCodes_t status;
    uint8_t pData[3];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = reason;

    status = HCI_sendHCICommand(GAP_TERMINATEAUTH, pData, 3, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_PasskeyUpdate(uint8_t *pPasskey, uint16_t connHandle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[8];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    memcpy(&pData[2], pPasskey, DEFAULT_PASSKEY_SIZE);

    status = HCI_sendHCICommand(GAP_PASSKEYUPDATE, pData, 8, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_Signable(uint16_t connHandle, uint8_t authenticated,
                              smSigningInfo_t *pParams)
{
    HCI_StatusCodes_t status;
    uint8_t pData[23];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = authenticated;

    memcpy(&pData[3], &pParams->srk, KEYLEN);

    memcpy(&pData[19], &pParams->signCounter, sizeof(pParams->signCounter));

    status = HCI_sendHCICommand(GAP_SIGNABLE, pData, 23, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_Bond(uint16_t connHandle, uint8_t authenticated,
                          uint8_t secureConnections, smSecurityInfo_t *pParams,
                          uint8_t startEncryption)
{
    HCI_StatusCodes_t status;
    uint8_t pData[31];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = authenticated;

    pData[3] = secureConnections;

    memcpy(&pData[4], &pParams->ltk, KEYLEN);

    pData[20] = LO_UINT16(pParams->div);
    pData[21] = HI_UINT16(pParams->div);

    memcpy(&pData[22], &pParams->rand, B_RANDOM_NUM_SIZE);

    pData[30] = pParams->keySize;

    status = HCI_sendHCICommand(GAP_BOND, pData, 31, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAP_RegisterConnEvent(GAP_CB_Action_t action,
                                       uint16_t connHandle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[3];

    pData[0] = action;

    pData[1] = LO_UINT16(connHandle);
    pData[2] = HI_UINT16(connHandle);

    status = HCI_sendHCICommand(GAP_REGISTERCONNEVENT, pData, 3, NULL, NULL);

    return status;
}


/******************* GAP INIT API *******************/

HCI_StatusCodes_t GAPINIT_connect(GAP_Peer_Addr_Types_t peerAddrType,
                         uint8_t* pPeerAddress, uint8_t phys, uint16_t timeout)
{
    HCI_StatusCodes_t status;
    uint8_t pData[10];
    uint8_t index;

    pData[0] = peerAddrType;

    for (index = 0; index < DEFAULT_ADDRESS_SIZE; index++)
    {
        pData[1 + index] = *pPeerAddress;
        pPeerAddress++;
    }

    pData[7] = phys;

    pData[8] = LO_UINT16(timeout);
    pData[9] = HI_UINT16(timeout);

    status = HCI_sendHCICommand(GAPINIT_CONNECT, pData, 10, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAPINIT_connectWl(uint8_t phys, uint16_t timeout)
{
    HCI_StatusCodes_t status;
    uint8_t pData[3];

    pData[0] = phys;

    pData[1] = LO_UINT16(timeout);
    pData[2] = HI_UINT16(timeout);

    status = HCI_sendHCICommand(GAPINIT_CONNECTWL, pData, 3, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAPINIT_cancelConnect(void)
{
    HCI_StatusCodes_t status;

    status = HCI_sendHCICommand(GAPINIT_CANCELCONNECT, NULL, 0, NULL, NULL);

    return status;
}


HCI_StatusCodes_t GAPINIT_getPhyParam(uint8_t phy, GapInit_PhyParamId_t paramId)
{
    HCI_StatusCodes_t status;
    uint8_t pData[2];

    pData[0] = phy;

    pData[1] = paramId;

    status = HCI_sendHCICommand(GAPINIT_GETPHYPARAM, pData, 2, NULL, NULL);

    return status;
}


/******************* GAP SCAN API *******************/

HCI_StatusCodes_t GAPSCAN_Enable(uint16_t period, uint16_t duration,
                                 uint8_t maxNumReport)
{
    HCI_StatusCodes_t status;
    uint8_t pData[5];

    pData[0] = LO_UINT16(period);
    pData[1] = HI_UINT16(period);

    pData[2] = LO_UINT16(duration);
    pData[3] = HI_UINT16(duration);

    pData[4] = maxNumReport;

    status = HCI_sendHCICommand(GAPSCAN_ENABLE, pData, 5, NULL, NULL);

    return status;
}


HCI_StatusCodes_t GAPSCAN_Disable(void)
{
    HCI_StatusCodes_t status;

    status = HCI_sendHCICommand(GAPSCAN_DISABLE, NULL, 0, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAPSCAN_setEventMask(GapScan_EventMask_t eventMask)
{
    HCI_StatusCodes_t status;
    uint8_t pData[4];

    pData[0] = BREAK_UINT32(eventMask, 0);
    pData[1] = BREAK_UINT32(eventMask, 1);
    pData[2] = BREAK_UINT32(eventMask, 2);
    pData[3] = BREAK_UINT32(eventMask, 3);

    status = HCI_sendHCICommand(GAPSCAN_SETEVENTMASK, pData, 4, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAPSCAN_getAdvReport(uint8_t rptIdx)
{
    HCI_StatusCodes_t status;
    uint8_t pData[1];

    pData[0] = rptIdx;

    status = HCI_sendHCICommand(GAPSCAN_GETADVREPORT, pData, 1, NULL, NULL);

    return status;
}


HCI_StatusCodes_t GAPSCAN_disable(void)
{
    HCI_StatusCodes_t status;

    status = HCI_sendHCICommand(GAPSCAN_DISABLE, NULL, 0, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAPSCAN_setPhyParams(uint8_t primPhys,
                                          GapScan_ScanType_t type,
                                          uint16_t interval,
                                          uint16_t window)
{
    HCI_StatusCodes_t status;
    uint8_t pData[6];

    pData[0] = primPhys;

    pData[1] = type;

    pData[2] = LO_UINT16(interval);
    pData[3] = HI_UINT16(interval);

    pData[4] = LO_UINT16(window);
    pData[5] = HI_UINT16(window);

    status = HCI_sendHCICommand(GAPSCAN_SETPHYPARAMS, pData, 6, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAPSCAN_getPhyParams(uint8_t primPhy)
{
    HCI_StatusCodes_t status;
    uint8_t pData[1];

    pData[0] = primPhy;

    status = HCI_sendHCICommand(GAPSCAN_GETPHYPARAMS, pData, 1, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GAPSCAN_setParam(GapScan_ParamId_t paramId, void* pValue)
{
    HCI_StatusCodes_t status;

    if (paramId == SCAN_PARAM_FLT_PDU_TYPE || paramId == SCAN_PARAM_RPT_FIELDS)
    {
        uint8_t pData[3];

        pData[0] = paramId;

        pData[1] = LO_UINT16(*(uint16_t*)pValue);
        pData[2] = HI_UINT16(*(uint16_t*)pValue);

        status = HCI_sendHCICommand(GAPSCAN_SETPARAM, pData, 3, NULL, NULL);
    }
    else
    {
        uint8_t pData[2];

        pData[0] = paramId;

        pData[1] = *(uint8_t*)pValue;

        status = HCI_sendHCICommand(GAPSCAN_SETPARAM, pData, 2, NULL, NULL);
    }

    return status;
}

HCI_StatusCodes_t GAPSCAN_getParam(GapScan_ParamId_t paramId)
{
    HCI_StatusCodes_t status;
    uint8_t pData[1];

    pData[0] = paramId;

    status = HCI_sendHCICommand(GAPSCAN_GETPARAM, pData, 1, NULL, NULL);

    return status;
}

/******************* GAP CONFIG API *******************/

HCI_StatusCodes_t GAPCONFIG_SetParameter(Gap_configParamIds_t param,
                                         void *pValue)
{
    HCI_StatusCodes_t status;
    uint8_t i;
    uint8_t pData[17];

    pData[0] = param;

    for(i = 0; i < KEYLEN; i++)
    {
        pData[1 + i] = *((uint8_t*)pValue);
        pValue = (uint8_t*)pValue + sizeof(uint8_t);
    }

    status = HCI_sendHCICommand(GAPCONFIG_SETPARAMETER, pData, 17, NULL, NULL);

    return status;
}


#endif
