 /* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

#ifndef HCI_TL_H
#define HCI_TL_H

/* Standard Includes */
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <semaphore.h>

/* HCI Packet Types */
#define HCI_CMD_PACKET                 0x01
#define HCI_ACL_DATA_PACKET            0x02
#define HCI_SCO_DATA_PACKET            0x03
#define HCI_EVENT_PACKET               0x04


/* HCI port types */
#define HCI_PORT_REMOTE_UART      0x01
#define HCI_PORT_REMOTE_SPI       0x02

/* Task configuration */
#define HCI_RX_TASK_PRIORITY        9
#define HCI_RX_TASK_STACK_SIZE      2048



/* HCI Port structure fields */
typedef struct
{
  uint8_t              boardID;        //!< Board ID for physical port, i.e. Board_UART0
  uint32_t             bitRate;        //!< Baud/Bit Rate for physical port

} HCI_Port_t;


/* HCI Transport Parameters */
typedef struct
{
    uint8_t        portType;
    HCI_Port_t     remote;
} HCI_Params;

/* HCI RX message */
typedef struct
{
    uint8_t     pktType;
    uint8_t     eventId;
    uint8_t     length;
    uint8_t     *pPayload;

    /* paresed per Event ID */
    uint8_t     status;
    uint16_t    eventOpCode;
} hciRxMsg_t;


/* Prototypes for the APIs */
void HCITL_open(HCI_Params *hciParams);
void HCITL_close(void);
void HCITL_init(HCI_Params *hciParams);

int HCITL_receive(hciRxMsg_t *pMsg);
void HCITL_transmit(uint8_t *pPkt, uint8_t length);

#endif /* HCI_TL_H */
