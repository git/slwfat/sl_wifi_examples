 /* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* Standard Includes */
#include <BLE/inc/gap.h>
#include <BLE/inc/hci_tl.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <mqueue.h>
#include <unistd.h>

/* TI-Driver Includes */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* ble5stack Includes */
#include "uart_term.h"

/* HCI Packet information */
#define HCI_READ_PACKET_TYPE_LEN                     0x01
#define HCI_READ_EVENT_CODE_LEN                      0x01
#define HCI_READ_PLD_LEN                             0x01

/* Enable or disable power management */
#define POWER_SAVING 0


/* Handle for HCI UART port */
static UART_Handle uartHCIHandle = NULL;

/* Task configuration */
static pthread_t hciRxTask;

/* BLE package received Message Queue */
static mqd_t hciRxMsgQ;


#if (POWER_SAVING == 1)
/* Indexes for pin configurations in GPIO configuration array */
#define MRDY_PIN      2
#define SRDY_PIN      3

/* Enable MRDY by setting it low */
#define MRDY_ENABLE()     GPIO_write(MRDY_PIN, 0);
/* Disable MRDY by setting it high */
#define MRDY_DISABLE()    GPIO_write(MRDY_PIN, 1);

/* Semaphores to block master until slave is ready for transfer */
sem_t masterSem;
sem_t slaveSem;

/* Is transaction initiated by master device */
bool masterTransaction = false;

bool rxActive = false;
bool txActive = false;

/* Callback function for the GPIO interrupt SRDY_PIN */
void slaveReadyFxn(uint_least8_t index);
#endif

/*******************************************************************************
 *                         LOCAL FUNCTIONS
 ******************************************************************************/
static void *RxTaskFxn(void *arg0);
extern pthread_t TaskCreate(int prio, size_t stacksize, void* (*fTask)(void*), void *arg);


/*******************************************************************************
 *                        PUBLIC FUNCTIONS
 ******************************************************************************/


/*******************************************************************************
 * @fn      HCI_init
 *
 * @brief   Initialize the HCI transport layer
 *
 * @param[in]  hciParams - Struct containing parameters to initialize the
 *              HCI transport layer
 *
 * @return : None
 ******************************************************************************/
void HCITL_init(HCI_Params *hciParams)
{
    mq_attr attr;
    UART_Params uartParams;

    // initializing the message queue for the MQTT module
    attr.mq_maxmsg = 4;
    attr.mq_msgsize = sizeof(hciRxMsg_t);
    hciRxMsgQ = mq_open("hcitlRX", O_CREAT, 0, &attr);
    while(hciRxMsgQ < 0);

    /* Close UART if already open*/
    if (uartHCIHandle != NULL)
     {
         UART_close(uartHCIHandle);
         uartHCIHandle = NULL;
     }

     if ((hciParams->portType == HCI_PORT_REMOTE_UART)
            && (uartHCIHandle == NULL))
    {
        /* Create a UART with data processing off. */
        UART_Params_init(&uartParams);
        uartParams.writeDataMode = UART_DATA_BINARY;
        uartParams.readDataMode = UART_DATA_BINARY;
        uartParams.readReturnMode = UART_RETURN_FULL;
        uartParams.readMode = UART_MODE_BLOCKING;
        uartParams.writeMode = UART_MODE_BLOCKING;
        uartParams.readEcho = UART_ECHO_OFF;
        uartParams.baudRate = 115200;

        uartHCIHandle = UART_open(hciParams->remote.boardID, &uartParams);
    }

#if (POWER_SAVING == 1)
    /* Initialize the semaphores used to handle the timing between master
     * and slave device communication.
     */
    sem_init(&masterSem, 1, 0);
    sem_init(&slaveSem, 1, 0);

    /* Below we set Board_MRDY & Board_SRDY initial conditions for the 'handshake'.
    * MRDY and SRDY are ACTIVE LOW signals. Then configure the interrupt on Board_SRDY
    */
    GPIO_setConfig(MRDY_PIN, GPIO_CFG_OUTPUT | GPIO_CFG_OUT_HIGH);
    GPIO_setConfig(SRDY_PIN, GPIO_CFG_INPUT);
    GPIO_setConfig(SRDY_PIN, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
    GPIO_setCallback(SRDY_PIN, slaveReadyFxn);
    GPIO_enableInt(SRDY_PIN);

#endif

    /* Open the HCI Task */
    hciRxTask = TaskCreate(HCI_RX_TASK_PRIORITY, HCI_RX_TASK_STACK_SIZE, RxTaskFxn, NULL);
    while(hciRxTask == 0)
        ;

}




/*******************************************************************************
 * @fn      HCI_transportHCIPacket
 *
 * @brief   Transport the HCI packet over the transport layer
 *          If power management is enabled, then the application will start
 *          the handshake sequence to ensure the slave device is ready to
 *          receive data.
 *
 * @param[in]   pPkt - The HCI packet to transport
 * @param[in]   dataLength - The length of the payload data to transport
 *
 * @return  None
 ******************************************************************************/
void HCITL_transmit(uint8_t *pPkt, uint8_t length)
{
#if (POWER_SAVING == 1)
    masterTransaction = true;

    /* Wait until slave device is ready to receive data, and all other RX and
     * TX actions are complete */
    while ((GPIO_read(SRDY_PIN) == 0) || rxActive || txActive) {}

    txActive = true;

    /* Master device is ready to transfer. Wait for slave device to be ready */
    MRDY_ENABLE();
    sem_wait(&masterSem);

    /* Slave device is ready. Start UART write */
    UART_write(uartHCIHandle, pPkt, HCI_FRAME_SIZE);
    UART_write(uartHCIHandle, pPkt->pData, dataLength);

    /* Done transmitting. Disable master device */
    masterTransaction = false;
    txActive = false;
    MRDY_DISABLE();

#else
    UART_write(uartHCIHandle, pPkt, length);
#endif
}


/*******************************************************************************
 * @fn      HCI_waitForEvent
 *
 * @brief   Wait to receive an HCI event over the transport layer
 *
 * @param   None
 *
 * @return  None
 ******************************************************************************/
int HCITL_receive(hciRxMsg_t *pMsg)
{
    unsigned int prio;
    int size = mq_receive(hciRxMsgQ, (char*)pMsg, sizeof(hciRxMsg_t) , &prio);
    while(size != sizeof(hciRxMsg_t));
    return 0;
}

/*******************************************************************************
 * @fn      HCIUART_taskFxn
 *
 * @brief   Task that handles receiving the HCI packets over the UART
 *          transport layer. If power management is enabled, then the
 *          task will pend on a semaphore until the slave device indicates
 *          it is ready to transport data. If power management is disabled,
 *          then the task blocks on a UART_read.
 *
 * @param   arg0 not used.
 *
 * @return  None.
 ******************************************************************************/
static void *RxTaskFxn(void *arg0)
{
    hciRxMsg_t msg;

    while (1)
    {
#if (POWER_SAVING == 1)

        /* Wait until slave device is ready to transmit data, then enable master
         * device to receive */
        if (!rxActive)
        {
            sem_wait(&slaveSem);
            rxActive = true;
            MRDY_ENABLE();
        }
#endif

        /* Read HCI header */
        do {
            UART_read(uartHCIHandle, &msg.pktType, 1);
        } while(msg.pktType != HCI_EVENT_PACKET);
        UART_read(uartHCIHandle, &msg.eventId, 2);

	/* Read HCI Payload */
        msg.pPayload = malloc(msg.length);
        while(msg.pPayload == 0)
            ;
        UART_read(uartHCIHandle, msg.pPayload, msg.length);

#if (POWER_SAVING == 1)
        MRDY_DISABLE();
        rxActive = false;
#endif
       mq_send(hciRxMsgQ, (char*)&msg, sizeof(hciRxMsg_t) , 1);
    }
}



#if (POWER_SAVING == 1)
/*******************************************************************************
 * @fn      slaveReadyFxn
 *
 * @brief   Callback function for the GPIO interrupt SRDY_PIN
 *
 * @param   None
 *
 * @return  None
 ******************************************************************************/
void slaveReadyFxn(uint_least8_t index)
{
    if (masterTransaction)
    {
        sem_post(&masterSem);

    }
    else
    {
        if (!rxActive && !txActive)
        {
            sem_post(&slaveSem);
        }
    }
}
#endif // POWER_SAVING = 1
