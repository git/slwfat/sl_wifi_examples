 /* --COPYRIGHT--,BSD
 * Copyright (c) 2016-2021, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/* Standard Includes */
#include <BLE/inc/att.h>
#include <BLE/inc/gatt.h>
#include <BLE/inc/gatt_uuid.h>
#include <BLE/inc/hal_defs.h>
#include <BLE/inc/hci.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "debug_if.h"

#undef DEBUG_IF_NAME
#undef DEBUG_IF_SEVERITY
#define DEBUG_IF_NAME       "GATT"
#define DEBUG_IF_SEVERITY   E_INFO

#define MAX_GATT_SERVICES   8
int g_gattServiceNum = 0;




/*******************************************************************************
 * @fn      GATT_Indication
 *
 * @brief   construct and send the GATTIndication message
 *
 * @param   connHandle - connHandle
 *          pInd - indication parameters
 *          authenticated -
 *
 * @return  status of message sent
 ******************************************************************************/
HCI_StatusCodes_t GATT_Indication( uint16_t connHandle, attHandleValueInd_t *pInd,
                                  uint8_t authenticated)
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + sizeof(authenticated) + sizeof(pInd->handle) + pInd->len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = authenticated;

        pData[3] = LO_UINT16(pInd->handle);
        pData[4] = HI_UINT16(pInd->handle);

        memcpy(&pData[5], pInd->pValue, pInd->len);
    }

    status = HCI_sendHCICommand(GATT_INDICATION, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

/*******************************************************************************
 * @fn      GATT_Notification
 *
 * @brief   construct and send the GATT Notification message
 *
 * @param   connHandle - connHandle
 *          pNoti - notification parameters
 *          authenticated -
 *
 * @return  status of message sent
 ******************************************************************************/
HCI_StatusCodes_t GATT_Notification( uint16_t connHandle, attHandleValueNoti_t *pNoti,
                                    uint8_t authenticated )
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = 5 + pNoti->len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = authenticated;

        pData[3] = LO_UINT16(pNoti->handle);
        pData[4] = HI_UINT16(pNoti->handle);

        memcpy(&pData[5], pNoti->pValue, pNoti->len);
    }

    status = HCI_sendHCICommand(GATT_NOTIFICATION, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}



/*******************************************************************************
 * @fn      GATT_AddService
 *
 * @brief   construct and send the Add Service message
 *
 * @param   typeUUID - GATT_PRIMARY_SERVICE_UUID / GATT_SECONDARY_SERVICE_UUID
 *          pService - service parameters (defined at the application level)
 *          encKeySize - encrypted key size
 *
 * @return  status of message sent
 ******************************************************************************/
static HCI_StatusCodes_t GATT_AddService( uint16_t typeUUID, serviceParams_t *pService, uint8_t encKeySize )
{
    HCI_StatusCodes_t status = FAILURE;
    uint8_t pData[5];

    if(g_gattServiceNum < MAX_GATT_SERVICES)
    {
        uint16_t handle;
        /* Update local DB */
        handle = ATT_SetAttribute(0xff, typeUUID, GATT_PERMIT_READ, pService);
        if(handle != INVALID_HANDLE)
        {
            g_gattServiceNum ++;

            /* Prepare ADD SERVICE message */
            pData[0] = LO_UINT16(typeUUID);
            pData[1] = HI_UINT16(typeUUID);

            pData[2] = LO_UINT16(pService->attrNum+1);
            pData[3] = HI_UINT16(pService->attrNum+1);

            pData[4] = encKeySize;

            status = HCI_sendHCICommand(GATT_ADDSERVICE, pData, 5, NULL, NULL);

            pService->handle = handle;
        }
    }

    return status;
}

/*******************************************************************************
 * @fn      GATT_DelService
 *
 * @brief   construct and send the Delete Service message
 *
 * @param   handle - handle of service to be deleted
 *          pService - service parameters (defined at the application level)
 *
 * @return  status of message sent
 ******************************************************************************/
HCI_StatusCodes_t GATT_DelService(uint16_t handle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[2];

    pData[0] = LO_UINT16(handle);
    pData[1] = HI_UINT16(handle);

    status = HCI_sendHCICommand(GATT_DELSERVICE, pData, 2, NULL, NULL);

    return status;
}

/*******************************************************************************
 * @fn      GATT_AddAttribute
 *
 * @brief   internal function - called by the GATT_AddPrimaryService to add
 *          attributes to the GATT DB
 *
 * @param   id - 0xff for chracter uuid and descriptor attribute or
 *               ref index of characteristics within the service (starting from 0)
 *          uuid - of the attribute
 *          perm - access permission
 *          pChar - characteristic structure (defined by the user as array within
 *                the service)
 *
 * @return  status (HCI_StatusCodes_t)
 ******************************************************************************/
static HCI_StatusCodes_t GATT_AddAttribute(uint16_t id, uint16_t uuid, uint8_t perm,
                                    charParams_t *pChar )
{
    HCI_StatusCodes_t status = FAILURE;
    uint8_t pData[3];
    uint16_t handle;

    /* Update local DB */
    handle = ATT_SetAttribute(id, uuid, perm, pChar);

    if(handle != INVALID_HANDLE)
    {
        pData[0] = LO_UINT16(uuid);
        pData[1] = HI_UINT16(uuid);
        pData[2] = perm;
        status = HCI_sendHCICommand(GATT_ADDATTRIBUTE, pData, 3, NULL, NULL);
        if(id != 0xff)
            pChar->handle = handle;
    }

    return status;
}

/*******************************************************************************
 * @fn      GATT_AddPrimaryService
 *
 * @brief   provided the serviceParams_t from the application, this method will
 *          create the internal GATT DB (service + characteristics)
 *
 *          pService - service parameters (defined at the application level)
 *
 * @return  status (HCI_StatusCodes_t)
 ******************************************************************************/
HCI_StatusCodes_t GATT_AddPrimaryService(serviceParams_t *pService)
{
    int i;
    HCI_StatusCodes_t status;
    pService->attrNum = 0;
    for(i=0; i<pService->charsNum; i++)
    {
        pService->attrNum += (pService->pChars[i].pDesc)?3:2;
    }
    HCI_CALL(GATT_AddService, GATT_PRIMARY_SERVICE_UUID, pService, 16);

    for(i=0; i<pService->charsNum; i++)
    {
        charParams_t *pChar = &pService->pChars[i];
        pChar->Servicehandle = pService->handle;
        HCI_CALL(GATT_AddAttribute, 0xff, GATT_CHARACTER_UUID, GATT_PERMIT_READ, pChar);

        HCI_CALL(GATT_AddAttribute, i, pChar->uuid, pChar->perm, pChar);

        if(pChar->pDesc)
        {
            HCI_CALL(GATT_AddAttribute, 0xff, GATT_CHAR_USER_DESC_UUID, GATT_PERMIT_READ, pChar);
        }
    }
    return status;
}



#if 0 // APIs not supported
/*******************************************************************************
 * @fn      GATT_UpdateMTU
 *
 * @brief   construct and send the Update MTU message
 *
 * @param   connHandle - connection handle
 *          mtuSize - supported MTU size
 *
 * @return  status of message sent
 ******************************************************************************/
HCI_StatusCodes_t GATT_UpdateMTU(uint16_t connHandle, uint16_t mtuSize)
{
    HCI_StatusCodes_t status;
    uint8_t pData[4];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(mtuSize);
    pData[3] = HI_UINT16(mtuSize);

    status = HCI_sendHCICommand(GATT_UPDATEMTU, pData, 4, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_ReadCharDesc(uint16_t connHandle, attReadReq_t *pReq)
{
    HCI_StatusCodes_t status;
    uint8_t pData[4];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(pReq->handle);
    pData[3] = HI_UINT16(pReq->handle);

    status = HCI_sendHCICommand(GATT_READCHARDESC, pData, 4, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_ReadLongCharDesc(uint16_t connHandle,
                                       attReadBlobReq_t *pReq)
{
    HCI_StatusCodes_t status;
    uint8_t pData[6];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(pReq->handle);
    pData[3] = HI_UINT16(pReq->handle);

    pData[4] = LO_UINT16(pReq->offset);
    pData[5] = HI_UINT16(pReq->offset);

    status = HCI_sendHCICommand(GATT_READLONGCHARDESC, pData, 6, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_WriteCharDesc( uint16_t connHandle, attWriteReq_t *pReq)
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + sizeof(pReq->handle) + pReq->len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = LO_UINT16(pReq->handle);
        pData[3] = HI_UINT16(pReq->handle);

        memcpy(&pData[4], pReq->pValue, pReq->len);
    }

    status = HCI_sendHCICommand(GATT_WRITECHARDESC, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_WriteLongCharDesc( uint16_t connHandle, attPrepareWriteReq_t *pReq)
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + sizeof(pReq->handle) + sizeof(pReq->offset) + pReq->len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = LO_UINT16(pReq->handle);
        pData[3] = HI_UINT16(pReq->handle);

        pData[4] = LO_UINT16(pReq->offset);
        pData[5] = HI_UINT16(pReq->offset);

        memcpy(&pData[6], pReq->pValue, pReq->len);
    }

    status = HCI_sendHCICommand(GATT_WRITELONGCHARDESC, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_DiscAllPrimaryServices(uint16_t connHandle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[2];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    status = HCI_sendHCICommand(GATT_DISCALLPRIMARYSERVICES, pData, 2, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_DiscPrimaryServiceByUUID( uint16_t connHandle, uint8_t *pUUID, uint8_t len)
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        memcpy(&pData[2], pUUID, len);
    }

    status = HCI_sendHCICommand(GATT_DISCPRIMARYSERVICEBYUUID, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}


HCI_StatusCodes_t GATT_ReadCharValue(uint16_t connHandle, attReadReq_t *pReq)
{
    HCI_StatusCodes_t status;
    uint8_t pData[4];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(pReq->handle);
    pData[3] = HI_UINT16(pReq->handle);

    status = HCI_sendHCICommand(GATT_READCHARVALUE, pData, 4, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_FindIncludedServices(uint16_t connHandle,
                                           uint16_t startHandle,
                                           uint16_t endHandle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[6];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(startHandle);
    pData[3] = HI_UINT16(startHandle);

    pData[4] = LO_UINT16(endHandle);
    pData[5] = HI_UINT16(endHandle);

    status = HCI_sendHCICommand(GATT_FINDINCLUDEDSERVICES, pData, 6, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_DiscAllChars(uint16_t connHandle, uint16_t startHandle,
                                   uint16_t endHandle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[6];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(startHandle);
    pData[3] = HI_UINT16(startHandle);

    pData[4] = LO_UINT16(endHandle);
    pData[5] = HI_UINT16(endHandle);

    status = HCI_sendHCICommand(GATT_DISCALLCHARS, pData, 6, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_DiscCharsByUUID(uint16_t connHandle,
                                      attReadByTypeReq_t *pReq)
{
    uint8_t dataLength, index;
    uint8_t *pData;
    HCI_StatusCodes_t status;

    dataLength = sizeof(connHandle) + sizeof(pReq->startHandle)
            + sizeof(pReq->endHandle) + pReq->type.len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = LO_UINT16(pReq->startHandle);
        pData[3] = HI_UINT16(pReq->startHandle);

        pData[4] = LO_UINT16(pReq->endHandle);
        pData[5] = HI_UINT16(pReq->endHandle);

        for(index = 0; index < pReq->type.len; index++)
        {
            pData[6 + index] = pReq->type.uuid[(pReq->type.len - 1 - index)];
        }
    }

    status = HCI_sendHCICommand(GATT_DISCCHARSBYUUID, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_DiscAllCharDescs(uint16_t connHandle,
                                       uint16_t startHandle, uint16_t endHandle)
{
    HCI_StatusCodes_t status;
    uint8_t pData[6];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(startHandle);
    pData[3] = HI_UINT16(startHandle);

    pData[4] = LO_UINT16(endHandle);
    pData[5] = HI_UINT16(endHandle);

    status = HCI_sendHCICommand(GATT_DISCALLCHARDESCS, pData, 6, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_ReadUsingCharUUID( uint16_t connHandle, attReadByTypeReq_t *pReq)
{
    uint8_t dataLength, index;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + sizeof(pReq->startHandle) + sizeof(pReq->endHandle) + pReq->type.len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = LO_UINT16(pReq->startHandle);
        pData[3] = HI_UINT16(pReq->startHandle);

        pData[4] = LO_UINT16(pReq->endHandle);
        pData[5] = HI_UINT16(pReq->endHandle);

        for(index = 0; index < pReq->type.len; index++)
        {
            pData[6 + index] = pReq->type.uuid[(pReq->type.len - 1 - index)];
        }
    }

    status = HCI_sendHCICommand(GATT_READUSINGCHARUUID, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_ReadLongCharValue(uint16_t connHandle,
                                        attReadBlobReq_t *pReq)
{
    HCI_StatusCodes_t status;
    uint8_t pData[6];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(pReq->handle);
    pData[3] = HI_UINT16(pReq->handle);

    pData[4] = LO_UINT16(pReq->offset);
    pData[5] = HI_UINT16(pReq->offset);

    status = HCI_sendHCICommand(GATT_READLONGCHARVALUE, pData, 6, NULL, NULL);

    return status;
}

HCI_StatusCodes_t GATT_ReadMultiCharValues( uint16_t connHandle, attReadMultiReq_t *pReq)
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + (pReq->numHandles * 2); // number of handles * two-byte handle sizes
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        memcpy(&pData[2], pReq->pHandles, (pReq->numHandles * 2));
    }

    status = HCI_sendHCICommand(GATT_READMULTILCHARVALUES, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_WriteNoRsp( uint16_t connHandle, attWriteReq_t *pReq )
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + sizeof(pReq->handle) + pReq->len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = LO_UINT16(pReq->handle);
        pData[3] = HI_UINT16(pReq->handle);

        memcpy(&pData[4], pReq->pValue, pReq->len);
    }

    status = HCI_sendHCICommand(GATT_WRITENORSP, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_SignedWriteNoRsp( uint16_t connHandle, attWriteReq_t *pReq )
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + sizeof(pReq->handle) + pReq->len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = LO_UINT16(pReq->handle);
        pData[3] = HI_UINT16(pReq->handle);

        memcpy(&pData[4], pReq->pValue, pReq->len);
    }

    status = HCI_sendHCICommand(GATT_SIGNEDWRITENORSP, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_WriteLongCharValue( uint16_t connHandle, attPrepareWriteReq_t *pReq)
{
    uint8_t dataLength;
    HCI_StatusCodes_t status;
    uint8_t *pData;

    dataLength = sizeof(connHandle) + sizeof(pReq->handle) + sizeof(pReq->offset) + pReq->len;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(connHandle);
        pData[1] = HI_UINT16(connHandle);

        pData[2] = LO_UINT16(pReq->handle);
        pData[3] = HI_UINT16(pReq->handle);

        pData[4] = LO_UINT16(pReq->offset);
        pData[5] = HI_UINT16(pReq->offset);

        memcpy(&pData[6], pReq->pValue, pReq->len);
    }

    status = HCI_sendHCICommand(GATT_WRITELONGCHARVALUE, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_WriteCharValue(GattWriteCharValue_t *para)
{
    uint8_t dataLength;
    uint8_t *pData;
    HCI_StatusCodes_t status;

    dataLength = sizeof(para->connHandle) + sizeof(para->handle) + para->dataSize;
    pData = (uint8_t *) malloc(dataLength);
    if (pData == NULL)
    {
        free(pData);
        return bleMemAllocError;
    }
    else
    {
        pData[0] = LO_UINT16(para->connHandle);
        pData[1] = HI_UINT16(para->connHandle);

        pData[2] = LO_UINT16(para->handle);
        pData[3] = HI_UINT16(para->handle);

        memcpy(&pData[4], para->value, para->dataSize);
    }

    status = HCI_sendHCICommand(GATT_WRITECHARVALUE, pData, dataLength, NULL, NULL);

    free(pData);

    return status;
}

HCI_StatusCodes_t GATT_ExchangeMTU(uint16_t connHandle, attExchangeMTUReq_t *pReq)
{
    HCI_StatusCodes_t status;
    uint8_t pData[4];

    pData[0] = LO_UINT16(connHandle);
    pData[1] = HI_UINT16(connHandle);

    pData[2] = LO_UINT16(pReq->clientRxMTU);
    pData[3] = HI_UINT16(pReq->clientRxMTU);

    status = HCI_sendHCICommand(GATT_EXCHANGEMTU, pData, 4, NULL, NULL);

    return status;
}

#endif



