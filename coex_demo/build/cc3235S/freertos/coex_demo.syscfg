/*
 * Copyright (c) 2018-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  coex_demo.syscfg
 *
 * These arguments were used when this file was generated. They will be automatically applied on subsequent loads
 * via the GUI or CLI. Run CLI with '--help' for additional information on how to override these arguments.
 * @cliArgs --device "CC3235S" --package "Default" --part "Default" --product "simplelink_cc32xx_sdk@5.10.00.02"
 */

/* ======== RTOS ======== */
var RTOS = scripting.addModule("/ti/drivers/RTOS");
RTOS.name = "FreeRTOS";
const GPIO           = scripting.addModule("/ti/drivers/GPIO");
const GPIO1          = GPIO.addInstance();
const GPIO2          = GPIO.addInstance();
const GPIO3          = GPIO.addInstance();
const GPIO4          = GPIO.addInstance();
const GPIO5          = GPIO.addInstance();
const GPIO6          = GPIO.addInstance();
const SPI            = scripting.addModule("/ti/drivers/SPI");
const UART           = scripting.addModule("/ti/drivers/UART");
const UART1          = UART.addInstance();
const UART2          = UART.addInstance();
const SimpleLinkWifi = scripting.addModule("/ti/drivers/net/wifi/SimpleLinkWifi");
const net_utils      = scripting.addModule("/ti/drivers/net/wifi/net_utils", {}, false);
const net_utils1     = net_utils.addInstance();
const SlNet          = scripting.addModule("/ti/net/SlNet");
const SlNet1         = SlNet.addInstance();
const SlNetConn      = scripting.addModule("/ti/net/SlNetConn");

/**
 * Write custom configuration values to the imported modules.
 */
GPIO1.mode            = "Output";
GPIO1.$name           = "CONFIG_GPIO_LED_0";
GPIO1.gpioPin.$assign = "GP09";

GPIO2.$name           = "CONFIG_GPIO_BLERESET";
GPIO2.gpioPin.$assign = "GP06";

GPIO3.mode            = "Output";
GPIO3.$name           = "CONFIG_GPIO_LED_1";
GPIO3.gpioPin.$assign = "GP11";

GPIO4.mode            = "Output";
GPIO4.$name           = "CONFIG_GPIO_LED_2";
GPIO4.gpioPin.$assign = "GP10";

GPIO5.interruptTrigger = "Rising Edge";
GPIO5.$name            = "CONFIG_GPIO_BUTTON_0";
GPIO5.gpioPin.$assign  = "GP14";

GPIO6.interruptTrigger = "Rising Edge";
GPIO6.$name            = "CONFIG_GPIO_BUTTON_1";
GPIO6.gpioPin.$assign  = "GP15";

const Power          = scripting.addModule("/ti/drivers/Power", {}, false);
Power.parkPins.$name = "ti_drivers_power_PowerCC32XXPins0";

UART1.useDMA                    = true;
UART1.$name                     = "CONFIG_UART_1";
UART1.uart.$assign              = "UART0";
UART1.uart.txPin.$assign        = "GP12";
UART1.uart.rxPin.$assign        = "GP13";
UART1.uart.txDmaChannel.$assign = "UDMA_CH9";
UART1.uart.rxDmaChannel.$assign = "UDMA_CH8";

UART2.useDMA                    = true;
UART2.interruptPriority         = "4";
UART2.$name                     = "CONFIG_UART_0";
UART2.uart.$assign              = "UART1";
UART2.uart.txPin.$assign        = "GP01";
UART2.uart.rxPin.$assign        = "GP02";
UART2.uart.txDmaChannel.$assign = "UDMA_CH11";
UART2.uart.rxDmaChannel.$assign = "UDMA_CH10";

net_utils1.$name = "CONFIG_NET_UTILS_0";

SlNet1.$name = "CONFIG_SLNET_0";
