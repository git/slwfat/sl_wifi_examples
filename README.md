# SL-WIFI-Examples #
This Repository contains various example applications that are using the simplelink_cc32xx_sdk. All the examples are based on the existence of the CC32xx SDK.
Please refer to the examples' internal README files to get specific instructions as well as version limitations to enable the example.

# Version #
1.0.0

# Content #

1. **WiFI-BLE Coexistence Demo**<br>
	Location: coex_demo/ <br>
	Device(s): CC323x + CC13x2/CC26x2 <br>
	Version: 01.00.00.02
2. **CC32XX Video Streaming Example**<br>
	Location: oa7000_demo/<br>
	Device(s): CC32xx (+ oa7000 board) <br>
	Version: 1.0
3. **MQTT Client with OTA (Over-The-Air) Uptate Support**<br>
	Location: mqtt_client_w_ota/<br>
	Device(s): CC32xx <br>
	Version: 02.00.01
4. **CC32XX Memfault Cli Example**<br>
	Location: memfault/<br>
	Device(s): CC32xx <br>
	Version: 1.0