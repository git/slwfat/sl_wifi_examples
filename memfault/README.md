# MEMFAULT CLI Example #
The TI SimpleLink CC32xx add-on provides end users an example of memfault integration.

# Version #
1.0.0

# Supported Features #
1. Keep alive metrics (every 1 hour - which is the default)
	* reports the RSSI of the corrent BSSID
1. Coredump on crash/assert
	* Can be triggered using the mflt CLI by typing: crash

# Installation Guide #
**Prerequisites:**
   1. CCS 11.x+
   2. CC32xx SDKv7.10
   3. FreeRTOS sources (version listed in the CC32xx SDK)
   4. Python 3.6+
   5. Git for Windows
   6. memfault account

**Installation Instructions:**
   1. Install the pyelftools python package: pip install pyelftools
   2. Import Project
      1. Launch CCS
      2. Go to File→Import CCS Project
      3. Browse the memfault directory
      4. Right-Click on the freertos_build project and select linked resources
         1. Add the FREERTOS_INSTALL_DIR variable with the FREERTOS source path
      5. Open the sl_memfault_config.h file
         1. Fill in the correct WLAN parameters
      6. Fill in your Memfault project key (Memfault cloud)
      7. Build project
      8. Program