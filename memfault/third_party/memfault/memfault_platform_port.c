//! @file
//!
//! Copyright 2022 Memfault, Inc
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//!     http://www.apache.org/licenses/LICENSE-2.0
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.
//!
//! Glue layer between the Memfault SDK and the underlying platform
//!
//! TODO: Fill in FIXMEs below for your platform

/*
Copyright (c) 2022 Texas  Instruments Incorporated
*/


#include "memfault/components.h"
#include "memfault/ports/reboot_reason.h"
#include "memfault/ports/freertos.h"
#include "memfault/core/platform/core.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include "uart_term.h"
#include "ti/devices/cc32xx/inc/hw_types.h"
#include "ti/devices/cc32xx/driverlib/prcm.h"
#include "ti/drivers/power/PowerCC32XX.h"
#include <ti/devices/cc32xx/driverlib/rom_map.h>
#include <ti/devices/cc32xx/driverlib/interrupt.h>
#include <ti/drivers/net/wifi/simplelink.h>

MEMFAULT_PUT_IN_SECTION(".noinit.mflt_reboot_tracking")
static uint8_t s_reboot_tracking[MEMFAULT_REBOOT_TRACKING_REGION_SIZE];

void memfault_platform_reboot_tracking_boot(void) {
  sResetBootupInfo reset_info = { 0 };
  memfault_reboot_reason_get(&reset_info);
  memfault_reboot_tracking_boot(s_reboot_tracking, &reset_info);
}

void memfault_platform_get_device_info(sMemfaultDeviceInfo *info) {
  // !FIXME: Populate with platform device information

  // IMPORTANT: All strings returned in info must be constant
  // or static as they will be used _after_ the function returns

  // See https://mflt.io/version-nomenclature for more context
  *info = (sMemfaultDeviceInfo) {
    // An ID that uniquely identifies the device in your fleet
    // (i.e serial number, mac addr, chip id, etc)
    // Regular expression defining valid device serials: ^[-a-zA-Z0-9_]+$
    .device_serial = "DEMOSERIAL",
     // A name to represent the firmware running on the MCU.
    // (i.e "ble-fw", "main-fw", or a codename for your project)
    .software_type = "app-fw",
    // The version of the "software_type" currently running.
    // "software_type" + "software_version" must uniquely represent
    // a single binary
    .software_version = "1.0.0",
    // The revision of hardware for the device. This value must remain
    // the same for a unique device.
    // (i.e evt, dvt, pvt, or rev1, rev2, etc)
    // Regular expression defining valid hardware versions: ^[-a-zA-Z0-9_\.\+]+$
    .hardware_version = "dvt1",
  };
}


extern void prv_memfault_reboot();
extern void powerShutdown(unsigned long shutdownTime);

//! Last function called after a coredump is saved. Should perform
//! any final cleanup and then reset the device
void memfault_platform_reboot(void) {
  // !FIXME: Perform any final system cleanup here

  // !FIXME: Reset System
    prv_memfault_reboot();
    //Power_shutdown(0, PowerCC32XX_TOTALTIMESHUTDOWN);
    //void powerShutdown(uint32_t shutdownTime);
    powerShutdown(1000);
  while (1) { } // unreachable
}

bool memfault_platform_time_get_current(sMemfaultCurrentTime *time) {
  // !FIXME: If the device tracks real time, update 'unix_timestamp_secs' with seconds since epoch
  // This will cause events logged by the SDK to be timestamped on the device rather than when they
  // arrive on the server
  *time = (sMemfaultCurrentTime) {
    .type = kMemfaultCurrentTimeType_UnixEpochTimeSec,
    .info = {
      .unix_timestamp_secs = 0
    },
  };

  // !FIXME: If device does not track time, return false, else return true if time is valid
  return false;
}

size_t memfault_platform_sanitize_address_range(void *start_addr, size_t desired_size) {
  static const struct {
    uint32_t start_addr;
    size_t length;
  } s_mcu_mem_regions[] = {
    // !FIXME: Update with list of valid memory banks to collect in a coredump
    {.start_addr = 0x00000000, .length = 0xFFFFFFFF},
  };

  for (size_t i = 0; i < MEMFAULT_ARRAY_SIZE(s_mcu_mem_regions); i++) {
    const uint32_t lower_addr = s_mcu_mem_regions[i].start_addr;
    const uint32_t upper_addr = lower_addr + s_mcu_mem_regions[i].length;
    if ((uint32_t)start_addr >= lower_addr && ((uint32_t)start_addr < upper_addr)) {
      return MEMFAULT_MIN(desired_size, upper_addr - (uint32_t)start_addr);
    }
  }

  return 0;
}

//! !FIXME: This function _must_ be called by your main() routine prior
//! to starting an RTOS or baremetal loop.
int memfault_platform_boot(void) {
    // Install Memfault Fault Handlers in Vector Table for Exceptions
    MAP_IntRegister(3, HardFault_Handler);
    MAP_IntRegister(4, MemoryManagement_Handler);
    MAP_IntRegister(5, BusFault_Handler);
    MAP_IntRegister(6, UsageFault_Handler);

  // !FIXME: Add init to any platform specific ports here.
  // (This will be done in later steps in the getting started Guide)
  memfault_freertos_port_boot();

  memfault_build_info_dump();
  memfault_device_info_dump();
  memfault_platform_reboot_tracking_boot();

  static uint8_t s_event_storage[1024];
  const sMemfaultEventStorageImpl *evt_storage =
      memfault_events_storage_boot(s_event_storage, sizeof(s_event_storage));
  memfault_trace_event_boot(evt_storage);

  memfault_reboot_tracking_collect_reset_info(evt_storage);

  sMemfaultMetricBootInfo boot_info = {
    .unexpected_reboot_count = memfault_reboot_tracking_get_crash_count(),
  };
  memfault_metrics_boot(evt_storage, &boot_info);

  MEMFAULT_LOG_INFO("Memfault Initialized!");

  return 0;
}

void memfault_reboot_reason_get(sResetBootupInfo *info) {
  const uint32_t reset_cause = MAP_PRCMSysResetCauseGet();
  eMemfaultRebootReason reset_reason = kMfltRebootReason_Unknown;

  switch (reset_cause) {
    case PRCM_POWER_ON:
      reset_reason = kMfltRebootReason_PowerOnReset;
      break;

    case PRCM_LPDS_EXIT:
      reset_reason = kMfltRebootReason_LowPower;
      break;

    //case PRCM_WDT_RESET:
    //  reset_reason = kMfltRebootReason_Watchdog;
    //  break;

    case PRCM_CORE_RESET:
    case PRCM_MCU_RESET:
    case PRCM_HIB_EXIT:
      reset_reason = kMfltRebootReason_UserReset;
      break;

    default:
      reset_reason = kMfltRebootReason_Unknown;
  }

  *info = (sResetBootupInfo) {
    .reset_reason_reg = reset_cause,
    .reset_reason = reset_reason,
  };
}

void memfault_platform_log(eMemfaultPlatformLogLevel level, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  char log_buf[128];
  vsnprintf(log_buf, sizeof(log_buf), fmt, args);

  const char *lvl_str;
  switch (level) {
    case kMemfaultPlatformLogLevel_Debug:
      lvl_str = "D";
      break;

    case kMemfaultPlatformLogLevel_Info:
      lvl_str = "I";
      break;

    case kMemfaultPlatformLogLevel_Warning:
      lvl_str = "W";
      break;

    case kMemfaultPlatformLogLevel_Error:
      lvl_str = "E";
      break;

    default:
      break;
  }

  vsnprintf(log_buf, sizeof(log_buf), fmt, args);

  Report("[%s] MFLT: %s\n", lvl_str, log_buf);
}

#include "memfault/metrics/platform/overrides.h"
// [...]
void memfault_metrics_heartbeat_collect_data(void) {
    SlWlanGetRxStatResponse_t rxStat;
    if (sl_WlanRxStatGet(&rxStat, 0) == 0)
    {
      memfault_metrics_heartbeat_set_signed(
        MEMFAULT_METRICS_KEY(WlanRxStatAvarageDataCtrlRssi), rxStat.AvarageDataCtrlRssi);
    }
}

#define USER_CHUNK_SIZE 512

bool try_send_memfault_data(void) {
  // buffer to copy chunk data into
  uint8_t buf[USER_CHUNK_SIZE];
  size_t buf_len = sizeof(buf);

  bool data_available = memfault_packetizer_get_chunk(buf, &buf_len);
  if (!data_available ) {
    return false; // no more data to send
  }

  // send payload collected to chunks endpoint
  user_transport_send_chunk_data(buf, buf_len);
  return true;
}

extern int sl_memfault_https_client_connect();
extern void sl_memfault_https_client_disconnect();

bool send_memfault_data(void) {
  if (!memfault_packetizer_data_available()) {
    return false; // no new data to send
  }

  if (sl_memfault_https_client_connect() < 0) {
      return false;
  }

  // [... user specific logic deciding when & how much data to send
  while (try_send_memfault_data()) { }

  sl_memfault_https_client_disconnect();

  return false;
}
