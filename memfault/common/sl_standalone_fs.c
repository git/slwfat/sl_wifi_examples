/*
 * Copyright (c) 2015-2022, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ti/devices/cc32xx/inc/hw_types.h>
#include <ti/devices/cc32xx/inc/hw_ocp_shared.h>
#include <ti/devices/cc32xx/inc/hw_common_reg.h>
#include <ti/devices/cc32xx/inc/hw_apps_rcm.h>
#include <ti/devices/cc32xx/inc/hw_gprcm.h>
#include <ti/devices/cc32xx/inc/hw_hib1p2.h>
#include <ti/devices/cc32xx/driverlib/spi.h>
#include <ti/devices/cc32xx/driverlib/rom.h>
#include <ti/devices/cc32xx/driverlib/prcm.h>
#include <ti/devices/cc32xx/inc/hw_memmap.h>
#include <ti/devices/cc32xx/inc/hw_ints.h>
#include <ti/devices/cc32xx/inc/hw_nvic.h>
#include <ti/devices/cc32xx/driverlib/rom_map.h>
#include <ti/devices/cc32xx/driverlib/udma.h>
#include <ti/devices/cc32xx/driverlib/interrupt.h>
#include <ti/drivers/SPI.h>
#include <ti/devices/cc32xx/driverlib/wdt.h>
#include <ti/drivers/Watchdog.h>
#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/net/wifi/source/protocol.h>
#include <ti/drivers/net/wifi/fs.h>
#include <ti/drivers/net/wifi/source/driver.h>

#include "sl_standalone_fs.h"


#define uSEC_DELAY(x)                   (ROM_UtilsDelayDirect(x*80/3))

static void initHw()
{
    /*
     * SPI peripheral should remain disabled until a transfer is requested.
     * This is done to prevent the RX FIFO from gathering data from other
     * transfers.
     */
    MAP_SPICSDisable(LSPI_BASE);
    MAP_SPIDisable(LSPI_BASE);
    MAP_SPIReset(LSPI_BASE);

    const _u32 bitRate = 30000000;
    const _u32 csControl = SPI_SW_CTRL_CS;
    const _u32 pinMode = SPI_4PIN_MODE;
    const _u32 turboMode = SPI_TURBO_OFF;
    const _u32 csPolarity = 0;
    const _u32 dataSize = 32;

    MAP_SPIConfigSetExpClk(LSPI_BASE,
            MAP_PRCMPeripheralClockGet(PRCM_LSPI), bitRate,
            SPI_MODE_MASTER, SPI_POL0_PHA0,
            (csControl | pinMode | turboMode |
            csPolarity | ((dataSize - 1) << 7)));

    const _u32 txFifoTrigger = 4;
    const _u32 rxFifoTrigger = 4;

    MAP_SPIFIFOEnable(LSPI_BASE, SPI_RX_FIFO | SPI_TX_FIFO);
    MAP_SPIFIFOLevelSet(LSPI_BASE, txFifoTrigger,
        rxFifoTrigger);
}


static void transferCancel()
{
    MAP_IntMasterDisable();

    /* Prevent interrupt from occurring while canceling the transfer */
    MAP_IntDisable(INT_LSPI);
    MAP_IntPendClear(INT_LSPI);

    const _u32 rxChannelIndex = 12;
    const _u32 txChannelIndex = 13;
    /* Clear DMA configuration */
    MAP_uDMAChannelDisable(rxChannelIndex);
    MAP_uDMAChannelDisable(txChannelIndex);

    MAP_SPIIntDisable(LSPI_BASE, SPI_INT_DMARX);
    MAP_SPIIntClear(LSPI_BASE, SPI_INT_DMARX);
    MAP_SPIDmaDisable(LSPI_BASE, SPI_RX_DMA | SPI_TX_DMA);

    MAP_IntMasterEnable();

    /*
     * Disables peripheral, clears all registers & reinitializes it to
     * parameters used in SPI_open()
     */
    initHw();

    MAP_IntEnable(INT_LSPI);
}

static int _writeLSPI(_u8 *buf, _u32 len)
{
    if (len == 0)
    {
         return 0;
    }
    //SPIWordCountSet(LSPI_BASE, 0);
    SPIEnable(LSPI_BASE);
    SPICSEnable(LSPI_BASE);

    int ret = SPITransfer(LSPI_BASE, buf, NULL, len, 0);

    SPICSDisable(LSPI_BASE);
    SPIDisable(LSPI_BASE);

    return ret;
}

static int _readLSPI(_u8 *buf, _u32 len)
{
    if (len == 0)
    {
         return 0;
    }
    //SPIWordCountSet(LSPI_BASE, 0);
    SPIEnable(LSPI_BASE);
    SPICSEnable(LSPI_BASE);

    int ret = SPITransfer(LSPI_BASE, NULL, buf, len, 0);

    SPICSDisable(LSPI_BASE);
    SPIDisable(LSPI_BASE);

    return ret;
}

#define _NWP_IF_WRITE_CHECK(buf, len) if (_writeLSPI(buf, len) == -1) { while (1); }
#define _NWP_IF_READ_CHECK(buf, len)  if (_readLSPI(buf, len) == -1) { while (1); }

static _u8 TxSeqNum;

static const _SlSyncPattern_t g_H2NSyncPattern = H2N_SYNC_PATTERN;
static const _SlSyncPattern_t g_H2NCnysPattern = H2N_CNYS_PATTERN;

/******************************************************************************/
#define N2H_SYNC_PATTERN_SEQ_NUM_BITS            ((_u32)0x00000003) /* Bits 0..1    - use the 2 LBS for seq num */
#define N2H_SYNC_PATTERN_SEQ_NUM_EXISTS          ((_u32)0x00000004) /* Bit  2       - sign that sequence number exists in the sync pattern */
#define N2H_SYNC_PATTERN_MASK                    ((_u32)0xFFFFFFF8) /* Bits 3..31   - constant SYNC PATTERN */
#define N2H_SYNC_SPI_BUGS_MASK                   ((_u32)0x7FFF7F7F) /* Bits 7,15,31 - ignore the SPI (8,16,32 bites bus) error bits  */
#define BUF_SYNC_SPIM(pBuf)                      ((*(_u32 *)(pBuf)) & N2H_SYNC_SPI_BUGS_MASK)

#define N2H_SYNC_SPIM                            (N2H_SYNC_PATTERN    & N2H_SYNC_SPI_BUGS_MASK)
#define N2H_SYNC_SPIM_WITH_SEQ(TxSeqNum)         ((N2H_SYNC_SPIM & N2H_SYNC_PATTERN_MASK) | N2H_SYNC_PATTERN_SEQ_NUM_EXISTS | ((TxSeqNum) & (N2H_SYNC_PATTERN_SEQ_NUM_BITS)))
#define MATCH_WOUT_SEQ_NUM(pBuf)                 ( BUF_SYNC_SPIM(pBuf) ==  N2H_SYNC_SPIM )
#define MATCH_WITH_SEQ_NUM(pBuf, TxSeqNum)       ( BUF_SYNC_SPIM(pBuf) == (N2H_SYNC_SPIM_WITH_SEQ(TxSeqNum)) )
#define N2H_SYNC_PATTERN_MATCH(pBuf, TxSeqNum) \
    ( \
    (  (*((_u32 *)pBuf) & N2H_SYNC_PATTERN_SEQ_NUM_EXISTS) && ( MATCH_WITH_SEQ_NUM(pBuf, TxSeqNum) ) )    || \
    ( !(*((_u32 *)pBuf) & N2H_SYNC_PATTERN_SEQ_NUM_EXISTS) && ( MATCH_WOUT_SEQ_NUM(pBuf          ) ) )       \
    )
/******************************************************************************/


static int _SlDrvRxHdrRead(_u8 *pBuf)
{
    _u8        ShiftIdx;
    _u8        SearchSync = TRUE;
    _u8        SyncPattern[4];

    NwpUnMaskInterrupt();
    while ((HWREG(NVIC_PEND5) & 0x800) == 0)
    {

    }

    NwpMaskInterrupt();
    MAP_IntPendClear(INT_NWPIC);

    _NWP_IF_WRITE_CHECK((_u8 *)&g_H2NCnysPattern.Short, SYNC_PATTERN_LEN);

    /*  2. Read 8 bytes (protocol aligned) - expected to be the sync pattern */
    _NWP_IF_READ_CHECK(&pBuf[0], 8);

    /* read while first 4 bytes are different than last 4 bytes */
    while ( *(_u32 *)&pBuf[0] == *(_u32 *)&pBuf[4])
    {
         _NWP_IF_READ_CHECK(&pBuf[4], 4);
    }

    /* scan for the sync pattern till found or timeout elapsed (if configured) */
    while (SearchSync)
    {
        /* scan till we get the real sync pattern */
        for (ShiftIdx =0; ShiftIdx <=4 ; ShiftIdx++)
        {
           /* copy to local variable to ensure starting address which is 4-bytes aligned */
           sl_Memcpy(&SyncPattern[0],  &pBuf[ShiftIdx], 4);

           /* sync pattern found so complete the read to  4 bytes aligned */
           if (N2H_SYNC_PATTERN_MATCH(&SyncPattern[0], TxSeqNum))
           {
                   /* copy the bytes following the sync pattern to the buffer start */
                   sl_Memcpy(&pBuf[0],  &pBuf[ShiftIdx + SYNC_PATTERN_LEN], 4);

                   if (ShiftIdx != 0)
                   {
                         /* read the rest of the bytes (only if wer'e not aligned) (expected to complete the opcode + length fields ) */
                      _NWP_IF_READ_CHECK(&pBuf[SYNC_PATTERN_LEN - ShiftIdx], ShiftIdx);
                   }

                  /* here we except to get the opcode + length or false doubled sync..*/
                  SearchSync = FALSE;
                  break;
           }
        }

        if (SearchSync == TRUE)
        {
            /* sync not found move top 4 bytes to bottom */
            *(_u32 *)&pBuf[0] = *(_u32 *)&pBuf[4];

            /* read 4 more bytes to the buffer top */
            _NWP_IF_READ_CHECK(&pBuf[4], 4);
        }
    } /* end of while */

    /*  6. Scan for Double pattern. */
    while ( N2H_SYNC_PATTERN_MATCH(pBuf, TxSeqNum) )
    {
        _NWP_IF_READ_CHECK(&pBuf[0], SYNC_PATTERN_LEN);
    }
    TxSeqNum++;

    /*  7. Here we've read Generic Header (4 bytes opcode+length).
     * Now Read the Resp Specific header (4 more bytes). */
    _NWP_IF_READ_CHECK(&pBuf[SYNC_PATTERN_LEN], _SL_RESP_SPEC_HDR_SIZE);

    return SL_RET_CODE_OK;
}


static inline void readDummyPayload(_u32 len)
{
    _u32 buf;
    if (len == 0)
    {
        return;
    }

    for (_u32 i = 0; i < len; ++i)
    {
        _readLSPI((_u8 *) &buf, sizeof(_u32));
    }
}

static int expectEvent(_u16 opcode, _u8 *buf)
{
    _u8 header_buf[_SL_RESP_HDR_SIZE];

    while (1)
    {
        _SlDrvRxHdrRead(header_buf);
        _SlGenericHeader_t *header = (_SlGenericHeader_t *) header_buf;

        _u16 len = header->Len - _SL_RESP_SPEC_HDR_SIZE;


        if (buf)
        {
            _readLSPI(buf, len);
        }
        else
        {
            readDummyPayload(len);
        }

        if (header->Opcode == opcode)
        {
            return 0;
        }
    }
}

static void waitForInitComplete()
{
    InitComplete_t resp;
    expectEvent(SL_OPCODE_DEVICE_INITCOMPLETE, (_u8 *) &resp);
    if (resp.Status < 0)
    {
        while (1) {}
    }
}

/*****************************************************************************/
/*  sl_FsOpen                                                                */
/*****************************************************************************/
typedef union
{
    SlFsOpenCommand_t     Cmd;
    SlFsOpenResponse_t    Rsp;
}_SlFsOpenMsg_u;



static const _SlCmdCtrl_t _SlFsOpenCmdCtrl =
{
    SL_OPCODE_NVMEM_FILEOPEN,
    (_SlArgSize_t)sizeof(SlFsOpenCommand_t),
    (_SlArgSize_t)sizeof(SlFsOpenResponse_t)
};


typedef enum
{
    FS_MODE_OPEN_READ            = 0,
    FS_MODE_OPEN_WRITE,
    FS_MODE_OPEN_CREATE,
    FS_MODE_OPEN_WRITE_CREATE_IF_NOT_EXIST
}FsFileOpenAccessType_e;

#define FS_CONVERT_FLAGS( ModeAndMaxSize )  (((_u32)ModeAndMaxSize & SL_FS_OPEN_FLAGS_BIT_MASK)>>SL_NUM_OF_MAXSIZE_BIT)

/*****************************************************************************/
/* _SlFsStrlen                                                                */
/*****************************************************************************/
static _u16 _SlFsStrlen(const _u8 *buffer)
{
    _u16 len = 0;
    if( buffer != NULL )
    {
      while(*buffer++) len++;
    }
    return len;
}

#define FS_MODE_ACCESS_RESERVED_OFFSET                       (27)
#define FS_MODE_ACCESS_RESERVED_MASK                         (0x1F)
#define FS_MODE_ACCESS_FLAGS_OFFSET                          (16)
#define FS_MODE_ACCESS_FLAGS_MASK                            (0x7FF)
#define FS_MODE_ACCESS_OFFSET                                (12)
#define FS_MODE_ACCESS_MASK                                  (0xF)
#define FS_MODE_OPEN_SIZE_GRAN_OFFSET                        (8)
#define FS_MODE_OPEN_SIZE_GRAN_MASK                          (0xF)
#define FS_MODE_OPEN_SIZE_OFFSET                             (0)
#define FS_MODE_OPEN_SIZE_MASK                               (0xFF)
#define FS_MAX_MODE_SIZE                                     (0xFF)

/* SizeGran is up to 4 bit , Size can be up to 8 bit */
#define FS_MODE(Access, SizeGran, Size,Flags)        (_u32)(((_u32)((Access) &FS_MODE_ACCESS_MASK)<<FS_MODE_ACCESS_OFFSET) |  \
                                                            ((_u32)((SizeGran) &FS_MODE_OPEN_SIZE_GRAN_MASK)<<FS_MODE_OPEN_SIZE_GRAN_OFFSET) | \
                                                            ((_u32)((Size) &FS_MODE_OPEN_SIZE_MASK)<<FS_MODE_OPEN_SIZE_OFFSET) | \
                                                            ((_u32)((Flags) &FS_MODE_ACCESS_FLAGS_MASK)<<FS_MODE_ACCESS_FLAGS_OFFSET))

typedef enum
{
    FS_MODE_SIZE_GRAN_256B    = 0,   /*  MAX_SIZE = 64K  */
    FS_MODE_SIZE_GRAN_1KB,           /*  MAX_SIZE = 256K */
    FS_MODE_SIZE_GRAN_4KB,           /*  MAX_SZIE = 1M   */
    FS_MODE_SIZE_GRAN_16KB,          /*  MAX_SIZE = 4M   */
    FS_MODE_SIZE_GRAN_64KB,          /*  MAX_SIZE = 16M  */
    FS_MAX_MODE_SIZE_GRAN
}FsFileOpenMaxSizeGran_e;

static _u32 FsGetCreateFsMode(_u8 Mode, _u32 MaxSizeInBytes,_u32 AccessFlags)
{
   _u32 granIdx = 0;
   _u32 granNum = 0;
   _u32 granTable[FS_MAX_MODE_SIZE_GRAN] = {256,1024,4096,16384,65536};

   for(granIdx= FS_MODE_SIZE_GRAN_256B ;granIdx< FS_MAX_MODE_SIZE_GRAN;granIdx++)
   {
       if( granTable[granIdx]*255 >= MaxSizeInBytes )
            break;
   }
   granNum = MaxSizeInBytes/granTable[granIdx];
   if( MaxSizeInBytes % granTable[granIdx] != 0 )
         granNum++;

   return (_u32)FS_MODE( Mode, granIdx, granNum, AccessFlags );

}


static _SlReturnVal_t _SlDrvMsgWrite(_SlCmdCtrl_t  *pCmdCtrl,_SlCmdExt_t  *pCmdExt, _u8 *pTxRxDescBuff)
{
    _u8 sendRxPayload = FALSE;
    _SL_ASSERT_ERROR(NULL != pCmdCtrl, SL_API_ABORTED);

    _SlCommandHeader_t TempProtocolHeader;
    TempProtocolHeader.Opcode   = pCmdCtrl->Opcode;
    TempProtocolHeader.Len   = (_u16)(_SL_PROTOCOL_CALC_LEN(pCmdCtrl, pCmdExt));

    if (pCmdExt && pCmdExt->RxPayloadLen < 0 )
    {
        pCmdExt->RxPayloadLen = pCmdExt->RxPayloadLen * (-1); /* change sign */
        sendRxPayload = TRUE;
        TempProtocolHeader.Len = TempProtocolHeader.Len + pCmdExt->RxPayloadLen;
    }

    /*  Write short sync pattern */
    _NWP_IF_WRITE_CHECK((_u8 *)&g_H2NSyncPattern.Short, SYNC_PATTERN_LEN);

    /*  Header */
    _NWP_IF_WRITE_CHECK((_u8 *)&TempProtocolHeader, _SL_CMD_HDR_SIZE);

    /*  Descriptors */
    if (pTxRxDescBuff && pCmdCtrl->TxDescLen > 0)
    {
        _NWP_IF_WRITE_CHECK(pTxRxDescBuff,
                           _SL_PROTOCOL_ALIGN_SIZE(pCmdCtrl->TxDescLen));
    }

    /*  A special mode where Rx payload and Rx length are used as Tx as well */
    /*  This mode requires no Rx payload on the response and currently used by fs_Close and sl_Send on */
    /*  transceiver mode */
    if (sendRxPayload == TRUE )
    {
        _NWP_IF_WRITE_CHECK(pCmdExt->pRxPayload,
                           _SL_PROTOCOL_ALIGN_SIZE(pCmdExt->RxPayloadLen));
    }


    /* if the message has some payload */
    if (pCmdExt)
    {
        /*  If the message has payload, it is mandatory that the message's arguments are protocol aligned. */
        /*  Otherwise the aligning of arguments will create a gap between arguments and payload. */
        VERIFY_PROTOCOL(_SL_IS_PROTOCOL_ALIGNED_SIZE(pCmdCtrl->TxDescLen));

        /* In case two seperated buffers were supplied we should merge the two buffers*/
        if ((pCmdExt->TxPayload1Len > 0) && (pCmdExt->TxPayload2Len > 0))
        {
            _u8 BuffInTheMiddle[4];
            _u8 FirstPayloadReminder = 0;
            _u8 SecondPayloadOffset = 0;

            FirstPayloadReminder = pCmdExt->TxPayload1Len & 3; /* calulate the first payload reminder */

            /* we first write the 4-bytes aligned payload part */
            pCmdExt->TxPayload1Len -= FirstPayloadReminder;

            /* writing the first transaction*/
            _NWP_IF_WRITE_CHECK(pCmdExt->pTxPayload1, pCmdExt->TxPayload1Len);

            /* Only if we the first payload is not aligned we need the intermediate transaction */
            if (FirstPayloadReminder != 0)
            {
                /* here we count how many bytes we need to take from the second buffer */
                SecondPayloadOffset = 4 - FirstPayloadReminder;

                /* copy the first payload reminder */
                sl_Memcpy(&BuffInTheMiddle[0], pCmdExt->pTxPayload1 + pCmdExt->TxPayload1Len, FirstPayloadReminder);

                /* add the beginning of the second payload to complete 4-bytes transaction */
                sl_Memcpy(&BuffInTheMiddle[FirstPayloadReminder], pCmdExt->pTxPayload2, SecondPayloadOffset);

                /* write the second transaction of the 4-bytes buffer */
                _NWP_IF_WRITE_CHECK(&BuffInTheMiddle[0], 4);
            }


            /* if we still has bytes to write in the second buffer  */
            if (pCmdExt->TxPayload2Len > SecondPayloadOffset)
            {
                /* write the third transaction (truncated second payload) */
                _NWP_IF_WRITE_CHECK(
                                   pCmdExt->pTxPayload2 + SecondPayloadOffset,
                                   _SL_PROTOCOL_ALIGN_SIZE(pCmdExt->TxPayload2Len - SecondPayloadOffset));
            }

        }
        else if (pCmdExt->TxPayload1Len > 0)
        {
            /* Only 1 payload supplied (Payload1) so just align to 4 bytes and send it */
            _NWP_IF_WRITE_CHECK(pCmdExt->pTxPayload1,
                            _SL_PROTOCOL_ALIGN_SIZE(pCmdExt->TxPayload1Len));
        }
        else if (pCmdExt->TxPayload2Len > 0)
        {
            /* Only 1 payload supplied (Payload2) so just align to 4 bytes and send it */
            _NWP_IF_WRITE_CHECK( pCmdExt->pTxPayload2,
                            _SL_PROTOCOL_ALIGN_SIZE(pCmdExt->TxPayload2Len));

        }
    }

    return SL_OS_RET_CODE_OK;
}




static int _SlDrvCmdOp2(_SlCmdCtrl_t *pCmdCtrl , void* pTxRxDescBuff , _SlCmdExt_t* pCmdExt)
{
    _SlDrvMsgWrite(pCmdCtrl, pCmdExt, pTxRxDescBuff);
    return 0;
}

_i32 _sl_NonOs_FsOpen(const _u8 *pFileName,const _u32 ModeAndMaxSize, _u32 *pToken)
{

    _SlFsOpenMsg_u        Msg;
    _SlCmdExt_t           CmdExt;
    _i32                  FileHandle;
    _u32                  MaxSizeInBytes;
    _u32                  OpenMode;
    _u8                   CreateMode;

    /* verify that this api is allowed. if not allowed then
    ignore the API execution and return immediately with an error */
    //VERIFY_API_ALLOWED(SL_OPCODE_SILO_FS);

    _SlDrvMemZero(&CmdExt, (_u16)sizeof(_SlCmdExt_t));

    if ( _SlFsStrlen(pFileName) >= SL_FS_MAX_FILE_NAME_LENGTH )
    {
        return SL_ERROR_FS_WRONG_FILE_NAME;
    }

    CmdExt.TxPayload1Len = (_u16)((_SlFsStrlen(pFileName)+4) & (~3)); /* add 4: 1 for NULL and the 3 for align */
    CmdExt.pTxPayload1 = (_u8*)pFileName;

    OpenMode = ModeAndMaxSize & SL_FS_OPEN_MODE_BIT_MASK;

    /*convert from the interface flags to the device flags*/
    if( OpenMode == SL_FS_READ )
    {
        Msg.Cmd.Mode = FS_MODE(FS_MODE_OPEN_READ, 0, 0, 0);
    }
    else if (( OpenMode == SL_FS_WRITE ) ||( OpenMode == SL_FS_OVERWRITE))
    {
        Msg.Cmd.Mode = FS_MODE(FS_MODE_OPEN_WRITE, 0, 0, FS_CONVERT_FLAGS ( ModeAndMaxSize));
    }
    /* one of the creation mode */
    else if ( ( OpenMode == (SL_FS_CREATE | SL_FS_OVERWRITE )) || ( OpenMode == SL_FS_CREATE) ||(OpenMode == (SL_FS_CREATE | SL_FS_WRITE )))
    {
       /* test that the size is correct */
       MaxSizeInBytes = (ModeAndMaxSize & SL_FS_OPEN_MAXSIZE_BIT_MASK) * 256;
       if (MaxSizeInBytes > 0xFF0000 )
       {
           return SL_ERROR_FS_FILE_MAX_SIZE_EXCEEDED;
       }

       CreateMode = ((OpenMode == (SL_FS_CREATE | SL_FS_OVERWRITE )) ? FS_MODE_OPEN_WRITE_CREATE_IF_NOT_EXIST : FS_MODE_OPEN_CREATE  );

        Msg.Cmd.Mode = FsGetCreateFsMode( CreateMode ,MaxSizeInBytes, FS_CONVERT_FLAGS ( ModeAndMaxSize)  );
    }
    else
    {
        return SL_ERROR_FS_INVALID_FILE_MODE;
    }

    if(pToken != NULL)
    {
        Msg.Cmd.Token         = *pToken;
    }
    else
    {
        Msg.Cmd.Token         = 0;
    }

    _SlDrvCmdOp2((_SlCmdCtrl_t *)&_SlFsOpenCmdCtrl, &Msg, &CmdExt);
    //uSEC_DELAY(1000);
    expectEvent(SL_OPCODE_NVMEM_FILEOPENRESPONSE, (_u8 *) &Msg);

    FileHandle = (_i32)Msg.Rsp.FileHandle;
    if (pToken != NULL)
    {
        *pToken =      Msg.Rsp.Token;
    }

    /* in case of an error, return the erros file handler as an error code */
    return FileHandle;
}


/*****************************************************************************/
/* sl_FsClose */
/*****************************************************************************/
typedef union
{
    SlFsCloseCommand_t    Cmd;
    _BasicResponse_t        Rsp;
}_SlFsCloseMsg_u;

#if _SL_INCLUDE_FUNC(sl_FsClose)

static const _SlCmdCtrl_t _SlFsCloseCmdCtrl =
{
    SL_OPCODE_NVMEM_FILECLOSE,
    (_SlArgSize_t)sizeof(SlFsCloseCommand_t),
    (_SlArgSize_t)sizeof(SlFsCloseResponse_t)
};

_i16 _sl_NonOs_FsClose(const _i32 FileHdl, const _u8*  pCeritificateFileName,const _u8*  pSignature ,const _u32 SignatureLen)
{
    _SlFsCloseMsg_u    Msg;
    _SlCmdExt_t        ExtCtrl;

    _SlDrvMemZero(&Msg, (_u16)sizeof(SlFsCloseCommand_t));

    Msg.Cmd.FileHandle             = (_u32)FileHdl;
    if( pCeritificateFileName != NULL )
    {
        Msg.Cmd.CertificFileNameLength = (_u32)((_SlFsStrlen(pCeritificateFileName)+4) & (~3)); /* add 4: 1 for NULL and the 3 for align */
    }
    Msg.Cmd.SignatureLen           = SignatureLen;

    _SlDrvMemZero(&ExtCtrl, (_u16)sizeof(_SlCmdExt_t));

    ExtCtrl.TxPayload1Len = (_u16)(((SignatureLen+3) & (~3))); /* align */
    ExtCtrl.pTxPayload1   = (_u8*)pSignature;
    ExtCtrl.RxPayloadLen  = (_i16)Msg.Cmd.CertificFileNameLength;
    ExtCtrl.pRxPayload    = (_u8*)pCeritificateFileName; /* Add signature */

    if(ExtCtrl.pRxPayload != NULL &&  ExtCtrl.RxPayloadLen != 0)
    {
       ExtCtrl.RxPayloadLen = ExtCtrl.RxPayloadLen * (-1);
    }

    VERIFY_RET_OK(_SlDrvCmdOp2((_SlCmdCtrl_t *)&_SlFsCloseCmdCtrl, &Msg, &ExtCtrl));
    expectEvent(SL_OPCODE_NVMEM_FILECLOSERESPONSE, (_u8 *) &Msg);

    return (_i16)((_i16)Msg.Rsp.status);
}
#endif

/*****************************************************************************/
/* sl_FsWrite */
/*****************************************************************************/
typedef union
{
    SlFsWriteCommand_t        Cmd;
    SlFsWriteResponse_t        Rsp;
}_SlFsWriteMsg_u;

#if _SL_INCLUDE_FUNC(sl_FsWrite)

#define sl_min(a,b) (((a) < (b)) ? (a) : (b))
#define MAX_NVMEM_CHUNK_SIZE  1456 /*should be 16 bytes align, because of encryption data*/

static const _SlCmdCtrl_t _SlFsWriteCmdCtrl =
{
    SL_OPCODE_NVMEM_FILEWRITECOMMAND,
    (_SlArgSize_t)sizeof(SlFsWriteCommand_t),
    (_SlArgSize_t)sizeof(SlFsWriteResponse_t)
};

_i32 _sl_NonOs_FsWrite(const _i32 FileHdl,_u32 Offset, _u8*  pData,_u32 Len)
{
    _SlFsWriteMsg_u     Msg;
    _SlCmdExt_t         ExtCtrl;
    _u16                ChunkLen;
    _SlReturnVal_t      RetVal;
    _i32                RetCount = 0;

    _SlDrvMemZero(&ExtCtrl, (_u16)sizeof(_SlCmdExt_t));

    ChunkLen              = (_u16)sl_min(MAX_NVMEM_CHUNK_SIZE,Len);
    ExtCtrl.TxPayload1Len = ChunkLen;
    ExtCtrl.pTxPayload1   = (_u8 *)(pData);
    Msg.Cmd.Offset        = Offset;
    Msg.Cmd.Len           = ChunkLen;
    Msg.Cmd.FileHandle    = (_u32)FileHdl;

    do
    {
        RetVal = _SlDrvCmdOp2((_SlCmdCtrl_t *)&_SlFsWriteCmdCtrl, &Msg, &ExtCtrl);
        if(SL_OS_RET_CODE_OK == RetVal)
        {
            expectEvent(SL_OPCODE_NVMEM_FILEWRITERESPONSE, (_u8 *) &Msg);
            if( Msg.Rsp.status < 0)
            {
                if( RetCount > 0)
                {
                    return RetCount;
                }
                else
                {
                    return Msg.Rsp.status;
                }
            }

            RetCount += (_i32)Msg.Rsp.status;
            Len -= ChunkLen;
            Offset += ChunkLen;
            Msg.Cmd.Offset = Offset;
            ExtCtrl.pTxPayload1 += ChunkLen;
            ChunkLen = (_u16)sl_min(MAX_NVMEM_CHUNK_SIZE,Len);
            ExtCtrl.TxPayload1Len  = ChunkLen;
            Msg.Cmd.Len = ChunkLen;
            Msg.Cmd.FileHandle = (_u32)FileHdl;
        }
        else
        {
            return RetVal;
        }
    }while(ChunkLen > 0);

    return (_i32)RetCount;
}
#endif


static inline int resetNWP()
{
#define NWP_SPARE_REG_5                 (OCP_SHARED_BASE + OCP_SHARED_O_SPARE_REG_5)
#define NWP_SPARE_REG_5_SLSTOP          (0x00000002)

    /* sl_stop ECO for PG1.32 devices */
//    HWREG(NWP_SPARE_REG_5) |= NWP_SPARE_REG_5_SLSTOP;

    NwpPowerOff();
    NwpPowerOn();

    return 0;
}

static void _NonOS_setWDT()
{
   const unsigned int wdtCycles = 2400000000 >> 1; // 30 seconds

   Watchdog_init();


   MAP_PRCMPeripheralClkEnable(11,
                   PRCM_RUN_MODE_CLK | PRCM_SLP_MODE_CLK);

               /* spin here until status returns TRUE */
   while(!MAP_PRCMPeripheralStatusGet(11)) {
   }

   MAP_WatchdogUnlock(WDT_BASE);
   MAP_WatchdogReloadSet(WDT_BASE, wdtCycles);
   MAP_WatchdogIntClear(WDT_BASE);

   MAP_WatchdogStallDisable(WDT_BASE);
   MAP_WatchdogEnable(WDT_BASE);
   MAP_WatchdogLock(WDT_BASE);
}

void static _NwpPowerOff(void);

void _sl_NonOs_FsPrepare()
{
    _NonOS_setWDT();

    ///
    _NwpPowerOff();
    uSEC_DELAY(100000);
    ///

    SPIIntUnregister(LSPI_BASE);

    initHw();
    transferCancel();
    //uSEC_DELAY(100000);

    //resetNWP();
    NwpPowerOn();
    //

    uSEC_DELAY(100000);
    waitForInitComplete();
}


#define NWP_LPDS_WAKEUPCFG              (GPRCM_BASE + GPRCM_O_NWP_LPDS_WAKEUP_CFG)
#define NWP_LPDS_WAKEUPCFG_APPS2NWP     (0x00000020)
#define NWP_LPDS_WAKEUPCFG_TIMEOUT_MSEC (600)
#define WAKENWP                         (ARCM_BASE + APPS_RCM_O_APPS_TO_NWP_WAKE_REQUEST)
#define WAKENWP_WAKEREQ                 (APPS_RCM_APPS_TO_NWP_WAKE_REQUEST_APPS_TO_NWP_WAKEUP_REQUEST)

/* A2N_INT_STS_CLR -                    (COMMON_REG_BASE + COMMON_REG_O_APPS_INT_STS_CLR)  */
#define A2N_INT_STS_CLR                 (COMMON_REG_BASE + COMMON_REG_O_APPS_INT_STS_CLR)
/* A2N_INT_TRIG -                       (COMMON_REG_BASE + COMMON_REG_O_APPS_INT_TRIG)     */
#define A2N_INT_TRIG                    (COMMON_REG_BASE + COMMON_REG_O_APPS_INT_TRIG)
/* A2N_INT_STS_RAW -                    (COMMON_REG_BASE + COMMON_REG_O_APPS_INT_STS_RAW)  */
#define A2N_INT_STS_RAW                 (COMMON_REG_BASE + COMMON_REG_O_APPS_INT_STS_RAW)

#define ANA_DCDC_PARAMS0                (HIB1P2_BASE + HIB1P2_O_ANA_DCDC_PARAMETERS0)
#define ANA_DCDC_PARAMS0_PWMOVERRIDE    (0x08000000)

static void _NwpPowerOff(void)
{

    volatile unsigned long apps_int_sts_raw;
    volatile unsigned long sl_stop_ind       = HWREG(NWP_SPARE_REG_5);
    volatile unsigned long nwp_lpds_wake_cfg = HWREG(NWP_LPDS_WAKEUPCFG);
    _SlTimeoutParams_t SlTimeoutInfo         = {0};

    if((nwp_lpds_wake_cfg != NWP_LPDS_WAKEUPCFG_APPS2NWP) &&     /* Check for NWP POR condition - APPS2NWP is reset condition */
                !(sl_stop_ind & NWP_SPARE_REG_5_SLSTOP))         /* Check if sl_stop was executed */
    {
        HWREG(0xE000E104) = 0x200;               /* Enable the out of band interrupt, this is not a wake-up source*/
        HWREG(A2N_INT_TRIG) = 0x1;               /* Trigger out of band interrupt  */
        HWREG(WAKENWP) = WAKENWP_WAKEREQ;        /* Wake-up the NWP */

        //_SlDrvStartMeasureTimeout(&SlTimeoutInfo, NWP_N2A_INT_ACK_TIMEOUT_MSEC);

       /* Wait for the A2N_INT_TRIG to be cleared by the NWP to indicate it's awake and ready for shutdown.
        * poll until APPs->NWP interrupt is cleared or timeout :
        * for service pack 3.1.99.1 or higher, this condition is fulfilled in less than 1 mSec.
        * Otherwise, in some cases it may require up to 3000 mSec of waiting.  */

        apps_int_sts_raw = HWREG(A2N_INT_STS_RAW);
        while(!(apps_int_sts_raw & 0x1))
        {
            //if(_SlDrvIsTimeoutExpired(&SlTimeoutInfo))
            {
                break;
            }
            apps_int_sts_raw = HWREG(A2N_INT_STS_RAW);
        }

        WAIT_NWP_SHUTDOWN_READY;
   }

   /* Clear Out of band interrupt, Acked by the NWP */
   HWREG(A2N_INT_STS_CLR) = 0x1;

   /* Mask Host Interrupt */
   NwpMaskInterrupt();

   /* Switch to PFM Mode */
   HWREG(ANA_DCDC_PARAMS0) &= ~ANA_DCDC_PARAMS0_PWMOVERRIDE;

   /* sl_stop ECO for PG1.32 devices */
   HWREG(NWP_SPARE_REG_5) |= NWP_SPARE_REG_5_SLSTOP;

   /* Wait for 20 uSec, which is the minimal time between on-off cycle */
   uSEC_DELAY(20);
}
