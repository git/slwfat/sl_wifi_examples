//! @file
//!
//! Copyright 2022 Memfault, Inc
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//!     http://www.apache.org/licenses/LICENSE-2.0
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.
//!
//! @brief
//! Logging depends on how your configuration does logging. See
//! https://docs.memfault.com/docs/mcu/self-serve/#logging-dependency

/*
Copyright (c) 2022 Texas  Instruments Incorporated
*/

#include "memfault/panics/platform/coredump.h"


#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "memfault/core/compiler.h"
#include "memfault/core/math.h"

#include "FreeRTOS.h"

#include "ti/devices/cc32xx/inc/hw_types.h"
#include "ti/devices/cc32xx/driverlib/flash.h"

#include "ti/drivers/net/wifi/simplelink.h"

#include "sl_standalone_fs.h"


#define LINKER_REGION_LEN(s, e) ((uint32_t)&e - (uint32_t)&s)

const sMfltCoredumpRegion *memfault_platform_coredump_get_regions(
    const sCoredumpCrashInfo *crash_info, size_t *num_regions) {
   static sMfltCoredumpRegion s_coredump_regions[4];

   // Linker variables that hold the addresses to the regions we to capture
   extern uint32_t __primary_heap_start__;
   extern uint32_t __primary_heap_end__;

   extern uint32_t __data_start__;
   extern uint32_t __data_end__;

   extern uint32_t __bss_start__;
   extern uint32_t __bss_end__;

   // symbols created automatically by linker for ISR stack
   extern uint32_t __STACK_END;
   extern uint32_t __stack;

   const size_t bss_length = LINKER_REGION_LEN(__bss_start__, __bss_end__);
   s_coredump_regions[0] = MEMFAULT_COREDUMP_MEMORY_REGION_INIT(
       &__bss_start__, bss_length);

   const size_t data_length = LINKER_REGION_LEN(__data_start__, __data_end__);
   s_coredump_regions[1] = MEMFAULT_COREDUMP_MEMORY_REGION_INIT(
           &__data_start__, data_length);


   const size_t heap_length = LINKER_REGION_LEN(__primary_heap_start__, __primary_heap_end__);
   s_coredump_regions[2] = MEMFAULT_COREDUMP_MEMORY_REGION_INIT(
           &__primary_heap_start__, heap_length);

   const size_t stack_length = LINKER_REGION_LEN(__stack, __STACK_END);
   s_coredump_regions[3] = MEMFAULT_COREDUMP_MEMORY_REGION_INIT(
           &__stack, stack_length);

   *num_regions = MEMFAULT_ARRAY_SIZE(s_coredump_regions);
   return &s_coredump_regions[0];
}

// Linker variables defined to reserve a flash
// region for Memfault Coredump Storage

extern uint32_t __coredump_storage_start__;
extern uint32_t __coredump_storage_end__;

#define COREDUMP_FLASH_BASE ((uint32_t)&__coredump_storage_start__)
#define COREDUMP_STORAGE_SIZE 256 * 1024 //LINKER_REGION_LEN(__coredump_storage_start__, __coredump_storage_end__)

// The sector sizes for the CC32XX Internal Flash are 2kB
#define COREDUMP_SECTOR_SIZE 2048

void memfault_platform_coredump_storage_get_info(sMfltCoredumpStorageInfo *info) {
  *info = (sMfltCoredumpStorageInfo) {
    .size = COREDUMP_STORAGE_SIZE,
    .sector_size = COREDUMP_SECTOR_SIZE,
  };
}

static _i32 _sl_fileHandle = -1;
static _i32 _sl_NonOs_fileHandle = -1;

#define COREDUMP_FILENAME "coredump.bin"

static bool _sl_appInFault = false;
static bool _sl_hasCoredump = true;

bool memfault_platform_coredump_storage_read(uint32_t offset, void *data,
                                             size_t read_len) {
    if ((offset + read_len) > COREDUMP_STORAGE_SIZE) {
        return false;
    }

    if (_sl_appInFault)
    {
      memset(data, 0, read_len);
      return true;
    }
    else
    {
        if (!_sl_hasCoredump) {
            return false;
        }

        if (_sl_fileHandle < 0)
        {
            _sl_fileHandle = sl_FsOpen((_u8 *) COREDUMP_FILENAME, SL_FS_READ, 0);
            if (_sl_fileHandle < 0)
            {
                _sl_hasCoredump = false;
                return false;
            }
        }

        if (sl_FsRead(_sl_fileHandle, offset, data, read_len) < 0)
        {
            return false;
        }

        if (offset == 0 && *((_u32 *) &data[0]) == 0)
        {
            sl_FsClose(_sl_fileHandle, NULL, NULL, 0);
            _sl_fileHandle = -1;
            _sl_hasCoredump = false;
        }
    }

    return true;
}

bool memfault_platform_coredump_storage_erase(uint32_t offset, size_t erase_size) {
    if ((offset + erase_size) > COREDUMP_STORAGE_SIZE ||
      ((erase_size % COREDUMP_SECTOR_SIZE) != 0)) {
        return false;
    }
    /*
    if (sl_FsDel((_u8 *) COREDUMP_FILENAME, 0) < 0)
    {
        return false;
    }
    */

  return true;
}

bool memfault_platform_coredump_save_begin(void) {
    // This gets called prior to any coredump save operation and is where
    // any initialization work can be done.
    _sl_NonOs_fileHandle = -1;
    _sl_appInFault = true;
    _sl_NonOs_FsPrepare();

    return true;
}



bool memfault_platform_coredump_storage_write(uint32_t offset, const void *data,
                                              size_t data_len) {
  if ((offset + data_len) > COREDUMP_STORAGE_SIZE) {
    if (_sl_NonOs_fileHandle > -1)
    {
        _sl_NonOs_FsClose(_sl_NonOs_fileHandle, NULL, NULL, 0);
    }
    return false;
  }

  if (_sl_NonOs_fileHandle < 0)
  {
      _sl_NonOs_fileHandle = _sl_NonOs_FsOpen((_u8 *) COREDUMP_FILENAME,
                                    SL_FS_CREATE|SL_FS_OVERWRITE| SL_FS_CREATE_MAX_SIZE( COREDUMP_STORAGE_SIZE ),
                                    NULL);
      if (_sl_NonOs_fileHandle < 0)
      {
          return false;
      }
  }

  if (_sl_NonOs_FsWrite(_sl_NonOs_fileHandle, offset, (_u8 *) data, data_len) < 0)
  {
      _sl_NonOs_FsClose(_sl_NonOs_fileHandle, NULL, NULL, 0);
      _sl_NonOs_fileHandle = -1;
      return false;
  }

  return true;
}

// Note: This function is called while the system is running after a coredump has been
// sent to memfault. To clear a coredump so it is not read again we just need to zero
// out the first byte
void memfault_platform_coredump_storage_clear(void) {
    uint8_t buf[32];

    memset(buf, 0, sizeof(buf));

    //memfault_platform_coredump_storage_write(0, &clear_word, sizeof(clear_word));
    if (_sl_fileHandle > -1)
    {
        if (sl_FsClose(_sl_fileHandle, NULL, NULL, 0) < 0)
        {
            return;
        }
    }

    _sl_fileHandle = sl_FsOpen((_u8 *) COREDUMP_FILENAME, SL_FS_WRITE, NULL);
    if (_sl_fileHandle < 0)
    {
        return;
    }

    sl_FsWrite(_sl_fileHandle, 0, (_u8 *) buf, sizeof(buf));
    sl_FsClose(_sl_fileHandle, NULL, NULL, 0);

    _sl_fileHandle = -1;
}


void prv_memfault_reboot()
{
    if (_sl_NonOs_fileHandle > -1)
    {
        _sl_NonOs_FsClose(_sl_NonOs_fileHandle, NULL, NULL, 0);
        _sl_NonOs_fileHandle = -1;
    }
}
