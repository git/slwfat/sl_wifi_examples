/*
 * Copyright (c) 2015-2022, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== empty.c ========
 */

/* For usleep() */
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
// #include <ti/drivers/I2C.h>
#include <ti/drivers/SPI.h>
// #include <ti/drivers/UART.h>
// #include <ti/drivers/Watchdog.h>
#include <ti/drivers/Power.h>

#include <pthread.h>

/* Driver configuration */
#include "ti_drivers_config.h"

#include "uart_term.h"
#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/net/http/httpclient.h>
#include <ti/drivers/net/wifi/errors.h>
#include <ti/net/slnet.h>


#include <stdarg.h>
#include "memfault/core/platform/debug_log.h"

#include <ti/devices/cc32xx/driverlib/wdt.h>
#include <ti/devices/cc32xx/driverlib/rom_map.h>
#include <ti/devices/cc32xx/inc/hw_memmap.h>
#include <ti/devices/cc32xx/driverlib/prcm.h>

#include "memfault/components.h"

#include "sl_memfault_config.h"


int isWlanConnected = 0;

//*****************************************************************************
// SimpleLink Callback Functions
//*****************************************************************************

void SimpleLinkNetAppRequestMemFreeEventHandler (uint8_t *buffer)
{
  // do nothing...
}

void SimpleLinkNetAppRequestEventHandler(SlNetAppRequest_t *pNetAppRequest,
                                         SlNetAppResponse_t *pNetAppResponse)
{
  // do nothing...
}

//*****************************************************************************
//
//! \brief The Function Handles WLAN Events
//!
//! \param[in]  pWlanEvent - Pointer to WLAN Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{
    char syncMsg;

    switch(pWlanEvent->Id)
    {
        case SL_WLAN_EVENT_CONNECT:
        {

        }
        break;

        case SL_WLAN_EVENT_DISCONNECT:
        {
            isWlanConnected = 0;
        }
        break;

        default:
        {
            UART_PRINT("[WLAN EVENT] Unexpected event [0x%x]\n\r",
                       pWlanEvent->Id);
        }
        break;
    }
}

//*****************************************************************************
//
//! \brief The Function Handles the Fatal errors
//!
//! \param[in]  slFatalErrorEvent - Pointer to Fatal Error Event info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkFatalErrorEventHandler(SlDeviceFatal_t *slFatalErrorEvent)
{
    switch (slFatalErrorEvent->Id)
    {
        case SL_DEVICE_EVENT_FATAL_DEVICE_ABORT:
        {
        UART_PRINT(
            "[ERROR] - FATAL ERROR: Abort NWP event detected:"
            " AbortType=%d, AbortData=0x%x\n\r",
            slFatalErrorEvent->Data.DeviceAssert.Code,
            slFatalErrorEvent->Data.DeviceAssert.Value);
        }
        break;

        case SL_DEVICE_EVENT_FATAL_DRIVER_ABORT:
        {
            UART_PRINT("[ERROR] - FATAL ERROR: Driver Abort detected. \n\r");
        }
        break;

        case SL_DEVICE_EVENT_FATAL_NO_CMD_ACK:
        {
        UART_PRINT(
            "[ERROR] - FATAL ERROR: No Cmd Ack detected"
            " [cmd opcode = 0x%x] \n\r",
            slFatalErrorEvent->Data.NoCmdAck.Code);
        }
        break;

        case SL_DEVICE_EVENT_FATAL_SYNC_LOSS:
        {
            UART_PRINT("[ERROR] - FATAL ERROR: Sync loss detected n\r");
        }
        break;

        case SL_DEVICE_EVENT_FATAL_CMD_TIMEOUT:
        {
        UART_PRINT(
            "[ERROR] - FATAL ERROR: Async event timeout detected"
            " [event opcode =0x%x]  \n\r",
            slFatalErrorEvent->Data.CmdTimeout.Code);
        }
        break;

        default:
            UART_PRINT("[ERROR] - FATAL ERROR: "
                       "Unspecified error detected \n\r");
        break;
    }
}

//*****************************************************************************
//
//! \brief This function handles network events such as IP acquisition, IP
//!           leased, IP released etc.
//!
//! \param[in]  pNetAppEvent - Pointer to NetApp Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
    char syncMsg;
    struct timespec ts;

    switch(pNetAppEvent->Id)
    {
        case SL_NETAPP_EVENT_IPV4_ACQUIRED:
        {
            int status = ti_net_SlNet_initConfig();
            if(0 != status)
            {
//                 Display_printf(display, 0, 0, "Failed to initialize SlNetSock\n\r");
            }
            UART_PRINT("[NETAPP EVENT] IP Acquired: IP=%d.%d.%d.%d , "
            "Gateway=%d.%d.%d.%d\n\r",
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,3),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,2),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,1),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,0),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,3),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,2),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,1),
            SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,0));

            isWlanConnected = 1;
        }
        break;

        default:
        {
            UART_PRINT("[NETAPP EVENT] Unexpected event [0x%x] \n\r",
                       pNetAppEvent->Id);
        }
        break;
    }
}

//*****************************************************************************
//
//! \brief This function handles HTTP server events
//!
//! \param[in]  pServerEvent - Contains the relevant event information
//! \param[in]    pServerResponse - Should be filled by the user with the
//!                                      relevant response information
//!
//! \return None
//!
//****************************************************************************
void SimpleLinkHttpServerEventHandler(
    SlNetAppHttpServerEvent_t *pHttpEvent,
    SlNetAppHttpServerResponse_t *
    pHttpResponse)
{
    // Unused in this application
}

//*****************************************************************************
//
//! \brief This function handles General Events
//!
//! \param[in]     pDevEvent - Pointer to General Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{
    //
    // Most of the general errors are not FATAL are are to be handled
    // appropriately by the application
    //
    UART_PRINT("[GENERAL EVENT] - ID=[%d] Sender=[%d]\n\n",
               pDevEvent->Data.Error.Code,
               pDevEvent->Data.Error.Source);
}

//*****************************************************************************
//
//! This function handles socket events indication
//!
//! \param[in]      pSock - Pointer to Socket Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{
    //
    // This application doesn't work w/ socket - Events are not expected
    //
    switch( pSock->Event )
    {
        case SL_SOCKET_TX_FAILED_EVENT:
            switch( pSock->SocketAsyncEvent.SockTxFailData.Status)
            {
                case SL_ERROR_BSD_ECLOSE:
                    UART_PRINT("[SOCK ERROR] - close socket (%d) operation "
                                "failed to transmit all queued packets\n\r",
                                    pSock->SocketAsyncEvent.SockTxFailData.Sd);
                    break;
                default:
            UART_PRINT(
                "[SOCK ERROR] - TX FAILED  :  socket %d , reason "
                                "(%d) \n\n",
                pSock->SocketAsyncEvent.SockTxFailData.Sd,
                pSock->SocketAsyncEvent.SockTxFailData.Status);
                  break;
            }
            break;

        default:
            UART_PRINT("[SOCK EVENT] - "
                       "Unexpected Event [%x0x]\n\n",pSock->Event);
          break;
    }

}


static HTTPClient_Handle sl_memfault_https_client = NULL;
int sl_memfault_https_client_connect()
{
    _i16 status = 0;

    if (!sl_memfault_https_client)
    {
        sl_memfault_https_client = HTTPClient_create(&status, NULL);

        if (status < 0) {
            return -1;
        }
    }

    HTTPClient_extSecParams exSecParams;
    memset((void *) &exSecParams, 0, sizeof(HTTPClient_extSecParams));

    if (HTTPClient_setHeaderByName(sl_memfault_https_client, HTTPClient_REQUEST_HEADER_MASK, "Memfault-Project-Key", MEMFAULT_PROJECT_KEY, strlen(MEMFAULT_PROJECT_KEY) + 1, HTTPClient_HFIELD_PERSISTENT))
    {
        return -1;
    }

    static char contentType[] = "application/octet-stream";
    if (HTTPClient_setHeaderByName(sl_memfault_https_client, HTTPClient_REQUEST_HEADER_MASK, "Content-Type", contentType, strlen(contentType) + 1, HTTPClient_HFIELD_PERSISTENT))
    {
        return -1;
    }

    status = HTTPClient_connect(sl_memfault_https_client, "https://chunks.memfault.com", &exSecParams, 0);
    if (status < 0)
    {
        HTTPClient_disconnect(sl_memfault_https_client);
        return -1;
    }

    return 0;
}

void sl_memfault_https_client_disconnect()
{
    HTTPClient_disconnect(sl_memfault_https_client);
}


void user_transport_send_chunk_data(void *chunk_data, size_t chunk_data_len) {
    static char DEVICE_SERIAL[15 + 13] = { 0 };

    if (*DEVICE_SERIAL == 0)
    {
        _u8 macAddressVal[SL_MAC_ADDR_LEN];
        _u16 macAddressLen = SL_MAC_ADDR_LEN;
        _u16 ConfigOpt = 0;

        if (sl_NetCfgGet(SL_NETCFG_MAC_ADDRESS_GET,&ConfigOpt,&macAddressLen,(_u8 *)macAddressVal) < 0)
        {
            return;
        }

        sprintf(DEVICE_SERIAL, "/api/v0/chunks/%x%x%x%x%x%x",
                macAddressVal[0],
                macAddressVal[1],
                macAddressVal[2],
                macAddressVal[3],
                macAddressVal[4],
                macAddressVal[5]);

    }

    _i16 status = HTTPClient_sendRequest(sl_memfault_https_client, HTTP_METHOD_POST, DEVICE_SERIAL, chunk_data, chunk_data_len, 0);
    if (status < 0)
    {
        return;
    }
}

extern bool send_memfault_data();

void *sl_memfault_task(void *arg)
{
    while (1)
    {
        if (isWlanConnected)
        {
            send_memfault_data();
        }

        sleep(5);
    }
}


int32_t spawnThread(pthread_t *thread, unsigned int stackSize, unsigned int priority, void *(*startroutine)(void *), void *arg)
{
    int32_t             status = 0;
    pthread_attr_t      pAttrs_spawn;
    struct sched_param  priParam;

    pthread_attr_init(&pAttrs_spawn);
    priParam.sched_priority = priority;
    status = pthread_attr_setschedparam(&pAttrs_spawn, &priParam);
    status |= pthread_attr_setstacksize(&pAttrs_spawn, stackSize);

    status = pthread_create(thread, &pAttrs_spawn, startroutine, arg);

    return status;
}


#define TASKSTACKSIZE               (4096)
#define SPAWN_TASK_PRIORITY         (9)

static int prv_send_char(char c)
{
    putch(c);
    return 0;
}

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    /* 1 second delay */
    uint32_t time = 1;

    int32_t             status = 0;
    pthread_t thread;

    /* Call driver init functions */
    GPIO_init();
    SPI_init();

    /* Configure the LED pin */
    GPIO_setConfig(CONFIG_GPIO_LED_0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    /* Turn on user LED */
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_ON);

    /* Start the SimpleLink Host */
    status = spawnThread(&thread, TASKSTACKSIZE, SPAWN_TASK_PRIORITY, sl_Task, NULL);
    if (status != 0)
    {
        UART_PRINT("could not create simpleLink task\n\r");
        while (1) {}
    }

    status = spawnThread(&thread, TASKSTACKSIZE, 10, sl_memfault_task, NULL);
    if (status != 0)
    {
         UART_PRINT("could not create memfault task\n\r");
         while (1) {}
    }

    status = sl_Start(NULL, NULL, NULL);
    if (status < 0)
    {
        UART_PRINT("sl_Start failure: %d\n\r", status);
        while (1) {}
    }

    status = sl_WlanSetMode(ROLE_STA);
    if (status < 0)
    {
        UART_PRINT("sl_WlanSetMode failure: %d\n\r", status);
        while (1) {}
    }


    // Connect
    SlWlanSecParams_t secParams = {
        .Type = SECURITY_TYPE,
        .Key = (_i8 *) PASSPHRASE_KEY,
        .KeyLen = strlen(PASSPHRASE_KEY)
    };

    status = sl_WlanConnect((_i8 *) SSID, strlen(SSID), NULL, &secParams, NULL);
    if(status < 0)
    {
        UART_PRINT("sl_Connect failure: %d\n\r", status);
        while (1) {}
    }

    const sMemfaultShellImpl impl = {
        .send_char = prv_send_char,
    };

    memfault_demo_shell_boot(&impl);

    while (1)
    {
        char ch = getch();
        memfault_demo_shell_receive_char(ch);
    }
}
