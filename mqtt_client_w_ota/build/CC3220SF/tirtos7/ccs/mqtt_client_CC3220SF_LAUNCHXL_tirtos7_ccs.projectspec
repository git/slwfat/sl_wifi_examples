<?xml version="1.0" encoding="UTF-8"?>
<projectSpec>
    <applicability>
        <when>
            <context
                deviceFamily="ARM"
                deviceId="Cortex M.CC3220SF"
            />
        </when>
    </applicability>

    <project
        title="MQTT Client App"
        name="mqtt_client_CC3220SF_LAUNCHXL_tirtos7_ccs"
		configurations="MCU+Image, Debug"
        toolChain="TI"
        connection="TIXDS110_Connection.xml"
        device="Cortex M.CC3220SF"
        ignoreDefaultDeviceSettings="true"
        ignoreDefaultCCSSettings="true"
        products="com.ti.SIMPLELINK_CC32XX_SDK;sysconfig"
        compilerBuildOptions="
            -I${PROJECT_ROOT}
            -I${PROJECT_ROOT}/${ConfigName}
             
            -DDeviceFamily_CC3220
            -I${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source
            -I${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/kernel/tirtos7/packages
            -I${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/posix/ccs
            --silicon_version=7M4
            --code_state=16
            --little_endian
            --display_error_number
            --diag_warning=255
            --diag_wrap=off
            --gen_func_subsections=on
            --float_support=vfplib
            --symdebug:dwarf
            -I${CG_TOOL_ROOT}/include
        "
        linkerBuildOptions="
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/net/mqtt/lib
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/net/lib
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/net/ota
            -i${PROJECT_LOC}/libs/http_lib_for_ota
            -lccs/m4/httpclient_release.a
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/utils/json/lib
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/drivers/net/wifi/slnetif
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/drivers/net/wifi
            -i${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/kernel/tirtos7/packages
            -i${PROJECT_BUILD_DIR}/syscfg
            -lti_utils_build_linker.cmd.genlibs
            -lti/devices/cc32xx/driverlib/ccs/Release/driverlib.a
            --warn_sections
            --display_error_number
            --diag_wrap=off
            --rom_model
            -i${CG_TOOL_ROOT}/lib
            -llibc.a
        "
        enableSysConfigTool="true"
        sysConfigBuildOptions="
            --product ${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/.metadata/product.json
            --compiler ccs
        "
        description="MQTT Client demo application">


        <customPostBuildTool
            name="Arm ObjCopy"
            command="${CG_TOOL_ROOT}/bin/armobjcopy"
            flags="
                -O binary --only-section .text --only-section .const --only-section .cinit --only-section .resetVecs
                &quot;${BuildArtifactFileName}&quot;
                &quot;${BuildArtifactFileBaseName}.bin&quot;
            "
            inputFiles="${BuildArtifactFileName}"
            outputFiles="${BuildArtifactFileBaseName}.bin"
            applicableConfigurations="MCU+Image, Debug">
        </customPostBuildTool>
        <customPostBuildTool
            name="Image Creator"
            command="${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/drivers/net/imagecreator/bin/SLImageCreator.exe"
            flags="
                syscfg create_image
                --sdk_path &quot;${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}&quot;
                --json &quot;${BuildDirectory}/syscfg/ti_drivers_net_wifi_config.json&quot;
                --file &quot;${BuildDirectory}/syscfg/${BuildArtifactFileBaseName}.sli&quot;
                --mcu &quot;${BuildDirectory}/${BuildArtifactFileBaseName}.bin&quot;
            "
            inputFiles="${BuildArtifactFileBaseName}.bin, syscfg/ti_drivers_net_wifi_config.json"
            outputFiles="syscfg/${BuildArtifactFileBaseName}.sli"
            applicableConfigurations="MCU+Image">
        </customPostBuildTool>    
        
        <property name="isHybrid" value="true"/>
        <file path="../../../../src/mqtt_client_app.c" openOnCreation="false" excludeFromBuild="false" action="copy">
        </file>
        <file path="../../../../src/README.html" openOnCreation="false" excludeFromBuild="false" action="copy">
        </file>
        <file path="../../../../src/wifi_settings.h" openOnCreation="false" excludeFromBuild="false" action="copy">
        </file>
        <file path="../../../../src/ota_settings.h" openOnCreation="false" excludeFromBuild="false" action="copy">
        </file>
        <file path="../../../../src/ifmod/mqtt_if.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/mqtt_if.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/debug_if.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/wifi_if.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/wifi_if.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/httpsrv_if.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/httpsrv_if.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/utils_if.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/utils_if.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/uart_if.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/uart_if.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/ota_if.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/ota_if.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/ota_vendors.h" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/ota_vendor_github.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/ifmod/ota_vendor_dropbox.c" openOnCreation="false" excludeFromBuild="false" action="copy" targetDirectory="ifmod">
        </file>
        <file path="../../../../src/tirtos/main_tirtos.c" openOnCreation="false" excludeFromBuild="false" action="copy">
        </file>
        <file path="../mqtt_client.syscfg" openOnCreation="false" excludeFromBuild="false" action="copy">
        </file>
        <file path="${COM_TI_SIMPLELINK_CC32XX_SDK_INSTALL_DIR}/source/ti/boards/CC3220SF_LAUNCHXL/Board.html" openOnCreation="false" excludeFromBuild="false" action="link">
        </file>
        <file path="../../../../src/tirtos/cc32xxsf_tirtos7.cmd" openOnCreation="false" excludeFromBuild="false" action="copy">
        </file>
	<file path="../../image.syscfg" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/dummy-root-ca-cert-key" targetDirectory="./userFiles/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/RootCACerts.pem" targetDirectory="./userFiles/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/DigiCert_High_Assurance_CA.der" targetDirectory="./userFiles/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/ota.dat" targetDirectory="./userFiles/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/dummy_ota_vendor_cert.der" targetDirectory="./userFiles/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/index.html" targetDirectory="./userFiles/www/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/settings.html" targetDirectory="./userFiles/www/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/ota.html" targetDirectory="./userFiles/www/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/help.html" targetDirectory="./userFiles/www/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/css/style.css" targetDirectory="./userFiles/www/css/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/js/jquery.min.js" targetDirectory="./userFiles/www/js/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/js/scripts.js" targetDirectory="./userFiles/www/js/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/images/tilogo.gif" targetDirectory="./userFiles/www/images/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/images/icons/help.png" targetDirectory="./userFiles/www/images/icons/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/images/icons/wirelessfull.png" targetDirectory="./userFiles/www/images/icons/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/images/icons/wireless.png" targetDirectory="./userFiles/www/images/icons/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/www/images/icons/menu.png" targetDirectory="./userFiles/www/images/icons/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/sys/mcubootinfo.bin" targetDirectory="./userFiles/sys/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/dummy-root-ca-cert" targetDirectory="./userFiles/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../src/userFiles/otaImages/cc3220sf.tar" targetDirectory="./userFiles/otaImages/" openOnCreation="false" excludeFromBuild="false" action="copy">
	</file>
	<file path="../../../../libs/http_lib_for_ota" targetDirectory="./libs/" openOnCreation="false" excludeFromBuild="true" action="copy">
	</file>
    </project>
</projectSpec>
