#!/bin/sh
if [ "$#" -ne 2 ] ; then
  echo "Usage: $0 \"from\" \"to\"" 
  exit 1
fi
sed -i s/$1/$2/g `find . -name "makefile"` 
