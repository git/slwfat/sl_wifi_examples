# Introduction #
The extended mqtt example presented here serves as an example code for adding OTA (over-The-Air) update capabilities to an existing project.<br>
The example is based on the existence of the "Simplelink CC32xx SDK" (see "Enable The Wi-Fi Firmware" below).
See guidance in the README document in this folder.

# Dependencies #
This release was built and tested using the following:<br>
* Reference coexistence board (see [SimpleLink™ Wi-Fi® CC313x, CC323x BLE Coexistence](https://www.ti.com/lit/pdf/swra608)))<br> 
* simplelink\_cc32xx\_sdk\_5\_30\_00\_08

# What's New #
2.0.1 - This is the first release that enables adding OTA to the mqtt_client

# Known Issues #

# Directory Structure #
* /src - application sources (including ifmod - folder that contain cc32xx interface modules)
* /build - application development projects per device, operating system and IDE (integrated development environment). 
* /misc - contain example uniflash (zipped) projects and example tar files

# Documentation #
See README in this folder for more details